class LocationsController < ApplicationController
  require "csv"
  load_and_authorize_resource
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, alert: exception.message
  end
  before_action :authenticate_user!
  before_action :set_location, only: %i[show edit update destroy]

  # GET /locations
  # GET /locations.json
  def index
    @locations = Location.where(biz_id: current_user.biz_id)

    respond_to do |format|
      format.html
      format.json
      format.csv { send_data @locations.to_csv }
      format.pdf do
        render pdf: "locations-#{Time.now.strftime("%Y%m%d-%H%M")}",
          page_size: "A4",
          layout: "pdf.pdf",
          orientation: "Portrait",
          lowquality: true,
          zoom: 1,
          dpi: 100
      end
    end
  end

  # GET /locations/1
  # GET /locations/1.json
  def show
  end

  # GET /locations/new
  def new
    @location = Location.new
  end

  # GET /locations/1/edit
  def edit
  end

  # POST /locations
  # POST /locations.json
  def create
    @location = Location.new(location_params)

    @location.user_id = current_user.id
    @location.biz_id = current_user.biz_id

    respond_to do |format|
      if @location.save
        format.html { redirect_to locations_url, notice: "Location was successfully created." }
        format.json { render :show, status: :created, location: @location }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @location.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /locations/1
  # PATCH/PUT /locations/1.json
  def update
    respond_to do |format|
      if @location.update(location_params)
        format.html { redirect_to locations_url, notice: "Location was successfully updated." }
        format.json { render :show, status: :ok, location: @location }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @location.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /locations/1
  # DELETE /locations/1.json
  def destroy
    @location.destroy
    respond_to do |format|
      format.html { redirect_to locations_url, notice: "Location was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_location
    @location = Location.find_by(id: params[:id], biz_id: current_user.biz_id)
  end

  # Only allow a list of trusted parameters through.
  def location_params
    # params.require(:location).permit(:title, :image_data, :description, :lat, :lng, :slug, :user, :biz)
    params.require(:location).permit(:title, :image_data, :description, :lat, :lng, :slug, :user_id, :biz_id)
  end
end
