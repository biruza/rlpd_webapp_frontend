FactoryGirl.define do
  factory :list do
    title "MyString"
    order_sort 1
    slug "MyString"
    board nil
    archived false
    user nil
    biz nil
  end
end
