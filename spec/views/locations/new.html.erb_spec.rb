require "rails_helper"

RSpec.describe "locations/new", type: :view do
  before(:each) do
    assign(:location, Location.new(
      title: "MyString",
      image_data: "MyText",
      description: "MyText",
      lat: "9.99",
      lng: "9.99",
      slug: "MyString",
      user: nil,
      biz: nil
    ))
  end

  it "renders new location form" do
    render

    assert_select "form[action=?][method=?]", locations_path, "post" do
      assert_select "input[name=?]", "location[title]"

      assert_select "textarea[name=?]", "location[image_data]"

      assert_select "textarea[name=?]", "location[description]"

      assert_select "input[name=?]", "location[lat]"

      assert_select "input[name=?]", "location[lng]"

      assert_select "input[name=?]", "location[slug]"

      assert_select "input[name=?]", "location[user_id]"

      assert_select "input[name=?]", "location[biz_id]"
    end
  end
end
