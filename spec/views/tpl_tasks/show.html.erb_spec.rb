require "rails_helper"

RSpec.describe "tpl_tasks/show", type: :view do
  before(:each) do
    @tpl_task = assign(:tpl_task, TplTask.create!(
      list: nil,
      title: "Title",
      description: "MyText",
      cover: "MyText",
      priority: 2,
      status: 3,
      achieve: false,
      user: nil,
      biz: nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/false/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
  end
end
