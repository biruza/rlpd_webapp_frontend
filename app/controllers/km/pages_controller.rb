class Km::PagesController < ApplicationController
  def dashboards
    url = URI("#{$server[@controller_title]}/api/v1/master_plans?order=updated_at&sort=desc&limit=10")
    @master_plans = request_api(url, "get", nil)

    params[:master_plan_id] = @master_plans["data"][0]["id"] if params[:master_plan_id].blank? && @master_plans && !@master_plans["data"].blank?

    url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params[:master_plan_id]}/plans?order=updated_at&sort=desc&limit=10")
    @plans = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/plan_reports?order=updated_at&sort=desc&limit=10")
    @plan_reports = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/plan_reports/get_group_years") 
    @plan_report_years = request_api(url,'get',nil)
    
  end

  def main_dashboards
    url = URI("#{$server[@controller_title]}/api/v1/master_plans?order=updated_at&sort=desc&limit=10")
    @master_plans = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/plan_reports?order=updated_at&sort=desc&limit=100")
    @plan_reports = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/plan_reports/get_group_years") 
    @plan_report_years = request_api(url,'get',nil)

    params[:master_plan_id] = @master_plans["data"][0]["id"] if params[:master_plan_id].blank? && @master_plans && !@master_plans["data"].blank?
  end

  def dashboards_overall
    url = URI("#{$server[@controller_title]}/api/v1/master_plans?order=updated_at&sort=desc&limit=10")
    @master_plans = request_api(url, "get", nil)

    params[:master_plan_id] = @master_plans["data"][0]["id"] if params[:master_plan_id].blank? && @master_plans && !@master_plans["data"].blank?

    url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params[:master_plan_id]}/plans?order=updated_at&sort=desc&limit=10")
    @plans = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/plan_reports?order=updated_at&sort=desc&limit=10")
    @plan_reports = request_api(url, "get", nil)
  end

  def downloads
    url = URI("#{$server[@controller_title]}/api/v1/downloads?order=title&sort=desc")
    @downloads = request_api(url, "get", nil)
  end

  def download_user_manuals
    url = URI("#{$server[@controller_title]}/api/v1/user_manuals?order=title&sort=desc")
    #url = URI("#{$server[@controller_title]}/api/v1/user_manuals")
    @download_user_manuals = request_api(url, "get", nil)
  end

  def search_result
    if !params[:search][:system_name].blank?
      case params[:search][:model_name]
      when "master_plans_risk"
        params[:search][:model_name] = "master_plans"
        params[:search][:type_name] = "risk"
      when "master_plans_control"
        params[:search][:model_name] = "master_plans"
        params[:search][:type_name] = "control"
      end

      url = URI("#{$server[params[:search][:system_name]]}/api/v1/mains/search")
      @datas = request_api(url, "post", search_params)
      @keyword = params[:search][:keyword]
      @model_name = params[:search][:model_name]
    end
  end


  def notifications
    url = URI("#{$server[@controller_title]}/api/v1/master_plans?order=period_year&sort=desc&limit=10&type_name=plan")
    @master_plans = request_api(url, "get", nil)

    params[:master_plan_id] = @master_plans["data"][0]["id"] if params[:master_plan_id].blank? && @master_plans && !@master_plans["data"].blank?

    url = URI("#{$server[@controller_title]}/api/v1/mains/notifications?master_plan_id=#{params[:master_plan_id]}")
    @notifications = request_api(url, "get", nil)
  end

  private

  def search_params
    params.require(:search).permit(:model_name, :keyword, :system_name)
  end

  def master_plan_params
    params.require(:master_plan).permit(:title, :description, :user_id, :biz_id, :type_name, :startdate, :enddate, :period_year)
  end
end

# def show
#         @master_plan = MasterPlan.find_by(id: params[:id])
#         render json: { status: "Success", message: "Show master plan",data:@master_plan.as_json(only: [:id, :title, :period_year, :status, :description, :slug, :user_id, :biz_id, :created_at, :updated_at],
#             include: {
#                   plans: {only: [:id, :title, :period, :description, :slug, :status, :startdate, :enddate, :master_plan_id, :user_id, :biz_id, :created_at, :updated_at],
#
#                     include: {
#                           categories: {only: [:id, :title, :cognitive, :slug, :approve, :plan_id, :institution_id, :issue_id, :kpi_id, :target_kpi_id, :responsible_id, :user_id, :biz_id, :updated_at, :created_at],
#
#                                   include: {
#                                       institution: {only: [:id, :title]},
#                                       responsible: {only: [:id, :title]},
#                                       issue: {only: [:id, :title]},
#                                       kpi: {only: [:id, :title]},
#                                       target_kpi: {only: [:id, :title, :condition_title, :condition_slug, :qty, :unit]},
#                                       lists: {only: [:id, :status, :slug, :order_sort, :category_id, :activity_id, :user_id, :biz_id],
#                                           include: {
#                                                 activity: {only: [:id, :title, :slug, :order_sort, :user_id, :biz_id]},
#                                                 list_activities: {only: [:id, :title, :slug, :order_sort, :list_id, :user_id, :biz_id]},
#                                                 list_periods: {only: [:id, :startdate, :enddate, :slug, :order_sort, :list_id, :user_id, :biz_id]},
#                                                 list_kpis: {only: [:id, :title, :slug, :order_sort, :list_id, :user_id, :biz_id]},
#                                                 list_target_kpis: {only: [:id, :title, :slug, :order_sort, :list_id, :user_id, :biz_id]},
#                                                 list_target_groups: {only: [:id, :title, :slug, :order_sort, :list_id, :user_id, :biz_id]},
#                                                 list_responsibles: {only: [:id, :title, :slug, :order_sort, :list_id, :user_id, :biz_id]},
#                                                 list_notes: {only: [:id, :title, :slug, :order_sort, :list_id, :user_id, :biz_id]}
#                                               }
#                                           }
#                                      }
#                                 }
#
#
#                           }
#                     },
#
#                   user: {only: [:id, :firstname, :lastname]}
#
#                 }
#
#           )} ,status: :ok
#       end
