class Risk::ListActivitiesController < ApplicationController
  before_action :set_default, only: %i[show]

  def show
    url = URI("#{$server[@controller_title]}/api/v1/list_activities/#{params["id"]}")
    @datas = request_api(url, "get", nil)

    redirect_to "/#{@controller_main_url}/plans/#{@datas["data"]["list"]["category"]["plan"]["id"] rescue nil}", notice: @datas["messages"]
  end

  private

  def set_default
    #@sub_system = "control"
    #@controller_main_url = "#{@controller_title}/#{@sub_system}"
    @controller_main_url = @controller_title
  end

end
