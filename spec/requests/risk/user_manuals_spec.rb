require 'rails_helper'

RSpec.describe "Risk::UserManuals", type: :request do
  describe "GET /index" do
    it "returns http success" do
      get "/risk/user_manuals/index"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /new" do
    it "returns http success" do
      get "/risk/user_manuals/new"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /edit" do
    it "returns http success" do
      get "/risk/user_manuals/edit"
      expect(response).to have_http_status(:success)
    end
  end

end
