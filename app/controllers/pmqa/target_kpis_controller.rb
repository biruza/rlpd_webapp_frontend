class Pmqa::TargetKpisController < ApplicationController
  def index
    url = URI("#{$server[@controller_title]}/api/v1/target_kpis")
    @datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/target_kpis/#{params["id"]}")
    @datas = request_api(url, "get", nil)
  end

  def new
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/target_kpis/#{params["id"]}")

    @datas = request_api(url, "get", nil)

    # puts "plan edit = #{@plan['title']}"
  end

  def create
    params[:target_kpi][:user_id] = $user_id
    params[:target_kpi][:biz_id] = $biz_id

    url = URI("#{$server[@controller_title]}/api/v1/target_kpis")
    @datas = request_api(url, "post", target_kpi_params)

    if @datas["status"] == "Success"
      redirect_to "/km/target_kpis", notice: @datas["messages"]
    else
      redirect_to "/km/target_kpis/new", alert: @datas["data"]
    end
  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/target_kpis/#{params["id"]}")
    @datas = request_api(url, "patch", target_kpi_params)

    # puts "update status : #{res}"

    if @datas["status"] == "Success"
      redirect_to "/km/target_kpis", notice: @datas["message"]
    else
      redirect_to "/km/target_kpis", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/target_kpis/#{params["id"]}")
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
      redirect_to "/km/target_kpis", notice: @datas["message"]
    else
      redirect_to "/km/target_kpis", alert: @datas["data"]
    end
  end

  private

  def target_kpi_params
    params.require(:target_kpi).permit(:title, :user_id, :biz_id)
  end
end
