require "test_helper"

class RecordsaItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @recordsa_item = recordsa_items(:one)
  end

  test "should get index" do
    get recordsa_items_url
    assert_response :success
  end

  test "should get new" do
    get new_recordsa_item_url
    assert_response :success
  end

  test "should create recordsa_item" do
    assert_difference("RecordsaItem.count") do
      post recordsa_items_url, params: {recordsa_item: {course_subject_student_id: @recordsa_item.course_subject_student_id, record_school_attendance_id: @recordsa_item.record_school_attendance_id, status: @recordsa_item.status, value: @recordsa_item.value}}
    end

    assert_redirected_to recordsa_item_url(RecordsaItem.last)
  end

  test "should show recordsa_item" do
    get recordsa_item_url(@recordsa_item)
    assert_response :success
  end

  test "should get edit" do
    get edit_recordsa_item_url(@recordsa_item)
    assert_response :success
  end

  test "should update recordsa_item" do
    patch recordsa_item_url(@recordsa_item), params: {recordsa_item: {course_subject_student_id: @recordsa_item.course_subject_student_id, record_school_attendance_id: @recordsa_item.record_school_attendance_id, status: @recordsa_item.status, value: @recordsa_item.value}}
    assert_redirected_to recordsa_item_url(@recordsa_item)
  end

  test "should destroy recordsa_item" do
    assert_difference("RecordsaItem.count", -1) do
      delete recordsa_item_url(@recordsa_item)
    end

    assert_redirected_to recordsa_items_url
  end
end
