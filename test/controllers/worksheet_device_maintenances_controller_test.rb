require "test_helper"

class WorksheetDeviceMaintenancesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @worksheet_device_maintenance = worksheet_device_maintenances(:one)
  end

  test "should get index" do
    get worksheet_device_maintenances_url
    assert_response :success
  end

  test "should get new" do
    get new_worksheet_device_maintenance_url
    assert_response :success
  end

  test "should create worksheet_device_maintenance" do
    assert_difference("WorksheetDeviceMaintenance.count") do
      post worksheet_device_maintenances_url, params: {worksheet_device_maintenance: {maintenance_id: @worksheet_device_maintenance.maintenance_id, value: @worksheet_device_maintenance.value, worksheet_device_id: @worksheet_device_maintenance.worksheet_device_id}}
    end

    assert_redirected_to worksheet_device_maintenance_url(WorksheetDeviceMaintenance.last)
  end

  test "should show worksheet_device_maintenance" do
    get worksheet_device_maintenance_url(@worksheet_device_maintenance)
    assert_response :success
  end

  test "should get edit" do
    get edit_worksheet_device_maintenance_url(@worksheet_device_maintenance)
    assert_response :success
  end

  test "should update worksheet_device_maintenance" do
    patch worksheet_device_maintenance_url(@worksheet_device_maintenance), params: {worksheet_device_maintenance: {maintenance_id: @worksheet_device_maintenance.maintenance_id, value: @worksheet_device_maintenance.value, worksheet_device_id: @worksheet_device_maintenance.worksheet_device_id}}
    assert_redirected_to worksheet_device_maintenance_url(@worksheet_device_maintenance)
  end

  test "should destroy worksheet_device_maintenance" do
    assert_difference("WorksheetDeviceMaintenance.count", -1) do
      delete worksheet_device_maintenance_url(@worksheet_device_maintenance)
    end

    assert_redirected_to worksheet_device_maintenances_url
  end
end
