# set :environment, "development"
# set :output, {:error => "log/cron_error_log.log", :standard => "log/cron_log.log"}

every :day, at: "12:26am" do
  rake "auto_create_egp:generate_egp"
end

every :day, at: "5:52pm" do
  rake "auto_create_egp2:generate_egp2"
end

every :day, at: "8:36pm" do
  rake "auto_create_egp3:generate_egp3"
end

every :day, at: "10:18pm" do
  rake "auto_create_egp4:generate_egp4"
end

# #for test
# every 1.minute do
#   command "echo 'you can use raw cron syntax too'"
#   rake 'auto_create_egp3:generate_egp3'
#   #puts "Hi cron 4 : #{Time.now}"
# end
