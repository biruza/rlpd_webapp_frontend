class Risk::DevisionDirectorsController < ApplicationController
  def index
    url = URI("#{$server[@controller_title]}/api/v1/devision_directors")

    @datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/devision_directors/#{params["id"]}")

    @datas = request_api(url, "get", nil)
  end

  def new
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/devision_directors/#{params["id"]}")

    @datas = request_api(url, "get", nil)

    # puts "plan edit = #{@plan['title']}"
  end

  def create
    params[:devision_director][:user_id] = $user_id
    params[:devision_director][:biz_id] = $biz_id

    url = URI("#{$server[@controller_title]}/api/v1/devision_directors")
    @datas = request_api(url, "post", devision_director_params)

    if @datas["status"] == "Success"
      redirect_to "/kpi/devision_directors", notice: @datas["messages"]
    else
      redirect_to "/kpi/devision_directors/new", alert: @datas["data"]
    end
  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/devision_directors/#{params["id"]}")
    @datas = request_api(url, "patch", devision_director_params)

    # puts "update status : #{res}"

    if @datas["status"] == "Success"
      redirect_to "/kpi/devision_directors", notice: @datas["message"]
    else
      redirect_to "/kpi/devision_directors", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/devision_directors/#{params["id"]}")
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
      redirect_to "/kpi/devision_directors", notice: @datas["message"]
    else
      redirect_to "/kpi/devision_directors", alert: @datas["data"]
    end
  end

  private

  def devision_director_params
    params.require(:devision_director).permit(:title, :user_id, :biz_id)
  end
end
