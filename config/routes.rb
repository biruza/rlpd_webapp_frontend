Rails.application.routes.draw do


  namespace :km do
    get 'user_manuals/index'
    get 'user_manuals/new'
    get 'user_manuals/edit'
  end
  namespace :risk do
    get 'user_manuals/index'
    get 'user_manuals/new'
    get 'user_manuals/edit'
  end
  namespace :pmqa do
    get 'user_manuals/index'
    get 'user_manuals/new'
    get 'user_manuals/edit'
  end
  namespace :kpi do
    get 'user_manuals/index'
    get 'user_manuals/edit'
    get 'user_manuals/new'
  end
  namespace :risk do
    get 'list_risk_types/index'
    get 'list_risk_types/show'
    get 'list_risk_types/edit'
    get 'list_risk_types/new'
  end
  namespace :risk do
    get 'list_risk_factors/index'
    get 'list_risk_factors/show'
    get 'list_risk_factors/edit'
    get 'list_risk_factors/new'
  end
  namespace :kpi do
    get 'period_reports/index'
    get 'period_reports/show'
    get 'period_reports/new'
    get 'period_reports/edit'
  end
  namespace :risk do
    namespace :control do
      get 'period_reports/index'
      get 'period_reports/show'
      get 'period_reports/new'
      get 'period_reports/edit'
    end
  end
  get 'errors/not_found'
  get 'errors/internal_server_error'
  resources :locations
  resources :bizs
  ["km", "kpi", "risk", "pmqa"].each do |system_name|
    namespace system_name do
      namespace :pages do
        get :download_user_manuals
        get :notifications
        get :downloads
        get :dashboards #สำหรับรายงานผลการดำเนินงาน
        get :dashboards_pmqa_fl
        get :dashboards_pmqa_fr
        get :dashboards_risk_report

        get :dashboards_overall #สำหรับ graph

        post :search
        get :search_result
        get :master_data
        post :search_result
      end

      ["pmqa_fl", "pmqa_fr", "control"].each do |sub_system_name|
        namespace sub_system_name do
          resources :evaluate_control_reports
          resources :evaluate_controls
          resources :master_plans do
            collection do
              get "/new_by_template", to: "master_plans#new_by_template"
              get "/print/:id", to: "master_plans#print"
            end
          end
          resources :master_plan_users
          resources :follow_up_control_histories
          resources :internal_control_certificates
          resources :component_reports

          resources :plans do
         
            get "/history/:id", to: "plans#history"  
            get "/tasks", to: "plans#tasks"
            collection do
              get "/print/:id", to: "plans#print"
            
              get "/user_input/:id", to: "plans#user_input"
              patch "/user_input_update_normal/:id", to: "plans#user_input_update_normal"
              patch "/user_input_update_seven/:id", to: "plans#user_input_update_seven"
              patch "/user_input_update_op/:id", to: "plans#user_input_update_op"
              patch "/user_input_submit/:id", to: "plans#user_input_submit"
              get "/opdc_input/:id", to: "plans#opdc_input"
              patch "/opdc_input_update_normal/:id", to: "plans#opdc_input_update_normal"
              patch "/opdc_input_update_seven/:id", to: "plans#opdc_input_update_seven"
              patch "/opdc_input_submit/:id", to: "plans#opdc_input_submit"
            end
          end
          get "/plans/history/:id", to: "plans#history"
          resources :period_reports
          resources :plan_users
          resources :categories
          resources :lists
          resources :list_risk_factors
          resources :list_risk_types
          resources :list_activities
          resources :list_activity_items
          resources :tasks
          resources :plan_reports
          resources :plan_report_users
          resources :plan_report_user_comments
          resources :risk_analysis_reports
          resources :risk_analysises
          resources :task_plan_reports
          resources :task_lists
          resources :risk_management_reports
          resources :risk_managements
          resources :follow_up_risk_reports
          resources :follow_up_risks
          resources :evaluate_control_reports
          resources :evaluate_controls
          resources :follow_up_control_reports
          resources :internal_control_certificates
          resources :component_reports
          resources :follow_up_control_histories
     
        end
      end #sub_system_name

      get "/plans/form_1", to: "plans#form_1"
      get "/plans/form_2", to: "plans#form_2"
      get "/plans/form_3", to: "plans#form_3"
      get "/plans/form_4", to: "plans#form_4"
      get "/plans/form_5", to: "plans#form_5"
      get "/plans/form_6", to: "plans#form_6"
      get "/plans/form_7", to: "plans#form_7"

      get "/master_plans/new_by_template", to: "master_plans#new_by_template"
      post "/master_plans/create_by_template", to: "master_plans#create_by_template"
      get "/master_plans/print/:id", to: "master_plans#print"
      get "/plans/print/:id", to: "plans#print"
      get "/plans/view/:id", to: "plans#view"

      get "/plans/history/:id", to: "plans#history"
      get "/master_plans/edit_form_2", to: "master_plans#edit_form_2"
      get "/master_plans/edit_form_3", to: "master_plans#edit_form_3"
      get "/master_plans/edit_form_4", to: "master_plans#edit_form_4"
      get "/master_plans/edit_form_5", to: "master_plans#edit_form_5"
      get "/plan_reports/print/:id", to: "plan_reports#print"
      get "/plan_reports/:id/plan_reports_by_master_plan", to: "plan_reports#plan_reports_by_master_plan"
      resources :master_plans
      resources :fl_master_plans
      resources :pmqa_master_plans
      resources :master_plan_users
      resources :master_plan_user_comments
   
      get "/plans/:id/status", to: "plans#status"
      resources :fl_plans
      resources :pmqa_plans
      resources :plans do
        get "/tasks", to: "plans#tasks"
      
      end
      
      resources :period_reports
      resources :plan_users
      resources :plan_reports
      resources :plan_report_users
      resources :plan_report_user_comments
      resources :risk_analysis_reports
      resources :risk_analysises
      resources :task_plan_reports
      resources :task_lists
      resources :risk_management_reports
      resources :risk_managements
      resources :follow_up_risk_reports
      resources :follow_up_risks
      resources :evaluate_control_reports
      resources :evaluate_controls
      resources :follow_up_control_reports
      resources :follow_up_control_histories
      resources :internal_control_certificates
      resources :component_reports
      resources :categories
      resources :fl_categories
      resources :plan_categories
      resources :activities
      resources :lists
      resources :list_activities
      resources :list_risk_factors
      resources :list_activity_items
      resources :fl_lists
      resources :pmqa_lists
      resources :tasks
      resources :list_risk_types
      resources :category_masters
      resources :category_master_lists
      resources :category_master_list_items
   

      get "/report_os_matrix", to: "master_plans#report_os_matrix"

      get "/pmqa_fls/edit", to: "pmqa_fls#edit"
      get "/pmqa_fls/edit_by_user", to: "pmqa_fls#edit_by_user"
      get "/pmqa_fls/edit_by_opdc", to: "pmqa_fls#edit_by_opdc"
      get "/pmqa_fls/edit_by_user_7", to: "pmqa_fls#edit_by_user_7"
      get "/pmqa_fls/edit_by_opdc_7", to: "pmqa_fls#edit_by_opdc_7"
      get "/pmqa_fls/print", to: "pmqa_fls#print"
      get "/pmqa_fours/edit_by_sp_1", to: "pmqa_fours#edit_by_sp_1"
      get "/pmqa_fours/edit_by_sp_7", to: "pmqa_fours#edit_by_sp_7"
      get "/pmqa_fours/edit_by_sp_op", to: "pmqa_fours#edit_by_sp_op"
      get "/pmqa_fours/edit_by_user_1", to: "pmqa_fours#edit_by_user_1"
      get "/pmqa_fours/edit_by_user_7", to: "pmqa_fours#edit_by_user_7"
      get "/pmqa_fours/edit_by_user_op", to: "pmqa_fours#edit_by_user_op"
      get "/pmqa_fours/edit_by_opdc_1", to: "pmqa_fours#edit_by_opdc_1"
      get "/pmqa_fours/edit_by_opdc_7", to: "pmqa_fours#edit_by_opdc_7"
      get "/pmqa_fours/edit_by_opdc_op", to: "pmqa_fours#edit_by_opdc_op"
      get "/pmqa_fours/print", to: "pmqa_fours#print"
      resources :pmqa_fls
      resources :pmqa_fours

      resources :institutions
      resources :risk_types
      resources :issues
      resources :strategic_issues
      resources :kpis
      resources :fl_kpis
      resources :pmqa_bas_masters
      resources :target_kpis
      resources :responsibles
      resources :accomplices
      resources :list_types
      resources :devision_directors
      resources :attachments

      resources :users
      resources :departments
      resources :downloads
      resources :user_manuals
    end
  end

  # share service
  get "accounts/sign_in"
  post "accounts/create_session"
  get "accounts/sign_out"
  get "accounts/delete_session"
  patch "/mains/set_user_active/:id", to: "mains#set_user_active"
  delete "/mains/delete_image", to: "mains#delete_image"

  # api
  namespace :api do
    namespace :v1 do
      get "/locations", to: "locations#index"
      post "/locations", to: "locations#create"

      get "/posts", to: "posts#index"
      post "/posts", to: "posts#create"
    end
  end
  ####

  get "/pages/index", to: "pages#index"
  get "/pages/visits", to: "pages#visits"
  get "/pages/notifications", to: "pages#notifications"

  root "accounts#sign_in"

  # user
  devise_for :users, controllers: {
    sessions: "users/sessions",
    registrations: "users/registrations"
  }

  resources :users, only: [:index, :new, :create, :edit, :update, :destroy]
  post "/users_create", to: "users#users_create"
  post "auth_user" => "authentication#authenticate_user"
  ####

  match '*path', via: :all, to: 'pages#error_404'

  if Rails.env.production?
    mount Shrine.presign_endpoint(:cache) => "/presign"
  else
    # In development and test environment we're using filesystem storage
    # for speed, so on the client side we'll upload files to our app.
    mount Shrine.upload_endpoint(:cache) => "/upload"
  end

  # noti for gem notifications
  resources :notifications, path: "" do
    collection do
      delete :clean
    end
  end
end
