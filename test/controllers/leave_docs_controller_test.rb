require "test_helper"

class LeaveDocsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @leave_doc = leave_docs(:one)
  end

  test "should get index" do
    get leave_docs_url
    assert_response :success
  end

  test "should get new" do
    get new_leave_doc_url
    assert_response :success
  end

  test "should create leave_doc" do
    assert_difference("LeaveDoc.count") do
      post leave_docs_url, params: {leave_doc: {date_end_leave: @leave_doc.date_end_leave, date_start_leave: @leave_doc.date_start_leave, description: @leave_doc.description, image_data: @leave_doc.image_data, parent_student_id: @leave_doc.parent_student_id, type_take_leave: @leave_doc.type_take_leave, user_id: @leave_doc.user_id}}
    end

    assert_redirected_to leave_doc_url(LeaveDoc.last)
  end

  test "should show leave_doc" do
    get leave_doc_url(@leave_doc)
    assert_response :success
  end

  test "should get edit" do
    get edit_leave_doc_url(@leave_doc)
    assert_response :success
  end

  test "should update leave_doc" do
    patch leave_doc_url(@leave_doc), params: {leave_doc: {date_end_leave: @leave_doc.date_end_leave, date_start_leave: @leave_doc.date_start_leave, description: @leave_doc.description, image_data: @leave_doc.image_data, parent_student_id: @leave_doc.parent_student_id, type_take_leave: @leave_doc.type_take_leave, user_id: @leave_doc.user_id}}
    assert_redirected_to leave_doc_url(@leave_doc)
  end

  test "should destroy leave_doc" do
    assert_difference("LeaveDoc.count", -1) do
      delete leave_doc_url(@leave_doc)
    end

    assert_redirected_to leave_docs_url
  end
end
