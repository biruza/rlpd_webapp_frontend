require "test_helper"

class WorksheetDevicesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @worksheet_device = worksheet_devices(:one)
  end

  test "should get index" do
    get worksheet_devices_url
    assert_response :success
  end

  test "should get new" do
    get new_worksheet_device_url
    assert_response :success
  end

  test "should create worksheet_device" do
    assert_difference("WorksheetDevice.count") do
      post worksheet_devices_url, params: {worksheet_device: {device_id: @worksheet_device.device_id, worksheet_id: @worksheet_device.worksheet_id}}
    end

    assert_redirected_to worksheet_device_url(WorksheetDevice.last)
  end

  test "should show worksheet_device" do
    get worksheet_device_url(@worksheet_device)
    assert_response :success
  end

  test "should get edit" do
    get edit_worksheet_device_url(@worksheet_device)
    assert_response :success
  end

  test "should update worksheet_device" do
    patch worksheet_device_url(@worksheet_device), params: {worksheet_device: {device_id: @worksheet_device.device_id, worksheet_id: @worksheet_device.worksheet_id}}
    assert_redirected_to worksheet_device_url(@worksheet_device)
  end

  test "should destroy worksheet_device" do
    assert_difference("WorksheetDevice.count", -1) do
      delete worksheet_device_url(@worksheet_device)
    end

    assert_redirected_to worksheet_devices_url
  end
end
