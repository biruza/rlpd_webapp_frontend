require "test_helper"

class PlanDeviceToolsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @plan_device_tool = plan_device_tools(:one)
  end

  test "should get index" do
    get plan_device_tools_url
    assert_response :success
  end

  test "should get new" do
    get new_plan_device_tool_url
    assert_response :success
  end

  test "should create plan_device_tool" do
    assert_difference("PlanDeviceTool.count") do
      post plan_device_tools_url, params: {plan_device_tool: {plan_device_id: @plan_device_tool.plan_device_id, tool_id: @plan_device_tool.tool_id}}
    end

    assert_redirected_to plan_device_tool_url(PlanDeviceTool.last)
  end

  test "should show plan_device_tool" do
    get plan_device_tool_url(@plan_device_tool)
    assert_response :success
  end

  test "should get edit" do
    get edit_plan_device_tool_url(@plan_device_tool)
    assert_response :success
  end

  test "should update plan_device_tool" do
    patch plan_device_tool_url(@plan_device_tool), params: {plan_device_tool: {plan_device_id: @plan_device_tool.plan_device_id, tool_id: @plan_device_tool.tool_id}}
    assert_redirected_to plan_device_tool_url(@plan_device_tool)
  end

  test "should destroy plan_device_tool" do
    assert_difference("PlanDeviceTool.count", -1) do
      delete plan_device_tool_url(@plan_device_tool)
    end

    assert_redirected_to plan_device_tools_url
  end
end
