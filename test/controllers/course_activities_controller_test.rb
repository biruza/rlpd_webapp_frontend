require "test_helper"

class CourseActivitiesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @course_activity = course_activities(:one)
  end

  test "should get index" do
    get course_activities_url
    assert_response :success
  end

  test "should get new" do
    get new_course_activity_url
    assert_response :success
  end

  test "should create course_activity" do
    assert_difference("CourseActivity.count") do
      post course_activities_url, params: {course_activity: {activity_id: @course_activity.activity_id, classroom_id: @course_activity.classroom_id, course_id: @course_activity.course_id, teacher_id: @course_activity.teacher_id}}
    end

    assert_redirected_to course_activity_url(CourseActivity.last)
  end

  test "should show course_activity" do
    get course_activity_url(@course_activity)
    assert_response :success
  end

  test "should get edit" do
    get edit_course_activity_url(@course_activity)
    assert_response :success
  end

  test "should update course_activity" do
    patch course_activity_url(@course_activity), params: {course_activity: {activity_id: @course_activity.activity_id, classroom_id: @course_activity.classroom_id, course_id: @course_activity.course_id, teacher_id: @course_activity.teacher_id}}
    assert_redirected_to course_activity_url(@course_activity)
  end

  test "should destroy course_activity" do
    assert_difference("CourseActivity.count", -1) do
      delete course_activity_url(@course_activity)
    end

    assert_redirected_to course_activities_url
  end
end
