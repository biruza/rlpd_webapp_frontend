class Pmqa::CategoryMastersController < ApplicationController
  def index
    url = URI("#{$server[@controller_title]}/api/v1/category_masters")

    @datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/category_masters/#{params["id"]}")

    @datas = request_api(url, "get", nil)
  end

  def new
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/category_masters/#{params["id"]}")

    @datas = request_api(url, "get", nil)

    # puts "plan edit = #{@plan['title']}"
  end

  def create
    params[:category_master][:user_id] = $user_id
    params[:category_master][:biz_id] = $biz_id

    url = URI("#{$server[@controller_title]}/api/v1/category_masters")
    @datas = request_api(url, "post", category_master_params)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/category_masters", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_title}/category_masters/new", alert: @datas["data"]
    end
  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/category_masters/#{params["id"]}")
    @datas = request_api(url, "patch", category_master_params)

    # puts "update status : #{res}"

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/category_masters", notice: @datas["message"]
    else
      redirect_to "/#{@controller_title}/category_masters", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/category_masters/#{params["id"]}")
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/category_masters", notice: @datas["message"]
    else
      redirect_to "/#{@controller_title}/category_masters", alert: @datas["data"]
    end
  end

  private

  def category_master_params
    params.require(:category_master).permit(:order_sort, :title, :user_id, :biz_id)
  end
end
