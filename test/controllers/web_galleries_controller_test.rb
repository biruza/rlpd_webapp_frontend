require "test_helper"

class WebGalleriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @web_gallery = web_galleries(:one)
  end

  test "should get index" do
    get web_galleries_url
    assert_response :success
  end

  test "should get new" do
    get new_web_gallery_url
    assert_response :success
  end

  test "should create web_gallery" do
    assert_difference("WebGallery.count") do
      post web_galleries_url, params: {web_gallery: {biz_id: @web_gallery.biz_id, description_th: @web_gallery.description_th, image: @web_gallery.image, module_web_id: @web_gallery.module_web_id, title_th: @web_gallery.title_th}}
    end

    assert_redirected_to web_gallery_url(WebGallery.last)
  end

  test "should show web_gallery" do
    get web_gallery_url(@web_gallery)
    assert_response :success
  end

  test "should get edit" do
    get edit_web_gallery_url(@web_gallery)
    assert_response :success
  end

  test "should update web_gallery" do
    patch web_gallery_url(@web_gallery), params: {web_gallery: {biz_id: @web_gallery.biz_id, description_th: @web_gallery.description_th, image: @web_gallery.image, module_web_id: @web_gallery.module_web_id, title_th: @web_gallery.title_th}}
    assert_redirected_to web_gallery_url(@web_gallery)
  end

  test "should destroy web_gallery" do
    assert_difference("WebGallery.count", -1) do
      delete web_gallery_url(@web_gallery)
    end

    assert_redirected_to web_galleries_url
  end
end
