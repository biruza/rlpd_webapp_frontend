require "test_helper"

class OrdersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @order = orders(:one)
  end

  test "should get index" do
    get orders_url
    assert_response :success
  end

  test "should get new" do
    get new_order_url
    assert_response :success
  end

  test "should create order" do
    assert_difference("Order.count") do
      post orders_url, params: {order: {biz_id: @order.biz_id, c_address1: @order.c_address1, c_birthdate: @order.c_birthdate, c_blood_group: @order.c_blood_group, c_city: @order.c_city, c_club: @order.c_club, c_congenital_disease: @order.c_congenital_disease, c_email: @order.c_email, c_emergency_contact_name: @order.c_emergency_contact_name, c_emergency_contact_phone: @order.c_emergency_contact_phone, c_firstname: @order.c_firstname, c_idcard: @order.c_idcard, c_lastname: @order.c_lastname, c_origin: @order.c_origin, c_phone: @order.c_phone, c_province: @order.c_province, c_sex: @order.c_sex, customer_id: @order.customer_id, status: @order.status, zipcode: @order.zipcode}}
    end

    assert_redirected_to order_url(Order.last)
  end

  test "should show order" do
    get order_url(@order)
    assert_response :success
  end

  test "should get edit" do
    get edit_order_url(@order)
    assert_response :success
  end

  test "should update order" do
    patch order_url(@order), params: {order: {biz_id: @order.biz_id, c_address1: @order.c_address1, c_birthdate: @order.c_birthdate, c_blood_group: @order.c_blood_group, c_city: @order.c_city, c_club: @order.c_club, c_congenital_disease: @order.c_congenital_disease, c_email: @order.c_email, c_emergency_contact_name: @order.c_emergency_contact_name, c_emergency_contact_phone: @order.c_emergency_contact_phone, c_firstname: @order.c_firstname, c_idcard: @order.c_idcard, c_lastname: @order.c_lastname, c_origin: @order.c_origin, c_phone: @order.c_phone, c_province: @order.c_province, c_sex: @order.c_sex, customer_id: @order.customer_id, status: @order.status, zipcode: @order.zipcode}}
    assert_redirected_to order_url(@order)
  end

  test "should destroy order" do
    assert_difference("Order.count", -1) do
      delete order_url(@order)
    end

    assert_redirected_to orders_url
  end
end
