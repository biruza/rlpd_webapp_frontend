require "rails_helper"

RSpec.describe "devices/new", type: :view do
  before(:each) do
    assign(:device, Device.new(
      location: nil,
      device_category: nil,
      title: "MyString",
      image_data: "MyText",
      description: "MyText",
      slug: "MyString",
      price: "9.99",
      cost: "9.99",
      qty_max: 1,
      qty_min: 1,
      qty_current: 1,
      sn: "MyString",
      code: "MyString",
      brand: "MyString",
      model: "MyString",
      manufacturer: nil,
      user: nil,
      biz: nil
    ))
  end

  it "renders new device form" do
    render

    assert_select "form[action=?][method=?]", devices_path, "post" do
      assert_select "input[name=?]", "device[location_id]"

      assert_select "input[name=?]", "device[device_category_id]"

      assert_select "input[name=?]", "device[title]"

      assert_select "textarea[name=?]", "device[image_data]"

      assert_select "textarea[name=?]", "device[description]"

      assert_select "input[name=?]", "device[slug]"

      assert_select "input[name=?]", "device[price]"

      assert_select "input[name=?]", "device[cost]"

      assert_select "input[name=?]", "device[qty_max]"

      assert_select "input[name=?]", "device[qty_min]"

      assert_select "input[name=?]", "device[qty_current]"

      assert_select "input[name=?]", "device[sn]"

      assert_select "input[name=?]", "device[code]"

      assert_select "input[name=?]", "device[brand]"

      assert_select "input[name=?]", "device[model]"

      assert_select "input[name=?]", "device[manufacturer_id]"

      assert_select "input[name=?]", "device[user_id]"

      assert_select "input[name=?]", "device[biz_id]"
    end
  end
end
