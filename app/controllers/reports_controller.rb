class ReportsController < ApplicationController
  authorize_resource class: false
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, alert: exception.message
  end

  def report1
  end
end
