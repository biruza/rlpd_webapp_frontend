require "test_helper"

class VoucherUsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @voucher_user = voucher_users(:one)
  end

  test "should get index" do
    get voucher_users_url
    assert_response :success
  end

  test "should get new" do
    get new_voucher_user_url
    assert_response :success
  end

  test "should create voucher_user" do
    assert_difference("VoucherUser.count") do
      post voucher_users_url, params: {voucher_user: {user_id: @voucher_user.user_id, voucher_id: @voucher_user.voucher_id}}
    end

    assert_redirected_to voucher_user_url(VoucherUser.last)
  end

  test "should show voucher_user" do
    get voucher_user_url(@voucher_user)
    assert_response :success
  end

  test "should get edit" do
    get edit_voucher_user_url(@voucher_user)
    assert_response :success
  end

  test "should update voucher_user" do
    patch voucher_user_url(@voucher_user), params: {voucher_user: {user_id: @voucher_user.user_id, voucher_id: @voucher_user.voucher_id}}
    assert_redirected_to voucher_user_url(@voucher_user)
  end

  test "should destroy voucher_user" do
    assert_difference("VoucherUser.count", -1) do
      delete voucher_user_url(@voucher_user)
    end

    assert_redirected_to voucher_users_url
  end
end
