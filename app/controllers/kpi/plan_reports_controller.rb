class Kpi::PlanReportsController < ApplicationController
  before_action :set_select, only: %i[new create edit update]

  def index
    url = URI("#{$server[@controller_title]}/api/v1/plan_reports")
    @datas = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/plan_reports?order=updated_at&sort=desc&limit=10")
    @plan_reports = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/plan_reports/#{params["id"]}")
    @data_plan_report = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{@data_plan_report["data"]["master_plan_id"]}")
    @datas = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/plan_reports/#{params["id"]}/plan_report_user_comments")
    @user_commments = request_api(url, "get", nil)
  end

  def plan_reports_by_master_plan
    url = URI("#{$server[@controller_title]}/api/v1/plan_reports/#{params["id"]}/plan_reports_by_master_plan")
    @data_plan_report_by_master_plan = request_api(url, "get", nil)

    #url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{@data_plan_report_by_master_plan["data"]["master_plan_id"]}")
    #@datas = request_api(url, "get", nil)

    #url = URI("#{$server[@controller_title]}/api/v1/plan_reports/#{params["id"]}/plan_report_user_comments")
    #@user_commments = request_api(url, "get", nil)
  end  

  def new
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/plan_reports/#{params["id"]}")
    @datas = request_api(url, "get", nil)
  end

  def create
    params[:plan_report][:user_id] = $user[session.id.to_s][@controller_title]["id"]
    params[:plan_report][:biz_id] = $user[session.id.to_s][@controller_title]["biz_id"]

    url = URI("#{$server[@controller_title]}/api/v1/plan_reports")
    @datas = request_api(url, "post", plan_report_params)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/plan_reports/#{@datas["data"]["id"]}", notice: @datas["messages"]
      #redirect_to "/#{@controller_title}/period_reports/#{@datas["data"]["master_plan_id"]}", notice: @datas["messages"]
    else
      render :new
    end
  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/plan_reports/#{params["id"]}")
    @datas = request_api(url, "patch", plan_report_params)

    # puts "update status : #{res}"

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/plan_reports", notice: @datas["message"]
    else
      redirect_to "/#{@controller_title}/plan_reports", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/plan_reports/#{params["id"]}")
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
    
      redirect_to "/#{@controller_title}/pages/dashboards/", notice: @datas["message"]
    else
      redirect_to "/#{@controller_title}/plan_reports/#{params["id"]}", alert: @datas["data"]
    end
  end

  def print
    url = URI("#{$server[@controller_title]}/api/v1/plan_reports/#{params["id"]}")
    @plan_report = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{@plan_report["data"]["master_plan_id"]}")
    @master_plans = request_api(url, "get", nil)
  end

  def plans_show
    url = URI("#{$server[@controller_title]}/api/v1/plans/#{params["id"]}")
    @plan = request_api(url, "get", nil)
  end

  def plans_print
    url = URI("#{$server[@controller_title]}/api/v1/plans/#{params["id"]}")
    @plan = request_api(url, "get", nil)
  end

  private

  def set_user
    @user = User.find(auth_token[:user_id])
  end

  def set_select
    url = URI("#{$server[@controller_title]}/api/v1/plans")
    @data_plans = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/categories")
    @data_categories = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/master_plans")
    @data_master_plans = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/period_reports")
    @data_period_reports = request_api(url, "get", nil)
  end

  def plan_report_params
    # params.require(:plan_report).permit(:title1, :title2, :period_report_title, :startdate, :enddate, :slug, :order_sort, :plan, :category, :user, :biz)
    params.require(:plan_report).permit(:master_plan_id, :title1, :title2, :period_report_title, :startdate, :enddate, :slug, :order_sort, :plan_id, :category_id, :user_id, :biz_id,
      tasks_attributes: [:id, :_destroy, :slug, :status, :order_sort, :list_id, :plan_report_id, :category_id, :user_id, :biz_id])
  end
end
