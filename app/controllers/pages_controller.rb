class PagesController < ApplicationController
  def error_404
    @title = "Page 404"
    render status: 404
  end
  
  def index
  end

  def visits
    @visits = Ahoy::Visit.includes(:events).all
  end

  def notifications
    
  end

  private

  def search_params
    params.require(:search).permit(:model_name, :keyword, :system_name)
  end
end
