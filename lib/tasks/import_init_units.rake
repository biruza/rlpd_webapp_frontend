require "csv"

desc "Import Unit from csv file"
task import_init_units: [:environment] do
  csv_text1 = File.read("db/for_init/units.csv")
  csv1 = CSV.parse(csv_text1, headers: true)
  csv1.each do |row|
    Unit.create!(row.to_hash)
  end

  # ActiveRecord::Base.connection.execute(%q{
  # select setval('redeem_codes_uuid_seq', m)
  # from (
  #    select max(uuid) from redeem_codes
  #    ) as dt(m)
  # })
end
