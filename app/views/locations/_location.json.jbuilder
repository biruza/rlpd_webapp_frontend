json.extract! location, :id, :title, :image_data, :description, :lat, :lng, :slug, :user, :biz_id, :created_at, :updated_at
json.url location_url(location, format: :json)
