require "test_helper"

class SparePartCategoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @spare_part_category = spare_part_categories(:one)
  end

  test "should get index" do
    get spare_part_categories_url
    assert_response :success
  end

  test "should get new" do
    get new_spare_part_category_url
    assert_response :success
  end

  test "should create spare_part_category" do
    assert_difference("SparePartCategory.count") do
      post spare_part_categories_url, params: {spare_part_category: {name: @spare_part_category.name}}
    end

    assert_redirected_to spare_part_category_url(SparePartCategory.last)
  end

  test "should show spare_part_category" do
    get spare_part_category_url(@spare_part_category)
    assert_response :success
  end

  test "should get edit" do
    get edit_spare_part_category_url(@spare_part_category)
    assert_response :success
  end

  test "should update spare_part_category" do
    patch spare_part_category_url(@spare_part_category), params: {spare_part_category: {name: @spare_part_category.name}}
    assert_redirected_to spare_part_category_url(@spare_part_category)
  end

  test "should destroy spare_part_category" do
    assert_difference("SparePartCategory.count", -1) do
      delete spare_part_category_url(@spare_part_category)
    end

    assert_redirected_to spare_part_categories_url
  end
end
