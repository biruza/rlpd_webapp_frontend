require "test_helper"

class MaintenanceGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @maintenance_group = maintenance_groups(:one)
  end

  test "should get index" do
    get maintenance_groups_url
    assert_response :success
  end

  test "should get new" do
    get new_maintenance_group_url
    assert_response :success
  end

  test "should create maintenance_group" do
    assert_difference("MaintenanceGroup.count") do
      post maintenance_groups_url, params: {maintenance_group: {device_id: @maintenance_group.device_id, machine_type_id: @maintenance_group.machine_type_id, maintenance_id: @maintenance_group.maintenance_id}}
    end

    assert_redirected_to maintenance_group_url(MaintenanceGroup.last)
  end

  test "should show maintenance_group" do
    get maintenance_group_url(@maintenance_group)
    assert_response :success
  end

  test "should get edit" do
    get edit_maintenance_group_url(@maintenance_group)
    assert_response :success
  end

  test "should update maintenance_group" do
    patch maintenance_group_url(@maintenance_group), params: {maintenance_group: {device_id: @maintenance_group.device_id, machine_type_id: @maintenance_group.machine_type_id, maintenance_id: @maintenance_group.maintenance_id}}
    assert_redirected_to maintenance_group_url(@maintenance_group)
  end

  test "should destroy maintenance_group" do
    assert_difference("MaintenanceGroup.count", -1) do
      delete maintenance_group_url(@maintenance_group)
    end

    assert_redirected_to maintenance_groups_url
  end
end
