$('.selectize-user').selectize({
	sortField: 'text',
	create: function(input, callback) {
		$.post('/users.json', { user: { title: input } })
		.done(function(response){
			console.log(response)
			callback({value: response.id, text: response.title });
		})
	}
});