require "rails_helper"

RSpec.describe "Km::FlowItemHistories", type: :request do
  describe "GET /index" do
    it "returns http success" do
      get "/km/flow_item_histories/index"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /show" do
    it "returns http success" do
      get "/km/flow_item_histories/show"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /edit" do
    it "returns http success" do
      get "/km/flow_item_histories/edit"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /new" do
    it "returns http success" do
      get "/km/flow_item_histories/new"
      expect(response).to have_http_status(:success)
    end
  end
end
