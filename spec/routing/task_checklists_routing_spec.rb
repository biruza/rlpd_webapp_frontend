require "rails_helper"

RSpec.describe TaskChecklistsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/task_checklists").to route_to("task_checklists#index")
    end

    it "routes to #new" do
      expect(get: "/task_checklists/new").to route_to("task_checklists#new")
    end

    it "routes to #show" do
      expect(get: "/task_checklists/1").to route_to("task_checklists#show", id: "1")
    end

    it "routes to #edit" do
      expect(get: "/task_checklists/1/edit").to route_to("task_checklists#edit", id: "1")
    end

    it "routes to #create" do
      expect(post: "/task_checklists").to route_to("task_checklists#create")
    end

    it "routes to #update via PUT" do
      expect(put: "/task_checklists/1").to route_to("task_checklists#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/task_checklists/1").to route_to("task_checklists#update", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/task_checklists/1").to route_to("task_checklists#destroy", id: "1")
    end
  end
end
