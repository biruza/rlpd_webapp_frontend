require "rails_helper"

RSpec.describe "task_attaches/index", type: :view do
  before(:each) do
    assign(:task_attaches, [
      TaskAttach.create!(
        task: nil,
        title: "Title",
        file: "MyText",
        slug: "Slug",
        source: "Source",
        user: nil,
        biz: nil
      ),
      TaskAttach.create!(
        task: nil,
        title: "Title",
        file: "MyText",
        slug: "Slug",
        source: "Source",
        user: nil,
        biz: nil
      )
    ])
  end

  it "renders a list of task_attaches" do
    render
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: "Title".to_s, count: 2
    assert_select "tr>td", text: "MyText".to_s, count: 2
    assert_select "tr>td", text: "Slug".to_s, count: 2
    assert_select "tr>td", text: "Source".to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
  end
end
