require "test_helper"

class MachineCategoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @machine_category = machine_categories(:one)
  end

  test "should get index" do
    get machine_categories_url
    assert_response :success
  end

  test "should get new" do
    get new_machine_category_url
    assert_response :success
  end

  test "should create machine_category" do
    assert_difference("MachineCategory.count") do
      post machine_categories_url, params: {machine_category: {name: @machine_category.name}}
    end

    assert_redirected_to machine_category_url(MachineCategory.last)
  end

  test "should show machine_category" do
    get machine_category_url(@machine_category)
    assert_response :success
  end

  test "should get edit" do
    get edit_machine_category_url(@machine_category)
    assert_response :success
  end

  test "should update machine_category" do
    patch machine_category_url(@machine_category), params: {machine_category: {name: @machine_category.name}}
    assert_redirected_to machine_category_url(@machine_category)
  end

  test "should destroy machine_category" do
    assert_difference("MachineCategory.count", -1) do
      delete machine_category_url(@machine_category)
    end

    assert_redirected_to machine_categories_url
  end
end
