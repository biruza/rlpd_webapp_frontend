FactoryGirl.define do
  factory :task_attach do
    task nil
    title "MyString"
    file "MyText"
    slug "MyString"
    source "MyString"
    user nil
    biz nil
  end
end
