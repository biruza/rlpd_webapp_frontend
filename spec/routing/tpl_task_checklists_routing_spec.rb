require "rails_helper"

RSpec.describe TplTaskChecklistsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/tpl_task_checklists").to route_to("tpl_task_checklists#index")
    end

    it "routes to #new" do
      expect(get: "/tpl_task_checklists/new").to route_to("tpl_task_checklists#new")
    end

    it "routes to #show" do
      expect(get: "/tpl_task_checklists/1").to route_to("tpl_task_checklists#show", id: "1")
    end

    it "routes to #edit" do
      expect(get: "/tpl_task_checklists/1/edit").to route_to("tpl_task_checklists#edit", id: "1")
    end

    it "routes to #create" do
      expect(post: "/tpl_task_checklists").to route_to("tpl_task_checklists#create")
    end

    it "routes to #update via PUT" do
      expect(put: "/tpl_task_checklists/1").to route_to("tpl_task_checklists#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/tpl_task_checklists/1").to route_to("tpl_task_checklists#update", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/tpl_task_checklists/1").to route_to("tpl_task_checklists#destroy", id: "1")
    end
  end
end
