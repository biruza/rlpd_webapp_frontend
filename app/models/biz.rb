class Biz < ApplicationRecord
  include ImageUploader::Attachment.new(:image)
  include ImageUploader::Attachment.new(:stamp)
  include ImageUploader::Attachment.new(:doc_type_re_sign_image)
  include ImageUploader::Attachment.new(:doc_type_iv_sign_image)
  include ImageUploader::Attachment.new(:doc_type_qt_sign_image)
  include ImageUploader::Attachment.new(:doc_type_ex_sign_image)

  has_many :users, dependent: :destroy

  # column uniq
  # validates_uniqueness_of :domain
  validates_uniqueness_of :subdomain

  after_validation :set_subdomain, on: [:create]

  def set_subdomain
    self.subdomain = subdomain.parameterize
  end

  def task_type_title
    case task_type
    when 1
      "normal"
    when 2
      "custom"
    end
  end
end
