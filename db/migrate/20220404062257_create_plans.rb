class CreatePlans < ActiveRecord::Migration[6.0]
  def change
    create_table :plans, id: :uuid do |t|
      t.string :title
      t.datetime :period
      t.text :description
      t.string :slug
      t.string :status
      t.references :user, foreign_key: true, type: :uuid
      t.references :biz, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
