require "test_helper"

class MachineListsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @machine_list = machine_lists(:one)
  end

  test "should get index" do
    get machine_lists_url
    assert_response :success
  end

  test "should get new" do
    get new_machine_list_url
    assert_response :success
  end

  test "should create machine_list" do
    assert_difference("MachineList.count") do
      post machine_lists_url, params: {machine_list: {code: @machine_list.code, department_id: @machine_list.department_id, location_id: @machine_list.location_id, machine_id: @machine_list.machine_id}}
    end

    assert_redirected_to machine_list_url(MachineList.last)
  end

  test "should show machine_list" do
    get machine_list_url(@machine_list)
    assert_response :success
  end

  test "should get edit" do
    get edit_machine_list_url(@machine_list)
    assert_response :success
  end

  test "should update machine_list" do
    patch machine_list_url(@machine_list), params: {machine_list: {code: @machine_list.code, department_id: @machine_list.department_id, location_id: @machine_list.location_id, machine_id: @machine_list.machine_id}}
    assert_redirected_to machine_list_url(@machine_list)
  end

  test "should destroy machine_list" do
    assert_difference("MachineList.count", -1) do
      delete machine_list_url(@machine_list)
    end

    assert_redirected_to machine_lists_url
  end
end
