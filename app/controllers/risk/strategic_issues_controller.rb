class Risk::StrategicIssuesController < ApplicationController
  before_action :set_default, only: %i[index show new edit create update destroy print]
  
  def index
    url = URI("#{$server[@controller_title]}/api/v1/strategic_issues")
    @datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/strategic_issues/#{params["id"]}")
    @datas = request_api(url, "get", nil)
  end

  def new
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/strategic_issues/#{params["id"]}")

    @datas = request_api(url, "get", nil)

    # puts "plan edit = #{@plan['title']}"
  end

  def create
    params[:strategic_issues][:user_id] = $user_id
    params[:strategic_issues][:biz_id] = $biz_id

    url = URI("#{$server[@controller_title]}/api/v1/strategic_issues")
    @datas = request_api(url, "post", strategic_issues_params)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/strategic_issues", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_main_url}/strategic_issues/new", alert: @datas["data"]
    end
  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/strategic_issues/#{params["id"]}")
    @datas = request_api(url, "patch", strategic_issues_params)

    # puts "update status : #{res}"

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/strategic_issues", notice: @datas["message"]
    else
      redirect_to "/#{@controller_main_url}/strategic_issues", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/strategic_issues/#{params["id"]}")
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/strategic_issues", notice: @datas["message"]
    else
      redirect_to "/#{@controller_main_url}/strategic_issues", alert: @datas["data"]
    end
  end

  def set_default
    #@sub_system = "control"
    #@controller_main_url = "#{@controller_title}/#{@sub_system}"
    @controller_main_url = @controller_title
  end

  private

  def strategic_issues_params
    params.require(:strategic_issues).permit(:title, :order_sort, :user_id, :biz_id)
  end
end
