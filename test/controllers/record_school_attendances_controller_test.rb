require "test_helper"

class RecordSchoolAttendancesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @record_school_attendance = record_school_attendances(:one)
  end

  test "should get index" do
    get record_school_attendances_url
    assert_response :success
  end

  test "should get new" do
    get new_record_school_attendance_url
    assert_response :success
  end

  test "should create record_school_attendance" do
    assert_difference("RecordSchoolAttendance.count") do
      post record_school_attendances_url, params: {record_school_attendance: {date_action: @record_school_attendance.date_action, school_year_id: @record_school_attendance.school_year_id, term_id: @record_school_attendance.term_id, user_id: @record_school_attendance.user_id}}
    end

    assert_redirected_to record_school_attendance_url(RecordSchoolAttendance.last)
  end

  test "should show record_school_attendance" do
    get record_school_attendance_url(@record_school_attendance)
    assert_response :success
  end

  test "should get edit" do
    get edit_record_school_attendance_url(@record_school_attendance)
    assert_response :success
  end

  test "should update record_school_attendance" do
    patch record_school_attendance_url(@record_school_attendance), params: {record_school_attendance: {date_action: @record_school_attendance.date_action, school_year_id: @record_school_attendance.school_year_id, term_id: @record_school_attendance.term_id, user_id: @record_school_attendance.user_id}}
    assert_redirected_to record_school_attendance_url(@record_school_attendance)
  end

  test "should destroy record_school_attendance" do
    assert_difference("RecordSchoolAttendance.count", -1) do
      delete record_school_attendance_url(@record_school_attendance)
    end

    assert_redirected_to record_school_attendances_url
  end
end
