require "test_helper"

class WebBlocksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @web_block = web_blocks(:one)
  end

  test "should get index" do
    get web_blocks_url
    assert_response :success
  end

  test "should get new" do
    get new_web_block_url
    assert_response :success
  end

  test "should create web_block" do
    assert_difference("WebBlock.count") do
      post web_blocks_url, params: {web_block: {background_image: @web_block.background_image, background_vdo: @web_block.background_vdo, biz_id: @web_block.biz_id, description_en: @web_block.description_en, description_th: @web_block.description_th, div_class: @web_block.div_class, div_id: @web_block.div_id, image_data: @web_block.image_data, ob_align: @web_block.ob_align, ob_url: @web_block.ob_url, order_number: @web_block.order_number, page_id: @web_block.page_id, slug: @web_block.slug, subtitle_en: @web_block.subtitle_en, subtitle_th: @web_block.subtitle_th, title_en: @web_block.title_en, title_th: @web_block.title_th, user: @web_block.user, web_block_type: @web_block.web_block_type}}
    end

    assert_redirected_to web_block_url(WebBlock.last)
  end

  test "should show web_block" do
    get web_block_url(@web_block)
    assert_response :success
  end

  test "should get edit" do
    get edit_web_block_url(@web_block)
    assert_response :success
  end

  test "should update web_block" do
    patch web_block_url(@web_block), params: {web_block: {background_image: @web_block.background_image, background_vdo: @web_block.background_vdo, biz_id: @web_block.biz_id, description_en: @web_block.description_en, description_th: @web_block.description_th, div_class: @web_block.div_class, div_id: @web_block.div_id, image_data: @web_block.image_data, ob_align: @web_block.ob_align, ob_url: @web_block.ob_url, order_number: @web_block.order_number, page_id: @web_block.page_id, slug: @web_block.slug, subtitle_en: @web_block.subtitle_en, subtitle_th: @web_block.subtitle_th, title_en: @web_block.title_en, title_th: @web_block.title_th, user: @web_block.user, web_block_type: @web_block.web_block_type}}
    assert_redirected_to web_block_url(@web_block)
  end

  test "should destroy web_block" do
    assert_difference("WebBlock.count", -1) do
      delete web_block_url(@web_block)
    end

    assert_redirected_to web_blocks_url
  end
end
