require "rails_helper"

RSpec.describe "boards/new", type: :view do
  before(:each) do
    assign(:board, Board.new(
      title: "MyString",
      description: "MyText",
      user: nil,
      biz: nil
    ))
  end

  it "renders new board form" do
    render

    assert_select "form[action=?][method=?]", boards_path, "post" do
      assert_select "input[name=?]", "board[title]"

      assert_select "textarea[name=?]", "board[description]"

      assert_select "input[name=?]", "board[user_id]"

      assert_select "input[name=?]", "board[biz_id]"
    end
  end
end
