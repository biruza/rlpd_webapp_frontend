class Km::InstitutionsController < ApplicationController
  def index
    url = URI("#{$server[@controller_title]}/api/v1/institutions")

    @datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/institutions/#{params["id"]}")

    @datas = request_api(url, "get", nil)
  end

  def new
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/institutions/#{params["id"]}")

    @datas = request_api(url, "get", nil)

    # puts "plan edit = #{@plan['title']}"
  end

  def create
    params[:institution][:user_id] = $user_id
    params[:institution][:biz_id] = $biz_id

    url = URI("#{$server[@controller_title]}/api/v1/institutions")
    @datas = request_api(url, "post", institution_params)

    if @datas["status"] == "Success"
      redirect_to "/km/institutions", notice: @datas["messages"]
    else
      redirect_to "/km/institutions/new", alert: @datas["data"]
    end
  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/institutions/#{params["id"]}")
    @datas = request_api(url, "patch", institution_params)

    # puts "update status : #{res}"

    if @datas["status"] == "Success"
      redirect_to "/km/institutions", notice: @datas["message"]
    else
      redirect_to "/km/institutions", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/institutions/#{params["id"]}")
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
      redirect_to "/km/institutions", notice: @datas["message"]
    else
      redirect_to "/km/institutions", alert: @datas["data"]
    end
  end

  private

  def institution_params
    params.require(:institution).permit(:title_short, :title_short_en, :title, :order_sort, :user_id, :biz_id)
  end
end
