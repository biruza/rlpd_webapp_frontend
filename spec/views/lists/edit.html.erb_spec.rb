require "rails_helper"

RSpec.describe "lists/edit", type: :view do
  before(:each) do
    @list = assign(:list, List.create!(
      title: "MyString",
      order_sort: 1,
      slug: "MyString",
      board: nil,
      archived: false,
      user: nil,
      biz: nil
    ))
  end

  it "renders the edit list form" do
    render

    assert_select "form[action=?][method=?]", list_path(@list), "post" do
      assert_select "input[name=?]", "list[title]"

      assert_select "input[name=?]", "list[order_sort]"

      assert_select "input[name=?]", "list[slug]"

      assert_select "input[name=?]", "list[board_id]"

      assert_select "input[name=?]", "list[archived]"

      assert_select "input[name=?]", "list[user_id]"

      assert_select "input[name=?]", "list[biz_id]"
    end
  end
end
