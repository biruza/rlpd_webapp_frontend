require "rails_helper"

RSpec.describe "plans/edit", type: :view do
  before(:each) do
    @plan = assign(:plan, Plan.create!(
      title: "MyString",
      description: "MyText",
      slug: "MyString",
      status: "MyString",
      user: nil,
      biz: nil
    ))
  end

  it "renders the edit plan form" do
    render

    assert_select "form[action=?][method=?]", plan_path(@plan), "post" do
      assert_select "input[name=?]", "plan[title]"

      assert_select "textarea[name=?]", "plan[description]"

      assert_select "input[name=?]", "plan[slug]"

      assert_select "input[name=?]", "plan[status]"

      assert_select "input[name=?]", "plan[user_id]"

      assert_select "input[name=?]", "plan[biz_id]"
    end
  end
end
