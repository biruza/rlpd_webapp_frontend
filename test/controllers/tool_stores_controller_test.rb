require "test_helper"

class ToolStoresControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tool_store = tool_stores(:one)
  end

  test "should get index" do
    get tool_stores_url
    assert_response :success
  end

  test "should get new" do
    get new_tool_store_url
    assert_response :success
  end

  test "should create tool_store" do
    assert_difference("ToolStore.count") do
      post tool_stores_url, params: {tool_store: {manufacturer_id: @tool_store.manufacturer_id, max: @tool_store.max, min: @tool_store.min, price_per_unit: @tool_store.price_per_unit, qty: @tool_store.qty, store_id: @tool_store.store_id, tool_id: @tool_store.tool_id}}
    end

    assert_redirected_to tool_store_url(ToolStore.last)
  end

  test "should show tool_store" do
    get tool_store_url(@tool_store)
    assert_response :success
  end

  test "should get edit" do
    get edit_tool_store_url(@tool_store)
    assert_response :success
  end

  test "should update tool_store" do
    patch tool_store_url(@tool_store), params: {tool_store: {manufacturer_id: @tool_store.manufacturer_id, max: @tool_store.max, min: @tool_store.min, price_per_unit: @tool_store.price_per_unit, qty: @tool_store.qty, store_id: @tool_store.store_id, tool_id: @tool_store.tool_id}}
    assert_redirected_to tool_store_url(@tool_store)
  end

  test "should destroy tool_store" do
    assert_difference("ToolStore.count", -1) do
      delete tool_store_url(@tool_store)
    end

    assert_redirected_to tool_stores_url
  end
end
