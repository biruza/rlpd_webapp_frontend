require "test_helper"

class ProductCustomFieldGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @product_custom_field_group = product_custom_field_groups(:one)
  end

  test "should get index" do
    get product_custom_field_groups_url
    assert_response :success
  end

  test "should get new" do
    get new_product_custom_field_group_url
    assert_response :success
  end

  test "should create product_custom_field_group" do
    assert_difference("ProductCustomFieldGroup.count") do
      post product_custom_field_groups_url, params: {product_custom_field_group: {biz_id: @product_custom_field_group.biz_id, parent: @product_custom_field_group.parent, product_custom_field_id: @product_custom_field_group.product_custom_field_id, title_th: @product_custom_field_group.title_th}}
    end

    assert_redirected_to product_custom_field_group_url(ProductCustomFieldGroup.last)
  end

  test "should show product_custom_field_group" do
    get product_custom_field_group_url(@product_custom_field_group)
    assert_response :success
  end

  test "should get edit" do
    get edit_product_custom_field_group_url(@product_custom_field_group)
    assert_response :success
  end

  test "should update product_custom_field_group" do
    patch product_custom_field_group_url(@product_custom_field_group), params: {product_custom_field_group: {biz_id: @product_custom_field_group.biz_id, parent: @product_custom_field_group.parent, product_custom_field_id: @product_custom_field_group.product_custom_field_id, title_th: @product_custom_field_group.title_th}}
    assert_redirected_to product_custom_field_group_url(@product_custom_field_group)
  end

  test "should destroy product_custom_field_group" do
    assert_difference("ProductCustomFieldGroup.count", -1) do
      delete product_custom_field_group_url(@product_custom_field_group)
    end

    assert_redirected_to product_custom_field_groups_url
  end
end
