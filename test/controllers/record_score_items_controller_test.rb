require "test_helper"

class RecordScoreItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @record_score_item = record_score_items(:one)
  end

  test "should get index" do
    get record_score_items_url
    assert_response :success
  end

  test "should get new" do
    get new_record_score_item_url
    assert_response :success
  end

  test "should create record_score_item" do
    assert_difference("RecordScoreItem.count") do
      post record_score_items_url, params: {record_score_item: {score_template_item_id: @record_score_item.score_template_item_id, status: @record_score_item.status, value1: @record_score_item.value1, value2: @record_score_item.value2, value3: @record_score_item.value3}}
    end

    assert_redirected_to record_score_item_url(RecordScoreItem.last)
  end

  test "should show record_score_item" do
    get record_score_item_url(@record_score_item)
    assert_response :success
  end

  test "should get edit" do
    get edit_record_score_item_url(@record_score_item)
    assert_response :success
  end

  test "should update record_score_item" do
    patch record_score_item_url(@record_score_item), params: {record_score_item: {score_template_item_id: @record_score_item.score_template_item_id, status: @record_score_item.status, value1: @record_score_item.value1, value2: @record_score_item.value2, value3: @record_score_item.value3}}
    assert_redirected_to record_score_item_url(@record_score_item)
  end

  test "should destroy record_score_item" do
    assert_difference("RecordScoreItem.count", -1) do
      delete record_score_item_url(@record_score_item)
    end

    assert_redirected_to record_score_items_url
  end
end
