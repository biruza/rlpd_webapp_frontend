require "image_processing/mini_magick"

class ImageUploader < Shrine
  ALLOWED_TYPES = %w[image/jpeg image/jpg image/png image/gif application/pdf]
  MAX_SIZE = 10 * 1024 * 1024 # 10 MB

  plugin :remove_attachment
  plugin :pretty_location
  plugin :processing
  plugin :versions
  plugin :validation_helpers
  plugin :store_dimensions, analyzer: :mini_magick
  # for test expire s3 - 4
  # plugin :derivation_endpoint,
  #  secret_key: "yrTUijArhyiAGA6BsCK9O2SyQ2QOVBtOT571hybF",
  #  prefix:     "derivations/image"

  # plugin :derivation_endpoint, expires_in: 125
  ####

  Attacher.validate do
    validate_max_size MAX_SIZE
    if validate_mime_type_inclusion(ALLOWED_TYPES)
      validate_max_width 5000
      validate_max_height 5000
    end
  end

  process(:store) do |io, context|
    original = io.download

    tiny = ImageProcessing::MiniMagick
      .source(original)
      .saver(quality: 75)
      .resize_to_limit!(96, nil)
    # .convert("png")

    thumbnail = ImageProcessing::MiniMagick
      .source(original)
      .saver(quality: 75)
      .resize_to_limit!(320, nil)
    # .convert("png")

    small = ImageProcessing::MiniMagick
      .source(original)
      .saver(quality: 75)
      .resize_to_limit!(640, nil)
    # .convert("png")

    medium = ImageProcessing::MiniMagick
      .source(original)
      .saver(quality: 75)
      .resize_to_limit!(1200, nil)
    # .convert("png")

    large = ImageProcessing::MiniMagick
      .source(original)
      .saver(quality: 75)
      .resize_to_limit!(1920, nil)
    # .convert("png")

    avatar = ImageProcessing::MiniMagick
      .source(original)
      .saver(quality: 75)
      .resize_to_fill!(96, 96)
    # .convert("png")

    original.close!

    {large: large, medium: medium, small: small, thumbnail: thumbnail, tiny: tiny, avatar: avatar}
  end
end
