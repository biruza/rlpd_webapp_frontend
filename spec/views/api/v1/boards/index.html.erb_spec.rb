require "rails_helper"

RSpec.describe "api/v1/boards/index", type: :view do
  before(:each) do
    assign(:api_v1_boards, [
      Api::V1::Board.create!(
        title: "Title",
        description: "MyText",
        user: nil,
        biz: nil
      ),
      Api::V1::Board.create!(
        title: "Title",
        description: "MyText",
        user: nil,
        biz: nil
      )
    ])
  end

  it "renders a list of api/v1/boards" do
    render
    assert_select "tr>td", text: "Title".to_s, count: 2
    assert_select "tr>td", text: "MyText".to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
  end
end
