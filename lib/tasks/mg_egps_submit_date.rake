desc "mg_egps_submit_date"
task mg_egps_submit_date: [:environment] do
  count = 0
  egps = Egp.where(submit_date: nil)
  egps.each do |egp|
    if egp.update(submit_date: egp.get_submit_date(egp.link))
      count += 1
    end
  end
  p "count = #{count}"
end
