require "rails_helper"

RSpec.describe "tpl_task_checklists/new", type: :view do
  before(:each) do
    assign(:tpl_task_checklist, TplTaskChecklist.new(
      tpl_task: nil,
      order_sort: 1,
      title: "MyString",
      checklist_type: "MyText",
      status: "MyString",
      score: "9.99",
      custom: "",
      description: "MyText",
      weight: "9.99",
      user: nil,
      biz: nil
    ))
  end

  it "renders new tpl_task_checklist form" do
    render

    assert_select "form[action=?][method=?]", tpl_task_checklists_path, "post" do
      assert_select "input[name=?]", "tpl_task_checklist[tpl_task_id]"

      assert_select "input[name=?]", "tpl_task_checklist[order_sort]"

      assert_select "input[name=?]", "tpl_task_checklist[title]"

      assert_select "textarea[name=?]", "tpl_task_checklist[checklist_type]"

      assert_select "input[name=?]", "tpl_task_checklist[status]"

      assert_select "input[name=?]", "tpl_task_checklist[score]"

      assert_select "input[name=?]", "tpl_task_checklist[custom]"

      assert_select "textarea[name=?]", "tpl_task_checklist[description]"

      assert_select "input[name=?]", "tpl_task_checklist[weight]"

      assert_select "input[name=?]", "tpl_task_checklist[user_id]"

      assert_select "input[name=?]", "tpl_task_checklist[biz_id]"
    end
  end
end
