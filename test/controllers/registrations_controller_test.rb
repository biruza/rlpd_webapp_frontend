require "test_helper"

class RegistrationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @registration = registrations(:one)
  end

  test "should get index" do
    get registrations_url
    assert_response :success
  end

  test "should get new" do
    get new_registration_url
    assert_response :success
  end

  test "should create registration" do
    assert_difference("Registration.count") do
      post registrations_url, params: {registration: {biz_id: @registration.biz_id, description_en: @registration.description_en, description_short_en: @registration.description_short_en, description_short_th: @registration.description_short_th, description_th: @registration.description_th, end_date: @registration.end_date, image_data: @registration.image_data, publish: @registration.publish, slug: @registration.slug, start_date: @registration.start_date, template: @registration.template, title_en: @registration.title_en, title_th: @registration.title_th, user_id: @registration.user_id}}
    end

    assert_redirected_to registration_url(Registration.last)
  end

  test "should show registration" do
    get registration_url(@registration)
    assert_response :success
  end

  test "should get edit" do
    get edit_registration_url(@registration)
    assert_response :success
  end

  test "should update registration" do
    patch registration_url(@registration), params: {registration: {biz_id: @registration.biz_id, description_en: @registration.description_en, description_short_en: @registration.description_short_en, description_short_th: @registration.description_short_th, description_th: @registration.description_th, end_date: @registration.end_date, image_data: @registration.image_data, publish: @registration.publish, slug: @registration.slug, start_date: @registration.start_date, template: @registration.template, title_en: @registration.title_en, title_th: @registration.title_th, user_id: @registration.user_id}}
    assert_redirected_to registration_url(@registration)
  end

  test "should destroy registration" do
    assert_difference("Registration.count", -1) do
      delete registration_url(@registration)
    end

    assert_redirected_to registrations_url
  end
end
