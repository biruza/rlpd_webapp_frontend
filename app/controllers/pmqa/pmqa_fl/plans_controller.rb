class Pmqa::PmqaFl::PlansController < ApplicationController
  before_action :set_select, only: %i[new create edit update]
  before_action :set_default, only: %i[user_input user_input_update user_input_submit opdc_input opdc_input_update opdc_input_submit index show new edit create update destroy print]
  @sub_system = "/pmqa_fl"

  def index
    url = URI("#{$server[@controller_title]}/api/v1/plans")
    @datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/plans/#{params["id"]}")
    @plan = request_api(url, "get", nil)
  end

  def new
    if params[:master_plan_id].blank?
      redirect_to "/#{@controller_main_url}/master_plans", notice: nil
    end
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/plans/#{params["id"]}")
    @datas = request_api(url, "get", nil)
  end

  def create
    # params[:plan][:user_id] = $user[session.id.to_s][@controller_title]['id']
    # params[:plan][:biz_id] = $user[session.id.to_s][@controller_title]['biz_id']

    url = URI("#{$server[@controller_title]}/api/v1/plans")
    @plan = request_api(url, "post", plan_params)

    if @plan["status"] == "Success"
      redirect_to "/#{@controller_main_url}/master_plans/#{@plan["data"]["master_plan_id"]}", notice: @plan["messages"]
    else
      redirect_to "/#{@controller_main_url}/plans/new?master_plan_id=#{params[:plan][:master_plan_id]}", alert: @plan["messages"]
    end
  end

  def update
 

    url = URI("#{$server[@controller_title]}/api/v1/plans/#{params["id"]}")
    @datas = request_api(url, "patch", plan_params)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/master_plans/#{@datas["data"]["master_plan_id"]}", notice: @datas["message"]
    else
      redirect_to "/#{@controller_main_url}/plans/#{params["id"]}/edit", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/plans/#{params["id"]}")
    @datas_ori = request_api(url, "get", nil)
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/master_plans/#{@datas_ori["data"]["master_plan_id"]}", notice: @datas["message"]
    else
      redirect_to "/#{@controller_main_url}/master_plans/#{@datas_ori["data"]["master_plan_id"]}", alert: t(:can_not_delete)
    end
  end

  def history
    url = URI("#{$server[@controller_title]}/api/v1/plans/#{params["id"]}")
    @plan = request_api(url, "get", nil)
  end

  def print
    url = URI("#{$server[@controller_title]}/api/v1/plans/#{params["id"]}")
    @plan = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/fl_kpis")
    @fl_kpis = request_api(url, "get", nil)

    @kpi_cond = [
      {
        title: "มากดี",
        value: "more"
      },
      {
        title: "น้อยดี",
        value: "less"
      }
    ]
  end

  def tasks
    url = URI("#{$server[@controller_title]}/api/v1/plans/#{params["plan_id"]}")
    @plan = request_api(url, "get", nil)
    @plan = @plan["data"] if !@plan.blank?

    url = URI("#{$server[@controller_title]}/api/v1/plan_reports/#{params["plan_report_id"]}")
    @plan_report = request_api(url, "get", nil)
  end

  def tasks_print
  end

  def status
    @list_id = params[:list_id]
    @list_activity_id = params[:list_activity_id]
    @plan_report_id = params[:plan_report_id]
    @original_url = params[:original_url]

    url = URI("#{$server[@controller_title]}/api/v1/lists/#{params["list_id"]}") if !params[:list_id].blank?
    @list = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/list_activities/#{params["list_activity_id"]}") if !params[:list_activity_id].blank?
    @list_activity = request_api(url, "get", nil)

    respond_to do |format|
      format.html
      format.js
    end
  end

  def user_input



    url = URI("#{$server[@controller_title]}/api/v1/plans/#{params["id"]}")
    @plan = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/fl_kpis")
    @fl_kpis = request_api(url, "get", nil)

    @kpi_cond = [
      {
        title: "มากดี",
        value: "more"
      },
      {
        title: "น้อยดี",
        value: "less"
      }
    ]
  end

  def user_input_update_normal
    if !params[:form].blank?

      params[:form].each do |form1|
        params[:form][(form1[0]).to_s].each do |form2|
          params[:form][(form1[0]).to_s][(form2[0]).to_s].each do |form3|
            form = {
              id: params[:form][(form1[0]).to_s][(form2[0]).to_s][(form3[0]).to_s][:id],
              fl_score: params[:form][(form1[0]).to_s][(form2[0]).to_s][(form3[0]).to_s][:fl_score].to_s,
              fl_result_text: params[:form][(form1[0]).to_s][(form2[0]).to_s][(form3[0]).to_s][:fl_result_text],
              file: params[:form][(form1[0]).to_s][(form2[0]).to_s][(form3[0]).to_s][:file]
            }

            url = URI("#{$server[@controller_title]}/api/v1/list_activities/#{params[:form][(form1[0]).to_s][(form2[0]).to_s][(form3[0]).to_s][:id]}")
            @list_activity = request_api(url, "patch", form)
          end
        end
      end


      if @list_activity && @list_activity["status"] == "Success"
        # redirect_to params[:original_url], notice: "ระบบทำการบันทึกร่างเรียบร้อยแล้ว"
        redirect_to params[:original_url], notice: "ระบบทำการบันทึกเสร็จเรียบร้อยแล้ว"
      else
        redirect_to params[:original_url], alert: @list_activity["message"]
      end


    end #if params[:form]

  end

  def user_input_update_seven
    
  
        if !params[:form].blank?
    
          params[:form].each do |form1|
            params[:form][(form1[0]).to_s].each do |form2|
              params[:form][(form1[0]).to_s][(form2[0]).to_s].each do |form3|
              form = {
                id: params[:form][(form1[0]).to_s][(form2[0]).to_s][(form3[0]).to_s][:id],
                fl_kpi_value: params[:form][(form1[0]).to_s][(form2[0]).to_s][(form3[0]).to_s][:fl_kpi_value],
                fl_first_value: params[:form][(form1[0]).to_s][(form2[0]).to_s][(form3[0]).to_s][:fl_first_value],
                fl_second_value: params[:form][(form1[0]).to_s][(form2[0]).to_s][(form3[0]).to_s][:fl_second_value],
                fl_third_value: params[:form][(form1[0]).to_s][(form2[0]).to_s][(form3[0]).to_s][:fl_third_value],
                fl_score_value: params[:form][(form1[0]).to_s][(form2[0]).to_s][(form3[0]).to_s][:fl_score_value],
                slug: params[:form][(form1[0]).to_s][(form2[0]).to_s][(form3[0]).to_s][:slug],
                description: params[:form][(form1[0]).to_s][(form2[0]).to_s][(form3[0]).to_s][:description]
              }
    
              url = URI("#{$server[@controller_title]}/api/v1/list_activities/#{params[:form][(form1[0]).to_s][(form2[0]).to_s][(form3[0]).to_s][:id]}")
              @list_activity = request_api(url, "patch", form)
            end
          end
        end
    
          if @list_activity && @list_activity["status"] == "Success"
            # redirect_to params[:original_url], notice: "ระบบทำการบันทึกร่างเรียบร้อยแล้ว"
            redirect_to params[:original_url], notice: "ระบบทำการบันทึกเสร็จเรียบร้อยแล้ว"
          else
            redirect_to params[:original_url], alert: @list_activities["message"]
          end
    
        end #if params[:form]
      end


=begin 
#old version  ปรับตอน แก้ไขตามที่พี่บอลขอ 
  def user_input_update_seven
    
p "=================>#{params[:form]}"
    if !params[:form].blank?

      params[:form].each do |form1|
        params[:form][(form1[0]).to_s].each do |form2|
          form = {
            id: params[:form][(form1[0]).to_s][(form2[0]).to_s][:id],
            fl_kpi_id: params[:form][(form1[0]).to_s][(form2[0]).to_s][:fl_kpi_id],
            kpi_cond: params[:form][(form1[0]).to_s][(form2[0]).to_s][:kpi_cond],
            fl_kpi_value: params[:form][(form1[0]).to_s][(form2[0]).to_s][:fl_kpi_value],
            kpi_unit: params[:form][(form1[0]).to_s][(form2[0]).to_s][:kpi_unit],
            fl_first_value: params[:form][(form1[0]).to_s][(form2[0]).to_s][:fl_first_value],
            fl_second_value: params[:form][(form1[0]).to_s][(form2[0]).to_s][:fl_second_value],
            fl_third_value: params[:form][(form1[0]).to_s][(form2[0]).to_s][:fl_third_value],
            fl_score_value: params[:form][(form1[0]).to_s][(form2[0]).to_s][:fl_score_value],
            file: params[:form][(form1[0]).to_s][(form2[0]).to_s][:file]
          }

          url = URI("#{$server[@controller_title]}/api/v1/lists/#{params[:form][(form1[0]).to_s][(form2[0]).to_s][:id]}")
          @list = request_api(url, "patch", form)
        end
      end

      if @list && @list["status"] == "Success"
        # redirect_to params[:original_url], notice: "ระบบทำการบันทึกร่างเรียบร้อยแล้ว"
        redirect_to params[:original_url], notice: "ระบบทำการบันทึกเสร็จเรียบร้อยแล้ว"
      else
        redirect_to params[:original_url], alert: @list["message"]
      end

    end #if params[:form]
  end
=end
  def user_input_submit

    url = URI("#{$server[@controller_title]}/api/v1/plans/#{params["id"]}")
    @plan = request_api(url, "patch", plan_params)

    if @plan["status"] == "Success"
      redirect_to "/#{@controller_main_url}/plans/user_input/#{@plan["data"]["id"]}", notice: "ระบบทำการส่งคะแนนเรียบร้อยแล้ว"
    else
      redirect_to "/#{@controller_main_url}/plans/user_input/#{@plan["data"]["id"]}", alert: @plan["message"]
    end
  end

  def opdc_input
    url = URI("#{$server[@controller_title]}/api/v1/plans/#{params["id"]}")
    @plan = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/fl_kpis")
    @fl_kpis = request_api(url, "get", nil)

    @kpi_cond = [
      {
        title: "มากดี",
        value: "more"
      },
      {
        title: "น้อยดี",
        value: "less"
      }
    ]
  end

  def opdc_input_update_normal


    #update score by opdc
    if !params[:category].blank?
      params[:category].each do |form1|
        form = {
          id: params[:category][(form1[0]).to_s][:id],
          fl_score: params[:category][(form1[0]).to_s][:fl_score].to_i,
        }
        url = URI("#{$server[@controller_title]}/api/v1/categories/#{params[:category][(form1[0]).to_s][:id]}")
        @category = request_api(url, "patch", form)
      end
    end


    #แก้ไขคะแนนของ user ได้ด้วย
    if !params[:form].blank?
      params[:form].each do |form1|
        params[:form][(form1[0]).to_s].each do |form2|
          params[:form][(form1[0]).to_s][(form2[0]).to_s].each do |form3|
            form = {
              id: params[:form][(form1[0]).to_s][(form2[0]).to_s][(form3[0]).to_s][:id],
              fl_score: params[:form][(form1[0]).to_s][(form2[0]).to_s][(form3[0]).to_s][:fl_score].to_i,
              fl_result_text: params[:form][(form1[0]).to_s][(form2[0]).to_s][(form3[0]).to_s][:fl_result_text],
              file: params[:form][(form1[0]).to_s][(form2[0]).to_s][(form3[0]).to_s][:file]
            }


            url = URI("#{$server[@controller_title]}/api/v1/list_activities/#{params[:form][(form1[0]).to_s][(form2[0]).to_s][(form3[0]).to_s][:id]}")
            @list_activity = request_api(url, "patch", form)
          end
        end
      end
    end
    

    if @category && @category["status"] == "Success"
      # redirect_to params[:original_url], notice: "ระบบทำการบันทึกร่างเรียบร้อยแล้ว"
      redirect_to params[:original_url], notice: "ระบบทำการบันทึกเสร็จเรียบร้อยแล้ว"
    else
      redirect_to params[:original_url], alert: @category["message"]
    end
  end

  def opdc_input_update_seven

  

    if !params[:form].blank?
      params[:form].each do |form1|
        params[:form][(form1[0]).to_s].each do |form2|
          form = {
            id: params[:form][(form1[0]).to_s][(form2[0]).to_s][:id],
            fl_score_value: params[:form][(form1[0]).to_s][(form2[0]).to_s][:fl_score_value],
            file: params[:form][(form1[0]).to_s][(form2[0]).to_s][:file],
          }

          url = URI("#{$server[@controller_title]}/api/v1/lists/#{params[:form][(form1[0]).to_s][(form2[0]).to_s][:id]}")
          @list = request_api(url, "patch", form)
        end
      end
    end

    if @list && @list["status"] == "Success"
      # redirect_to params[:original_url], notice: "ระบบทำการบันทึกร่างเรียบร้อยแล้ว"
      redirect_to params[:original_url], notice: "ระบบทำการบันทึกเสร็จเรียบร้อยแล้ว"
    else
      redirect_to params[:original_url], alert: @list["message"]
    end
  end

  private

  def set_default
    @sub_system = "pmqa_fl"
    @controller_main_url = "#{@controller_title}/#{@sub_system}"
  end

  def set_select
    url = URI("#{$server[@controller_title]}/api/v1/category_masters")
    @category_masters = request_api(url, "get", nil)

    @type_names = [
      {
        title: "หมวด 1-6",
        value: "normal"
      },
      {
        title: "หมวด 7",
        value: "seven"
      }
    ]
  end

  def plan_params
    params.require(:plan).permit(:order_sort, :status, :type_name, :fl_total_score, :title, :master_plan_id, :user_id, :biz_id, :category_master_id, :category_master_list_id, :file)
  end

  def form_params
    params.require(:form).permit(:order_sort, :list_activity_id, :fl_result_text, :file)
  end

  def form_seven_params
    params.require(:form_seven).permit(:order_sort, :list_id, :fl_kpi_value, :fl_first_value, :fl_second_value, :fl_third_value, :fl_score_value, :file)
  end
end
