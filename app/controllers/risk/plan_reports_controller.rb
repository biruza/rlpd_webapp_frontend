class Risk::PlanReportsController < ApplicationController
  before_action :set_default, only: %i[index show new edit create update destroy print]
  before_action :set_select, only: %i[new create edit update]

  def index
    url = URI("#{$server[@controller_title]}/api/v1/plan_reports?order=updated_at&sort=desc&limit=10")
    @plan_reports = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/plan_reports/#{params["id"]}")
    @plan_report = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/categories/#{@plan_report["data"]["category_id"]}")
    @category = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/plan_reports/#{params["id"]}/plan_report_user_comments")
    @user_commments = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/risk_types?order=updated_at&sort=desc")
    @risk_types = request_api(url, "get", nil)
  end

  def new
    if !params[:category_id].blank?
      url = URI("#{$server[@controller_title]}/api/v1/categories/#{params[:category_id]}")
      @category = request_api(url, "get", nil)

      plan_report_params = {
        category_id: @category["data"]["id"],
        plan_id: @category["data"]["plan_id"]
      }

      url = URI("#{$server[@controller_title]}/api/v1/plan_reports")
      @datas = request_api(url, "post", plan_report_params)

      if @datas["status"] == "Success"
        url = URI("#{$server[@controller_title]}/api/v1/plan_reports/#{@datas["data"]["id"]}")
        @plan_report = request_api(url, "get", nil)

        @plan_report["data"]["category"]["lists"].each do |list|
         
               task_params = {
                              list_id: list["id"],
                              category_id:  list["category_id"],
                              risk_chance: "0",
                              risk_effect: "0",
                              order_sort: list["order_sort"]&.to_i,
                              plan_report_id: @plan_report["data"]["id"],
                              user_id: list["user_id"],
                              biz_id: list["biz_id"]
          }
          url_task = URI("#{$server[@controller_title]}/api/v1/tasks")
          task_new = request_api(url_task, "post", task_params)
      
     
    end

        redirect_to "/#{@controller_main_url}/plan_reports/#{@datas["data"]["id"]}/edit", notice: @datas["messages"]
      else
        render :new, status: :unprocessable_entity
      end

    else
      redirect_to "/#{@controller_main_url}/master_plans", alert: (begin
        t(:can_not_found)
      rescue
        nil
      end).to_s
    end
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/plan_reports/#{params["id"]}")
    @plan_report = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/categories/#{@plan_report["data"]["category_id"]}")
    @category = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/risk_types?order=updated_at&sort=desc")
    @risk_types = request_api(url, "get", nil)
  end

  def update
    idx = 0
    if !params[:plan_report][:tasks_attributes].blank?
      params[:plan_report][:tasks_attributes].each do |item|
        params[:plan_report][:tasks_attributes][idx.to_s]["plan_report_id"] = params[:plan_report][:tasks_attributes][idx.to_s]["plan_report_id"].to_i
        params[:plan_report][:tasks_attributes][idx.to_s]["category_id"] = params[:plan_report][:tasks_attributes][idx.to_s]["category_id"].to_i
        params[:plan_report][:tasks_attributes][idx.to_s]["risk_chance"] = params[:plan_report][:tasks_attributes][idx.to_s]["risk_chance"].to_i
        params[:plan_report][:tasks_attributes][idx.to_s]["risk_effect"] = params[:plan_report][:tasks_attributes][idx.to_s]["risk_effect"].to_i
        params[:plan_report][:tasks_attributes][idx.to_s]["user_id"] = $user[session.id.to_s][@controller_title]["id"]
        params[:plan_report][:tasks_attributes][idx.to_s]["biz_id"] = $user[session.id.to_s][@controller_title]["biz_id"]
        idx += 1
      end
    end

    url = URI("#{$server[@controller_title]}/api/v1/plan_reports/#{params["id"]}")
    @datas = request_api(url, "patch", plan_report_params)

    url_plan_report = URI("#{$server[@controller_title]}/api/v1/plan_reports/#{params["id"]}")
    @plan_report = request_api(url_plan_report, "get", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/plan_reports/#{@plan_report["data"]["id"]}/edit", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_main_url}/plan_reports/#{@plan_report["data"]["id"]}/edit", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/plan_reports/#{params["id"]}")
    @plan_report = request_api(url, "get", nil)

    url_category = URI("#{$server[@controller_title]}/api/v1/categories/#{@plan_report["data"]["category_id"]}")
    @category = request_api(url_category, "get", nil)

    @datas = request_api(url, "delete", nil)

    if @plan_report["status"] == "Success"
      redirect_to "/#{@controller_main_url}/plans/#{@category["data"]["plan_id"]}", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_main_url}/plans/#{@category["data"]["plan_id"]}", alert: @datas["data"]
    end
  end

  def print
    url = URI("#{$server[@controller_title]}/api/v1/plan_reports/#{params["id"]}")
    @plan_report = request_api(url, "get", nil)
  end

  private

  def set_default
    # @sub_system = "pmqa_fl"
    # @controller_main_url = "#{@controller_title}/#{@sub_system}"
    @controller_main_url = @controller_title
  end

  def set_user
    @user = User.find(auth_token[:user_id])
  end

  def set_select
    # url = URI("#{$server[@controller_title]}/api/v1/plans")
    # @data_plans = request_api(url,'get',nil)

    # url = URI("#{$server[@controller_title]}/api/v1/categories")
    # @data_categories = request_api(url,'get',nil)

    # url = URI("#{$server[@controller_title]}/api/v1/master_plans")
    # @data_master_plans = request_api(url,'get',nil)
  end

  def plan_report_params
    params.require(:plan_report).permit(:master_plan_id, :title1, :title2, :startdate, :enddate, :slug, :order_sort, :plan_id, :category_id, :user_id, :biz_id,
      tasks_attributes: [:id, :_destroy, :title, :risk_chance, :risk_effect, :risk_score, :order_sort, :plan_report_id, :category_id, :list_id, :user_id, :biz_id])
  end
end
