require "test_helper"

class FormNewstudentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @form_newstudent = form_newstudents(:one)
  end

  test "should get index" do
    get form_newstudents_url
    assert_response :success
  end

  test "should get new" do
    get new_form_newstudent_url
    assert_response :success
  end

  test "should create form_newstudent" do
    assert_difference("FormNewstudent.count") do
      post form_newstudents_url, params: {form_newstudent: {address1: @form_newstudent.address1, address2: @form_newstudent.address2, address_father: @form_newstudent.address_father, address_mother: @form_newstudent.address_mother, address_parent: @form_newstudent.address_parent, birth: @form_newstudent.birth, birth_location: @form_newstudent.birth_location, blood: @form_newstudent.blood, cdisease: @form_newstudent.cdisease, chumclass: @form_newstudent.chumclass, city1: @form_newstudent.city1, city2: @form_newstudent.city2, city_father: @form_newstudent.city_father, city_mother: @form_newstudent.city_mother, city_parent: @form_newstudent.city_parent, datestart: @form_newstudent.datestart, email: @form_newstudent.email, email_father: @form_newstudent.email_father, email_mother: @form_newstudent.email_mother, email_parent: @form_newstudent.email_parent, finisholdschool: @form_newstudent.finisholdschool, firstname: @form_newstudent.firstname, firstname_father: @form_newstudent.firstname_father, firstname_mother: @form_newstudent.firstname_mother, firstname_parent: @form_newstudent.firstname_parent, grownoldschool: @form_newstudent.grownoldschool, height: @form_newstudent.height, image_data: @form_newstudent.image_data, job_father: @form_newstudent.job_father, job_location_father: @form_newstudent.job_location_father, job_location_mother: @form_newstudent.job_location_mother, job_location_parent: @form_newstudent.job_location_parent, job_mother: @form_newstudent.job_mother, job_parent: @form_newstudent.job_parent, job_phone_father: @form_newstudent.job_phone_father, job_phone_mother: @form_newstudent.job_phone_mother, job_phone_parent: @form_newstudent.job_phone_parent, lastname: @form_newstudent.lastname, lastname_father: @form_newstudent.lastname_father, lastname_mother: @form_newstudent.lastname_mother, lastname_parent: @form_newstudent.lastname_parent, mobile: @form_newstudent.mobile, mobile_father: @form_newstudent.mobile_father, mobile_mother: @form_newstudent.mobile_mother, mobile_parent: @form_newstudent.mobile_parent, nation: @form_newstudent.nation, nation_father: @form_newstudent.nation_father, nation_mother: @form_newstudent.nation_mother, nation_parent: @form_newstudent.nation_parent, nickname: @form_newstudent.nickname, number_elder: @form_newstudent.number_elder, number_you: @form_newstudent.number_you, number_younger: @form_newstudent.number_younger, oldschool: @form_newstudent.oldschool, orgin_father: @form_newstudent.orgin_father, orgin_mother: @form_newstudent.orgin_mother, orgin_parent: @form_newstudent.orgin_parent, origin: @form_newstudent.origin, percode: @form_newstudent.percode, percode_father: @form_newstudent.percode_father, percode_mother: @form_newstudent.percode_mother, percode_parent: @form_newstudent.percode_parent, phone: @form_newstudent.phone, phone_father: @form_newstudent.phone_father, phone_mother: @form_newstudent.phone_mother, phone_parent: @form_newstudent.phone_parent, prefix: @form_newstudent.prefix, prefix_father: @form_newstudent.prefix_father, prefix_mother: @form_newstudent.prefix_mother, prefix_parent: @form_newstudent.prefix_parent, province1: @form_newstudent.province1, province2: @form_newstudent.province2, province_father: @form_newstudent.province_father, province_mother: @form_newstudent.province_mother, province_parent: @form_newstudent.province_parent, race: @form_newstudent.race, race_father: @form_newstudent.race_father, race_mother: @form_newstudent.race_mother, race_parent: @form_newstudent.race_parent, registrationform_id: @form_newstudent.registrationform_id, relation: @form_newstudent.relation, s_code: @form_newstudent.s_code, s_number: @form_newstudent.s_number, salary_father: @form_newstudent.salary_father, salary_mother: @form_newstudent.salary_mother, salary_parent: @form_newstudent.salary_parent, startclass: @form_newstudent.startclass, status: @form_newstudent.status, talent: @form_newstudent.talent, termstart: @form_newstudent.termstart, weight: @form_newstudent.weight, zipcode1: @form_newstudent.zipcode1, zipcode2: @form_newstudent.zipcode2, zipcode_father: @form_newstudent.zipcode_father, zipcode_mother: @form_newstudent.zipcode_mother, zipcode_parent: @form_newstudent.zipcode_parent}}
    end

    assert_redirected_to form_newstudent_url(FormNewstudent.last)
  end

  test "should show form_newstudent" do
    get form_newstudent_url(@form_newstudent)
    assert_response :success
  end

  test "should get edit" do
    get edit_form_newstudent_url(@form_newstudent)
    assert_response :success
  end

  test "should update form_newstudent" do
    patch form_newstudent_url(@form_newstudent), params: {form_newstudent: {address1: @form_newstudent.address1, address2: @form_newstudent.address2, address_father: @form_newstudent.address_father, address_mother: @form_newstudent.address_mother, address_parent: @form_newstudent.address_parent, birth: @form_newstudent.birth, birth_location: @form_newstudent.birth_location, blood: @form_newstudent.blood, cdisease: @form_newstudent.cdisease, chumclass: @form_newstudent.chumclass, city1: @form_newstudent.city1, city2: @form_newstudent.city2, city_father: @form_newstudent.city_father, city_mother: @form_newstudent.city_mother, city_parent: @form_newstudent.city_parent, datestart: @form_newstudent.datestart, email: @form_newstudent.email, email_father: @form_newstudent.email_father, email_mother: @form_newstudent.email_mother, email_parent: @form_newstudent.email_parent, finisholdschool: @form_newstudent.finisholdschool, firstname: @form_newstudent.firstname, firstname_father: @form_newstudent.firstname_father, firstname_mother: @form_newstudent.firstname_mother, firstname_parent: @form_newstudent.firstname_parent, grownoldschool: @form_newstudent.grownoldschool, height: @form_newstudent.height, image_data: @form_newstudent.image_data, job_father: @form_newstudent.job_father, job_location_father: @form_newstudent.job_location_father, job_location_mother: @form_newstudent.job_location_mother, job_location_parent: @form_newstudent.job_location_parent, job_mother: @form_newstudent.job_mother, job_parent: @form_newstudent.job_parent, job_phone_father: @form_newstudent.job_phone_father, job_phone_mother: @form_newstudent.job_phone_mother, job_phone_parent: @form_newstudent.job_phone_parent, lastname: @form_newstudent.lastname, lastname_father: @form_newstudent.lastname_father, lastname_mother: @form_newstudent.lastname_mother, lastname_parent: @form_newstudent.lastname_parent, mobile: @form_newstudent.mobile, mobile_father: @form_newstudent.mobile_father, mobile_mother: @form_newstudent.mobile_mother, mobile_parent: @form_newstudent.mobile_parent, nation: @form_newstudent.nation, nation_father: @form_newstudent.nation_father, nation_mother: @form_newstudent.nation_mother, nation_parent: @form_newstudent.nation_parent, nickname: @form_newstudent.nickname, number_elder: @form_newstudent.number_elder, number_you: @form_newstudent.number_you, number_younger: @form_newstudent.number_younger, oldschool: @form_newstudent.oldschool, orgin_father: @form_newstudent.orgin_father, orgin_mother: @form_newstudent.orgin_mother, orgin_parent: @form_newstudent.orgin_parent, origin: @form_newstudent.origin, percode: @form_newstudent.percode, percode_father: @form_newstudent.percode_father, percode_mother: @form_newstudent.percode_mother, percode_parent: @form_newstudent.percode_parent, phone: @form_newstudent.phone, phone_father: @form_newstudent.phone_father, phone_mother: @form_newstudent.phone_mother, phone_parent: @form_newstudent.phone_parent, prefix: @form_newstudent.prefix, prefix_father: @form_newstudent.prefix_father, prefix_mother: @form_newstudent.prefix_mother, prefix_parent: @form_newstudent.prefix_parent, province1: @form_newstudent.province1, province2: @form_newstudent.province2, province_father: @form_newstudent.province_father, province_mother: @form_newstudent.province_mother, province_parent: @form_newstudent.province_parent, race: @form_newstudent.race, race_father: @form_newstudent.race_father, race_mother: @form_newstudent.race_mother, race_parent: @form_newstudent.race_parent, registrationform_id: @form_newstudent.registrationform_id, relation: @form_newstudent.relation, s_code: @form_newstudent.s_code, s_number: @form_newstudent.s_number, salary_father: @form_newstudent.salary_father, salary_mother: @form_newstudent.salary_mother, salary_parent: @form_newstudent.salary_parent, startclass: @form_newstudent.startclass, status: @form_newstudent.status, talent: @form_newstudent.talent, termstart: @form_newstudent.termstart, weight: @form_newstudent.weight, zipcode1: @form_newstudent.zipcode1, zipcode2: @form_newstudent.zipcode2, zipcode_father: @form_newstudent.zipcode_father, zipcode_mother: @form_newstudent.zipcode_mother, zipcode_parent: @form_newstudent.zipcode_parent}}
    assert_redirected_to form_newstudent_url(@form_newstudent)
  end

  test "should destroy form_newstudent" do
    assert_difference("FormNewstudent.count", -1) do
      delete form_newstudent_url(@form_newstudent)
    end

    assert_redirected_to form_newstudents_url
  end
end
