require "rails_helper"

RSpec.describe "task_users/show", type: :view do
  before(:each) do
    @task_user = assign(:task_user, TaskUser.create!(
      task: nil,
      user: nil,
      primary: false,
      status: 2,
      biz: nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(/false/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(//)
  end
end
