# frozen_string_literal: true

require "rails/generators/erb"
require "rails/generators/resource_helpers"

module Erb # :nodoc:
  module Generators # :nodoc:
    class ScaffoldGenerator < Base # :nodoc:
      include Rails::Generators::ResourceHelpers

      argument :attributes, type: :array, default: [], banner: "field:type field:type"

      def create_root_folder
        empty_directory File.join("app/views", controller_file_path)
      end

      def copy_view_files
        available_views.each do |view|
          # formats.each do |format|
          # filename = filename_with_extensions(view, format)
          # template filename, File.join("app/views", controller_file_path, filename)
          # end
          template view, File.join("app/views", controller_file_path, view)
        end

        template "class_name.js", File.join("app/assets/javascripts/#{controller_file_path}.js")
      end

      private

      def available_views
        %w[index.html.erb index.csv.erb index.pdf.erb edit.html.erb show.html.erb new.html.erb _form.html.erb]
      end
    end
  end
end
