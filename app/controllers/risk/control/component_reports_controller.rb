class Risk::Control::ComponentReportsController < ApplicationController
  before_action :set_default, only: %i[index show new edit create update destroy print]
  before_action :set_select, only: %i[new create edit update]

  def index
    url = URI("#{$server[@controller_title]}/api/v1/component_reports?order=updated_at&sort=desc&limit=10")
    @component_reports = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/component_reports/#{params["id"]}")
    @component_report = request_api(url, "get", nil)
  end

  def new
  end

  def create


    url = URI("#{$server[@controller_title]}/api/v1/component_reports")
    @datas = request_api(url, "post", component_report_params)

    if @datas["status"] == "Success"

      redirect_to "/#{@controller_main_url}/component_reports/#{@datas["data"]["id"]}/edit" , notice: @datas["messages"]
      # "/#{@controller_main_url}/component_reports/new?component_report_id=#{params[:component_report][:component_report_id]}", alert: @datas["data"]
    else
      redirect_to "/#{@controller_main_url}/component_reports/new?component_report_id=#{params[:component_report][:component_report_id]}", alert: @datas["data"]
    end
  end

 def update
 
  url = URI("#{$server[@controller_title]}/api/v1/component_reports/#{params["id"]}")
  @component_report = request_api(url, "get", nil)

  @datas = request_api(url, "patch", component_report_params)


  if @datas && @datas["status"] == "Success"
    #redirect_to "/#{@controller_main_url}/master_plans/#{@component_report["data"]["master_plan_id"]}", notice: @datas["messages"]
    redirect_to "/#{@controller_main_url}/component_reports/#{params[:id]}/edit" #, alert: @datas["data"]
  else
    redirect_to "/#{@controller_main_url}/component_reports/#{params[:id]}/edit" #, alert: @datas["data"]
  end
 end


  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/component_reports/#{params["id"]}")
    @datas_ori = request_api(url, "get", nil)
    @datas = request_api(url, "delete", nil)

    url = URI("#{$server[@controller_title]}/api/v1/categories/#{@datas_ori["data"]["category_id"]}")
    @category = request_api(url, "get", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/plans/#{@category["data"]["plan_id"]}", notice: @datas["message"]
    else
      redirect_to "/#{@controller_main_url}/plans/#{@category["data"]["plan_id"]}", alert: @datas["data"]
    end
  end

  private

  def set_select

    url = URI("#{$server[@controller_title]}/api/v1/component_reports/#{params["id"]}")
    @component_report = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/users")
    @user_datas = request_api(url, "get", nil)

  end

  def set_default
    @sub_system = "control"
    @controller_main_url = "#{@controller_title}/#{@sub_system}"
    # @controller_main_url = @controller_title
  end

  def component_report_params
    # params.require(:component_report).permit(:title1, :title2, :title3, :title4, :title5, :title6, :title7, :title8, :title9, :title10, :title11, :title12, :approvedate, :master_plan, :user, :biz)
    params.require(:component_report).permit(:title1, :title2, :title3, :title4, :title5, :title6, :title7, :approvedate, :master_plan_id, :user_id, :biz_id,
    component_lists_attributes: [:id, :_destroy, :component_report_id, :component_id, :title, :order_sort, :slug, :user_id, :biz_id,
        {
          component_tasks_attributes: [:id, :_destroy, :component_list_id, :title, :order_sort, :slug, :user_id, :biz_id]
        }
        ]
      )
  end
end
