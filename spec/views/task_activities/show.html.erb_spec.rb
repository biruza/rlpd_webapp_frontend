require "rails_helper"

RSpec.describe "task_activities/show", type: :view do
  before(:each) do
    @task_activity = assign(:task_activity, TaskActivity.create!(
      task: nil,
      description: "MyText",
      activity_type: 2,
      user: nil,
      biz: nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
  end
end
