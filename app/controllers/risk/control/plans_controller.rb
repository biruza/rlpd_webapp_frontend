class Risk::Control::PlansController < ApplicationController
  before_action :set_default, only: %i[index show new edit create update destroy print tasks]

  before_action :set_select, only: %i[new create edit update]

  def index
    url = URI("#{$server[@controller_title]}/api/v1/plans")
    @datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/plans/#{params["id"]}")
    @plan = request_api(url, "get", nil)
  end

  def new
    if params[:master_plan_id].blank?
      redirect_to "/#{@controller_main_url}/master_plans", notice: nil
    else

      plan_params_v2 = {
        master_plan_id: params[:master_plan_id],
        user_id: $user[session.id.to_s][@controller_title]["id"],
        biz_id: $user[session.id.to_s][@controller_title]["biz_id"]
      }
      url = URI("#{$server[@controller_title]}/api/v1/plans")
      @datas = request_api(url, "post", plan_params_v2)

      if @datas["status"] == "Success"
        redirect_to "/#{@controller_main_url}/categories/new?plan_id=#{@datas["data"]["id"]}", notice: @datas["messages"]
      else
        redirect_to "/#{@controller_main_url}/master_plans/#{params[:master_plan_id]}", alert: @datas["data"]
      end
    end
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/plans/#{params["id"]}")
    @plan = request_api(url, "get", nil)
  end

  def create
  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/plans/#{params["id"]}")
    @datas = request_api(url, "patch", plan_params)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/master_plans/#{@datas["data"]["master_plan_id"]}", notice: @datas["message"]
    else
      redirect_to "/#{@controller_main_url}/plans/#{params["id"]}/edit", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/plans/#{params["id"]}")
    @datas_ori = request_api(url, "get", nil)
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/master_plans/#{@datas_ori["data"]["master_plan_id"]}", notice: @datas["message"]
    else
      redirect_to "/#{@controller_main_url}/master_plans/#{@datas_ori["data"]["master_plan_id"]}", alert: t(:can_not_delete)
    end
  end

  def print
    url = URI("#{$server[@controller_title]}/api/v1/plans/#{params["id"]}")
    @plan = request_api(url, "get", nil)
  end

  def history
    url = URI("#{$server[@controller_title]}/api/v1/plans/#{params["id"]}")
    @plan = request_api(url, "get", nil)
  end

  def tasks
    url = URI("#{$server[@controller_title]}/api/v1/plans/#{params["plan_id"]}")
    @plan = request_api(url, "get", nil)
    @plan = @plan["data"] if !@plan.blank?

    url = URI("#{$server[@controller_title]}/api/v1/plan_reports/#{params["plan_report_id"]}")
    @plan_report = request_api(url, "get", nil)
  end

  def tasks_print
  end

  def form_1
    url = URI("#{$server[@controller_title]}/api/v1/risk_types")
    @data_risk_types = request_api(url, "get", nil)
  end

  def form_2
  end

  def form_3
  end

  def form_4
  end

  def form_5
  end

  def form_6
  end

  def status
    @list_id = params[:list_id]
    @list_activity_id = params[:list_activity_id]
    @plan_report_id = params[:plan_report_id]
    @original_url = params[:original_url]

    url = URI("#{$server[@controller_title]}/api/v1/lists/#{params["list_id"]}") if !params[:list_id].blank?
    @list = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/list_activities/#{params["list_activity_id"]}") if !params[:list_activity_id].blank?
    @list_activity = request_api(url, "get", nil)

    respond_to do |format|
      format.html
      format.js
    end
  end

  private

  def set_default
    @sub_system = "control"
    @controller_main_url = "#{@controller_title}/#{@sub_system}"
    # @controller_main_url = @controller_title
    
  end

  def set_select
    url = URI("#{$server[@controller_title]}/api/v1/master_plans")
    @datas_master_plan = request_api(url, "get", nil)
  end

  def plan_params
    params.require(:plan).permit(:order_sort, :title, :master_plan_id, :user_id, :biz_id, :description)
  end
end
