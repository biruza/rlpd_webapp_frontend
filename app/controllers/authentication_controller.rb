class AuthenticationController < ApplicationController
  def authenticate_user
    user = User.find_for_database_authentication(email: params[:email])
    # user_password = params[:password]

    if user&.valid_password?(params[:password])
      # if user.valid_password?(params[:password])
      # if user and user.valid_password? user_password
      render json: payload(user)
    else
      render json: {errors: ["Invalid Username/Password"]}, status: :unauthorized
    end
  end

  private

  def payload(user)
    return nil unless user&.id
    {
      auth_token: JsonWebToken.encode({user_id: user.id}),
      user: {id: user.id, email: user.email, biz_id: user.biz_id}
    }
  end
end
