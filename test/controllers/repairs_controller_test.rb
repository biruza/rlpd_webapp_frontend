require "test_helper"

class RepairsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @repair = repairs(:one)
  end

  test "should get index" do
    get repairs_url
    assert_response :success
  end

  test "should get new" do
    get new_repair_url
    assert_response :success
  end

  test "should create repair" do
    assert_difference("Repair.count") do
      post repairs_url, params: {repair: {bad_symptom_id: @repair.bad_symptom_id, bad_symptom_other: @repair.bad_symptom_other, device_id: @repair.device_id, doc_number: @repair.doc_number, image_data: @repair.image_data, location_id: @repair.location_id, machine_id: @repair.machine_id, priority_id: @repair.priority_id, user_id: @repair.user_id}}
    end

    assert_redirected_to repair_url(Repair.last)
  end

  test "should show repair" do
    get repair_url(@repair)
    assert_response :success
  end

  test "should get edit" do
    get edit_repair_url(@repair)
    assert_response :success
  end

  test "should update repair" do
    patch repair_url(@repair), params: {repair: {bad_symptom_id: @repair.bad_symptom_id, bad_symptom_other: @repair.bad_symptom_other, device_id: @repair.device_id, doc_number: @repair.doc_number, image_data: @repair.image_data, location_id: @repair.location_id, machine_id: @repair.machine_id, priority_id: @repair.priority_id, user_id: @repair.user_id}}
    assert_redirected_to repair_url(@repair)
  end

  test "should destroy repair" do
    assert_difference("Repair.count", -1) do
      delete repair_url(@repair)
    end

    assert_redirected_to repairs_url
  end
end
