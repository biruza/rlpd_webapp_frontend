require "test_helper"

class ProofPaymentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @proof_payment = proof_payments(:one)
  end

  test "should get index" do
    get proof_payments_url
    assert_response :success
  end

  test "should get new" do
    get new_proof_payment_url
    assert_response :success
  end

  test "should create proof_payment" do
    assert_difference("ProofPayment.count") do
      post proof_payments_url, params: {proof_payment: {amount: @proof_payment.amount, c_email: @proof_payment.c_email, c_firstname: @proof_payment.c_firstname, c_lastname: @proof_payment.c_lastname, c_phone: @proof_payment.c_phone, company_id: @proof_payment.company_id, image_data: @proof_payment.image_data, order_number: @proof_payment.order_number, product_id: @proof_payment.product_id, status: @proof_payment.status}}
    end

    assert_redirected_to proof_payment_url(ProofPayment.last)
  end

  test "should show proof_payment" do
    get proof_payment_url(@proof_payment)
    assert_response :success
  end

  test "should get edit" do
    get edit_proof_payment_url(@proof_payment)
    assert_response :success
  end

  test "should update proof_payment" do
    patch proof_payment_url(@proof_payment), params: {proof_payment: {amount: @proof_payment.amount, c_email: @proof_payment.c_email, c_firstname: @proof_payment.c_firstname, c_lastname: @proof_payment.c_lastname, c_phone: @proof_payment.c_phone, company_id: @proof_payment.company_id, image_data: @proof_payment.image_data, order_number: @proof_payment.order_number, product_id: @proof_payment.product_id, status: @proof_payment.status}}
    assert_redirected_to proof_payment_url(@proof_payment)
  end

  test "should destroy proof_payment" do
    assert_difference("ProofPayment.count", -1) do
      delete proof_payment_url(@proof_payment)
    end

    assert_redirected_to proof_payments_url
  end
end
