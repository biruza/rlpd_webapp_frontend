require "test_helper"

class RegistrationAnnouncementsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @registration_announcement = registration_announcements(:one)
  end

  test "should get index" do
    get registration_announcements_url
    assert_response :success
  end

  test "should get new" do
    get new_registration_announcement_url
    assert_response :success
  end

  test "should create registration_announcement" do
    assert_difference("RegistrationAnnouncement.count") do
      post registration_announcements_url, params: {registration_announcement: {biz_id: @registration_announcement.biz_id, description_en: @registration_announcement.description_en, description_th: @registration_announcement.description_th, end_date: @registration_announcement.end_date, registration_id: @registration_announcement.registration_id, slug: @registration_announcement.slug, slug: @registration_announcement.slug, start_date: @registration_announcement.start_date, title_en: @registration_announcement.title_en, title_th: @registration_announcement.title_th, user_id: @registration_announcement.user_id}}
    end

    assert_redirected_to registration_announcement_url(RegistrationAnnouncement.last)
  end

  test "should show registration_announcement" do
    get registration_announcement_url(@registration_announcement)
    assert_response :success
  end

  test "should get edit" do
    get edit_registration_announcement_url(@registration_announcement)
    assert_response :success
  end

  test "should update registration_announcement" do
    patch registration_announcement_url(@registration_announcement), params: {registration_announcement: {biz_id: @registration_announcement.biz_id, description_en: @registration_announcement.description_en, description_th: @registration_announcement.description_th, end_date: @registration_announcement.end_date, registration_id: @registration_announcement.registration_id, slug: @registration_announcement.slug, slug: @registration_announcement.slug, start_date: @registration_announcement.start_date, title_en: @registration_announcement.title_en, title_th: @registration_announcement.title_th, user_id: @registration_announcement.user_id}}
    assert_redirected_to registration_announcement_url(@registration_announcement)
  end

  test "should destroy registration_announcement" do
    assert_difference("RegistrationAnnouncement.count", -1) do
      delete registration_announcement_url(@registration_announcement)
    end

    assert_redirected_to registration_announcements_url
  end
end
