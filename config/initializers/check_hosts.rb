class CheckHosts
  def self.matches? request
    case request.host
    when "nextbiz.io", "www.nextbiz.io", "localhost"
      true
    else
      false
    end
  end
end
