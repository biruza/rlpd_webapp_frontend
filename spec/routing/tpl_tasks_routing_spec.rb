require "rails_helper"

RSpec.describe TplTasksController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/tpl_tasks").to route_to("tpl_tasks#index")
    end

    it "routes to #new" do
      expect(get: "/tpl_tasks/new").to route_to("tpl_tasks#new")
    end

    it "routes to #show" do
      expect(get: "/tpl_tasks/1").to route_to("tpl_tasks#show", id: "1")
    end

    it "routes to #edit" do
      expect(get: "/tpl_tasks/1/edit").to route_to("tpl_tasks#edit", id: "1")
    end

    it "routes to #create" do
      expect(post: "/tpl_tasks").to route_to("tpl_tasks#create")
    end

    it "routes to #update via PUT" do
      expect(put: "/tpl_tasks/1").to route_to("tpl_tasks#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/tpl_tasks/1").to route_to("tpl_tasks#update", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/tpl_tasks/1").to route_to("tpl_tasks#destroy", id: "1")
    end
  end
end
