require "rails_helper"

RSpec.describe "manufacturers/edit", type: :view do
  before(:each) do
    @manufacturer = assign(:manufacturer, Manufacturer.create!(
      name: "MyString",
      image_data: "MyText",
      address: "MyText",
      phone: "MyString",
      fax: "MyString",
      email: "MyString",
      slug: "MyString",
      personal_firstname: "MyString",
      personal_lastname: "MyString",
      personal_phone: "MyString",
      personal_email: "MyString",
      user: nil,
      biz: nil
    ))
  end

  it "renders the edit manufacturer form" do
    render

    assert_select "form[action=?][method=?]", manufacturer_path(@manufacturer), "post" do
      assert_select "input[name=?]", "manufacturer[name]"

      assert_select "textarea[name=?]", "manufacturer[image_data]"

      assert_select "textarea[name=?]", "manufacturer[address]"

      assert_select "input[name=?]", "manufacturer[phone]"

      assert_select "input[name=?]", "manufacturer[fax]"

      assert_select "input[name=?]", "manufacturer[email]"

      assert_select "input[name=?]", "manufacturer[slug]"

      assert_select "input[name=?]", "manufacturer[personal_firstname]"

      assert_select "input[name=?]", "manufacturer[personal_lastname]"

      assert_select "input[name=?]", "manufacturer[personal_phone]"

      assert_select "input[name=?]", "manufacturer[personal_email]"

      assert_select "input[name=?]", "manufacturer[user_id]"

      assert_select "input[name=?]", "manufacturer[biz_id]"
    end
  end
end
