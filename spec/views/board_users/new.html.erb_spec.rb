require "rails_helper"

RSpec.describe "board_users/new", type: :view do
  before(:each) do
    assign(:board_user, BoardUser.new(
      board: nil,
      user: nil,
      order_sort: 1,
      biz: nil
    ))
  end

  it "renders new board_user form" do
    render

    assert_select "form[action=?][method=?]", board_users_path, "post" do
      assert_select "input[name=?]", "board_user[board_id]"

      assert_select "input[name=?]", "board_user[user_id]"

      assert_select "input[name=?]", "board_user[order_sort]"

      assert_select "input[name=?]", "board_user[biz_id]"
    end
  end
end
