class ApplicationMailer < ActionMailer::Base
  default from: "noreply@nextbiz.io"
  layout "mailer"

  helper_method :get_amount, :get_amount_total, :get_vat, :get_withholding_tax_amount, :get_sum_amount

  def get_amount(b)
    if b.instance_of?(Receipt)
      b.receipt_items.sum(:amount)
    elsif b.instance_of?(InputTax)
      b.input_tax_items.sum(:amount)
    elsif b.instance_of?(Quotation)
      b.quotation_items.sum(:amount)
    elsif b.instance_of?(Invoice)
      b.invoice_items.sum(:amount)
    elsif b.instance_of?(Expense)
      b.expense_items.sum(:amount)
    else
      0
    end
  end

  def get_amount_total(b)
    get_amount(b) - b.discount_amount.to_f
  end

  def get_vat(b)
    b.vat = if b.vat_status == true
      get_amount_total(b) * 7 / 100
    else
      0
    end
    b.vat
  end

  def get_withholding_tax_amount(b)
    b.withholding_tax_amount = if !b.withholding_tax_per.blank?
      get_amount_total(b) * b.withholding_tax_per / 100
    else
      0
    end
    b.withholding_tax_amount
  end

  def get_sum_amount(b)
    b.vat = get_vat(b)
    b.withholding_tax_amount = get_withholding_tax_amount(b)
    get_amount_total(b).to_f + b.vat.to_f - b.withholding_tax_amount.to_f - b.fee.to_f
  end
end
