require "rails_helper"

RSpec.describe TaskAttachesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/task_attaches").to route_to("task_attaches#index")
    end

    it "routes to #new" do
      expect(get: "/task_attaches/new").to route_to("task_attaches#new")
    end

    it "routes to #show" do
      expect(get: "/task_attaches/1").to route_to("task_attaches#show", id: "1")
    end

    it "routes to #edit" do
      expect(get: "/task_attaches/1/edit").to route_to("task_attaches#edit", id: "1")
    end

    it "routes to #create" do
      expect(post: "/task_attaches").to route_to("task_attaches#create")
    end

    it "routes to #update via PUT" do
      expect(put: "/task_attaches/1").to route_to("task_attaches#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/task_attaches/1").to route_to("task_attaches#update", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/task_attaches/1").to route_to("task_attaches#destroy", id: "1")
    end
  end
end
