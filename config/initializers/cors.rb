# for cors rails 6
Rails.application.configure do
  config.hosts.clear # allow all domain
  # config.hosts << ".beesys.me"
end
####

Rails.application.config do |config|
  config.middleware.insert_before 0, Rack::Cors do
    allow do
      origins "*"
      resource "*", headers: :any, methods: [:get, :post, :patch, :put]
    end
  end
end
