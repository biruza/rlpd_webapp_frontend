class Risk::Control::MasterPlansController < ApplicationController
  before_action :set_default, only: %i[index show new edit create update destroy print]

  def index
    url = URI("#{$server[@controller_title]}/api/v1/master_plans?order=period_year&sort=desc&type_name=control") 
    @datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params["id"]}?type_name=fl")
    @datas = request_api(url, "get", nil)

    url_master_plan_user = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params["id"]}/master_plan_users?order=order_sort&sort=asc")
    @master_plan_users = request_api(url_master_plan_user, "get", nil)


    url_internal_control_certificate = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params["id"]}/internal_control_certificates?order=order_sort&sort=asc")
    @internal_control_certificates = request_api(url_internal_control_certificate, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params["id"]}/master_plan_user_comments")
    @user_commments = request_api(url, "get", nil)
  end

  def new
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params["id"]}")

    @datas = request_api(url, "get", nil)

    # puts "plan edit = #{@plan['title']}"
  end

  def create
    master_plan_params = {
    master_plan: {
      type_name: "control",
      period_year: Date.new(params[:master_plan][:period_year].to_i - 543, 1, 1),
      startdate: Date.new(params[:master_plan][:period_year].to_i - 543 - 1, 10, 1),
      enddate: Date.new(params[:master_plan][:period_year].to_i - 543, 9, 30)
      }
    }
    
    url = URI("#{$server[@controller_title]}/api/v1/master_plans")
    @datas = request_api(url, "post", master_plan_params)

    if @datas["status"] == "Success"

      url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{@datas["data"]["id"]}")
      @datas = request_api(url, "get", nil)

      redirect_to "/#{@controller_main_url}/master_plans", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_main_url}/master_plans/new", alert: @datas["data"]
    end
  end

  def update
    master_plan_params = {
    master_plan: {
      period_year: Date.new(params[:master_plan][:period_year].to_i - 543, 1, 1),
      startdate: Date.new(params[:master_plan][:period_year].to_i - 543 - 1, 10, 1),
      enddate: Date.new(params[:master_plan][:period_year].to_i - 543, 9, 30)
      }
    }

    url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params["id"]}")
    @datas = request_api(url, "patch", master_plan_params)

    # puts "update status : #{res}"

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/master_plans/#{params[:id]}", notice: @datas["message"]
    else
      redirect_to "/#{@controller_main_url}/master_plans/#{params[:id]}/edit", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params["id"]}")
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/master_plans", notice: @datas["message"]
    else
      redirect_to "/#{@controller_main_url}/master_plans", alert: @datas["data"]
    end
  end

  def print
    url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params["id"]}")
    @datas = request_api(url, "get", nil)
  end

  private

  def set_default
    @sub_system = "control"
    @controller_main_url = "#{@controller_title}/#{@sub_system}"
  end

  def master_plan_params
    params.require(:master_plan).permit(:title, :description, :user_id, :biz_id, :type_name, :startdate, :enddate, :period_year)
  end
end
