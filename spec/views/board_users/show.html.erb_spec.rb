require "rails_helper"

RSpec.describe "board_users/show", type: :view do
  before(:each) do
    @board_user = assign(:board_user, BoardUser.create!(
      board: nil,
      user: nil,
      order_sort: 2,
      biz: nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(/2/)
    expect(rendered).to match(//)
  end
end
