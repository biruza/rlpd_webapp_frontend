require "test_helper"

class CareersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @career = careers(:one)
  end

  test "should get index" do
    get careers_url
    assert_response :success
  end

  test "should get new" do
    get new_career_url
    assert_response :success
  end

  test "should create career" do
    assert_difference("Career.count") do
      post careers_url, params: {career: {biz_id: @career.biz_id, description_en: @career.description_en, description_th: @career.description_th, image_data: @career.image_data, order_number: @career.order_number, publish_end_date: @career.publish_end_date, publish_start_date: @career.publish_start_date, slug: @career.slug, title_en: @career.title_en, title_th: @career.title_th, user: @career.user}}
    end

    assert_redirected_to career_url(Career.last)
  end

  test "should show career" do
    get career_url(@career)
    assert_response :success
  end

  test "should get edit" do
    get edit_career_url(@career)
    assert_response :success
  end

  test "should update career" do
    patch career_url(@career), params: {career: {biz_id: @career.biz_id, description_en: @career.description_en, description_th: @career.description_th, image_data: @career.image_data, order_number: @career.order_number, publish_end_date: @career.publish_end_date, publish_start_date: @career.publish_start_date, slug: @career.slug, title_en: @career.title_en, title_th: @career.title_th, user: @career.user}}
    assert_redirected_to career_url(@career)
  end

  test "should destroy career" do
    assert_difference("Career.count", -1) do
      delete career_url(@career)
    end

    assert_redirected_to careers_url
  end
end
