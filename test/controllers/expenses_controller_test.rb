require "test_helper"

class ExpensesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @expense = expenses(:one)
  end

  test "should get index" do
    get expenses_url
    assert_response :success
  end

  test "should get new" do
    get new_expense_url
    assert_response :success
  end

  test "should create expense" do
    assert_difference("Expense.count") do
      post expenses_url, params: {expense: {amount: @expense.amount, biz_id: @expense.biz_id, company_id: @expense.company_id, description: @expense.description, discount_amount: @expense.discount_amount, doc_number: @expense.doc_number, doc_ref: @expense.doc_ref, doc_ref_date: @expense.doc_ref_date, doc_sign: @expense.doc_sign, image_data: @expense.image_data, payee_type: @expense.payee_type, payment_bank: @expense.payment_bank, payment_number: @expense.payment_number, payment_type: @expense.payment_type, project_id: @expense.project_id, revenue_type_id: @expense.revenue_type_id, status: @expense.status, vat: @expense.vat, vat_inout: @expense.vat_inout, vat_status: @expense.vat_status, withholding_tax_amount: @expense.withholding_tax_amount, withholding_tax_per: @expense.withholding_tax_per, withholding_tax_status: @expense.withholding_tax_status}}
    end

    assert_redirected_to expense_url(Expense.last)
  end

  test "should show expense" do
    get expense_url(@expense)
    assert_response :success
  end

  test "should get edit" do
    get edit_expense_url(@expense)
    assert_response :success
  end

  test "should update expense" do
    patch expense_url(@expense), params: {expense: {amount: @expense.amount, biz_id: @expense.biz_id, company_id: @expense.company_id, description: @expense.description, discount_amount: @expense.discount_amount, doc_number: @expense.doc_number, doc_ref: @expense.doc_ref, doc_ref_date: @expense.doc_ref_date, doc_sign: @expense.doc_sign, image_data: @expense.image_data, payee_type: @expense.payee_type, payment_bank: @expense.payment_bank, payment_number: @expense.payment_number, payment_type: @expense.payment_type, project_id: @expense.project_id, revenue_type_id: @expense.revenue_type_id, status: @expense.status, vat: @expense.vat, vat_inout: @expense.vat_inout, vat_status: @expense.vat_status, withholding_tax_amount: @expense.withholding_tax_amount, withholding_tax_per: @expense.withholding_tax_per, withholding_tax_status: @expense.withholding_tax_status}}
    assert_redirected_to expense_url(@expense)
  end

  test "should destroy expense" do
    assert_difference("Expense.count", -1) do
      delete expense_url(@expense)
    end

    assert_redirected_to expenses_url
  end
end
