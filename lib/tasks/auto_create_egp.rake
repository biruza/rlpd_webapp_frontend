namespace :auto_create_egp do
  desc "genarate egp record"
  task generate_egp: [:environment] do
    puts "Update inner 1 : #{Time.now}"

    (1..1000).each do |dep_id|
      # (1..300).each do |dep_id|
      # dep_id = '0307'
      dep_id = sprintf("%04d", dep_id)
      # anounce_type = 'W0'
      anounce_type = "D0"

      url = "http://process3.gprocurement.go.th/EPROCRssFeedWeb/egpannouncerss.xml?deptId=#{dep_id}&anounceType=#{anounce_type}"

      items = Hash.from_xml(Nokogiri::XML(open(url)).to_s)
      # items = Hash.from_xml(Nokogiri::XML(open(url),nil, 'Windows-874').to_s)
      # items = Hash.from_xml(Nokogiri::XML(open(url),nil, 'UTF-8'))

      if items && items["rss"] && items["rss"]["channel"] && !items["rss"]["channel"]["item"].blank?

        count = 0

        items["rss"]["channel"]["item"].each_with_index do |item, idx|
          egps_new = Egp.new
          if item[0].blank? && !Egp.find_by(link: item["link"].to_s)
            egps_new.dep_id = dep_id
            egps_new.category = anounce_type
            egps_new.title = item["title"]
            egps_new.link = item["link"]
            egps_new.description = item["description"]
            egps_new.pub_date = item["pubDate"].to_date
            egps_new.full_text = get_full_text(item["link"])
            if egps_new.save
              count += 1
              egps_new.update(submit_date: egps_new.get_submit_date(item["link"])) if !item["link"].blank?
            end
          end
        end

      end

      puts "#{dep_id} | #{count} record"

      # sleep rand(0..3)
    end # loop dep_id
  end
end

def get_full_text(link_doc)
  full_text = nil
  Nokogiri::HTML(open(link_doc), nil, "Windows-874")
end
