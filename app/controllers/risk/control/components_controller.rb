class Risk::Control::ComponentsController < ApplicationController
  before_action :set_default, only: %i[index show new edit create update destroy print]
  before_action :set_select, only: %i[new create edit update]

  def index
    url = URI("#{$server[@controller_title]}/api/v1/components?order=updated_at&sort=desc&limit=10")
    @components = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/components/#{params["id"]}")
    @component = request_api(url, "get", nil)
  end

  def new
  end

  def create


    url = URI("#{$server[@controller_title]}/api/v1/components")
    @datas = request_api(url, "post", component_params)

    if @datas["status"] == "Success"

      redirect_to "/#{@controller_main_url}/components/#{@datas["data"]["id"]}/edit", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_main_url}/components/new?evaluate_control_report_id=#{params[:component][:evaluate_control_report_id]}", alert: @datas["data"]
    end
  end
  def edit
  end

 def update
 
  url = URI("#{$server[@controller_title]}/api/v1/components/#{params["id"]}")
  @component = request_api(url, "get", nil)

  @datas = request_api(url, "patch", component_params)

  if @datas && @datas["status"] == "Success"
    redirect_to "/#{@controller_main_url}/plans/#{@category["data"]["plan_id"]}", notice: @datas["messages"]

  else
    redirect_to "/#{@controller_main_url}/lists/#{params[:id]}/edit", alert: @datas["data"]
  end
 end


  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/components/#{params["id"]}")
    @datas_ori = request_api(url, "get", nil)
    @datas = request_api(url, "delete", nil)

    url = URI("#{$server[@controller_title]}/api/v1/categories/#{@datas_ori["data"]["category_id"]}")
    @category = request_api(url, "get", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/plans/#{@category["data"]["plan_id"]}", notice: @datas["message"]
    else
      redirect_to "/#{@controller_main_url}/plans/#{@category["data"]["plan_id"]}", alert: @datas["data"]
    end
  end

  private

  def set_select

  end

  def set_default
    @sub_system = "control"
    @controller_main_url = "#{@controller_title}/#{@sub_system}"
    # @controller_main_url = @controller_title
  end

  def component_params
    # params.require(:component).permit(:title1, :title2, :title3, :title4, :title5, :title6, :title7, :title8, :title9, :title10, :title11, :title12, :approvedate, :master_plan, :user, :biz)
    params.require(:component).permit(:title1, :title2, :title3, :title4, :title5, :title6, :title7, :title8, :title9, :title10, :title11, :title12, :approvedate, :category_id, :evaluate_control_report_id, :user_id, :biz_id)
  end
end
