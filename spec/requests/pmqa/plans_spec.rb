require "rails_helper"

RSpec.describe "Pmqa::Plans", type: :request do
  describe "GET /index" do
    it "returns http success" do
      get "/pmqa/plans/index"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /show" do
    it "returns http success" do
      get "/pmqa/plans/show"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /edit" do
    it "returns http success" do
      get "/pmqa/plans/edit"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /new" do
    it "returns http success" do
      get "/pmqa/plans/new"
      expect(response).to have_http_status(:success)
    end
  end
end
