require "rails_helper"

RSpec.describe "egp_configs/edit", type: :view do
  before(:each) do
    @egp_config = assign(:egp_config, EgpConfig.create!(
      tags: "MyString",
      categories: "MyString",
      biz: nil
    ))
  end

  it "renders the edit egp_config form" do
    render

    assert_select "form[action=?][method=?]", egp_config_path(@egp_config), "post" do
      assert_select "input[name=?]", "egp_config[tags]"

      assert_select "input[name=?]", "egp_config[categories]"

      assert_select "input[name=?]", "egp_config[biz_id]"
    end
  end
end
