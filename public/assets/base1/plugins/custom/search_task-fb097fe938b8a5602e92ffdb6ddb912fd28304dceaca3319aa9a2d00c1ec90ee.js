// Search task
function TemplateFormatResult (item) {
  if((item.list) && (item.list.board) && (item.list.board.title)) {
    return $('<div class="card p-2"><div class="card-body p-2"><div class="card-title mb-3">' + item.title + '</div><p class="card-text"><span class="badge badge-info">' + item.list.board.title + '</span></p></div></div>');
  }else{
    return $('<div class="card p-2"><div class="card-body p-2"><div class="card-title mb-3">' + item.title + '</div></div></div>');
  }
}

function TemplateFormatSelection(item) {
  if(!item.id){
    return ('Search Task');
  }
  return (item.title);
}

$('#search-task').select2({
    ajax: {
        url: "/api/v1/tasks_search.json",  
        type: 'get',
        dataType: 'json',
        delay: 250,
        data: function(params) {
            return {
                q: params.term,
            };
        },
        processResults: function(data) {
            return {results: data};
        }
    },
    placeholder: 'Search Task',
    minimumInputLength: 3,
    templateResult: TemplateFormatResult,
    templateSelection: TemplateFormatSelection, 
});


$('#search-task').on('select2:select', function (e) {
  var data = e.params.data;
  //alert(data.id,data.list.id,data.list.board.id);
  window.location.href = '/api/v1/boards/' + data.list.board.id + '/lists/' + data.list.id + '/tasks/' + data.id;
});
////
;
