require "test_helper"

class SummarySolutionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @summary_solution = summary_solutions(:one)
  end

  test "should get index" do
    get summary_solutions_url
    assert_response :success
  end

  test "should get new" do
    get new_summary_solution_url
    assert_response :success
  end

  test "should create summary_solution" do
    assert_difference("SummarySolution.count") do
      post summary_solutions_url, params: {summary_solution: {description: @summary_solution.description}}
    end

    assert_redirected_to summary_solution_url(SummarySolution.last)
  end

  test "should show summary_solution" do
    get summary_solution_url(@summary_solution)
    assert_response :success
  end

  test "should get edit" do
    get edit_summary_solution_url(@summary_solution)
    assert_response :success
  end

  test "should update summary_solution" do
    patch summary_solution_url(@summary_solution), params: {summary_solution: {description: @summary_solution.description}}
    assert_redirected_to summary_solution_url(@summary_solution)
  end

  test "should destroy summary_solution" do
    assert_difference("SummarySolution.count", -1) do
      delete summary_solution_url(@summary_solution)
    end

    assert_redirected_to summary_solutions_url
  end
end
