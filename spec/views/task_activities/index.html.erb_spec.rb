require "rails_helper"

RSpec.describe "task_activities/index", type: :view do
  before(:each) do
    assign(:task_activities, [
      TaskActivity.create!(
        task: nil,
        description: "MyText",
        activity_type: 2,
        user: nil,
        biz: nil
      ),
      TaskActivity.create!(
        task: nil,
        description: "MyText",
        activity_type: 2,
        user: nil,
        biz: nil
      )
    ])
  end

  it "renders a list of task_activities" do
    render
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: "MyText".to_s, count: 2
    assert_select "tr>td", text: 2.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
  end
end
