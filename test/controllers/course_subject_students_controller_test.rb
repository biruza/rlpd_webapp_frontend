require "test_helper"

class CourseSubjectStudentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @course_subject_student = course_subject_students(:one)
  end

  test "should get index" do
    get course_subject_students_url
    assert_response :success
  end

  test "should get new" do
    get new_course_subject_student_url
    assert_response :success
  end

  test "should create course_subject_student" do
    assert_difference("CourseSubjectStudent.count") do
      post course_subject_students_url, params: {course_subject_student: {course_subject_id: @course_subject_student.course_subject_id, klass_id: @course_subject_student.klass_id, status: @course_subject_student.status, student_id: @course_subject_student.student_id}}
    end

    assert_redirected_to course_subject_student_url(CourseSubjectStudent.last)
  end

  test "should show course_subject_student" do
    get course_subject_student_url(@course_subject_student)
    assert_response :success
  end

  test "should get edit" do
    get edit_course_subject_student_url(@course_subject_student)
    assert_response :success
  end

  test "should update course_subject_student" do
    patch course_subject_student_url(@course_subject_student), params: {course_subject_student: {course_subject_id: @course_subject_student.course_subject_id, klass_id: @course_subject_student.klass_id, status: @course_subject_student.status, student_id: @course_subject_student.student_id}}
    assert_redirected_to course_subject_student_url(@course_subject_student)
  end

  test "should destroy course_subject_student" do
    assert_difference("CourseSubjectStudent.count", -1) do
      delete course_subject_student_url(@course_subject_student)
    end

    assert_redirected_to course_subject_students_url
  end
end
