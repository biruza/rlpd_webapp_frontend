// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//

//--- require jquery
//--- require jquery_ujs
//--- require_tree .

//= require base1/plugins/global/plugins.bundle.js
//= require base1/plugins/custom/prismjs/prismjs.bundle.js
//= require base1/js/scripts.bundle.js

// Kanban //
//= require base1/plugins/custom/kanban/kanban.bundle.js


// for Login //
//--- require base1/js/pages/custom/login/login-general.js
////

// Base Form //
//--- require base1/js/pages/crud/forms/widgets/bootstrap-datetimepicker.js
//--- require base1/plugins/custom/datatables/datatables.bundle.js
//= require select2
//= require custom-upload.js

// tags & input
//= require selectize

// ให้ใช้ bootstrap-editable ตัวนี้ เพราะมีการแก้ bug size() -> length แล้ว //
//= require editable-bs4/bootstrap-editable
//= require editable/rails
//--- require turbolinks
//--- nested for
//--- require cocoon
////


//= require base1/plugins/custom/search_task.js










//Alert auto close
window.setTimeout(function() {
    $(".alert").fadeTo(400, 0).slideUp(400, function(){
        $(this).remove(); 
    });
}, 4000);
//

$(document).ready(function() {

$('.editable').editable({
mode: 'inline',
display: function(value) {
  $(this).text(value);
}
});

$('.datetimepicker-input').datetimepicker({
locale: 'th'
});
$('#datetimepicker_1_1').datetimepicker({
locale: 'th'
});
$('#datetimepicker_1_2').datetimepicker({
locale: 'th'
});

$('.select2-basic-multiple').select2({
theme: "bootstrap"
});


/*
$('.select2').select2({
	theme: "bootstrap"
});


$('.redirect-board').select2({
    placeholder: "Select Board",
    sorter: data => data.sort((a, b) => a.text.localeCompare(b.text)),
});
*/

});







document.addEventListener("turbo:before-stream-render", function(event) {
	// ยังใช้ไม่ได้ เลยใช้วิธีเพิ่ม class animate-task-in ลงใน _task.html.erb เลย
	// Add a class to an element we are about to add to the page
	// as defined by its "data-stream-enter-class"
	if (event.target.firstElementChild instanceof HTMLTemplateElement) {
	var enterAnimationClass = event.target.templateContent.firstElementChild.dataset.streamEnterClass
	if (enterAnimationClass) {
		//console.log('Add Task');
	  event.target.templateContent.firstElementChild.classList.add(enterAnimationClass);
	  //event.target.templateContent.firstElementChild.classList.add('animate-task-in');
	}
	}



	// Add a class to an element we are about to remove from the page
	// as defined by its "data-stream-exit-class"
	var elementToRemove = document.getElementById(event.target.target)
	
	if (elementToRemove) {
		var streamExitClass = elementToRemove.dataset.streamExitClass
		if (streamExitClass) {
			//console.log('Exit Task');
		// Intercept the removal of the element
		event.preventDefault()
		elementToRemove.classList.add(streamExitClass)
		// Wait for its animation to end before removing the element
		elementToRemove.addEventListener("animationend", function() {
		  event.target.performAction()
		})
		}
	}

})






