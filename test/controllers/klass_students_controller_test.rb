require "test_helper"

class KlassStudentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @klass_student = klass_students(:one)
  end

  test "should get index" do
    get klass_students_url
    assert_response :success
  end

  test "should get new" do
    get new_klass_student_url
    assert_response :success
  end

  test "should create klass_student" do
    assert_difference("KlassStudent.count") do
      post klass_students_url, params: {klass_student: {klass_id: @klass_student.klass_id, snumber: @klass_student.snumber, status: @klass_student.status, student_id: @klass_student.student_id}}
    end

    assert_redirected_to klass_student_url(KlassStudent.last)
  end

  test "should show klass_student" do
    get klass_student_url(@klass_student)
    assert_response :success
  end

  test "should get edit" do
    get edit_klass_student_url(@klass_student)
    assert_response :success
  end

  test "should update klass_student" do
    patch klass_student_url(@klass_student), params: {klass_student: {klass_id: @klass_student.klass_id, snumber: @klass_student.snumber, status: @klass_student.status, student_id: @klass_student.student_id}}
    assert_redirected_to klass_student_url(@klass_student)
  end

  test "should destroy klass_student" do
    assert_difference("KlassStudent.count", -1) do
      delete klass_student_url(@klass_student)
    end

    assert_redirected_to klass_students_url
  end
end
