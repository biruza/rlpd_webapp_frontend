require "test_helper"

class RecordScoresControllerTest < ActionDispatch::IntegrationTest
  setup do
    @record_score = record_scores(:one)
  end

  test "should get index" do
    get record_scores_url
    assert_response :success
  end

  test "should get new" do
    get new_record_score_url
    assert_response :success
  end

  test "should create record_score" do
    assert_difference("RecordScore.count") do
      post record_scores_url, params: {record_score: {course_subject_id: @record_score.course_subject_id, klass_id: @record_score.klass_id, school_year_id: @record_score.school_year_id}}
    end

    assert_redirected_to record_score_url(RecordScore.last)
  end

  test "should show record_score" do
    get record_score_url(@record_score)
    assert_response :success
  end

  test "should get edit" do
    get edit_record_score_url(@record_score)
    assert_response :success
  end

  test "should update record_score" do
    patch record_score_url(@record_score), params: {record_score: {course_subject_id: @record_score.course_subject_id, klass_id: @record_score.klass_id, school_year_id: @record_score.school_year_id}}
    assert_redirected_to record_score_url(@record_score)
  end

  test "should destroy record_score" do
    assert_difference("RecordScore.count", -1) do
      delete record_score_url(@record_score)
    end

    assert_redirected_to record_scores_url
  end
end
