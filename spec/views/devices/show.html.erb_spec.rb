require "rails_helper"

RSpec.describe "devices/show", type: :view do
  before(:each) do
    @device = assign(:device, Device.create!(
      location: nil,
      device_category: nil,
      title: "Title",
      image_data: "MyText",
      description: "MyText",
      slug: "Slug",
      price: "9.99",
      cost: "9.99",
      qty_max: 2,
      qty_min: 3,
      qty_current: 4,
      sn: "Sn",
      code: "Code",
      brand: "Brand",
      model: "Model",
      manufacturer: nil,
      user: nil,
      biz: nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/Slug/)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/4/)
    expect(rendered).to match(/Sn/)
    expect(rendered).to match(/Code/)
    expect(rendered).to match(/Brand/)
    expect(rendered).to match(/Model/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
  end
end
