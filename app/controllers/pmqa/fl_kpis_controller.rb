class Pmqa::FlKpisController < ApplicationController
  def index
    url = URI("#{$server[@controller_title]}/api/v1/fl_kpis")
    @datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/fl_kpis/#{params["id"]}")
    @datas = request_api(url, "get", nil)
  end

  def new
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/fl_kpis/#{params["id"]}")

    @datas = request_api(url, "get", nil)

    # puts "plan edit = #{@plan['title']}"
  end

  def create
    params[:fl_kpi][:user_id] = $user_id
    params[:fl_kpi][:biz_id] = $biz_id

    url = URI("#{$server[@controller_title]}/api/v1/fl_kpis")
    @datas = request_api(url, "post", fl_kpi_params)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/fl_kpis", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_title}/fl_kpis/new", alert: @datas["data"]
    end
  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/fl_kpis/#{params["id"]}")
    @datas = request_api(url, "patch", fl_kpi_params)

    # puts "update status : #{res}"

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/fl_kpis", notice: @datas["message"]
    else
      redirect_to "/#{@controller_title}/fl_kpis", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/fl_kpis/#{params["id"]}")
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/fl_kpis", notice: @datas["message"]
    else
      redirect_to "/#{@controller_title}/fl_kpis", alert: @datas["data"]
    end
  end

  private

  def fl_kpi_params
    params.require(:fl_kpi).permit(:title, :order_sort, :user_id, :biz_id, :primary)
  end
end
