class Risk::TasksController < ApplicationController
  def index
    url = URI("#{$server[@controller_title]}/api/v1/tasks")
    @datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/tasks/#{params["id"]}")
    @datas = request_api(url, "get", nil)
  end

  def new
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/tasks/#{params["id"]}")
    @datas = request_api(url, "get", nil)
  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/tasks/#{params["id"]}")
    @task = request_api(url, "get", nil)
    @datas = request_api(url, "patch", task_params)

    # puts "update status : #{res}"

    if @datas && @datas["status"] == "Success"
      redirect_to "#{params[:original_url]}?plan_report_id=#{@task["data"]["plan_report_id"]}", notice: @datas["messages"]
    else
      redirect_to "#{params[:original_url]}?plan_report_id=#{@task["data"]["plan_report_id"]}", alert: @datas["data"]
    end
  end

  def create
    url = URI("#{$server[@controller_title]}/api/v1/tasks")
    @datas = request_api(url, "post", task_params)

    if @datas["status"] == "Success"
      # url = URI("#{$server[@controller_title]}/api/v1/categories/#{params[:list][:category_id]}")
      # @category = request_api(url,'get',nil)
      # redirect_to "/#{@controller_main_url}/plans/#{@category['data']['plan_id']}", notice: @datas['messages']
      redirect_to "#{params[:original_url]}?plan_report_id=#{params[:task][:plan_report_id]}", notice: @datas["messages"]
    else
      redirect_to "#{params[:original_url]}?plan_report_id=#{params[:task][:plan_report_id]}", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/tasks/#{params["id"]}")
    @datas_ori = request_api(url, "get", nil)
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
    end
    redirect_to "#{params[:original_url]}?plan_report_id=#{@datas_ori["data"]["plan_report_id"]}", notice: @datas["messages"]
  end

  private

  def task_params
    # params.require(:task).permit(:slug, :status, :order_sort, :list, :plan_report, :user, :biz)
    params.require(:task).permit(:category_id, :slug, :status, :order_sort, :list_id, :list_activity_id, :plan_report_id, :user_id, :biz_id,
      task_results_attributes: [:id, :_destroy, :title, :slug, :order_sort, :task_id, :user_id, :biz_id],
      task_notes_attributes: [:id, :_destroy, :title, :slug, :order_sort, :task_id, :user_id, :biz_id],
      task_attachments_attributes: [:id, :_destroy, :file, :file_data_uri, :description, :slug, :task_id, :user_id, :biz_id])
  end
end
