class Post < ApplicationRecord
  belongs_to :biz, optional: true

  has_many :lists, dependent: :destroy
  accepts_nested_attributes_for :lists, reject_if: :all_blank, allow_destroy: true
  def self.to_csv(options = {})
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |item|
        csv << item.attributes.values_at(*column_names)
      end
    end
  end
end
