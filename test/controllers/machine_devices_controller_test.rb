require "test_helper"

class MachineDevicesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @machine_device = machine_devices(:one)
  end

  test "should get index" do
    get machine_devices_url
    assert_response :success
  end

  test "should get new" do
    get new_machine_device_url
    assert_response :success
  end

  test "should create machine_device" do
    assert_difference("MachineDevice.count") do
      post machine_devices_url, params: {machine_device: {device_id: @machine_device.device_id, machine_id: @machine_device.machine_id}}
    end

    assert_redirected_to machine_device_url(MachineDevice.last)
  end

  test "should show machine_device" do
    get machine_device_url(@machine_device)
    assert_response :success
  end

  test "should get edit" do
    get edit_machine_device_url(@machine_device)
    assert_response :success
  end

  test "should update machine_device" do
    patch machine_device_url(@machine_device), params: {machine_device: {device_id: @machine_device.device_id, machine_id: @machine_device.machine_id}}
    assert_redirected_to machine_device_url(@machine_device)
  end

  test "should destroy machine_device" do
    assert_difference("MachineDevice.count", -1) do
      delete machine_device_url(@machine_device)
    end

    assert_redirected_to machine_devices_url
  end
end
