require "test_helper"

class MembersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @member = members(:one)
  end

  test "should get index" do
    get members_url
    assert_response :success
  end

  test "should get new" do
    get new_member_url
    assert_response :success
  end

  test "should create member" do
    assert_difference("Member.count") do
      post members_url, params: {member: {address1: @member.address1, amphoe: @member.amphoe, birthdate: @member.birthdate, biz_id: @member.biz_id, blood_group: @member.blood_group, club: @member.club, congenital_disease: @member.congenital_disease, district: @member.district, email: @member.email, emergency_contact_name: @member.emergency_contact_name, emergency_contact_phone: @member.emergency_contact_phone, facebook: @member.facebook, firstname: @member.firstname, idcard: @member.idcard, image_data: @member.image_data, lastname: @member.lastname, line: @member.line, origin: @member.origin, phone: @member.phone, prefix: @member.prefix, province: @member.province, sex: @member.sex, status: @member.status, twitter: @member.twitter, youtube: @member.youtube, zipcode: @member.zipcode}}
    end

    assert_redirected_to member_url(Member.last)
  end

  test "should show member" do
    get member_url(@member)
    assert_response :success
  end

  test "should get edit" do
    get edit_member_url(@member)
    assert_response :success
  end

  test "should update member" do
    patch member_url(@member), params: {member: {address1: @member.address1, amphoe: @member.amphoe, birthdate: @member.birthdate, biz_id: @member.biz_id, blood_group: @member.blood_group, club: @member.club, congenital_disease: @member.congenital_disease, district: @member.district, email: @member.email, emergency_contact_name: @member.emergency_contact_name, emergency_contact_phone: @member.emergency_contact_phone, facebook: @member.facebook, firstname: @member.firstname, idcard: @member.idcard, image_data: @member.image_data, lastname: @member.lastname, line: @member.line, origin: @member.origin, phone: @member.phone, prefix: @member.prefix, province: @member.province, sex: @member.sex, status: @member.status, twitter: @member.twitter, youtube: @member.youtube, zipcode: @member.zipcode}}
    assert_redirected_to member_url(@member)
  end

  test "should destroy member" do
    assert_difference("Member.count", -1) do
      delete member_url(@member)
    end

    assert_redirected_to members_url
  end
end
