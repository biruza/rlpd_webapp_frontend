# To deliver this notification:
#
# ScheduleNotification.with(post: @post).deliver_later(current_user)
# ScheduleNotification.with(post: @post).deliver(current_user)

class ScheduleNotification < Noticed::Base
  # Add your delivery methods
  #
  # deliver_by :database
  deliver_by :database
  # deliver_by :email, mailer: "TestMailer", if: :email_notifications?
  deliver_by :email, mailer: "TestMailer"
  # deliver_by :slack
  # deliver_by :custom, class: "MyDeliveryMethod"

  # Add required params
  #
  # param :post

  # Define helper methods to make rendering easier.
  #
  def message
    t(".message")
  end

  def url
    post_path(params[:post])
  end

  # def email_notifications?
  #  !!recipient.preferences[:email]
  # end
end
