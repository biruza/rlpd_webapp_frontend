require "rails_helper"

RSpec.describe "plans/index", type: :view do
  before(:each) do
    assign(:plans, [
      Plan.create!(
        title: "Title",
        description: "MyText",
        slug: "Slug",
        status: "Status",
        user: nil,
        biz: nil
      ),
      Plan.create!(
        title: "Title",
        description: "MyText",
        slug: "Slug",
        status: "Status",
        user: nil,
        biz: nil
      )
    ])
  end

  it "renders a list of plans" do
    render
    assert_select "tr>td", text: "Title".to_s, count: 2
    assert_select "tr>td", text: "MyText".to_s, count: 2
    assert_select "tr>td", text: "Slug".to_s, count: 2
    assert_select "tr>td", text: "Status".to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
  end
end
