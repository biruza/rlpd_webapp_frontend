class Kpi::PeriodReportsController < ApplicationController
  before_action :set_select, only: %i[index show new edit create update destroy print]
  def index
    url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params[:master_plan_id]}")
    @master_plan_datas = request_api(url, "get", nil)
  end

  def show
  end

  def new
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params[:master_plan_id]}")
    @master_plan_datas = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/period_reports/#{params["id"]}")
    @datas = request_api(url, "get", nil)
  end

  def create


    url = URI("#{$server[@controller_title]}/api/v1/period_reports")
    @datas = request_api(url, "post", period_report_params)


    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/period_reports?master_plan_id=#{params[:period_report][:master_plan_id]}&original_url=#{@controller_main_url}/master_plans/#{params[:period_report][:master_plan_id]}", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_title}/period_reports/new?master_plan_id=#{params[:period_report][:master_plan_id]}&original_url=#{@controller_main_url}/period_reports", alert: @datas["data"]
    end
  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/period_reports/#{params["id"]}")
    @datas = request_api(url, "patch", period_report_params)

    # puts "update status : #{res}"

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/period_reports?master_plan_id=#{params[:period_report][:master_plan_id]}&original_url=#{@controller_main_url}/master_plans/#{params[:period_report][:master_plan_id]}", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_title}/period_reports?master_plan_id=#{params[:period_report][:master_plan_id]}/edit&original_url=#{@controller_main_url}/period_reports", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/period_reports/#{params["id"]}")
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/period_reports?master_plan_id=#{@datas['data']['master_plan_id']}&original_url=#{@controller_main_url}/master_plans/#{@datas['data']['master_plan_id']}", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_title}/period_reports?master_plan_id=#{params[:period_report][:master_plan_id]}&original_url=#{@controller_main_url}/period_reports", alert: @datas["data"]
    end
  end
  
  def set_select
    url_user = URI("#{$server[@controller_title]}/api/v1/users")
    @data_users = request_api(url_user, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params[:master_plan_id]}")
    @master_plan_datas = request_api(url, "get", nil)

  end

  private
  
  def period_report_params
    # params.require(:period_report).permit(:title, :startdate, :enddate, :slug, :order_sort, :master_plan, :user, :biz)
    params.require(:period_report).permit(:title, :startdate, :enddate, :slug, :order_sort, :master_plan_id, :user_id, :biz_id)
  end

end
