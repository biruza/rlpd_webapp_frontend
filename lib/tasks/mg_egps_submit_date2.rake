desc "mg_egps_submit_date"
task mg_egps_submit_date2: [:environment] do
  count = 0
  egps = Egp.all
  egps.each do |egp|
    if egp.update(submit_date: egp.get_submit_date(egp.link)) && egp.link && egp.get_submit_date(egp.link)
      count += 1
      p "#{count} | submit_date = #{egp.get_submit_date(egp.link)}"
    end
  end
  p "sum count = #{count}"
end
