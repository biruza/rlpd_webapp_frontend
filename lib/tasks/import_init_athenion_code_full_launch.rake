require "csv"

desc "Import Redeem Code full launch from csv file"
task import_init_athenion_code_full_launch: [:environment] do
  csv_text1 = File.read("db/for_init/athenion/code_full_launch.csv")
  csv1 = CSV.parse(csv_text1, headers: true)
  csv1.each do |row|
    RedeemCode.create!(row.to_hash)
  end

  # ActiveRecord::Base.connection.execute(%q{
  # select setval('redeem_codes_uuid_seq', m)
  # from (
  #    select max(uuid) from redeem_codes
  #    ) as dt(m)
  # })
end
