Rails.application.routes.draw do
  namespace :km do
    get "departments/index"
    get "departments/show"
    get "departments/new"
    get "departments/edit"
  end
  get "accounts/sign_in"
  post "accounts/create_session"
  get "accounts/sign_out"
  get "accounts/delete_session"

  resources :posts
  resources :locations
  resources :bizs

  # km
  ["km", "kpi", "risk", "pmqa"].each do |system_name|
    namespace system_name do
      namespace :user do
        namespace :pages do
          get :dashboards
          get :dashboards_pmqa_fl
          get :dashboards_pmqa_4
          post :search
          get :search_result
          get :master_data
        end

        get "/plans/form_1", to: "plans#form_1"
        get "/plans/form_2", to: "plans#form_2"
        get "/plans/form_3", to: "plans#form_3"
        get "/plans/form_4", to: "plans#form_4"
        get "/plans/form_5", to: "plans#form_5"
        get "/plans/form_6", to: "plans#form_6"
        get "/plans/form_7", to: "plans#form_7"

        get "/master_plans/new_by_template", to: "master_plans#new_by_template"
        post "/master_plans/create_by_template", to: "master_plans#create_by_template"
        get "/master_plans/print", to: "master_plans#print"
        get "/master_plans/edit_form_2", to: "master_plans#edit_form_2"
        get "/master_plans/edit_form_3", to: "master_plans#edit_form_3"
        get "/master_plans/edit_form_4", to: "master_plans#edit_form_4"
        get "/master_plans/edit_form_5", to: "master_plans#edit_form_5"
        get "/plan_reports/print", to: "plan_reports#print"
        resources :master_plans
        resources :master_plan_users
        resources :master_plan_user_comments
        resources :plans
        resources :plan_reports
        resources :plan_report_users
        resources :plan_report_user_comments

        resources :categories
        resources :activities
        resources :lists
        resources :tasks
      end

      resources :categories
      resources :lists

      get "/master_plans/new_by_template", to: "master_plans#new_by_template"
      post "/master_plans/create_by_template", to: "master_plans#create_by_template"
      get "/master_plans/print", to: "master_plans#print"
      get "/report_os_matrix", to: "master_plans#report_os_matrix"
      get "/plan_reports/print", to: "plan_reports#print"
      resources :master_plans

      get "/pmqa_fls/edit", to: "pmqa_fls#edit"
      get "/pmqa_fls/edit_by_user", to: "pmqa_fls#edit_by_user"
      get "/pmqa_fls/edit_by_opdc", to: "pmqa_fls#edit_by_opdc"
      get "/pmqa_fls/edit_by_user_7", to: "pmqa_fls#edit_by_user_7"
      get "/pmqa_fls/edit_by_opdc_7", to: "pmqa_fls#edit_by_opdc_7"
      get "/pmqa_fls/print", to: "pmqa_fls#print"
      get "/pmqa_fours/edit_by_sp_1", to: "pmqa_fours#edit_by_sp_1"
      get "/pmqa_fours/edit_by_sp_7", to: "pmqa_fours#edit_by_sp_7"
      get "/pmqa_fours/edit_by_sp_op", to: "pmqa_fours#edit_by_sp_op"
      get "/pmqa_fours/edit_by_user_1", to: "pmqa_fours#edit_by_user_1"
      get "/pmqa_fours/edit_by_user_7", to: "pmqa_fours#edit_by_user_7"
      get "/pmqa_fours/edit_by_user_op", to: "pmqa_fours#edit_by_user_op"
      get "/pmqa_fours/edit_by_opdc_1", to: "pmqa_fours#edit_by_opdc_1"
      get "/pmqa_fours/edit_by_opdc_7", to: "pmqa_fours#edit_by_opdc_7"
      get "/pmqa_fours/edit_by_opdc_op", to: "pmqa_fours#edit_by_opdc_op"
      get "/pmqa_fours/print", to: "pmqa_fours#print"
      resources :pmqa_fls
      resources :pmqa_fours
      resources :master_plan_users
      resources :master_plan_user_comments

      resources :plans
      resources :institutions
      resources :risk_types
      resources :issues
      resources :kpis
      resources :target_kpis
      resources :responsibles
      resources :activities
      resources :devision_directors

      resources :tasks
      resources :users
      resources :departments
    end
  end

  # share service
  patch "/mains/set_user_active/:id", to: "mains#set_user_active"

  # api
  namespace :api do
    namespace :v1 do
      get "/locations", to: "locations#index"
      post "/locations", to: "locations#create"

      get "/posts", to: "posts#index"
      post "/posts", to: "posts#create"
    end
  end
  ####

  get "/pages/index", to: "pages#index"

  root "pages#index"

  # user
  devise_for :users, controllers: {
    sessions: "users/sessions",
    registrations: "users/registrations"
  }

  resources :users, only: [:index, :new, :create, :edit, :update, :destroy]
  post "/users_create", to: "users#users_create"
  post "auth_user" => "authentication#authenticate_user"
  ####

  if Rails.env.production?
    mount Shrine.presign_endpoint(:cache) => "/presign"
  else
    # In development and test environment we're using filesystem storage
    # for speed, so on the client side we'll upload files to our app.
    mount Shrine.upload_endpoint(:cache) => "/upload"
  end

  # noti for gem notifications
  resources :notifications, path: "" do
    collection do
      delete :clean
    end
  end
end
