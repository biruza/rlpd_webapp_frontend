class Pmqa::PmqaBasMastersController < ApplicationController
  def index
    url = URI("#{$server[@controller_title]}/api/v1/pmqa_bas_masters")
    @datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/pmqa_bas_masters/#{params["id"]}")
    @datas = request_api(url, "get", nil)
  end

  def new
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/pmqa_bas_masters/#{params["id"]}")

    @datas = request_api(url, "get", nil)

    # puts "plan edit = #{@plan['title']}"
  end

  def create
    params[:pmqa_bas_master][:user_id] = $user_id
    params[:pmqa_bas_master][:biz_id] = $biz_id

    url = URI("#{$server[@controller_title]}/api/v1/pmqa_bas_masters")
    @datas = request_api(url, "post", pmqa_bas_master_params)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/pmqa_bas_masters", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_title}/pmqa_bas_masters/new", alert: @datas["data"]
    end
  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/pmqa_bas_masters/#{params["id"]}")
    @datas = request_api(url, "patch", pmqa_bas_master_params)

    # puts "update status : #{res}"

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/pmqa_bas_masters", notice: @datas["message"]
    else
      redirect_to "/#{@controller_title}/pmqa_bas_masters", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/pmqa_bas_masters/#{params["id"]}")
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/pmqa_bas_masters", notice: @datas["message"]
    else
      redirect_to "/#{@controller_title}/pmqa_bas_masters", alert: @datas["data"]
    end
  end

  private

  def pmqa_bas_master_params
    params.require(:pmqa_bas_master).permit(:title, :user_id, :order_sort, :biz_id, :score_min, :score_max)
  end
end
