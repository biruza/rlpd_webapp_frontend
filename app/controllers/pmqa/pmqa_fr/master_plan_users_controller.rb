class Pmqa::PmqaFr::MasterPlanUsersController < ApplicationController
  before_action :set_select, only: %i[new edit]
  before_action :set_default, only: %i[index show new edit create update destroy print]
  # @sub_system = "/pmqa_fr"

  def index
    url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params[:master_plan_id]}/master_plan_users?order=order_sort&sort=asc")
    @datas = request_api(url, "get", nil)

    # p "url =#{url}"

    if !@datas || @datas["status"] != "Success"
      redirect_to params[:original_url].to_s, alert: @datas["data"]
    end
  end

  def new
    url = URI("#{$server[@controller_title]}/api/v1/master_plan_users?order=order_sort&sort=desc&limit=1")
    @data_mp = request_api(url, "get", nil)
    @order_sort_max = begin
      @data_mp["data"][0]["order_sort"].to_i
    rescue
      nil
    end
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/master_plan_users/#{params["id"]}")

    @datas = request_api(url, "get", nil)
  end

  def create
    params[:master_plan_user][:user_id] = $user[session.id.to_s][@controller_title]["id"]
    # params[:master_plan_user][:biz_id] = $user[session.id.to_s][@controller_title]['biz_id']

    url = URI("#{$server[@controller_title]}/api/v1/master_plan_users")
    @datas = request_api(url, "post", master_plan_user_params)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/master_plan_users?master_plan_id=#{params[:master_plan_user][:master_plan_id]}", notice: @datas["message"]
    else
      redirect_to "/#{@controller_main_url}/master_plan_users?master_plan_id=#{params[:master_plan_user][:master_plan_id]}", alert: @datas["message"]
    end
  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/master_plan_users/#{params["id"]}")
    @datas = request_api(url, "patch", master_plan_user_params)

    url = URI("#{$server[@controller_title]}/api/v1/master_plan_users/#{params["id"]}")
    @datas = request_api(url, "get", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/master_plan_users?master_plan_id=#{params[:master_plan_user][:master_plan_id]}", notice: @datas["message"]
    else
      redirect_to "/#{@controller_main_url}/master_plan_users?master_plan_id=#{params[:master_plan_user][:master_plan_id]}", alert: @datas["message"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/master_plan_users/#{params["id"]}")
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
    end
    redirect_to "/#{@controller_main_url}/master_plan_users?master_plan_id=#{@datas["data"]["master_plan_id"]}", notice: @datas["message"]
  end

  private

  def set_select
    url_user = URI("#{$server[@controller_title]}/api/v1/users")
    @data_users = request_api(url_user, "get", nil)

    url_user_role = URI("#{$server[@controller_title]}/api/v1/user_roles")
    @data_user_roles = request_api(url_user_role, "get", nil)
  end

  def set_default
    @sub_system = "pmqa_fr"
    @controller_main_url = "#{@controller_title}/#{@sub_system}"
  end

  def master_plan_user_params
    # params.require(:kpi).permit(:title, :slug, :order_sort, :user, :biz)
    params.require(:master_plan_user).permit(:status, :order_sort, :master_plan_id, :user_id, :user_role_id,
      master_plan_user_comments_attributes: [:id, :_destroy, :title, :description, :image_data, :slug, :status, :dead_line_start, :date_line_end, :master_plan_user_id, :user_id, :biz_id])
  end
end
