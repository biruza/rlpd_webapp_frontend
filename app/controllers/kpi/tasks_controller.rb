class Kpi::TasksController < ApplicationController
  before_action :set_select, only: %i[new edit]

  def index
    url = URI("#{$server[@controller_title]}/api/v1/tasks")
    @datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/tasks/#{params["id"]}")
    @datas = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/tasks/#{params["id"]}")
    @task = request_api(url, "get", nil)
  end

  def new
    url = URI("#{$server[@controller_title]}/api/v1/lists/#{params[:list_id]}")
    @list = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/categories/#{@list["data"]["category_id"]}")
    @category = request_api(url, "get", nil)

    @task_params = {
      "list_id" => params[:list_id],
      "category_id" => @category["data"]["id"],
      "plan_report_id" => params[:plan_report_id],
      "user_id" => $user[session.id.to_s][@controller_title]["id"],
      "biz_id" => $user[session.id.to_s][@controller_title]["biz_id"]
    }

    url = URI("#{$server[@controller_title]}/api/v1/tasks")
    @datas = request_api(url, "post", @task_params)

    original_url = "/#{@controller_title}/plans/#{@category["data"]["plan_id"]}/tasks?plan_report_id=#{params[:plan_report_id]}"

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/tasks/#{@datas["data"]["id"]}/edit?original_url=#{original_url}", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_title}/plans/#{@category["data"]["plan_id"]}/tasks?plan_report_id=#{params[:plan_report_id]}", alert: @datas["data"]
    end
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/tasks/#{params["id"]}")
    @task = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/lists/#{@task["data"]["list_id"]}")
    @list = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/categories/#{@list["data"]["category_id"]}")
    @category = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/plan_reports/#{@task["data"]["plan_report_id"]}")
    @plan_report = request_api(url, "get", nil)
  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/tasks/#{params["id"]}")
    @task = request_api(url, "get", nil)
    @datas = request_api(url, "patch", task_params)

    # puts "update status : #{res}"

    url = URI("#{$server[@controller_title]}/api/v1/lists/#{@task["data"]["list_id"]}")
    @list = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/categories/#{@task["data"]["category_id"]}")
    @category = request_api(url, "get", nil)

    if @datas && @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/plans/#{@category["data"]["plan_id"]}/tasks?plan_report_id=#{@task["data"]["plan_report_id"]}&original_url=/kpi/plan_reports/#{@task["data"]["plan_report_id"]}", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_title}/plans/#{@category["data"]["plan_id"]}/tasks?plan_report_id=#{@task["data"]["plan_report_id"]}&original_url=/kpi/plan_reports/#{@task["data"]["plan_report_id"]}", alert: @datas["data"]
    end
  end

  def create
    url = URI("#{$server[@controller_title]}/api/v1/tasks")
    @datas = request_api(url, "post", task_params)

    if @datas["status"] == "Success"
      # url = URI("#{$server[@controller_title]}/api/v1/categories/#{params[:list][:category_id]}")
      # @category = request_api(url,'get',nil)
      # redirect_to "/#{@controller_title}/plans/#{@category['data']['plan_id']}", notice: @datas['messages']
      redirect_to "#{params[:original_url]}?plan_report_id=#{params[:task][:plan_report_id]}", notice: @datas["messages"]
    else
      redirect_to "#{params[:original_url]}?plan_report_id=#{params[:task][:plan_report_id]}", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/tasks/#{params["id"]}")
    @datas_ori = request_api(url, "get", nil)
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
     # redirect_to "/#{@controller_title}/plans/#{@category["data"]["plan_id"]}/tasks?plan_report_id=#{@task["data"]["plan_report_id"]}&original_url=/kpi/plan_reports/#{@task["data"]["plan_report_id"]}", alert: @datas["data"]
    end
    redirect_to "#{params[:original_url]}?plan_report_id=#{@datas_ori["data"]["plan_report_id"]}&original_url=/kpi/plan_reports/#{@datas_ori["data"]["plan_report_id"]}", notice: @datas["messages"]
    #redirect_to "/#{@controller_title}/plans/#{@category["data"]["plan_id"]}/tasks?plan_report_id=#{@task["data"]["plan_report_id"]}&original_url=/kpi/plan_reports/#{@task["data"]["plan_report_id"]}", alert: @datas["data"]
  end

  def set_select
    url = URI("#{$server[@controller_title]}/api/v1/plan_reports/#{params["plan_report_id"]}")
    @plan_report = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/lists/#{params["list_id"]}")
    @list = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/period_reports/#{params["master_plan_report_id"]}")
    @select_period_report = request_api(url, "get", nil)

    # if @list && !@list['data'].blank? && !@list['data']['category_id'].blank?
    # url = URI("#{$server[@controller_title]}/api/v1/categories/#{@list['data']['category_id']}")
    # @category = request_api(url,'get',nil)
    # end

    if !params["id"].blank?
      url = URI("#{$server[@controller_title]}/api/v1/tasks/#{params["id"]}")
      @task = request_api(url, "get", nil)
    end
  end

  private

  def task_params
    # params.require(:task).permit(:slug, :status, :order_sort, :list, :plan_report, :user, :biz)
    params.require(:task).permit(:category_id, :slug, :status, :order_sort, :list_id, :list_activity_id, :plan_report_id, :user_id, :biz_id,
      task_kpi_persons_attributes: [:id, :title, :phone, :slug, :order_sort, :task_id, :user_id, :biz_id],
      task_data_collectors_attributes: [:id, :title, :phone, :slug, :order_sort, :task_id, :user_id, :biz_id],
      task_results_attributes: [:id, :title, :slug, :order_sort, :task_id, :user_id, :biz_id],
      task_score_results_attributes: [:id, :title, :title_first, :value_first, :unit_first, :result, :title_second, :value_second, :unit_second, :title_third, :value_third, :unit_third, :summary_result, :slug, :order_sort, :task_id, :user_id, :biz_id],
      task_formulas_attributes: [:id, :_destroy, :title, :image, :image_data, :slug, :order_sort, :task_id, :user_id, :biz_id],
      task_statements_attributes: [:id, :_destroy, :title, :image, :image_data, :slug, :order_sort, :task_id, :user_id, :biz_id],
      task_supports_attributes: [:id, :title, :slug, :order_sort, :task_id, :user_id, :biz_id],
      task_obstacles_attributes: [:id, :title, :slug, :order_sort, :task_id, :user_id, :biz_id],
      task_notes_attributes: [:id, :title, :slug, :order_sort, :task_id, :user_id, :biz_id])
  end
end
