require "rails_helper"

RSpec.describe DeviceCategoriesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/device_categories").to route_to("device_categories#index")
    end

    it "routes to #new" do
      expect(get: "/device_categories/new").to route_to("device_categories#new")
    end

    it "routes to #show" do
      expect(get: "/device_categories/1").to route_to("device_categories#show", id: "1")
    end

    it "routes to #edit" do
      expect(get: "/device_categories/1/edit").to route_to("device_categories#edit", id: "1")
    end

    it "routes to #create" do
      expect(post: "/device_categories").to route_to("device_categories#create")
    end

    it "routes to #update via PUT" do
      expect(put: "/device_categories/1").to route_to("device_categories#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/device_categories/1").to route_to("device_categories#update", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/device_categories/1").to route_to("device_categories#destroy", id: "1")
    end
  end
end
