require "test_helper"

class OrderItemOptionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @order_item_option = order_item_options(:one)
  end

  test "should get index" do
    get order_item_options_url
    assert_response :success
  end

  test "should get new" do
    get new_order_item_option_url
    assert_response :success
  end

  test "should create order_item_option" do
    assert_difference("OrderItemOption.count") do
      post order_item_options_url, params: {order_item_option: {biz_id: @order_item_option.biz_id, order_item_id: @order_item_option.order_item_id, product_option_id: @order_item_option.product_option_id, product_option_price: @order_item_option.product_option_price, product_option_title: @order_item_option.product_option_title, qty: @order_item_option.qty}}
    end

    assert_redirected_to order_item_option_url(OrderItemOption.last)
  end

  test "should show order_item_option" do
    get order_item_option_url(@order_item_option)
    assert_response :success
  end

  test "should get edit" do
    get edit_order_item_option_url(@order_item_option)
    assert_response :success
  end

  test "should update order_item_option" do
    patch order_item_option_url(@order_item_option), params: {order_item_option: {biz_id: @order_item_option.biz_id, order_item_id: @order_item_option.order_item_id, product_option_id: @order_item_option.product_option_id, product_option_price: @order_item_option.product_option_price, product_option_title: @order_item_option.product_option_title, qty: @order_item_option.qty}}
    assert_redirected_to order_item_option_url(@order_item_option)
  end

  test "should destroy order_item_option" do
    assert_difference("OrderItemOption.count", -1) do
      delete order_item_option_url(@order_item_option)
    end

    assert_redirected_to order_item_options_url
  end
end
