require "rails_helper"

RSpec.describe "task_attaches/edit", type: :view do
  before(:each) do
    @task_attach = assign(:task_attach, TaskAttach.create!(
      task: nil,
      title: "MyString",
      file: "MyText",
      slug: "MyString",
      source: "MyString",
      user: nil,
      biz: nil
    ))
  end

  it "renders the edit task_attach form" do
    render

    assert_select "form[action=?][method=?]", task_attach_path(@task_attach), "post" do
      assert_select "input[name=?]", "task_attach[task_id]"

      assert_select "input[name=?]", "task_attach[title]"

      assert_select "textarea[name=?]", "task_attach[file]"

      assert_select "input[name=?]", "task_attach[slug]"

      assert_select "input[name=?]", "task_attach[source]"

      assert_select "input[name=?]", "task_attach[user_id]"

      assert_select "input[name=?]", "task_attach[biz_id]"
    end
  end
end
