require "rails_helper"

RSpec.describe "egps/show", type: :view do
  before(:each) do
    @egp = assign(:egp, Egp.create!(
      dep_id: "Dep",
      category: "Category",
      title: "Title",
      link: "Link",
      description: "MyText",
      status: "Status"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Dep/)
    expect(rendered).to match(/Category/)
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/Link/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/Status/)
  end
end
