class Kpi::PlanUsersController < ApplicationController
  before_action :set_select, only: %i[new edit]

  def index
    url = URI("#{$server[@controller_title]}/api/v1/plans/#{params[:plan_id]}/plan_users?order=order_sort&sort=asc")
    @datas = request_api(url, "get", nil)

    # p "url =#{url}"

    if !@datas || @datas["status"] != "Success"
      redirect_to params[:original_url].to_s, alert: @datas["data"]
    end
  end

  def new
    url = URI("#{$server[@controller_title]}/api/v1/plans/#{params[:plan_id]}/plan_users?order=order_sort&sort=desc&limit=1")
    @data_mp = request_api(url, "get", nil)
    @order_sort_max = begin
      @data_mp["data"][0]["order_sort"].to_i + 1
    rescue
      nil
    end
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/plan_users/#{params["id"]}")

    @datas = request_api(url, "get", nil)
  end

  def create
    #params[:plan_user][:user_id] = $user[session.id.to_s][@controller_title]["id"]
    # params[:plan_user][:biz_id] = $user[session.id.to_s][@controller_title]['biz_id']

    url = URI("#{$server[@controller_title]}/api/v1/plan_users")
    @datas = request_api(url, "post", plan_user_params)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/plan_users?plan_id=#{params[:plan_user][:plan_id]}", notice: @datas["message"]
    else
      redirect_to "/#{@controller_title}/plan_users?plan_id=#{params[:plan_user][:plan_id]}", alert: @datas["message"]
    end
  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/plan_users/#{params["id"]}")
    @datas = request_api(url, "patch", plan_user_params)

    url = URI("#{$server[@controller_title]}/api/v1/plan_users/#{params["id"]}")
    @datas = request_api(url, "get", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/plan_users?plan_id=#{params[:plan_user][:plan_id]}", notice: @datas["message"]
    else
      redirect_to "/#{@controller_title}/plan_users?plan_id=#{params[:plan_user][:plan_id]}", alert: @datas["message"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/plan_users/#{params["id"]}")
    @plan_user = request_api(url, "get", nil)
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
    end
    redirect_to "/#{@controller_title}/plan_users?plan_id=#{begin
      @plan_user["data"]["plan_id"]
    rescue
      nil
    end}", notice: @datas["message"]
  end



  private
  def set_select
    url_user = URI("#{$server[@controller_title]}/api/v1/users")
    @data_users = request_api(url_user, "get", nil)
   
    url_user_role = URI("#{$server[@controller_title]}/api/v1/user_roles")
    @data_user_roles = request_api(url_user_role, "get", nil)
  end
  
  def plan_user_params
    # params.require(:kpi).permit(:title, :slug, :order_sort, :user, :biz)
    params.require(:plan_user).permit(:status, :order_sort, :plan_id, :user_id, :user_role_id,
      plan_user_comments_attributes: [:id, :_destroy, :title, :description, :image_data, :slug, :status, :dead_line_start, :date_line_end, :plan_user_id, :user_id, :biz_id])
  end
end
