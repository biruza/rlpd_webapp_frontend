require "test_helper"

class WebSlidersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @web_slider = web_sliders(:one)
  end

  test "should get index" do
    get web_sliders_url
    assert_response :success
  end

  test "should get new" do
    get new_web_slider_url
    assert_response :success
  end

  test "should create web_slider" do
    assert_difference("WebSlider.count") do
      post web_sliders_url, params: {web_slider: {biz_id: @web_slider.biz_id, description_th: @web_slider.description_th, end_date: @web_slider.end_date, image: @web_slider.image, module_web_id: @web_slider.module_web_id, order: @web_slider.order, start_date: @web_slider.start_date, title_th: @web_slider.title_th, url: @web_slider.url}}
    end

    assert_redirected_to web_slider_url(WebSlider.last)
  end

  test "should show web_slider" do
    get web_slider_url(@web_slider)
    assert_response :success
  end

  test "should get edit" do
    get edit_web_slider_url(@web_slider)
    assert_response :success
  end

  test "should update web_slider" do
    patch web_slider_url(@web_slider), params: {web_slider: {biz_id: @web_slider.biz_id, description_th: @web_slider.description_th, end_date: @web_slider.end_date, image: @web_slider.image, module_web_id: @web_slider.module_web_id, order: @web_slider.order, start_date: @web_slider.start_date, title_th: @web_slider.title_th, url: @web_slider.url}}
    assert_redirected_to web_slider_url(@web_slider)
  end

  test "should destroy web_slider" do
    assert_difference("WebSlider.count", -1) do
      delete web_slider_url(@web_slider)
    end

    assert_redirected_to web_sliders_url
  end
end
