require "net/ldap"

class LdapAuth
  def self.call(account_params)
    new(account_params).call
  end

  def initialize(account_params)
    @email = account_params[:email]
    @password = account_params[:password]
    @system_name = account_params[:system_name]
  end

  attr_accessor :email, :password, :system_name

  def call
    # username = "Wichai.l"
    # password = "AsyluM99#"

    # username = "Kimtest"
    # password = "12345678"

    ldap = Net::LDAP.new(
      host: "10.136.27.15",
      port: 389,
      auth: {
        method: :simple,
        username: email,
        password: password
      }
    )

    if ldap.bind
      treebase = "OU=Group User,DC=rlpd,DC=go,DC=th"

      username = email.split("@").first
      filter = Net::LDAP::Filter.eq("samaccountname", username.to_s)
      results = ldap.search(base: treebase, filter: filter)

      user_data = {}
      (results || []).each do |entry|
        puts "DN: #{entry.dn}"
        user_data[:dn] = entry.dn
        entry.each do |attribute, values|
          next if attribute == "objectguid"
          user_data[attribute] = values
          puts "   #{attribute}: => #{values}"
        end
      end

      [true, user_data]
    else
      [false, {}]
    end
  rescue Net::LDAP::Error
    [false, {}]
  end
end
