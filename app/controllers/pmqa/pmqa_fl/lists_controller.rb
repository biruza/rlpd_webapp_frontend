class Pmqa::PmqaFl::ListsController < ApplicationController
  before_action :set_select, only: %i[new create edit update]
  before_action :set_default, only: %i[index show new edit create update destroy print]
  @sub_system = "/pmqa_fl"

  def index
    url = URI("#{$server[@controller_title]}/api/v1/categories")
    @catagory_datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/categories/#{params["id"]}")
    @datas = request_api(url, "get", nil)
  end

  def new
    url = URI("#{$server[@controller_title]}/api/v1/categories/#{params["category_id"]}")
    @catagory_datas = request_api(url, "get", nil)
  end

  def create
    url = URI("#{$server[@controller_title]}/api/v1/lists")
    @datas = request_api(url, "post", list_params)

    if @datas["status"] == "Success"
      url = URI("#{$server[@controller_title]}/api/v1/categories/#{params[:list][:category_id]}")
      @category = request_api(url, "get", nil)

      redirect_to "/#{@controller_main_url}/plans/#{@category["data"]["plan_id"]}", notice: @datas["messages"]
    else
      redirect_to params[:original_url].to_s, alert: @datas["data"]
    end
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/lists/#{params["id"]}")
    @datas = request_api(url, "get", nil)
    url = URI("#{$server[@controller_title]}/api/v1/categories/#{params["id"]}")
    @catagory_datas = request_api(url, "get", nil)
  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/lists/#{params["id"]}")
    @datas = request_api(url, "patch", list_params)

    # puts "update status : #{res}"

    if @datas && @datas["status"] == "Success"
      # case params[:current_url]
      # when 'plan_reports'
      redirect_to "#{params[:original_url]}?plan_report_id=#{begin
        params[:list][:plan_report_id]
      rescue
        nil
      end}", notice: @datas["message"]
      # else
      # redirect_to "/#{@controller_title}/categories/#{@datas['data']['category_id']}/edit", notice: @datas['message']
      # end

    else
      redirect_to "/#{@controller_main_url}/lists/#{params[:id]}/edit", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/lists/#{params["id"]}")
    @datas_ori = request_api(url, "get", nil)
    @datas = request_api(url, "delete", nil)

    url = URI("#{$server[@controller_title]}/api/v1/categories/#{@datas_ori["data"]["category_id"]}")
    @category = request_api(url, "get", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/plans/#{@category["data"]["plan_id"]}", notice: @datas["message"]
    else
      redirect_to "/#{@controller_main_url}/plans/#{@category["data"]["plan_id"]}", alert: @datas["data"]
    end
  end

  def set_select
    url = URI("#{$server[@controller_title]}/api/v1/activities")
    @data_activities = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/fl_kpis")
    @fl_kpis = request_api(url, "get", nil)
  end

  def set_default
    @sub_system = "pmqa_fl"
    @controller_main_url = "#{@controller_title}/#{@sub_system}"
  end

  def list_params
    # params.require(:list).permit( :status, :slug, :order_sort, :category, :activity, :user, :biz)
    params.require(:list).permit(:title, :status, :slug, :order_sort, :category_id, :activity_id, :user_id, :biz_id, 
      :fl_kpi_id, :fl_kpi_value, :fl_first_value, :fl_second_value, :fl_third_value, :fl_success, :fl_score, 
      list_activities_attributes: [:id, :_destroy, :status, :title, :slug, :order_sort, :list_id, :user_id, :biz_id, :file, :file_data_uri, :file_data],
      list_periods_attributes: [:id, :_destroy, :startdate, :enddate, :slug, :order_sort, :list_id, :user_id, :biz_id],
      list_kpis_attributes: [:id, :_destroy, :title, :slug, :order_sort, :list_id, :user_id, :biz_id],
      list_target_kpis_attributes: [:id, :_destroy, :title, :slug, :order_sort, :list_id, :user_id, :biz_id],
      list_responsibles_attributes: [:id, :_destroy, :title, :slug, :order_sort, :list_id, :user_id, :biz_id],
      list_target_groups_attributes: [:id, :_destroy, :title, :slug, :order_sort, :list_id, :user_id, :biz_id],
      list_notes_attributes: [:id, :_destroy, :title, :slug, :order_sort, :list_id, :user_id, :biz_id],
      tasks_attributes: [:id, :_destroy, :slug, :status, :order_sort, :list_id, :plan_report_id, :user_id, :biz_id])
  end
end
