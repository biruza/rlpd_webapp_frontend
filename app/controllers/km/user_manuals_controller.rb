class Km::UserManualsController < ApplicationController
  def index
    url = URI("#{$server[@controller_title]}/api/v1/user_manuals")
    @datas = request_api(url, "get", nil)
  end
  def show
    url = URI("#{$server[@controller_title]}/api/v1/user_manuals/#{params["id"]}")

    @datas = request_api(url, "get", nil)
  end

  def new
  end


  def edit
    url = URI("#{$server[@controller_title]}/api/v1/user_manuals/#{params["id"]}")

    @datas = request_api(url, "get", nil)
  end

  def create
    # params[:download][:user_id] = $user_id
    # params[:download][:biz_id] = $biz_id

    url = URI("#{$server[@controller_title]}/api/v1/user_manuals")
    @datas = request_api(url, "post", user_manual_params)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/user_manuals", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_title}/user_manuals/new", alert: @datas["data"]
    end
  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/user_manuals/#{params["id"]}")
    @datas = request_api(url, "patch", user_manual_params)

    # puts "update status : #{res}"

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/user_manuals", notice: @datas["message"]
    else
      redirect_to "/#{@controller_title}/user_manuals", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/user_manuals/#{params["id"]}")
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/user_manuals", notice: @datas["message"]
    else
      redirect_to "/#{@controller_title}/user_manuals", alert: @datas["data"]
    end
  end

  private

  def user_manual_params
    params.require(:user_manual).permit(:order_sort, :title, :file_data, :file, :file_data_uri, :view_count, :order_sort, :user_id, :biz_id)
  end
end
