require "test_helper"

class BadSymptomsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @bad_symptom = bad_symptoms(:one)
  end

  test "should get index" do
    get bad_symptoms_url
    assert_response :success
  end

  test "should get new" do
    get new_bad_symptom_url
    assert_response :success
  end

  test "should create bad_symptom" do
    assert_difference("BadSymptom.count") do
      post bad_symptoms_url, params: {bad_symptom: {name: @bad_symptom.name}}
    end

    assert_redirected_to bad_symptom_url(BadSymptom.last)
  end

  test "should show bad_symptom" do
    get bad_symptom_url(@bad_symptom)
    assert_response :success
  end

  test "should get edit" do
    get edit_bad_symptom_url(@bad_symptom)
    assert_response :success
  end

  test "should update bad_symptom" do
    patch bad_symptom_url(@bad_symptom), params: {bad_symptom: {name: @bad_symptom.name}}
    assert_redirected_to bad_symptom_url(@bad_symptom)
  end

  test "should destroy bad_symptom" do
    assert_difference("BadSymptom.count", -1) do
      delete bad_symptom_url(@bad_symptom)
    end

    assert_redirected_to bad_symptoms_url
  end
end
