require "csv"

desc "Import Init Data User Klass Teacher from csv file"
task import_init_data_user_klass_teacher: [:environment] do
  csv_text1 = File.read("db/for_init/user_klass_teachers.csv")
  csv1 = CSV.parse(csv_text1, headers: true)
  csv1.each do |row|
    User.create!(row.to_hash)
  end

  ActiveRecord::Base.connection.execute("
    select setval('users_id_seq', m)
	from (
	    select max(id) from users
	    ) as dt(m)
	")
end
