require "test_helper"

class PlanDeviceMaintenancesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @plan_device_maintenance = plan_device_maintenances(:one)
  end

  test "should get index" do
    get plan_device_maintenances_url
    assert_response :success
  end

  test "should get new" do
    get new_plan_device_maintenance_url
    assert_response :success
  end

  test "should create plan_device_maintenance" do
    assert_difference("PlanDeviceMaintenance.count") do
      post plan_device_maintenances_url, params: {plan_device_maintenance: {maintenance_id: @plan_device_maintenance.maintenance_id, plan_device_id: @plan_device_maintenance.plan_device_id}}
    end

    assert_redirected_to plan_device_maintenance_url(PlanDeviceMaintenance.last)
  end

  test "should show plan_device_maintenance" do
    get plan_device_maintenance_url(@plan_device_maintenance)
    assert_response :success
  end

  test "should get edit" do
    get edit_plan_device_maintenance_url(@plan_device_maintenance)
    assert_response :success
  end

  test "should update plan_device_maintenance" do
    patch plan_device_maintenance_url(@plan_device_maintenance), params: {plan_device_maintenance: {maintenance_id: @plan_device_maintenance.maintenance_id, plan_device_id: @plan_device_maintenance.plan_device_id}}
    assert_redirected_to plan_device_maintenance_url(@plan_device_maintenance)
  end

  test "should destroy plan_device_maintenance" do
    assert_difference("PlanDeviceMaintenance.count", -1) do
      delete plan_device_maintenance_url(@plan_device_maintenance)
    end

    assert_redirected_to plan_device_maintenances_url
  end
end
