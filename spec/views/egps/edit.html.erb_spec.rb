require "rails_helper"

RSpec.describe "egps/edit", type: :view do
  before(:each) do
    @egp = assign(:egp, Egp.create!(
      dep_id: "MyString",
      category: "MyString",
      title: "MyString",
      link: "MyString",
      description: "MyText",
      status: "MyString"
    ))
  end

  it "renders the edit egp form" do
    render

    assert_select "form[action=?][method=?]", egp_path(@egp), "post" do
      assert_select "input[name=?]", "egp[dep_id]"

      assert_select "input[name=?]", "egp[category]"

      assert_select "input[name=?]", "egp[title]"

      assert_select "input[name=?]", "egp[link]"

      assert_select "textarea[name=?]", "egp[description]"

      assert_select "input[name=?]", "egp[status]"
    end
  end
end
