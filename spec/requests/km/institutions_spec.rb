require "rails_helper"

RSpec.describe "Km::Institutions", type: :request do
  describe "GET /index" do
    it "returns http success" do
      get "/km/institutions/index"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /show" do
    it "returns http success" do
      get "/km/institutions/show"
      expect(response).to have_http_status(:success)
    end
  end
end
