$('.selectize-plan').selectize({
	sortField: 'text',
	create: function(input, callback) {
		$.post('/plans.json', { plan: { title: input } })
		.done(function(response){
			console.log(response)
			callback({value: response.id, text: response.title });
		})
	}
}); 

$('.selectize-responsible').selectize({
	sortField: 'text',
	create: function(input, callback) {
		$.post('/responsibles.json', { responsible: { title: input } })
		.done(function(response){
			console.log(response)
			callback({value: response.id, text: response.title });
		})
	}
}); 
$('.selectize-institution').selectize({
	sortField: 'text',
	create: function(input, callback) {
		$.post('/institutions.json', { institution: { title: input } })
		.done(function(response){
			console.log(response)
			callback({value: response.id, text: response.title });
		})
	}
});

$('.selectize-issue').selectize({
	sortField: 'text',
	create: function(input, callback) {
		$.post('/issues.json', { issue: { title: input } })
		.done(function(response){
			console.log(response)
			callback({value: response.id, text: response.title });
		})
	}
}); 


$('.selectize-strategic_issue').selectize({
	sortField: 'text',
	create: function(input, callback) {
		$.post('/strategic_issue.json', { strategic_issue: { title: input } })
		.done(function(response){
			console.log(response)
			callback({value: response.id, text: response.title });
		})
	}
}); 


$('.selectize-kpi').selectize({
	sortField: 'text',
	create: function(input, callback) {
		$.post('/kpis.json', { kpi: { title: input } })
		.done(function(response){
			console.log(response)
			callback({value: response.id, text: response.title });
		})
	}
}); 

$('.selectize-target_kpi').selectize({
	sortField: 'text',
	create: function(input, callback) {
		$.post('/target_kpis.json', { target_kpi: { title: input } })
		.done(function(response){
			console.log(response)
			callback({value: response.id, text: response.title });
		})
	}
}); 


$('.selectize-activity').selectize({
	sortField: 'text',
	create: function(input, callback) {
		$.post('/activities.json', { plan: { title: input } })
		.done(function(response){
			console.log(response)
			callback({value: response.id, text: response.title });
		})
	}
}); 

