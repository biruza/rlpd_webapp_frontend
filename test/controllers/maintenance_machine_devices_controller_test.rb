require "test_helper"

class MaintenanceMachineDevicesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @maintenance_machine_device = maintenance_machine_devices(:one)
  end

  test "should get index" do
    get maintenance_machine_devices_url
    assert_response :success
  end

  test "should get new" do
    get new_maintenance_machine_device_url
    assert_response :success
  end

  test "should create maintenance_machine_device" do
    assert_difference("MaintenanceMachineDevice.count") do
      post maintenance_machine_devices_url, params: {maintenance_machine_device: {frequency: @maintenance_machine_device.frequency, machine_device_id: @maintenance_machine_device.machine_device_id, maintenance_id: @maintenance_machine_device.maintenance_id, test_method: @maintenance_machine_device.test_method, time_spent: @maintenance_machine_device.time_spent}}
    end

    assert_redirected_to maintenance_machine_device_url(MaintenanceMachineDevice.last)
  end

  test "should show maintenance_machine_device" do
    get maintenance_machine_device_url(@maintenance_machine_device)
    assert_response :success
  end

  test "should get edit" do
    get edit_maintenance_machine_device_url(@maintenance_machine_device)
    assert_response :success
  end

  test "should update maintenance_machine_device" do
    patch maintenance_machine_device_url(@maintenance_machine_device), params: {maintenance_machine_device: {frequency: @maintenance_machine_device.frequency, machine_device_id: @maintenance_machine_device.machine_device_id, maintenance_id: @maintenance_machine_device.maintenance_id, test_method: @maintenance_machine_device.test_method, time_spent: @maintenance_machine_device.time_spent}}
    assert_redirected_to maintenance_machine_device_url(@maintenance_machine_device)
  end

  test "should destroy maintenance_machine_device" do
    assert_difference("MaintenanceMachineDevice.count", -1) do
      delete maintenance_machine_device_url(@maintenance_machine_device)
    end

    assert_redirected_to maintenance_machine_devices_url
  end
end
