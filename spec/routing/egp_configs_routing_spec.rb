require "rails_helper"

RSpec.describe EgpConfigsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/egp_configs").to route_to("egp_configs#index")
    end

    it "routes to #new" do
      expect(get: "/egp_configs/new").to route_to("egp_configs#new")
    end

    it "routes to #show" do
      expect(get: "/egp_configs/1").to route_to("egp_configs#show", id: "1")
    end

    it "routes to #edit" do
      expect(get: "/egp_configs/1/edit").to route_to("egp_configs#edit", id: "1")
    end

    it "routes to #create" do
      expect(post: "/egp_configs").to route_to("egp_configs#create")
    end

    it "routes to #update via PUT" do
      expect(put: "/egp_configs/1").to route_to("egp_configs#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/egp_configs/1").to route_to("egp_configs#update", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/egp_configs/1").to route_to("egp_configs#destroy", id: "1")
    end
  end
end
