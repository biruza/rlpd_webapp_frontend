require "rails_helper"

RSpec.describe "task_users/index", type: :view do
  before(:each) do
    assign(:task_users, [
      TaskUser.create!(
        task: nil,
        user: nil,
        primary: false,
        status: 2,
        biz: nil
      ),
      TaskUser.create!(
        task: nil,
        user: nil,
        primary: false,
        status: 2,
        biz: nil
      )
    ])
  end

  it "renders a list of task_users" do
    render
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: false.to_s, count: 2
    assert_select "tr>td", text: 2.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
  end
end
