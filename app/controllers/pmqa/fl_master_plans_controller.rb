class Pmqa::FlMasterPlansController < ApplicationController
  before_action :set_select, only: %i[new create edit update]
  def index
    url = URI("#{$server[@controller_title]}/api/v1/fl_fl_master_plans")
    @datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/fl_fl_master_plans/#{params["id"]}")
    @datas = request_api(url, "get", nil)

    url_fl_fl_master_plan_user = URI("#{$server[@controller_title]}/api/v1/fl_fl_master_plans/#{params["id"]}/fl_fl_master_plan_users?order=order_sort&sort=asc")
    @fl_master_plan_users = request_api(url_fl_master_plan_user, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/fl_master_plans/#{params["id"]}/fl_master_plan_user_comments")
    @user_commments = request_api(url, "get", nil)
  end

  def new
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/fl_master_plans/#{params["id"]}")

    @datas = request_api(url, "get", nil)

    # puts "plan edit = #{@plan['title']}"
  end

  def create
    # params[:fl_master_plan][:user_id] = $user[session.id.to_s][@controller_title]['id']
    # params[:fl_master_plan][:biz_id] = $user[session.id.to_s][@controller_title]['biz_id']

    url = URI("#{$server[@controller_title]}/api/v1/fl_master_plans")
    @datas = request_api(url, "post", fl_master_plan_params)

    if @datas["status"] == "Success"

      url = URI("#{$server[@controller_title]}/api/v1/fl_master_plans/#{@datas["data"]["id"]}")
      @datas = request_api(url, "get", nil)

      url_category_masters = URI("#{$server[@controller_title]}/api/v1/category_masters")
      url_plans = URI("#{$server[@controller_title]}/api/v1/plans")
      url_categories = URI("#{$server[@controller_title]}/api/v1/categories")
      url_lists = URI("#{$server[@controller_title]}/api/v1/lists")

      @category_masters = request_api(url_category_masters, "get", nil)

      @category_masters["data"].each do |category_master|
        plan_params_create = {
          plan: {
            fl_master_plan_id: @datas["data"]["id"],
            category_master_id: category_master["id"],
            user_id: @datas["data"]["user_id"],
            biz_id: @datas["data"]["biz_id"],
            title: category_master["title"],
            order_sort: category_master["order_sort"]
          }
        }
        plan_new = request_api(url_plans, "post", plan_params_create)

        category_master["category_master_lists"].each do |category_master_list|
          category_params_create = {
            category: {
              plan_id: plan_new["data"]["id"],
              category_master_list_id: category_master_list["id"],
              user_id: @datas["data"]["user_id"],
              biz_id: @datas["data"]["biz_id"],
              title: category_master_list["title"],
              order_sort: category_master_list["order_sort"]
            }
          }
          category_new = request_api(url_categories, "post", category_params_create)

          category_master_list["category_master_list_items"].each do |category_master_list_item|
            list_params_create = {
              list: {
                category_id: category_new["data"]["id"],
                category_master_list_item_id: category_master_list_item["id"],
                user_id: @datas["data"]["user_id"],
                biz_id: @datas["data"]["biz_id"],
                title: category_master_list_item["title"],
                order_sort: category_master_list_item["order_sort"]
              }
            }
            list_new = request_api(url_lists, "post", list_params_create)
          end # loop category_master_list['category_master_list_items']
        end # loop category_master['category_master_list']
      end # loop @category_masters['data']

      redirect_to "/#{@controller_title}/fl_master_plans", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_title}/fl_master_plans/new", alert: @datas["data"]
    end
  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/fl_master_plans/#{params["id"]}")
    @datas = request_api(url, "patch", fl_master_plan_params)

    # puts "update status : #{res}"

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/fl_master_plans/#{params[:id]}", notice: @datas["message"]
    else
      redirect_to "/#{@controller_title}/fl_master_plans/#{params[:id]}/edit", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/fl_master_plans/#{params["id"]}")
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/fl_master_plans", notice: @datas["message"]
    else
      redirect_to "/#{@controller_title}/fl_master_plans", alert: @datas["data"]
    end
  end

  def print
    url = URI("#{$server[@controller_title]}/api/v1/fl_master_plans/#{params["id"]}")
    @datas = request_api(url, "get", nil)
  end

  private
  def set_select

    url_ins = URI("#{$server[@controller_title]}/api/v1/institutions")
    @institution_datas = request_api(url_ins, "get", nil)

  end
  def fl_master_plan_params
    params.require(:fl_master_plan).permit(:title, :description, :user_id, :biz_id)
  end
end
