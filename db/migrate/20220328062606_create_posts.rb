class CreatePosts < ActiveRecord::Migration[6.0]
  def change
    create_table :posts, id: :uuid do |t|
      t.string :title
      t.text :image_data
      t.references :biz, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
