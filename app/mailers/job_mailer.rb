class JobMailer < ApplicationMailer
  def send_jobs_email(email_to, subject, email_from, name_form, doc, doc_number, message_top, message_middle, message_bottom)
    if !doc.blank?
      @doc = doc
      @biz = Biz.find(@doc.biz_id)
    end

    @message_top = message_top
    @message_middle = message_middle
    @message_bottom = message_bottom

    if !doc.blank?
      attachments["#{doc_number}.pdf"] = WickedPdf.new.pdf_from_string(
        render_to_string(
          pdf: "todo",
          page_size: "A4",
          template: "#{doc.class.name.tableize}/print.pdf.erb",
          layout: "pdf.pdf.erb",
          orientation: "Portrait",
          lowquality: true,
          zoom: 1,
          dpi: 75
        )
      )
    end

    if !doc.blank?
      mail(to: email_to, subject: subject, from: email_from, from_name: name_form, todo: doc)
    else
      mail(to: email_to, subject: subject, from: email_from, from_name: name_form)
    end
  end

  def send_email_md_orga(email_to, subject, email_from, name_form, order, message)
    @order = order
    @message = message

    mail(to: email_to, subject: subject, from: email_from, from_name: name_form)
  end
end
