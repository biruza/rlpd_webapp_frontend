require "test_helper"

class ExpenseStudentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @expense_student = expense_students(:one)
  end

  test "should get index" do
    get expense_students_url
    assert_response :success
  end

  test "should get new" do
    get new_expense_student_url
    assert_response :success
  end

  test "should create expense_student" do
    assert_difference("ExpenseStudent.count") do
      post expense_students_url, params: {expense_student: {description: @expense_student.description, end_date: @expense_student.end_date, expense_id: @expense_student.expense_id, klass_id: @expense_student.klass_id, start_date: @expense_student.start_date, student_id: @expense_student.student_id, user_id: @expense_student.user_id}}
    end

    assert_redirected_to expense_student_url(ExpenseStudent.last)
  end

  test "should show expense_student" do
    get expense_student_url(@expense_student)
    assert_response :success
  end

  test "should get edit" do
    get edit_expense_student_url(@expense_student)
    assert_response :success
  end

  test "should update expense_student" do
    patch expense_student_url(@expense_student), params: {expense_student: {description: @expense_student.description, end_date: @expense_student.end_date, expense_id: @expense_student.expense_id, klass_id: @expense_student.klass_id, start_date: @expense_student.start_date, student_id: @expense_student.student_id, user_id: @expense_student.user_id}}
    assert_redirected_to expense_student_url(@expense_student)
  end

  test "should destroy expense_student" do
    assert_difference("ExpenseStudent.count", -1) do
      delete expense_student_url(@expense_student)
    end

    assert_redirected_to expense_students_url
  end
end
