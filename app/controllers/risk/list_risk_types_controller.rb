class Risk::ListRiskTypesController < ApplicationController
  before_action :set_select, :set_default,  only: %i[new create edit update destroy]
  def index
    url = URI("#{$server[@controller_title]}/api/v1/list_risk_types")
    @datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/list_risk_types/#{params["id"]}")
    @datas = request_api(url, "get", nil)
  end

  def edit
  end

  def new
    #url = URI("#{$server[@controller_title]}/api/v1/list_risk_types/#{params["list_risk_factor_id"]}/get_list_risk_type_by_list_risk_factor")
    #@datas = request_api(url, "get", nil)

  
    $plan_report = "#{params['original_url']}"
  end

  def create


    url = URI("#{$server[@controller_title]}/api/v1/list_risk_types")
    @datas = request_api(url, "post", list_risk_type_params)
 
    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/lists/#{@datas['data']['list_id']}/edit?original_url=#{$plan_report}", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_main_url}/list_risk_types/new", alert: @datas["data"]
    end

  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/list_risk_types/#{params["id"]}")
    @datas = request_api(url, "patch", list_risk_type_params)

    # puts "update status : #{res}"

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/list_risk_types", notice: @datas["message"]
    else
      redirect_to "/#{@controller_main_url}/list_risk_types", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/list_risk_types/#{params["id"]}")
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
     # redirect_to "/#{@controller_main_url}/list_risk_types", notice: @datas["message"]
     redirect_to "/#{@controller_main_url}/lists/#{@datas["data"]["list_id"]}/edit?original_url=#{params['original_url']}", notice: @datas["message"]
    else
      redirect_to "/#{@controller_main_url}/list_risk_types", alert: @datas["data"]
    end
  end



  private
  def set_user
    @user = User.find(auth_token[:user_id])
  end

  def set_default
    # @sub_system = "pmqa_fl"
    # @controller_main_url = "#{@controller_title}/#{@sub_system}"
    @controller_main_url = @controller_title
  end

  def set_select
    url = URI("#{$server[@controller_title]}/api/v1/risk_types")
    @risk_type_datas = request_api(url, "get", nil)
  end

  def list_risk_type_params
    params.require(:list_risk_type).permit(:list_risk_factor_id, :risk_type_id, :slug, :order_sort, :list_id, :user_id, :biz_id)
  end

end
