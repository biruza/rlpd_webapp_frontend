require "test_helper"

class ModuleWebsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @module_web = module_webs(:one)
  end

  test "should get index" do
    get module_webs_url
    assert_response :success
  end

  test "should get new" do
    get new_module_web_url
    assert_response :success
  end

  test "should create module_web" do
    assert_difference("ModuleWeb.count") do
      post module_webs_url, params: {module_web: {aboutus_h1: @module_web.aboutus_h1, aboutus_image: @module_web.aboutus_image, aboutus_text: @module_web.aboutus_text, biz_id: @module_web.biz_id, color_primary: @module_web.color_primary, color_secondary: @module_web.color_secondary, color_third: @module_web.color_third, favicon: @module_web.favicon, gallery_h1: @module_web.gallery_h1, gallery_text: @module_web.gallery_text, intro_h1: @module_web.intro_h1, intro_text: @module_web.intro_text, logo_dark: @module_web.logo_dark, logo_light: @module_web.logo_light, s: @module_web.s, section_enable: @module_web.section_enable, service_h1: @module_web.service_h1, service_image: @module_web.service_image, service_text: @module_web.service_text, status: @module_web.status}}
    end

    assert_redirected_to module_web_url(ModuleWeb.last)
  end

  test "should show module_web" do
    get module_web_url(@module_web)
    assert_response :success
  end

  test "should get edit" do
    get edit_module_web_url(@module_web)
    assert_response :success
  end

  test "should update module_web" do
    patch module_web_url(@module_web), params: {module_web: {aboutus_h1: @module_web.aboutus_h1, aboutus_image: @module_web.aboutus_image, aboutus_text: @module_web.aboutus_text, biz_id: @module_web.biz_id, color_primary: @module_web.color_primary, color_secondary: @module_web.color_secondary, color_third: @module_web.color_third, favicon: @module_web.favicon, gallery_h1: @module_web.gallery_h1, gallery_text: @module_web.gallery_text, intro_h1: @module_web.intro_h1, intro_text: @module_web.intro_text, logo_dark: @module_web.logo_dark, logo_light: @module_web.logo_light, s: @module_web.s, section_enable: @module_web.section_enable, service_h1: @module_web.service_h1, service_image: @module_web.service_image, service_text: @module_web.service_text, status: @module_web.status}}
    assert_redirected_to module_web_url(@module_web)
  end

  test "should destroy module_web" do
    assert_difference("ModuleWeb.count", -1) do
      delete module_web_url(@module_web)
    end

    assert_redirected_to module_webs_url
  end
end
