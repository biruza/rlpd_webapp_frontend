class Pmqa::DepartmentsController < ApplicationController
  def index
    url = URI("#{$server[@controller_title]}/api/v1/departments")

    @datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/departments/#{params["id"]}")
    @datas = request_api(url, "get", nil)
  end

  def new
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/departments/#{params["id"]}")

    @datas = request_api(url, "get", nil)

    # puts "plan edit = #{@plan['title']}"
  end

  def create
    params[:department][:user_id] = $user_id
    params[:department][:biz_id] = $biz_id

    url = URI("#{$server[@controller_title]}/api/v1/departments")
    @datas = request_api(url, "post", department_params)

    if @datas["status"] == "Success"
      redirect_to "/km/departments", notice: @datas["messages"]
    else
      redirect_to "/km/departments/new", alert: @datas["data"]
    end
  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/departments/#{params["id"]}")
    @datas = request_api(url, "patch", department_params)

    # puts "update status : #{res}"

    if @datas["status"] == "Success"
      redirect_to "/km/departments", notice: @datas["message"]
    else
      redirect_to "/km/departments", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/departments/#{params["id"]}")
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
      redirect_to "/km/departments", notice: @datas["message"]
    else
      redirect_to "/km/departments", alert: @datas["data"]
    end
  end

  private

  def department_params
    params.require(:department).permit(:title, :short_name, :slug, :description, :user_id, :biz_id)
  end

  def authenticate_user!
    if user_signed_in?
      super
    else
      redirect_to "accounts/sign_in", notice: "Please Login to view that page!"
    end
  end
end
