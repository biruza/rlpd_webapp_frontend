require "test_helper"

class WebBlogsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @web_blog = web_blogs(:one)
  end

  test "should get index" do
    get web_blogs_url
    assert_response :success
  end

  test "should get new" do
    get new_web_blog_url
    assert_response :success
  end

  test "should create web_blog" do
    assert_difference("WebBlog.count") do
      post web_blogs_url, params: {web_blog: {biz_id: @web_blog.biz_id, description_th: @web_blog.description_th, end_date: @web_blog.end_date, image: @web_blog.image, module_web_id: @web_blog.module_web_id, start_date: @web_blog.start_date, title_th: @web_blog.title_th, web_blog_category_id: @web_blog.web_blog_category_id}}
    end

    assert_redirected_to web_blog_url(WebBlog.last)
  end

  test "should show web_blog" do
    get web_blog_url(@web_blog)
    assert_response :success
  end

  test "should get edit" do
    get edit_web_blog_url(@web_blog)
    assert_response :success
  end

  test "should update web_blog" do
    patch web_blog_url(@web_blog), params: {web_blog: {biz_id: @web_blog.biz_id, description_th: @web_blog.description_th, end_date: @web_blog.end_date, image: @web_blog.image, module_web_id: @web_blog.module_web_id, start_date: @web_blog.start_date, title_th: @web_blog.title_th, web_blog_category_id: @web_blog.web_blog_category_id}}
    assert_redirected_to web_blog_url(@web_blog)
  end

  test "should destroy web_blog" do
    assert_difference("WebBlog.count", -1) do
      delete web_blog_url(@web_blog)
    end

    assert_redirected_to web_blogs_url
  end
end
