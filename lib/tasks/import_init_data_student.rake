require "csv"

desc "Import Init Data Student from csv file"
task import_init_data_student: [:environment] do
  csv_text1 = File.read("db/for_init/students.csv")
  csv1 = CSV.parse(csv_text1, headers: true)
  csv1.each do |row|
    Student.create!(row.to_hash)
  end

  ActiveRecord::Base.connection.execute("
    select setval('students_id_seq', m)
	from (
	    select max(id) from students
	    ) as dt(m)
	")
end
