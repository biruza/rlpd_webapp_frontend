
$('.selectize-activity').selectize({
	sortField: 'text',
	create: function(input, callback) {
		$.post('/activities.json', { activity: { title: input } })
		.done(function(response){
			console.log(response)
			callback({value: response.id, text: response.title });
		})
	}
}); 

$('.selectize-devision_director').selectize({
	sortField: 'text',
	create: function(input, callback) {
		$.post('/devision_directors.json', { devision_director: { title: input } })
		.done(function(response){
			console.log(response)
			callback({value: response.id, text: response.title });
		})
	}
}); 
