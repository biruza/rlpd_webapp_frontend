# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_07_04_033814) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "ahoy_events", force: :cascade do |t|
    t.bigint "visit_id"
    t.bigint "user_id"
    t.string "name"
    t.jsonb "properties"
    t.datetime "time"
    t.index ["name", "time"], name: "index_ahoy_events_on_name_and_time"
    t.index ["properties"], name: "index_ahoy_events_on_properties", opclass: :jsonb_path_ops, using: :gin
    t.index ["user_id"], name: "index_ahoy_events_on_user_id"
    t.index ["visit_id"], name: "index_ahoy_events_on_visit_id"
  end

  create_table "ahoy_visits", force: :cascade do |t|
    t.string "visit_token"
    t.string "visitor_token"
    t.bigint "user_id"
    t.string "ip"
    t.text "user_agent"
    t.text "referrer"
    t.string "referring_domain"
    t.text "landing_page"
    t.string "browser"
    t.string "os"
    t.string "device_type"
    t.string "country"
    t.string "region"
    t.string "city"
    t.float "latitude"
    t.float "longitude"
    t.string "utm_source"
    t.string "utm_medium"
    t.string "utm_term"
    t.string "utm_content"
    t.string "utm_campaign"
    t.string "app_version"
    t.string "os_version"
    t.string "platform"
    t.datetime "started_at"
    t.index ["user_id"], name: "index_ahoy_visits_on_user_id"
    t.index ["visit_token"], name: "index_ahoy_visits_on_visit_token", unique: true
  end

  create_table "bizs", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.datetime "start_date"
    t.integer "qty_package"
    t.boolean "status"
    t.string "title_th"
    t.string "title_en"
    t.text "address_th"
    t.text "address_en"
    t.string "phone"
    t.string "fax"
    t.string "email"
    t.string "taxid"
    t.text "image_data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "branch"
    t.string "doc_type_re"
    t.string "doc_type_iv"
    t.string "doc_type_qt"
    t.integer "doc_type_re_start"
    t.integer "doc_type_iv_start"
    t.integer "doc_type_qt_start"
    t.text "doc_type_re_note"
    t.text "doc_type_iv_note"
    t.text "doc_type_qt_note"
    t.string "doc_type_re_sign"
    t.string "doc_type_iv_sign"
    t.string "doc_type_qt_sign"
    t.text "stamp_data"
    t.text "fb"
    t.text "tw"
    t.text "ig"
    t.text "yt"
    t.text "googlemap"
    t.string "doc_type_ex"
    t.integer "doc_type_ex_start"
    t.text "doc_type_ex_note"
    t.string "doc_type_ex_sign"
    t.text "doc_type_re_sign_image_data"
    t.text "doc_type_iv_sign_image_data"
    t.text "doc_type_qt_sign_image_data"
    t.text "doc_type_ex_sign_image_data"
    t.string "li"
    t.text "fb_url"
    t.text "tw_url"
    t.text "ig_url"
    t.text "yt_url"
    t.text "li_url"
    t.integer "task_type", default: 1
    t.string "domain"
    t.string "subdomain"
  end

  create_table "jwt_blacklist", force: :cascade do |t|
    t.string "jti", null: false
    t.index ["jti"], name: "index_jwt_blacklist_on_jti"
  end

  create_table "km_master_plans", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "title"
    t.datetime "period_year"
    t.string "status"
    t.text "description"
    t.text "slug"
    t.uuid "user_id", null: false
    t.uuid "biz_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["biz_id"], name: "index_km_master_plans_on_biz_id"
    t.index ["user_id"], name: "index_km_master_plans_on_user_id"
  end

  create_table "locations", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "title"
    t.text "image_data"
    t.text "description"
    t.decimal "lat"
    t.decimal "lng"
    t.string "slug"
    t.uuid "user_id", null: false
    t.uuid "biz_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["biz_id"], name: "index_locations_on_biz_id"
    t.index ["user_id"], name: "index_locations_on_user_id"
  end

  create_table "notifications", force: :cascade do |t|
    t.string "recipient_type", null: false
    t.uuid "recipient_id", null: false
    t.string "type", null: false
    t.jsonb "params"
    t.datetime "read_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["read_at"], name: "index_notifications_on_read_at"
    t.index ["recipient_type", "recipient_id"], name: "index_notifications_on_recipient"
  end

  create_table "plans", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "title"
    t.datetime "period"
    t.text "description"
    t.string "slug"
    t.string "status"
    t.uuid "user_id"
    t.uuid "biz_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["biz_id"], name: "index_plans_on_biz_id"
    t.index ["user_id"], name: "index_plans_on_user_id"
  end

  create_table "posts", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "title"
    t.text "image_data"
    t.uuid "biz_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["biz_id"], name: "index_posts_on_biz_id"
  end

  create_table "taggings", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid "tag_id"
    t.string "taggable_type"
    t.uuid "taggable_id"
    t.string "tagger_type"
    t.uuid "tagger_id"
    t.string "context"
    t.datetime "created_at"
    t.index ["context"], name: "index_taggings_on_context"
    t.index ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true
    t.index ["tag_id"], name: "index_taggings_on_tag_id"
    t.index ["taggable_id", "taggable_type", "context"], name: "taggings_taggable_context_idx"
    t.index ["taggable_id", "taggable_type", "tagger_id", "context"], name: "taggings_idy"
    t.index ["taggable_id"], name: "index_taggings_on_taggable_id"
    t.index ["taggable_type"], name: "index_taggings_on_taggable_type"
    t.index ["tagger_id", "tagger_type"], name: "index_taggings_on_tagger_id_and_tagger_type"
    t.index ["tagger_id"], name: "index_taggings_on_tagger_id"
  end

  create_table "tags", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "taggings_count", default: 0
    t.index ["name"], name: "index_tags_on_name", unique: true
  end

  create_table "users", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "firstname"
    t.string "lastname"
    t.text "avatar_data"
    t.boolean "superadmin_role", default: false
    t.boolean "customer_role", default: false
    t.boolean "user_role", default: true
    t.boolean "registration_role", default: false
    t.string "provider", default: "email", null: false
    t.string "uid", default: "", null: false
    t.text "tokens"
    t.integer "prefix"
    t.string "percode"
    t.string "nation"
    t.string "race"
    t.string "origin"
    t.string "phone"
    t.string "mobile"
    t.text "address"
    t.string "province"
    t.string "zipcode"
    t.string "job"
    t.text "job_location"
    t.string "job_phone"
    t.decimal "salary"
    t.uuid "biz_id"
    t.string "amphoe"
    t.string "district"
    t.boolean "administrator_role", default: false
    t.string "ancestry"
    t.boolean "supervisor_role", default: false
    t.index ["ancestry"], name: "index_users_on_ancestry"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "km_master_plans", "bizs"
  add_foreign_key "km_master_plans", "users"
  add_foreign_key "locations", "bizs"
  add_foreign_key "locations", "users"
  add_foreign_key "plans", "bizs"
  add_foreign_key "plans", "users"
  add_foreign_key "posts", "bizs"
end
