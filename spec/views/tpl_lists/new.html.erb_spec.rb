require "rails_helper"

RSpec.describe "tpl_lists/new", type: :view do
  before(:each) do
    assign(:tpl_list, TplList.new(
      list: nil,
      biz: nil
    ))
  end

  it "renders new tpl_list form" do
    render

    assert_select "form[action=?][method=?]", tpl_lists_path, "post" do
      assert_select "input[name=?]", "tpl_list[list_id]"

      assert_select "input[name=?]", "tpl_list[biz_id]"
    end
  end
end
