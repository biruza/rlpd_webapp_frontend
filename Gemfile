source "https://rubygems.org"

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

gem "bootsnap", require: false

# ruby '2.6.6'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem "rails", "~> 6.0"

# gem for upgrade 6.0
gem "activesupport", ">= 5.2.5"
gem "actionpack"
# gem 'devise'
gem "devise", "= 4.7.0"
# gem 'sass-rails'
gem "sass-rails", "~> 6.0"
# gem 'sass-rails', '~> 5.0'
gem "railties"
####

# Use postgresql as the database for Active Record
gem "pg", ">= 0.18", "< 2.0"
# Use Puma as the app server
gem "puma", "~> 3.7"
# Use SCSS for stylesheets

# Use Uglifier as compressor for JavaScript assets
gem "uglifier", ">= 1.3.0"
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

gem "terser"

# Use CoffeeScript for .coffee assets and views
gem "coffee-rails", "~> 4.2"
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
# gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem "jbuilder", "~> 2.5"
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
gem "redis"
gem "redis-namespace"
gem "redis-rails"
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

gem "net-ldap", "~> 0.15.0"

# for mobile app
# gem 'devise_token_auth'
# gem 'devise-jwt', '~> 0.5.8'
gem "jwt"
# gem 'knock'
# gem 'knock', github: 'psantos10/knock', branch: 'master'

gem "bcrypt", git: "https://github.com/codahale/bcrypt-ruby.git", require: "bcrypt"

gem "will_paginate", "~> 3.1.0"

gem "jquery-rails"

gem "bootstrap-sass"

# Shrine
gem "sucker_punch", "~> 2.0"
gem "dotenv-rails"
gem "shrine", "~> 2.11"
gem "aws-sdk-s3", "~> 1.2"
gem "image_processing", "~> 1.0"

# gem 'social-share-button', '0.2.1'

gem "rails_refactor"

# role
#gem "cancancan", "~> 3.5.0"
####

gem "activerecord-import"

gem "activerecord-reset-pk-sequence"

# gem 'time_difference'

# calendar
gem "fullcalendar-rails"
gem "momentjs-rails"

# qrcode
gem "rqrcode"

# noti
# gem 'activity_notification'
gem "notifications"

# for time thai
# gem 'r18n-rails'

# for editor
gem "x-editable-rails"

# for error capistrano
gem "ed25519", ">= 1.2", "< 2.0"
gem "bcrypt_pbkdf", ">= 1.0", "< 2.0"
# แก้ไขเรื่อง javascript
# gem 'therubyracer'
# gem 'mini_racer', platforms: :ruby

# for debug
gem "better_errors"
gem "sentry-raven"

# gem 'bootstrap-generators', git: 'git://github.com/decioferreira/bootstrap-generators.git'

# rails-generate-pdf/Gemfile
gem "bootstrap", "~> 4.1.3"
gem "wicked_pdf"
gem "wkhtmltopdf-binary"
## end

gem "bootstrap-colorpicker-rails", "~> 0.4.0"

gem "rake", "12.3.3"

# for tag & select 2
gem 'acts-as-taggable-on', '~> 6.0'
gem "selectize-rails"
gem "select2-rails"
#gem "acts-as-taggable-on", git: "https://github.com/mbleigh/acts-as-taggable-on"
# gem 'acts-as-taggable-on', '~> 7.0'

gem "recaptcha"

# for pagination
gem "pagy"

# color
gem "chroma"

# for search
gem "has_scope"

# for friendly
gem "friendly_id"

# for Opti
# gem 'rack-mini-profiler'

# for Speed
# gem 'brotli'
gem "lazyload-rails"
# เหมือนว่า webpacker ไม่ต้องใช้เพระ rails 5 มันมีตัว pack อยู่แล้ว ดูในไฟล์​ base_web_pro.js , css.scss
# gem 'webpacker', '~> 4.x'
####

# for react
gem "webpacker"
gem "react-rails"

# child & parent
gem "ancestry"

# nested
# gem 'cocoon'

# real time
gem "hotwire-rails"

# schedule cron job
gem "whenever", require: false

# xml
gem "nokogiri", "~> 1.6", ">= 1.6.8"
gem "xmlhasher"

gem "htmltoword"

# for mobile app
gem "rack-cors"

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem "byebug", platforms: [:mri, :mingw, :x64_mingw]
  # Adds support for Capybara system testing and selenium driver
  gem "capybara", "~> 2.13"
  gem "selenium-webdriver"

  # for api
  gem "rspec-rails", ">= 3.5.0"
  # Use Factory Girl for generating random test data
  # gem 'factory_girl_rails'
  ####
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem "web-console", ">= 3.3.0"
  gem "listen", ">= 3.0.5", "< 3.2"
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem "spring"
  gem "spring-watcher-listen", "~> 2.0.0"
end

group :development, :test do
  gem "mailcatcher"

  gem "standardrb"

  # for deployment
  gem "capistrano"
  gem "capistrano-bundler"
  gem "capistrano-rails"
  gem "capistrano-rails-console"
  gem "capistrano-rails-db"
  gem "capistrano-rails-log"
  gem "capistrano-rbenv"
  gem "capistrano-yarn"
  gem "capistrano-db-tasks", require: false
end

gem "actionview-encoded_mail_to"
gem "sidekiq"
gem "redis-rails"

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem "tzinfo-data", platforms: [:mingw, :mswin, :x64_mingw, :jruby]

gem "noticed", "~> 1.5"

gem "ahoy_matey", "~> 4.1"

gem "awesome_print", require: "ap"
