class Risk::FollowUpRiskReportsController < ApplicationController
  before_action :set_default, only: %i[index show new edit create update destroy print]
  before_action :set_select, only: %i[new create edit update]

  def index
    url = URI("#{$server[@controller_title]}/api/v1/follow_up_risk_reports?order=updated_at&sort=desc&limit=10")
    @follow_up_risk_reports = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/follow_up_risk_reports/#{params["id"]}")
    @follow_up_risk_report = request_api(url, "get", nil)
  end

  def new
    if !params[:risk_management_report_id].blank?
      url = URI("#{$server[@controller_title]}/api/v1/risk_management_reports/#{params[:risk_management_report_id]}")
      @risk_management_report = request_api(url, "get", nil)
    else
      redirect_to "/#{@controller_main_url}/master_plans", alert: (begin
        t(:can_not_found)
      rescue
        nil
      end).to_s
    end
  end

  def create
    url = URI("#{$server[@controller_title]}/api/v1/risk_management_reports/#{params[:follow_up_risk_report][:risk_management_report_id]}")
    @risk_management_report = request_api(url, "get", nil)

    follow_up_risk_report_params = {
      risk_management_report_id: params[:follow_up_risk_report][:risk_management_report_id],
      user_id: @risk_management_report["data"]["user_id"],
      biz_id: @risk_management_report["data"]["biz_id"],
      period_report_id: params[:follow_up_risk_report][:period_report_id],
      startdate: params[:follow_up_risk_report][:startdate],
      enddate: params[:follow_up_risk_report][:enddate]
    }

    url = URI("#{$server[@controller_title]}/api/v1/follow_up_risk_reports")
    @datas = request_api(url, "post", follow_up_risk_report_params)

    if @datas["status"] == "Success"
      url = URI("#{$server[@controller_title]}/api/v1/follow_up_risk_reports/#{@datas["data"]["id"]}")
      @follow_up_risk_report = request_api(url, "get", nil)

      @follow_up_risk_report["data"]["risk_management_report"]["risk_managements"].each do |risk_management|
        follow_up_risks_params = {
          risk_management_id: risk_management["id"],
          category_id: risk_management["category_id"],
          list_id: risk_management["list_id"],
          list_risk_factor_id: risk_management['list_risk_factor_id'],
          order_sort: risk_management["order_sort"],
          follow_up_risk_report_id: @follow_up_risk_report["data"]["id"],
          user_id: risk_management["user_id"],
          biz_id: risk_management["biz_id"]
        }
        url_follow_up_risks = URI("#{$server[@controller_title]}/api/v1/follow_up_risks")
        url_follow_up_risks_new = request_api(url_follow_up_risks, "post", follow_up_risks_params)
      end

      redirect_to "/#{@controller_main_url}/follow_up_risk_reports/#{@datas["data"]["id"]}/edit", notice: @datas["messages"]
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/follow_up_risk_reports/#{params["id"]}")
    @follow_up_risk_report = request_api(url, "get", nil)
  end

  def update
    
=begin

    params[:follow_up_risk].each do |item|
      item_params = {
        follow_up_risk: {
          id: params[:follow_up_risk][(item[0]).to_s][:id],
          success_date: params[:follow_up_risk][(item[0]).to_s][:success_date],
          success_date_title: params[:follow_up_risk][(item[0]).to_s][:success_date_title],
          risk_progress: params[:follow_up_risk][(item[0]).to_s][:risk_progress],
          risk_management_results_attributes: {
            "0": {
              follow_up_risk_id: params[:follow_up_risk][(item[0]).to_s][:id],
              title: (params[:follow_up_risk][(item[0]).to_s][:risk_management_results_attributes]["0"][:title] rescue nil),
              user_id: $user[session.id.to_s][@controller_title]["id"],
              biz_id: $user[session.id.to_s][@controller_title]["biz_id"],
              id: (params[:follow_up_risk][(item[0]).to_s][:risk_management_results_attributes]["0"][:id]  rescue nil)
            }
 
          },
          risk_management_issues_attributes: {
            "0": {
              follow_up_risk_id: params[:follow_up_risk][(item[0]).to_s][:id],
              title: (params[:follow_up_risk][(item[0]).to_s][:risk_management_issues_attributes]["0"][:title] rescue nil),
              user_id: $user[session.id.to_s][@controller_title]["id"],
              biz_id: $user[session.id.to_s][@controller_title]["biz_id"],
              id: (params[:follow_up_risk][(item[0]).to_s][:risk_management_issues_attributes]["0"][:id] rescue nil)
            }
          }
        }
      }
=end


params[:follow_up_risk].each do |item|
  $i = 0

  if item[$i] != "risk_management"
  #p "==i==> #{$i} ===#{item[$i]}=== item ====> #{item}"

  item_params = {
    follow_up_risk: {
      id: params[:follow_up_risk][(item[$i]).to_s][:id],
      success_date: params[:follow_up_risk][(item[$i]).to_s][:success_date],
      success_date_title: params[:follow_up_risk][(item[$i]).to_s][:success_date_title],
      risk_progress: params[:follow_up_risk][(item[$i]).to_s][:risk_progress],
      risk_management_results_attributes: {
        "#{$i}": {
          follow_up_risk_id: params[:follow_up_risk][(item[$i]).to_s][:id],
          title: (params[:follow_up_risk][(item[$i]).to_s][:risk_management_results_attributes]["#{$i}"][:title] rescue nil),
          user_id: $user[session.id.to_s][@controller_title]["id"],
          biz_id: $user[session.id.to_s][@controller_title]["biz_id"],
          id: (params[:follow_up_risk][(item[$i]).to_s][:risk_management_results_attributes]["#{$i}"][:id]  rescue nil)
        }

      },
      risk_management_issues_attributes: {
        "#{$i}": {
          follow_up_risk_id: params[:follow_up_risk][(item[$i]).to_s][:id],
          title: (params[:follow_up_risk][(item[$i]).to_s][:risk_management_issues_attributes]["#{$i}"][:title] rescue nil),
          user_id: $user[session.id.to_s][@controller_title]["id"],
          biz_id: $user[session.id.to_s][@controller_title]["biz_id"],
          id: (params[:follow_up_risk][(item[$i]).to_s][:risk_management_issues_attributes]["#{$i}"][:id] rescue nil)
        }
      }
    }
  }

  url = URI("#{$server[@controller_title]}/api/v1/follow_up_risks/#{params[:follow_up_risk][(item[$i]).to_s][:id]}")
  @datas = request_api(url, "patch", item_params)

  $i = $i + 1


    end 
    end

    url_follow_up_risk_report = URI("#{$server[@controller_title]}/api/v1/follow_up_risk_reports/#{params["id"]}")
    @url_follow_up_risk_report = request_api(url_follow_up_risk_report, "get", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/follow_up_risk_reports/#{@url_follow_up_risk_report["data"]["id"]}/edit", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_main_url}/follow_up_risk_reports/#{@url_follow_up_risk_report["data"]["id"]}/edit", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/follow_up_risk_reports/#{params["id"]}")
    @follow_up_risk_report = request_api(url, "get", nil)

    @datas = request_api(url, "delete", nil)

    if @follow_up_risk_report["status"] == "Success"
      redirect_to "/#{@controller_main_url}/plans/#{@follow_up_risk_report["data"]["risk_management_report"]["task_plan_report"]["risk_analysis_report"]["plan_report"]["category"]["plan_id"]}", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_main_url}/plans/#{@follow_up_risk_report["data"]["risk_management_report"]["task_plan_report"]["risk_analysis_report"]["plan_report"]["category"]["plan_id"]}", alert: @datas["data"]
    end
  end

  def print
    url = URI("#{$server[@controller_title]}/api/v1/follow_up_risk_reports/#{params["id"]}")
    @follow_up_risk_report = request_api(url, "get", nil)
  end

  private

  def set_default
    # @sub_system = "pmqa_fl"
    # @controller_main_url = "#{@controller_title}/#{@sub_system}"
    @controller_main_url = @controller_title
  end

  def set_user
    @user = User.find(auth_token[:user_id])
  end

  def set_select
    # url = URI("#{$server[@controller_title]}/api/v1/plans")
    # @data_plans = request_api(url,'get',nil)

    # url = URI("#{$server[@controller_title]}/api/v1/categories")
    # @data_categories = request_api(url,'get',nil)

    # url = URI("#{$server[@controller_title]}/api/v1/master_plans")
    # @data_master_plans = request_api(url,'get',nil)
  end

  def follow_up_risk_report_params
    # params.require(:follow_up_risk_report).permit(:title1, :title2, :startdate, :enddate, :slug, :order_sort, :master_plan, :category, :user, :biz)
    params.require(:follow_up_risk_report).permit(:title1, :title2, :startdate, :enddate, :slug, :order_sort, :risk_management_report_id, :period_report_id, :master_plan_id, :category_id, :plan_id, :user_id, :biz_id,
     follow_up_risks_attributes: [:id, :_destroy, :list_risk_factor_id, :success_date, :success_date_title, :risk_result, :risk_date, :risk_date_title, :risk_progress, :risk_problem, :follow_up_risk_report_id, :risk_management_id, :list_id, :user_id, :biz_id,
        {
          risk_management_results_attributes: [:id, :_destroy, :title, :follow_up_risk_id, :order_sort, :user_id, :biz_id],
          risk_management_progresses_attributes: [:id, :_destroy, :title, :follow_up_risk_id, :order_sort, :user_id, :biz_id],
          risk_management_issues_attributes: [:id, :_destroy, :title, :follow_up_risk_id, :order_sort, :user_id, :biz_id]
        }])
  end
end
