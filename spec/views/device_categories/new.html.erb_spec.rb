require "rails_helper"

RSpec.describe "device_categories/new", type: :view do
  before(:each) do
    assign(:device_category, DeviceCategory.new(
      title: "MyString",
      image_data: "MyText",
      description: "MyText",
      slug: "MyString",
      user: nil,
      biz: nil
    ))
  end

  it "renders new device_category form" do
    render

    assert_select "form[action=?][method=?]", device_categories_path, "post" do
      assert_select "input[name=?]", "device_category[title]"

      assert_select "textarea[name=?]", "device_category[image_data]"

      assert_select "textarea[name=?]", "device_category[description]"

      assert_select "input[name=?]", "device_category[slug]"

      assert_select "input[name=?]", "device_category[user_id]"

      assert_select "input[name=?]", "device_category[biz_id]"
    end
  end
end
