require "rails_helper"

RSpec.describe "Risk::Control::FollowUpControlHistories", type: :request do
  describe "GET /edit" do
    it "returns http success" do
      get "/risk/control/follow_up_control_histories/edit"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /index" do
    it "returns http success" do
      get "/risk/control/follow_up_control_histories/index"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /new" do
    it "returns http success" do
      get "/risk/control/follow_up_control_histories/new"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /show" do
    it "returns http success" do
      get "/risk/control/follow_up_control_histories/show"
      expect(response).to have_http_status(:success)
    end
  end
end
