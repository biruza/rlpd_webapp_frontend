require "rails_helper"

RSpec.describe TplListsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/tpl_lists").to route_to("tpl_lists#index")
    end

    it "routes to #new" do
      expect(get: "/tpl_lists/new").to route_to("tpl_lists#new")
    end

    it "routes to #show" do
      expect(get: "/tpl_lists/1").to route_to("tpl_lists#show", id: "1")
    end

    it "routes to #edit" do
      expect(get: "/tpl_lists/1/edit").to route_to("tpl_lists#edit", id: "1")
    end

    it "routes to #create" do
      expect(post: "/tpl_lists").to route_to("tpl_lists#create")
    end

    it "routes to #update via PUT" do
      expect(put: "/tpl_lists/1").to route_to("tpl_lists#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/tpl_lists/1").to route_to("tpl_lists#update", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/tpl_lists/1").to route_to("tpl_lists#destroy", id: "1")
    end
  end
end
