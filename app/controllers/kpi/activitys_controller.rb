class Kpi::ActivitysController < ApplicationController
  def index
    url = URI("#{$server[@controller_title]}/api/v1/activitys")

    @datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/activitys/#{params["id"]}")

    @datas = request_api(url, "get", nil)
  end

  def new
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/activitys/#{params["id"]}")

    @datas = request_api(url, "get", nil)

    # puts "plan edit = #{@plan['title']}"
  end

  def create
    params[:activity][:user_id] = $user_id
    params[:activity][:biz_id] = $biz_id

    url = URI("#{$server[@controller_title]}/api/v1/activitys")
    @datas = request_api(url, "post", activity_params)

    if @datas["status"] == "Success"
      redirect_to "/kpi/activitys", notice: @datas["messages"]
    else
      redirect_to "/kpi/activitys/new", alert: @datas["data"]
    end
  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/activitys/#{params["id"]}")
    @datas = request_api(url, "patch", activity_params)

    # puts "update status : #{res}"

    if @datas["status"] == "Success"
      redirect_to "/kpi/activitys", notice: @datas["message"]
    else
      redirect_to "/kpi/activitys", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/activitys/#{params["id"]}")
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
      redirect_to "/kpi/activitys", notice: @datas["message"]
    else
      redirect_to "/kpi/activitys", alert: @datas["data"]
    end
  end

  private

  def activity_params
    params.require(:activity).permit(:title, :user_id, :biz_id)
  end
end
