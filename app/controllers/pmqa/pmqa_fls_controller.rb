class Pmqa::PmqaFlsController < ApplicationController
  def index
    url = URI("#{$server[@controller_title]}/api/v1/master_plans")
    @datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params["id"]}")
    @datas = request_api(url, "get", nil)

    url_master_plan_user = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params["id"]}/master_plan_users?order=order_sort&sort=asc")
    @master_plan_users = request_api(url_master_plan_user, "get", nil)
  end

  def new
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params["id"]}")

    @datas = request_api(url, "get", nil)

    # puts "plan edit = #{@plan['title']}"
  end

  def create
    # params[:master_plan][:user_id] = $user[session.id.to_s][@controller_title]['id']
    # params[:master_plan][:biz_id] = $user[session.id.to_s][@controller_title]['biz_id']

    url = URI("#{$server[@controller_title]}/api/v1/master_plans")
    @datas = request_api(url, "post", master_plan_params)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/master_plans", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_title}/master_plans/new", alert: @datas["data"]
    end
  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params["id"]}")
    @datas = request_api(url, "patch", master_plan_params)

    # puts "update status : #{res}"

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/master_plans/#{params[:id]}", notice: @datas["message"]
    else
      redirect_to "/#{@controller_title}/master_plans/#{params[:id]}/edit", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params["id"]}")
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/master_plans", notice: @datas["message"]
    else
      redirect_to "/#{@controller_title}/master_plans", alert: t(:can_not_delete)
    end
  end

  def print
    url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params["id"]}")
    @datas = request_api(url, "get", nil)
  end

  private

  def master_plan_params
    params.require(:master_plan).permit(:title, :description, :user_id, :biz_id, :type_name, :startdate, :enddate, :period_year)
  end
end
