require "rails_helper"

RSpec.describe "boards/index", type: :view do
  before(:each) do
    assign(:boards, [
      Board.create!(
        title: "Title",
        description: "MyText",
        user: nil,
        biz: nil
      ),
      Board.create!(
        title: "Title",
        description: "MyText",
        user: nil,
        biz: nil
      )
    ])
  end

  it "renders a list of boards" do
    render
    assert_select "tr>td", text: "Title".to_s, count: 2
    assert_select "tr>td", text: "MyText".to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
  end
end
