class Pmqa::PmqaFl::ListActivityItemsController < ApplicationController
  before_action :set_select, only: %i[new create edit update]

  before_action :set_default, only: %i[index show new edit create update destroy print]
  @sub_system = "/pmqa_fl"

  def new
  end

  def create
    url = URI("#{$server[@controller_title]}/api/v1/list_activity_items")
    @datas = request_api(url, "post", list_activity_items_params)

    if @datas["status"] == "Success"
      url = URI("#{$server[@controller_title]}/api/v1/list_activity_items/#{@datas["data"]["id"]}")
      @list_activity_item = request_api(url, "get", nil)

      url = URI("#{$server[@controller_title]}/api/v1/list_activities/#{@list_activity_item["data"]["list_activity_id"]}")
      @list_activity = request_api(url, "get", nil)

      url = URI("#{$server[@controller_title]}/api/v1/lists/#{@list_activity["data"]["list_id"]}")
      @list = request_api(url, "get", nil)

      url = URI("#{$server[@controller_title]}/api/v1/categories/#{@list["data"]["category_id"]}")
      @category = request_api(url, "get", nil)

      redirect_to "/#{@controller_main_url}/plans/#{@category["data"]["plan_id"]}", notice: @datas["messages"]
    else
      redirect_to params[:original_url].to_s, alert: @datas["data"]
    end
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/list_activity_items/#{params["id"]}")
    @datas = request_api(url, "get", nil)
  end

  def update
    #       idx = 0
    #       if !params[:list_activity_item][:list_results_attributes].blank?
    #         params[:list_activity_item][:list_results_attributes].each do |item|
    #           if params[:list_activity_item][:list_results_attributes]["#{idx}"]['title'].blank?
    #             params[:list_activity_item][:list_results_attributes]["#{idx}"] = nil
    #
    #           else
    #             params[:list_activity_item][:list_results_attributes]["#{idx}"]['user_id'] = $user[session.id.to_s][@controller_title]['id']
    #             params[:list_activity_item][:list_results_attributes]["#{idx}"]['biz_id'] = $user[session.id.to_s][@controller_title]['biz_id']
    #           end
    #           idx = idx + 1
    #         end
    #       end

    url = URI("#{$server[@controller_title]}/api/v1/list_activity_items/#{params["id"]}")
    @datas = request_api(url, "patch", list_activity_items_params)

    if @datas && @datas["status"] == "Success"
      redirect_to params[:original_url], notice: @datas["message"]
    else
      redirect_to "/#{@controller_main_url}/list_activity_items/#{params[:id]}/edit", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/list_activity_items/#{params["id"]}")
    @datas_ori = request_api(url, "get", nil)
    @datas = request_api(url, "delete", nil)

    url = URI("#{$server[@controller_title]}/api/v1/list_activities/#{@datas_ori["data"]["list_activity_id"]}")
    @list_activity = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/lists/#{@list_activity["data"]["list_id"]}")
    @list = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/categories/#{@list["data"]["category_id"]}")
    @category = request_api(url, "get", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/plans/#{@category["data"]["plan_id"]}", notice: @datas["message"]
    else
      redirect_to "/#{@controller_main_url}/plans/#{@category["data"]["plan_id"]}", alert: @datas["data"]
    end
  end

  def set_select
  end

  def set_default
    @sub_system = "pmqa_fl"
    @controller_main_url = "#{@controller_title}/#{@sub_system}"
  end

  def list_activity_items_params
    params.require(:list_activity_item).permit(:order_sort, :title, :startdate, :enddate, :list_activity_id, :user_id, :biz_id,
      list_periods_attributes: [:id, :_destroy, :amount, :unit],
      list_monthly_periods_attributes: [:id, :_destroy, :startdate, :enddate])
  end
end
