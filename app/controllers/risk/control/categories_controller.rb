class Risk::Control::CategoriesController < ApplicationController
  before_action :set_default, only: %i[index show new edit create update destroy print]

  before_action :set_select, only: %i[new edit]

  def index
    url = URI("#{$server[@controller_title]}/api/v1/categories")
    @datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/categories/#{params["id"]}")
    @category = request_api(url, "get", nil)
  end

  def new
    url = URI("#{$server[@controller_title]}/api/v1/users/#{$user[session.id.to_s][@controller_title]["id"]}")
    @user = request_api(url, "get", nil)
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/categories/#{params["id"]}")
    @datas = request_api(url, "get", nil)
  end

  def create
    # params[:category][:user_id] = $user[session.id.to_s][@controller_title]['id']
    # params[:category][:biz_id] = $user[session.id.to_s][@controller_title]['biz_id']
    # params[:category][:institution_id] = $user[session.id.to_s][@controller_title]['institution_id']
    url = URI("#{$server[@controller_title]}/api/v1/categories")
    @datas = request_api(url, "post", category_params)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/plans/#{@datas["data"]["plan_id"]}", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_main_url}/plans/#{params["plan_id"]}", alert: @datas["data"]
    end
  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/categories/#{params["id"]}")
    @datas = request_api(url, "patch", category_params)

    if @datas && @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/plans/#{@datas["data"]["plan_id"]}", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_main_url}/categories/#{params["id"]}/edit?plan_id=#{params["category"]["plan_id"]}", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/categories/#{params["id"]}")
    @datas_ori = request_api(url, "get", nil)
    @datas = request_api(url, "delete", nil)

    url_plan = URI("#{$server[@controller_title]}/api/v1/plans/#{@datas_ori["data"]["plan_id"]}")
    @plan = request_api(url_plan, "get", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/master_plans/#{@plan["data"]["master_plan_id"]}", notice: @datas["message"]
    else
      redirect_to "/#{@controller_main_url}/master_plans/#{@plan["data"]["master_plan_id"]}", alert: t(:can_not_delete)
    end
  end

  private

  def set_default
    @sub_system = "control"
    @controller_main_url = "#{@controller_title}/#{@sub_system}"
    # @controller_main_url = @controller_title
  end

  def set_select
    url = URI("#{$server[@controller_title]}/api/v1/strategic_issues")
    @data_strategic_issues = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/institutions")
    @data_institutions = request_api(url, "get", nil)
  end

  def category_params
    params.require(:category).permit(:title_code, :strategic_issue_id, :description, :title, :cognitive, :slug, :approve, :plan_id, :institution_id, :issue_id, :kpi_id, :target_kpi_id, :responsible_id, :user_id, :biz_id,
      category_objectives_attributes: [:id, :_destroy, :title, :order_sort, :slug, :category_id, :user_id, :biz_id],
      lists_attributes: [:id, :_destroy, :category_id, :status, :slug, :order_sort, :activity_id, :user_id, :biz_id,
        tasks_attributes: [:id, :_destroy, :category_id, :slug, :status, :order_sort, :list_id, :plan_report_id, :user_id, :biz_id]])
  end

  def authenticate_user!
    if user_signed_in?
      super
    else
      redirect_to "accounts/sign_in", notice: "Please Login to view that page!"
    end
  end
end
