require "rails_helper"

RSpec.describe "task_activities/edit", type: :view do
  before(:each) do
    @task_activity = assign(:task_activity, TaskActivity.create!(
      task: nil,
      description: "MyText",
      activity_type: 1,
      user: nil,
      biz: nil
    ))
  end

  it "renders the edit task_activity form" do
    render

    assert_select "form[action=?][method=?]", task_activity_path(@task_activity), "post" do
      assert_select "input[name=?]", "task_activity[task_id]"

      assert_select "textarea[name=?]", "task_activity[description]"

      assert_select "input[name=?]", "task_activity[activity_type]"

      assert_select "input[name=?]", "task_activity[user_id]"

      assert_select "input[name=?]", "task_activity[biz_id]"
    end
  end
end
