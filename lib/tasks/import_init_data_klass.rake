require "csv"

desc "Import Init Data Klass from csv file"
task import_init_data_klass: [:environment] do
  csv_text1 = File.read("db/for_init/klasses.csv")
  csv1 = CSV.parse(csv_text1, headers: true)
  csv1.each do |row|
    Klass.create!(row.to_hash)
  end

  ActiveRecord::Base.connection.execute("
    select setval('klasses_id_seq', m)
	from (
	    select max(id) from klasses
	    ) as dt(m)
	")
end
