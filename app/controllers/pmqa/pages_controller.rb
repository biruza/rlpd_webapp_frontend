class Pmqa::PagesController < ApplicationController
  def dashboards
    url = URI("#{$server[@controller_title]}/api/v1/master_plans?order=updated_at&sort=desc&limit=10&type_name=plan")
    @master_plans = request_api(url, "get", nil)

    params[:master_plan_id] = @master_plans["data"][0]["id"] if params[:master_plan_id].blank? && @master_plans && !@master_plans["data"].blank?

    url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params[:master_plan_id]}/plans?order=order_sort&sort=desc&limit=10&select=id,title")
    @plans = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/plan_reports?order=updated_at&sort=desc&limit=10")
    @plan_reports = request_api(url, "get", nil)
  end

  def dashboards_overall
    url = URI("#{$server[@controller_title]}/api/v1/master_plans?order=updated_at&sort=desc&limit=10&type_name=plan")
    @master_plans = request_api(url, "get", nil)

    params[:master_plan_id] = @master_plans["data"][0]["id"] if params[:master_plan_id].blank? && @master_plans && !@master_plans["data"].blank?

    url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params[:master_plan_id]}/plans?order=order_sort&sort=desc&limit=10&select=id,title")
    @plans = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/plan_reports?order=updated_at&sort=desc&limit=10")
    @plan_reports = request_api(url, "get", nil)
  end

  def dashboards_pmqa_fl
    url = URI("#{$server[@controller_title]}/api/v1/master_plans?order=updated_at&sort=desc&limit=10&type_name=pmqa_fl")
    @master_plans = request_api(url, "get", nil)

    params[:master_plan_id] = @master_plans["data"][0]["id"] if params[:master_plan_id].blank? && @master_plans && !@master_plans["data"].blank?

    url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params[:master_plan_id]}")
    @master_plan = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params[:master_plan_id]}/plans?order=order_sort&sort=desc&limit=10&select=id,title")
    @plans = request_api(url, "get", nil)

  end

  def dashboards_pmqa_fr
    url = URI("#{$server[@controller_title]}/api/v1/master_plans?order=updated_at&sort=desc&limit=10&type_name=pmqa_fr")
    @master_plans = request_api(url, "get", nil)

    params[:master_plan_id] = @master_plans["data"][0]["id"] if params[:master_plan_id].blank? && @master_plans && !@master_plans["data"].blank?

    url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params[:master_plan_id]}")
    @master_plan = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params[:master_plan_id]}/plans?order=order_sort&sort=desc&limit=10&select=id,title")
    @plans = request_api(url, "get", nil)

  end

  def downloads
    url = URI("#{$server[@controller_title]}/api/v1/downloads?order=title&sort=desc")
    @downloads = request_api(url, "get", nil)
  end

  def download_user_manuals
    url = URI("#{$server[@controller_title]}/api/v1/user_manuals?order=title&sort=desc")
    #url = URI("#{$server[@controller_title]}/api/v1/user_manuals")
    @download_user_manuals = request_api(url, "get", nil)
  end

  def search_result
    if !params[:search][:system_name].blank?
      case params[:search][:model_name]
      when "master_plans_risk"
        params[:search][:model_name] = "master_plans"
        params[:search][:type_name] = "risk"
      when "master_plans_control"
        params[:search][:model_name] = "master_plans"
        params[:search][:type_name] = "control"
      end

      url = URI("#{$server[params[:search][:system_name]]}/api/v1/mains/search")
      @datas = request_api(url, "post", search_params)
      @keyword = params[:search][:keyword]
      @model_name = params[:search][:model_name]
    end
  end

  def notifications
    url = URI("#{$server[@controller_title]}/api/v1/master_plans?order=period_year&sort=desc&limit=10&type_name=plan")
    @master_plans = request_api(url, "get", nil)

    params[:master_plan_id] = @master_plans["data"][0]["id"] if params[:master_plan_id].blank? && @master_plans && !@master_plans["data"].blank?

    url = URI("#{$server[@controller_title]}/api/v1/mains/notifications?master_plan_id=#{params[:master_plan_id]}")
    @notifications = request_api(url, "get", nil)
  end

  private

  def search_params
    params.require(:search).permit(:model_name, :keyword, :system_name)
  end

  def master_plan_params
    params.require(:master_plan).permit(:title, :description, :user_id, :biz_id, :type_name, :startdate, :enddate, :period_year)
  end
end
