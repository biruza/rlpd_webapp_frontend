class Risk::Control::RiskManagementReportsController < ApplicationController
  before_action :set_default, only: %i[index show new edit create update destroy print]
  before_action :set_select, only: %i[new create edit update]

  def index
    url = URI("#{$server[@controller_title]}/api/v1/risk_management_reports?order=updated_at&sort=desc&limit=10")
    @risk_management_reports = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/risk_management_reports/#{params["id"]}")
    @risk_management_report = request_api(url, "get", nil)
  end

  def new
    if !params[:task_plan_report_id].blank?
      url = URI("#{$server[@controller_title]}/api/v1/task_plan_reports/#{params[:task_plan_report_id]}")
      @task_plan_report = request_api(url, "get", nil)

      risk_management_report_params = {
        task_plan_report_id: @task_plan_report["data"]["id"],
        user_id: @task_plan_report["data"]["user_id"],
        biz_id: @task_plan_report["data"]["biz_id"]
      }

      url = URI("#{$server[@controller_title]}/api/v1/risk_management_reports")
      @datas = request_api(url, "post", risk_management_report_params)

      if @datas["status"] == "Success"

        url = URI("#{$server[@controller_title]}/api/v1/risk_management_reports/#{@datas["data"]["id"]}")
        @risk_management_report = request_api(url, "get", nil)

       # @risk_management_report["data"]["task_plan_report"]["task_lists"].each do |task_list|
       #   risk_managements_params = {
       #     task_list_id: task_list["id"],
       #     risk_management_report_id: @risk_management_report["data"]["id"],
       #     order_sort: task_list["order_sort"],
       #     user_id: task_list["user_id"],
       #     biz_id: task_list["biz_id"]
       #   }
       @task_plan_report["data"]["task_lists"].each_with_index do |task_list,idx|
      
        if  (task_list["existing_manage"] == "adjust")
    
      url = URI("#{$server[@controller_title]}/api/v1/task_lists/get_by_list/#{@task_plan_report["data"]["risk_analysis_report"]["risk_analyses"][idx]["list_id"]}")
      @task_list_datas = request_api(url, "get", nil)
          risk_managements_params = {
            task_list_id: @task_list_datas["data"]["id"],
            category_id: @task_list_datas["data"]["category_id"],
            list_id: @task_list_datas["data"]["list_id"],
            risk_management_report_id: @risk_management_report["data"]["id"],
            order_sort: @task_list_datas["data"]["order_sort"],
            user_id: @task_list_datas["data"]["user_id"],
            biz_id: @task_list_datas["data"]["biz_id"]
          }

          url_risk_managements = URI("#{$server[@controller_title]}/api/v1/risk_managements")
          url_risk_managements_new = request_api(url_risk_managements, "post", risk_managements_params)
        end
      end
        redirect_to "/#{@controller_main_url}/risk_management_reports/#{@datas["data"]["id"]}/edit", notice: @datas["messages"]
      else
        render :new, status: :unprocessable_entity
      end

    else
      redirect_to "/#{@controller_main_url}/master_plans", alert: (begin
        t(:can_not_found)
      rescue
        nil
      end).to_s
    end
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/risk_management_reports/#{params["id"]}")
    @risk_management_report = request_api(url, "get", nil)
  end

  def update
    params[:task_list].each do |item|
      params[:task_list][(item[0]).to_s][:risk_accept] = true if params[:task_list][(item[0]).to_s][:risk_accept] == "true"

      item_params = {
        task_list: {
          risk_accept: params[:task_list][(item[0]).to_s][:risk_accept],
          risk_avert: params[:task_list][(item[0]).to_s][:risk_avert],
          risk_transfer: params[:task_list][(item[0]).to_s][:risk_transfer],
          risk_control: params[:task_list][(item[0]).to_s][:risk_control],
          existing_manage: params[:task_list][(item[0]).to_s][:existing_manage],

          task_list_controls_attributes: {
            "0": {
              task_list_id: params[:task_list][(item[0]).to_s][:id],
              title: params[:task_list][(item[0]).to_s][:task_list_controls_attributes]["0"][:title],
              user_id: $user[session.id.to_s][@controller_title]["id"],
              biz_id: $user[session.id.to_s][@controller_title]["biz_id"],
              id: params[:task_list][(item[0]).to_s][:task_list_controls_attributes]["0"][:id]
            }
          }
        }
      }

      url = URI("#{$server[@controller_title]}/api/v1/task_lists/#{params[:task_list][(item[0]).to_s][:id]}")
      @datas = request_api(url, "patch", item_params)
    end

    url_task_plan_report = URI("#{$server[@controller_title]}/api/v1/task_plan_reports/#{params["id"]}")
    @url_task_plan_report = request_api(url_task_plan_report, "get", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/task_plan_reports/#{@url_task_plan_report["data"]["id"]}/edit", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_main_url}/task_plan_reports/#{@url_task_plan_report["data"]["id"]}/edit", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/risk_management_reports/#{params["id"]}")
    @risk_management_report = request_api(url, "get", nil)

    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/plans/#{@risk_management_report["data"]["task_plan_report"]["risk_analysis_report"]["plan_report"]["category"]["plan_id"]}", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_main_url}/plans/#{@risk_management_report["data"]["task_plan_report"]["risk_analysis_report"]["plan_report"]["category"]["plan_id"]}", alert: @datas["data"]
    end
  end

  def print
    url = URI("#{$server[@controller_title]}/api/v1/risk_management_reports/#{params["id"]}")
    @risk_management_report = request_api(url, "get", nil)
  end

  private

  def set_default
    @sub_system = "control"
    @controller_main_url = "#{@controller_title}/#{@sub_system}"
    # @controller_main_url = @controller_title
  end

  def set_user
    @user = User.find(auth_token[:user_id])
  end

  def set_select
    # url = URI("#{$server[@controller_title]}/api/v1/plans")
    # @data_plans = request_api(url,'get',nil)

    # url = URI("#{$server[@controller_title]}/api/v1/categories")
    # @data_categories = request_api(url,'get',nil)

    # url = URI("#{$server[@controller_title]}/api/v1/master_plans")
    # @data_master_plans = request_api(url,'get',nil)
  end

  def risk_management_report_params
    params.require(:risk_management_report).permit(:title1, :title2, :startdate, :enddate, :slug, :order_sort, :task_plan_report_id, :master_plan_id, :category_id, :plan_id, :user_id, :biz_id,
      risk_managements_attributes: [:id, :_destroy, :success_date, :task_list_id, :risk_management_report_id, :list_id, :category_id, :list_id, :user_id, :biz_id])
  end
end
