require "test_helper"

class PagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @page = pages(:one)
  end

  test "should get index" do
    get pages_url
    assert_response :success
  end

  test "should get new" do
    get new_page_url
    assert_response :success
  end

  test "should create page" do
    assert_difference("Page.count") do
      post pages_url, params: {page: {biz_id: @page.biz_id, description_en: @page.description_en, description_th: @page.description_th, image_data: @page.image_data, order_number: @page.order_number, page_parent: @page.page_parent, slug: @page.slug, title_en: @page.title_en, title_th: @page.title_th, web_template_page: @page.web_template_page}}
    end

    assert_redirected_to page_url(Page.last)
  end

  test "should show page" do
    get page_url(@page)
    assert_response :success
  end

  test "should get edit" do
    get edit_page_url(@page)
    assert_response :success
  end

  test "should update page" do
    patch page_url(@page), params: {page: {biz_id: @page.biz_id, description_en: @page.description_en, description_th: @page.description_th, image_data: @page.image_data, order_number: @page.order_number, page_parent: @page.page_parent, slug: @page.slug, title_en: @page.title_en, title_th: @page.title_th, web_template_page: @page.web_template_page}}
    assert_redirected_to page_url(@page)
  end

  test "should destroy page" do
    assert_difference("Page.count", -1) do
      delete page_url(@page)
    end

    assert_redirected_to pages_url
  end
end
