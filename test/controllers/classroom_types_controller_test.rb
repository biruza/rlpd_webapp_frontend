require "test_helper"

class ClassroomTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @classroom_type = classroom_types(:one)
  end

  test "should get index" do
    get classroom_types_url
    assert_response :success
  end

  test "should get new" do
    get new_classroom_type_url
    assert_response :success
  end

  test "should create classroom_type" do
    assert_difference("ClassroomType.count") do
      post classroom_types_url, params: {classroom_type: {title_en: @classroom_type.title_en, title_th: @classroom_type.title_th}}
    end

    assert_redirected_to classroom_type_url(ClassroomType.last)
  end

  test "should show classroom_type" do
    get classroom_type_url(@classroom_type)
    assert_response :success
  end

  test "should get edit" do
    get edit_classroom_type_url(@classroom_type)
    assert_response :success
  end

  test "should update classroom_type" do
    patch classroom_type_url(@classroom_type), params: {classroom_type: {title_en: @classroom_type.title_en, title_th: @classroom_type.title_th}}
    assert_redirected_to classroom_type_url(@classroom_type)
  end

  test "should destroy classroom_type" do
    assert_difference("ClassroomType.count", -1) do
      delete classroom_type_url(@classroom_type)
    end

    assert_redirected_to classroom_types_url
  end
end
