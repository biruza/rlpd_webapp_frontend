require "csv"

desc "Import Init Data Klass Student from csv file"
task import_init_data_klass_student: [:environment] do
  csv_text1 = File.read("db/for_init/klass_students.csv")
  csv1 = CSV.parse(csv_text1, headers: true)
  csv1.each do |row|
    KlassStudent.create!(row.to_hash)
  end

  ActiveRecord::Base.connection.execute("
    select setval('klass_students_id_seq', m)
	from (
	    select max(id) from klass_students
	    ) as dt(m)
	")
end
