require "rails_helper"

RSpec.describe "device_categories/index", type: :view do
  before(:each) do
    assign(:device_categories, [
      DeviceCategory.create!(
        title: "Title",
        image_data: "MyText",
        description: "MyText",
        slug: "Slug",
        user: nil,
        biz: nil
      ),
      DeviceCategory.create!(
        title: "Title",
        image_data: "MyText",
        description: "MyText",
        slug: "Slug",
        user: nil,
        biz: nil
      )
    ])
  end

  it "renders a list of device_categories" do
    render
    assert_select "tr>td", text: "Title".to_s, count: 2
    assert_select "tr>td", text: "MyText".to_s, count: 2
    assert_select "tr>td", text: "MyText".to_s, count: 2
    assert_select "tr>td", text: "Slug".to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
  end
end
