require "test_helper"

class KlassTeachersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @klass_teacher = klass_teachers(:one)
  end

  test "should get index" do
    get klass_teachers_url
    assert_response :success
  end

  test "should get new" do
    get new_klass_teacher_url
    assert_response :success
  end

  test "should create klass_teacher" do
    assert_difference("KlassTeacher.count") do
      post klass_teachers_url, params: {klass_teacher: {klass_id: @klass_teacher.klass_id}}
    end

    assert_redirected_to klass_teacher_url(KlassTeacher.last)
  end

  test "should show klass_teacher" do
    get klass_teacher_url(@klass_teacher)
    assert_response :success
  end

  test "should get edit" do
    get edit_klass_teacher_url(@klass_teacher)
    assert_response :success
  end

  test "should update klass_teacher" do
    patch klass_teacher_url(@klass_teacher), params: {klass_teacher: {klass_id: @klass_teacher.klass_id}}
    assert_redirected_to klass_teacher_url(@klass_teacher)
  end

  test "should destroy klass_teacher" do
    assert_difference("KlassTeacher.count", -1) do
      delete klass_teacher_url(@klass_teacher)
    end

    assert_redirected_to klass_teachers_url
  end
end
