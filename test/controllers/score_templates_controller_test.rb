require "test_helper"

class ScoreTemplatesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @score_template = score_templates(:one)
  end

  test "should get index" do
    get score_templates_url
    assert_response :success
  end

  test "should get new" do
    get new_score_template_url
    assert_response :success
  end

  test "should create score_template" do
    assert_difference("ScoreTemplate.count") do
      post score_templates_url, params: {score_template: {title_en: @score_template.title_en, title_th: @score_template.title_th}}
    end

    assert_redirected_to score_template_url(ScoreTemplate.last)
  end

  test "should show score_template" do
    get score_template_url(@score_template)
    assert_response :success
  end

  test "should get edit" do
    get edit_score_template_url(@score_template)
    assert_response :success
  end

  test "should update score_template" do
    patch score_template_url(@score_template), params: {score_template: {title_en: @score_template.title_en, title_th: @score_template.title_th}}
    assert_redirected_to score_template_url(@score_template)
  end

  test "should destroy score_template" do
    assert_difference("ScoreTemplate.count", -1) do
      delete score_template_url(@score_template)
    end

    assert_redirected_to score_templates_url
  end
end
