class AccountsController < ApplicationController
  layout :resolve_layout

  require "json"
  require "base64"
  require "openssl"
  $server = {}
  $auth_token = {}
  $user = {}

  # $server[@controller_title] ="http://167.172.65.136:3000"
  # $server[@controller_title] ="http://127.0.0.1:3000"
  # $server_pmqa ="http://127.0.0.1:3000"

  # $server[@controller_title] = "http://167.172.65.136:3000"
  #==========Staging===================

  # $server['km'] = "http://167.172.65.136:3000"
  # $server['pmqa'] = "http://167.172.65.136:3001"
  # $server['kpi'] = "http://167.172.65.136:3002"
  # $server['risk'] = "http://167.172.65.136:3003"

  #===========================

  #==========Production===================
  # $server["km"] = "https://api-km.rlpd.go.th"
  # $server["pmqa"] = "https://api-pmqa.rlpd.go.th"
  # $server["kpi"] = "https://api-kpi.rlpd.go.th"
  # $server["risk"] = "https://api.risk.rlpd.go.th"

  $server["km"] = Rails.application.secrets.api_km_url
  $server["pmqa"] = Rails.application.secrets.api_pmqa_url
  $server["kpi"] = Rails.application.secrets.api_kpi_url
  $server["risk"] = Rails.application.secrets.api_risk_url

  #=======================================
  def sign_in
  end

  def create_session
    system_name = params[:account][:system_name]
    auth_success, ldap_data = LdapAuth.call(account_params)

    $auth_token[session.id.to_s] = {}
    $user[session.id.to_s] = {}

    ['km','pmqa','kpi','risk'].each do |server_name|

      server = $server[server_name]

#p server
      if server.present?
        url = URI("#{server}/api/v1/auth_user")

        http = Net::HTTP.new(url.host, url.port)
        http.use_ssl = Rails.env.production? || Rails.env.staging?
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE

        request = Net::HTTP::Post.new(url)
        request["content-type"] = "application/json"

        if auth_success
          name = ldap_data[:name].first
          name_split = name.split(" ")

          request.body = {
            email: account_params[:email],
            password: account_params[:password],
            system_name: account_params[:system_name],
            dn: ldap_data[:dn].first,
            firstname: name_split.first,
            lastname: name_split.last,
            ldap: true
          }.to_json
        else
          request.body = account_params.to_json
        end

        response = http.request(request)

       # p "response = #{response}"
        #p "response.read_body = #{response.read_body}"

        if response.present? && response.read_body.present?
          data = JSON.parse(response.read_body.force_encoding("UTF-8"))
         # $auth_token[session.id.to_s] = {} if $auth_token[session.id.to_s].blank?
          $auth_token[session.id.to_s][server_name] = data["auth_token"]
         # $user[session.id.to_s] = {} if $user[session.id.to_s].blank?
          $user[session.id.to_s][server_name] = data["user"]

          #check ldap_data
          $user[session.id.to_s]['ldap_data'] = ldap_data rescue nil

          status = data && data["errors"].blank?
          #p "$auth_token[session.id.to_s] = #{$auth_token[session.id.to_s]}"
          #p "$user[session.id.to_s] = #{$user[session.id.to_s]}"

        else
          status = false
        end

      else #if server.present?
        status = false
      end #if server.present?



    end #loop ['km','pmqa','kpi','risk'].each do |server_name|



    if status == true
      #redirect_to "/#{system_name}/pages/dashboards", notice: "Welcome #{data["user"]["email"]}"
      redirect_to "/#{system_name}/master_plans", notice: "#{t(:welcome)} #{data["user"]["email"]}"
    else
      data = begin
        JSON.parse(response.body)
      rescue
        {}
      end
      redirect_to "/accounts/sign_in?system_name=#{system_name}", alert: begin
        data["errors"]
      rescue
        nil
      end
    end







  end

  def sign_out
    $auth_token[session.id.to_s][params[:system_name]] = nil if $auth_token[session.id.to_s] && $auth_token[session.id.to_s][params[:system_name]]
    $user[session.id.to_s][params[:system_name]] = nil if $user[session.id.to_s] && $user[session.id.to_s][params[:system_name]]

    redirect_to "/accounts/sign_in"
  end

  private

  def account_params
    permitted = params.require(:account).permit(:email, :password, :system_name)

    unless permitted[:email].match? /@.*/
      permitted[:email] = "#{permitted[:email]}@rlpd.go.th"
    end

    permitted
  end

  def resolve_layout
    "blank"
  end
end
