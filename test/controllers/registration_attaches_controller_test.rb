require "test_helper"

class RegistrationAttachesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @registration_attach = registration_attaches(:one)
  end

  test "should get index" do
    get registration_attaches_url
    assert_response :success
  end

  test "should get new" do
    get new_registration_attach_url
    assert_response :success
  end

  test "should create registration_attach" do
    assert_difference("RegistrationAttach.count") do
      post registration_attaches_url, params: {registration_attach: {biz_id: @registration_attach.biz_id, description_en: @registration_attach.description_en, description_th: @registration_attach.description_th, file_data: @registration_attach.file_data, registration_id: @registration_attach.registration_id, slug: @registration_attach.slug, slug: @registration_attach.slug, title_en: @registration_attach.title_en, title_th: @registration_attach.title_th, user_id: @registration_attach.user_id}}
    end

    assert_redirected_to registration_attach_url(RegistrationAttach.last)
  end

  test "should show registration_attach" do
    get registration_attach_url(@registration_attach)
    assert_response :success
  end

  test "should get edit" do
    get edit_registration_attach_url(@registration_attach)
    assert_response :success
  end

  test "should update registration_attach" do
    patch registration_attach_url(@registration_attach), params: {registration_attach: {biz_id: @registration_attach.biz_id, description_en: @registration_attach.description_en, description_th: @registration_attach.description_th, file_data: @registration_attach.file_data, registration_id: @registration_attach.registration_id, slug: @registration_attach.slug, slug: @registration_attach.slug, title_en: @registration_attach.title_en, title_th: @registration_attach.title_th, user_id: @registration_attach.user_id}}
    assert_redirected_to registration_attach_url(@registration_attach)
  end

  test "should destroy registration_attach" do
    assert_difference("RegistrationAttach.count", -1) do
      delete registration_attach_url(@registration_attach)
    end

    assert_redirected_to registration_attaches_url
  end
end
