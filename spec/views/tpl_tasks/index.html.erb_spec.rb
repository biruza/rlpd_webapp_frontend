require "rails_helper"

RSpec.describe "tpl_tasks/index", type: :view do
  before(:each) do
    assign(:tpl_tasks, [
      TplTask.create!(
        list: nil,
        title: "Title",
        description: "MyText",
        cover: "MyText",
        priority: 2,
        status: 3,
        achieve: false,
        user: nil,
        biz: nil
      ),
      TplTask.create!(
        list: nil,
        title: "Title",
        description: "MyText",
        cover: "MyText",
        priority: 2,
        status: 3,
        achieve: false,
        user: nil,
        biz: nil
      )
    ])
  end

  it "renders a list of tpl_tasks" do
    render
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: "Title".to_s, count: 2
    assert_select "tr>td", text: "MyText".to_s, count: 2
    assert_select "tr>td", text: "MyText".to_s, count: 2
    assert_select "tr>td", text: 2.to_s, count: 2
    assert_select "tr>td", text: 3.to_s, count: 2
    assert_select "tr>td", text: false.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
  end
end
