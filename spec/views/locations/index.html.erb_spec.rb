require "rails_helper"

RSpec.describe "locations/index", type: :view do
  before(:each) do
    assign(:locations, [
      Location.create!(
        title: "Title",
        image_data: "MyText",
        description: "MyText",
        lat: "9.99",
        lng: "9.99",
        slug: "Slug",
        user: nil,
        biz: nil
      ),
      Location.create!(
        title: "Title",
        image_data: "MyText",
        description: "MyText",
        lat: "9.99",
        lng: "9.99",
        slug: "Slug",
        user: nil,
        biz: nil
      )
    ])
  end

  it "renders a list of locations" do
    render
    assert_select "tr>td", text: "Title".to_s, count: 2
    assert_select "tr>td", text: "MyText".to_s, count: 2
    assert_select "tr>td", text: "MyText".to_s, count: 2
    assert_select "tr>td", text: "9.99".to_s, count: 2
    assert_select "tr>td", text: "9.99".to_s, count: 2
    assert_select "tr>td", text: "Slug".to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
  end
end
