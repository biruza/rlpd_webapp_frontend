class Risk::Control::InternalControlCertificatesController < ApplicationController
  before_action :set_default, only: %i[index show new edit create update destroy print]
  before_action :set_select, only: %i[new create edit update]

  def index
    url = URI("#{$server[@controller_title]}/api/v1/internal_control_certificates?order=updated_at&sort=desc&limit=10")
    @internal_control_certificates = request_api(url, "get", nil)

  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/internal_control_certificates/#{params["id"]}")
    @internal_control_certificate = request_api(url, "get", nil)
  end

  def new

  end

  def create


    url = URI("#{$server[@controller_title]}/api/v1/internal_control_certificates")
    @datas = request_api(url, "post", internal_control_certificate_params)

    if @datas["status"] == "Success"

      redirect_to "/#{@controller_main_url}/internal_control_certificates/#{@datas["data"]["id"]}/edit", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_main_url}/internal_control_certificates/new?master_plan_id=#{params[:internal_control_certificate][:master_plan_id]}", alert: @datas["data"]
    end
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/internal_control_certificates/#{params["id"]}")
    @internal_control_certificates = request_api(url, "get", nil)
  end

 def update
 
  url = URI("#{$server[@controller_title]}/api/v1/internal_control_certificates/#{params["id"]}")
  @internal_control_certificate = request_api(url, "get", nil)

  @datas = request_api(url, "patch", internal_control_certificate_params)


  if @datas["status"] == "Success"

    redirect_to "/#{@controller_main_url}/internal_control_certificates/#{@datas["data"]["id"]}/edit", notice: @datas["messages"]
  else
    redirect_to "/#{@controller_main_url}/internal_control_certificates/#{@datas["data"]["id"]}/edit}", alert: @datas["data"]
  end
 end


  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/internal_control_certificates/#{params["id"]}")
    @datas_ori = request_api(url, "get", nil)
    @datas = request_api(url, "delete", nil)

    url = URI("#{$server[@controller_title]}/api/v1/categories/#{@datas_ori["data"]["category_id"]}")
    @category = request_api(url, "get", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/plans/#{@category["data"]["plan_id"]}", notice: @datas["message"]
    else
      redirect_to "/#{@controller_main_url}/plans/#{@category["data"]["plan_id"]}", alert: @datas["data"]
    end
  end

  private

  def set_select
    url = URI("#{$server[@controller_title]}/api/v1/users")
    @user_datas = request_api(url, "get", nil)
  end

  def set_default
    @sub_system = "control"
    @controller_main_url = "#{@controller_title}/#{@sub_system}"
    # @controller_main_url = @controller_title
  end

  def internal_control_certificate_params
    # params.require(:internal_control_certificate).permit(:title1, :title2, :title3, :title4, :title5, :title6, :title7, :title8, :title9, :title10, :title11, :title12, :approvedate, :master_plan, :user, :biz)
    params.require(:internal_control_certificate).permit(:title1, :title2, :title3, :title4, :title5, :title6, :title7, :title8, :title9, :title10, :title11, :title12, :approvedate, :master_plan_id, :user_id, :biz_id)
  end
end
