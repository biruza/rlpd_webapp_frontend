require "image_processing/mini_magick"

class FileUploader < Shrine
  ALLOWED_TYPES = %w[image/jpeg image/jpg image/png image/gif application/pdf application/powerpoint application/vnd.ms-powerpoint application/vnd.ms-office application/vnd.openxmlformats-officedocument.presentationml.presentation application/msword application/vnd.openxmlformats-officedocument.wordprocessingml.document application/vnd.ms-excel application/vnd.openxmlformats-officedocument.spreadsheetml.sheet application/x-zip application/zip application/x-zip-compressed application/x-rar application/rar application/x-rar-compressed application/force-download application/octet-stream application/x-rar application/rar application/x-rar-compressed application/force-download application/octet-stream application/x-7z-compressed]
  #MAX_SIZE = 15 * 1024 * 1024 # 15 MB
  MAX_SIZE = 50 * 1024 * 1024 # 50 MB

  plugin :remove_attachment
  plugin :pretty_location
  plugin :processing
  plugin :versions
  plugin :validation_helpers
  plugin :store_dimensions, analyzer: :mini_magick
  plugin :metadata_attributes, filename: :original_filename

  Attacher.validate do
    validate_max_size MAX_SIZE
    if validate_mime_type_inclusion(ALLOWED_TYPES)
      # validate_max_width 5000
      # validate_max_height 5000
    end
  end

  process(:store) do |io, context|
    # puts "io.mime_type = #{io.mime_type}"

    original = io.download

    if io.mime_type == "image/jpeg" || io.mime_type == "image/jpg" || io.mime_type == "image/png" || io.mime_type == "image/gif"

      tiny = ImageProcessing::MiniMagick
        .source(original)
        .resize_to_limit!(96, nil)
      # .convert("png")

      thumbnail = ImageProcessing::MiniMagick
        .source(original)
        .resize_to_limit!(320, nil)
      # .convert("png")

      medium = ImageProcessing::MiniMagick
        .source(original)
        .resize_to_limit!(1024, nil)
      # .convert("png")

      large = ImageProcessing::MiniMagick
        .source(original)
        .resize_to_limit!(1600, nil)
      # .convert("png")

      avatar = ImageProcessing::MiniMagick
        .source(original)
        .resize_to_fill!(96, 96)
      # .convert("png")

      original.close!
      {original: io, large: large, medium: medium, thumbnail: thumbnail, tiny: tiny, avatar: avatar}

    else # ถ้าไม่ใช่พวก image ให้เก็บเฉพาะ original file

      original.close!
      {original: io}

    end
  end
end
