require "test_helper"

class SubjectGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @subject_group = subject_groups(:one)
  end

  test "should get index" do
    get subject_groups_url
    assert_response :success
  end

  test "should get new" do
    get new_subject_group_url
    assert_response :success
  end

  test "should create subject_group" do
    assert_difference("SubjectGroup.count") do
      post subject_groups_url, params: {subject_group: {title_en: @subject_group.title_en, title_th: @subject_group.title_th}}
    end

    assert_redirected_to subject_group_url(SubjectGroup.last)
  end

  test "should show subject_group" do
    get subject_group_url(@subject_group)
    assert_response :success
  end

  test "should get edit" do
    get edit_subject_group_url(@subject_group)
    assert_response :success
  end

  test "should update subject_group" do
    patch subject_group_url(@subject_group), params: {subject_group: {title_en: @subject_group.title_en, title_th: @subject_group.title_th}}
    assert_redirected_to subject_group_url(@subject_group)
  end

  test "should destroy subject_group" do
    assert_difference("SubjectGroup.count", -1) do
      delete subject_group_url(@subject_group)
    end

    assert_redirected_to subject_groups_url
  end
end
