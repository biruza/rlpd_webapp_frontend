require "rails_helper"

RSpec.describe "task_checklists/show", type: :view do
  before(:each) do
    @task_checklist = assign(:task_checklist, TaskChecklist.create!(
      task: nil,
      order_sort: 2,
      title: "Title",
      type: "Type",
      checklist_type: "Checklist Type",
      respons_data: "MyText",
      status: "Status",
      score: "9.99",
      custom: "",
      description: "MyText",
      user: nil,
      biz: nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/Type/)
    expect(rendered).to match(/Checklist Type/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/Status/)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(//)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
  end
end
