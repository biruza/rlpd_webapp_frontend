require "rails_helper"

RSpec.describe "tpl_task_checklists/show", type: :view do
  before(:each) do
    @tpl_task_checklist = assign(:tpl_task_checklist, TplTaskChecklist.create!(
      tpl_task: nil,
      order_sort: 2,
      title: "Title",
      checklist_type: "MyText",
      status: "Status",
      score: "9.99",
      custom: "",
      description: "MyText",
      weight: "9.99",
      user: nil,
      biz: nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/Status/)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(//)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
  end
end
