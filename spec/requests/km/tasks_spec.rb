require "rails_helper"

RSpec.describe "Km::Tasks", type: :request do
  describe "GET /index" do
    it "returns http success" do
      get "/km/tasks/index"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /show" do
    it "returns http success" do
      get "/km/tasks/show"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /new" do
    it "returns http success" do
      get "/km/tasks/new"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /edit" do
    it "returns http success" do
      get "/km/tasks/edit"
      expect(response).to have_http_status(:success)
    end
  end
end
