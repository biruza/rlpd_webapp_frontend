require "rails_helper"

RSpec.describe "api/v1/boards/edit", type: :view do
  before(:each) do
    @api / v1_board = assign(:api / v1_board, Api::V1::Board.create!(
      title: "MyString",
      description: "MyText",
      user: nil,
      biz: nil
    ))
  end

  it "renders the edit api/v1_board form" do
    render

    assert_select "form[action=?][method=?]", api / v1_api_v1_board_path(@api / v1_board), "post" do
      assert_select "input[name=?]", "api/v1_board[title]"

      assert_select "textarea[name=?]", "api/v1_board[description]"

      assert_select "input[name=?]", "api/v1_board[user_id]"

      assert_select "input[name=?]", "api/v1_board[biz_id]"
    end
  end
end
