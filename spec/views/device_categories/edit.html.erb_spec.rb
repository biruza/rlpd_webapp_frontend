require "rails_helper"

RSpec.describe "device_categories/edit", type: :view do
  before(:each) do
    @device_category = assign(:device_category, DeviceCategory.create!(
      title: "MyString",
      image_data: "MyText",
      description: "MyText",
      slug: "MyString",
      user: nil,
      biz: nil
    ))
  end

  it "renders the edit device_category form" do
    render

    assert_select "form[action=?][method=?]", device_category_path(@device_category), "post" do
      assert_select "input[name=?]", "device_category[title]"

      assert_select "textarea[name=?]", "device_category[image_data]"

      assert_select "textarea[name=?]", "device_category[description]"

      assert_select "input[name=?]", "device_category[slug]"

      assert_select "input[name=?]", "device_category[user_id]"

      assert_select "input[name=?]", "device_category[biz_id]"
    end
  end
end
