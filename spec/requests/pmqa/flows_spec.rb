require "rails_helper"

RSpec.describe "Pmqa::Flows", type: :request do
  describe "GET /index" do
    it "returns http success" do
      get "/pmqa/flows/index"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /show" do
    it "returns http success" do
      get "/pmqa/flows/show"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /edit" do
    it "returns http success" do
      get "/pmqa/flows/edit"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /new" do
    it "returns http success" do
      get "/pmqa/flows/new"
      expect(response).to have_http_status(:success)
    end
  end
end
