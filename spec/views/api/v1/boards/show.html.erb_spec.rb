require "rails_helper"

RSpec.describe "api/v1/boards/show", type: :view do
  before(:each) do
    @api / v1_board = assign(:api / v1_board, Api::V1::Board.create!(
      title: "Title",
      description: "MyText",
      user: nil,
      biz: nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
  end
end
