class Pmqa::AccomplicesController < ApplicationController
  def index
    url = URI("#{$server[@controller_title]}/api/v1/accomplices")

    @datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/accomplices/#{params["id"]}")

    @datas = request_api(url, "get", nil)
  end

  def new
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/accomplices/#{params["id"]}")

    @datas = request_api(url, "get", nil)

    # puts "plan edit = #{@plan['title']}"
  end

  def create
    params[:accomplice][:user_id] = $user_id
    params[:accomplice][:biz_id] = $biz_id

    url = URI("#{$server[@controller_title]}/api/v1/accomplices")
    @datas = request_api(url, "post", accomplice_params)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/accomplices", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_title}/accomplices/new", alert: @datas["data"]
    end
  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/accomplices/#{params["id"]}")
    @datas = request_api(url, "patch", accomplice_params)

    # puts "update status : #{res}"

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/accomplices", notice: @datas["message"]
    else
      redirect_to "/#{@controller_title}/accomplices", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/accomplices/#{params["id"]}")
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/accomplices", notice: @datas["message"]
    else
      redirect_to "/#{@controller_title}/accomplices", alert: @datas["data"]
    end
  end

  private

  def accomplice_params
    params.require(:accomplice).permit(:order_sort, :title, :user_id, :biz_id)
  end
end
