require "rails_helper"

RSpec.describe "links/index", type: :view do
  before(:each) do
    assign(:links, [
      Link.create!(
        source: nil,
        target: nil,
        link_type: "Link Type",
        user: nil,
        biz: nil
      ),
      Link.create!(
        source: nil,
        target: nil,
        link_type: "Link Type",
        user: nil,
        biz: nil
      )
    ])
  end

  it "renders a list of links" do
    render
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: "Link Type".to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
  end
end
