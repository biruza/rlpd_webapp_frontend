require "rails_helper"

RSpec.describe BoardUsersController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/board_users").to route_to("board_users#index")
    end

    it "routes to #new" do
      expect(get: "/board_users/new").to route_to("board_users#new")
    end

    it "routes to #show" do
      expect(get: "/board_users/1").to route_to("board_users#show", id: "1")
    end

    it "routes to #edit" do
      expect(get: "/board_users/1/edit").to route_to("board_users#edit", id: "1")
    end

    it "routes to #create" do
      expect(post: "/board_users").to route_to("board_users#create")
    end

    it "routes to #update via PUT" do
      expect(put: "/board_users/1").to route_to("board_users#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/board_users/1").to route_to("board_users#update", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/board_users/1").to route_to("board_users#destroy", id: "1")
    end
  end
end
