require "test_helper"

class ClassscheduleStudyperiodsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @classschedule_studyperiod = classschedule_studyperiods(:one)
  end

  test "should get index" do
    get classschedule_studyperiods_url
    assert_response :success
  end

  test "should get new" do
    get new_classschedule_studyperiod_url
    assert_response :success
  end

  test "should create classschedule_studyperiod" do
    assert_difference("ClassscheduleStudyperiod.count") do
      post classschedule_studyperiods_url, params: {classschedule_studyperiod: {classroom_id: @classschedule_studyperiod.classroom_id, course_subject_id: @classschedule_studyperiod.course_subject_id, klass_id: @classschedule_studyperiod.klass_id, school_year_id: @classschedule_studyperiod.school_year_id, studyperiod_id: @classschedule_studyperiod.studyperiod_id, term_id: @classschedule_studyperiod.term_id, time: @classschedule_studyperiod.time}}
    end

    assert_redirected_to classschedule_studyperiod_url(ClassscheduleStudyperiod.last)
  end

  test "should show classschedule_studyperiod" do
    get classschedule_studyperiod_url(@classschedule_studyperiod)
    assert_response :success
  end

  test "should get edit" do
    get edit_classschedule_studyperiod_url(@classschedule_studyperiod)
    assert_response :success
  end

  test "should update classschedule_studyperiod" do
    patch classschedule_studyperiod_url(@classschedule_studyperiod), params: {classschedule_studyperiod: {classroom_id: @classschedule_studyperiod.classroom_id, course_subject_id: @classschedule_studyperiod.course_subject_id, klass_id: @classschedule_studyperiod.klass_id, school_year_id: @classschedule_studyperiod.school_year_id, studyperiod_id: @classschedule_studyperiod.studyperiod_id, term_id: @classschedule_studyperiod.term_id, time: @classschedule_studyperiod.time}}
    assert_redirected_to classschedule_studyperiod_url(@classschedule_studyperiod)
  end

  test "should destroy classschedule_studyperiod" do
    assert_difference("ClassscheduleStudyperiod.count", -1) do
      delete classschedule_studyperiod_url(@classschedule_studyperiod)
    end

    assert_redirected_to classschedule_studyperiods_url
  end
end
