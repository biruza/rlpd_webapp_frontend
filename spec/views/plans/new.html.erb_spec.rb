require "rails_helper"

RSpec.describe "plans/new", type: :view do
  before(:each) do
    assign(:plan, Plan.new(
      title: "MyString",
      description: "MyText",
      slug: "MyString",
      status: "MyString",
      user: nil,
      biz: nil
    ))
  end

  it "renders new plan form" do
    render

    assert_select "form[action=?][method=?]", plans_path, "post" do
      assert_select "input[name=?]", "plan[title]"

      assert_select "textarea[name=?]", "plan[description]"

      assert_select "input[name=?]", "plan[slug]"

      assert_select "input[name=?]", "plan[status]"

      assert_select "input[name=?]", "plan[user_id]"

      assert_select "input[name=?]", "plan[biz_id]"
    end
  end
end
