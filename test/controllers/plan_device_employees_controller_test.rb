require "test_helper"

class PlanDeviceEmployeesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @plan_device_employee = plan_device_employees(:one)
  end

  test "should get index" do
    get plan_device_employees_url
    assert_response :success
  end

  test "should get new" do
    get new_plan_device_employee_url
    assert_response :success
  end

  test "should create plan_device_employee" do
    assert_difference("PlanDeviceEmployee.count") do
      post plan_device_employees_url, params: {plan_device_employee: {employee_id: @plan_device_employee.employee_id, plan_device_id: @plan_device_employee.plan_device_id}}
    end

    assert_redirected_to plan_device_employee_url(PlanDeviceEmployee.last)
  end

  test "should show plan_device_employee" do
    get plan_device_employee_url(@plan_device_employee)
    assert_response :success
  end

  test "should get edit" do
    get edit_plan_device_employee_url(@plan_device_employee)
    assert_response :success
  end

  test "should update plan_device_employee" do
    patch plan_device_employee_url(@plan_device_employee), params: {plan_device_employee: {employee_id: @plan_device_employee.employee_id, plan_device_id: @plan_device_employee.plan_device_id}}
    assert_redirected_to plan_device_employee_url(@plan_device_employee)
  end

  test "should destroy plan_device_employee" do
    assert_difference("PlanDeviceEmployee.count", -1) do
      delete plan_device_employee_url(@plan_device_employee)
    end

    assert_redirected_to plan_device_employees_url
  end
end
