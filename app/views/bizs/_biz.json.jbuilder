json.extract! biz, :id, :package_id, :start_date, :qty_package, :status, :title_th, :title_en, :address_th, :address_en, :phone, :fax, :email, :taxid, :image_data, :created_at, :updated_at
json.url biz_url(biz, format: :json)
