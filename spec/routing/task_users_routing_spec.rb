require "rails_helper"

RSpec.describe TaskUsersController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/task_users").to route_to("task_users#index")
    end

    it "routes to #new" do
      expect(get: "/task_users/new").to route_to("task_users#new")
    end

    it "routes to #show" do
      expect(get: "/task_users/1").to route_to("task_users#show", id: "1")
    end

    it "routes to #edit" do
      expect(get: "/task_users/1/edit").to route_to("task_users#edit", id: "1")
    end

    it "routes to #create" do
      expect(post: "/task_users").to route_to("task_users#create")
    end

    it "routes to #update via PUT" do
      expect(put: "/task_users/1").to route_to("task_users#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/task_users/1").to route_to("task_users#update", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/task_users/1").to route_to("task_users#destroy", id: "1")
    end
  end
end
