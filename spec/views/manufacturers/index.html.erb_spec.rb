require "rails_helper"

RSpec.describe "manufacturers/index", type: :view do
  before(:each) do
    assign(:manufacturers, [
      Manufacturer.create!(
        name: "Name",
        image_data: "MyText",
        address: "MyText",
        phone: "Phone",
        fax: "Fax",
        email: "Email",
        slug: "Slug",
        personal_firstname: "Personal Firstname",
        personal_lastname: "Personal Lastname",
        personal_phone: "Personal Phone",
        personal_email: "Personal Email",
        user: nil,
        biz: nil
      ),
      Manufacturer.create!(
        name: "Name",
        image_data: "MyText",
        address: "MyText",
        phone: "Phone",
        fax: "Fax",
        email: "Email",
        slug: "Slug",
        personal_firstname: "Personal Firstname",
        personal_lastname: "Personal Lastname",
        personal_phone: "Personal Phone",
        personal_email: "Personal Email",
        user: nil,
        biz: nil
      )
    ])
  end

  it "renders a list of manufacturers" do
    render
    assert_select "tr>td", text: "Name".to_s, count: 2
    assert_select "tr>td", text: "MyText".to_s, count: 2
    assert_select "tr>td", text: "MyText".to_s, count: 2
    assert_select "tr>td", text: "Phone".to_s, count: 2
    assert_select "tr>td", text: "Fax".to_s, count: 2
    assert_select "tr>td", text: "Email".to_s, count: 2
    assert_select "tr>td", text: "Slug".to_s, count: 2
    assert_select "tr>td", text: "Personal Firstname".to_s, count: 2
    assert_select "tr>td", text: "Personal Lastname".to_s, count: 2
    assert_select "tr>td", text: "Personal Phone".to_s, count: 2
    assert_select "tr>td", text: "Personal Email".to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
  end
end
