require "rails_helper"

RSpec.describe "device_categories/show", type: :view do
  before(:each) do
    @device_category = assign(:device_category, DeviceCategory.create!(
      title: "Title",
      image_data: "MyText",
      description: "MyText",
      slug: "Slug",
      user: nil,
      biz: nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/Slug/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
  end
end
