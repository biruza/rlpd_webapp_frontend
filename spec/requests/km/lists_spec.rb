require "rails_helper"

RSpec.describe "Km::Lists", type: :request do
  describe "GET /index" do
    it "returns http success" do
      get "/km/lists/index"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /show" do
    it "returns http success" do
      get "/km/lists/show"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /new" do
    it "returns http success" do
      get "/km/lists/new"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /edit" do
    it "returns http success" do
      get "/km/lists/edit"
      expect(response).to have_http_status(:success)
    end
  end
end
