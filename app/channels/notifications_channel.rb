class NotificationsChannel < ApplicationCable::Channel
  def subscribed
    # stream_from "notifications_channel:#{current_user.id}"
    stream_from "notifications_channel"
  end

  def unsubscribed
    stop_all_streams
  end
end
