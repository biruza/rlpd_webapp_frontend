require "test_helper"

class ExpenseStudentRecordsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @expense_student_record = expense_student_records(:one)
  end

  test "should get index" do
    get expense_student_records_url
    assert_response :success
  end

  test "should get new" do
    get new_expense_student_record_url
    assert_response :success
  end

  test "should create expense_student_record" do
    assert_difference("ExpenseStudentRecord.count") do
      post expense_student_records_url, params: {expense_student_record: {action_date: @expense_student_record.action_date, amount: @expense_student_record.amount, description: @expense_student_record.description, expense_student_id: @expense_student_record.expense_student_id, image_data: @expense_student_record.image_data, user_id: @expense_student_record.user_id}}
    end

    assert_redirected_to expense_student_record_url(ExpenseStudentRecord.last)
  end

  test "should show expense_student_record" do
    get expense_student_record_url(@expense_student_record)
    assert_response :success
  end

  test "should get edit" do
    get edit_expense_student_record_url(@expense_student_record)
    assert_response :success
  end

  test "should update expense_student_record" do
    patch expense_student_record_url(@expense_student_record), params: {expense_student_record: {action_date: @expense_student_record.action_date, amount: @expense_student_record.amount, description: @expense_student_record.description, expense_student_id: @expense_student_record.expense_student_id, image_data: @expense_student_record.image_data, user_id: @expense_student_record.user_id}}
    assert_redirected_to expense_student_record_url(@expense_student_record)
  end

  test "should destroy expense_student_record" do
    assert_difference("ExpenseStudentRecord.count", -1) do
      delete expense_student_record_url(@expense_student_record)
    end

    assert_redirected_to expense_student_records_url
  end
end
