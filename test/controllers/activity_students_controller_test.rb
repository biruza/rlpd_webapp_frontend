require "test_helper"

class ActivityStudentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @activity_student = activity_students(:one)
  end

  test "should get index" do
    get activity_students_url
    assert_response :success
  end

  test "should get new" do
    get new_activity_student_url
    assert_response :success
  end

  test "should create activity_student" do
    assert_difference("ActivityStudent.count") do
      post activity_students_url, params: {activity_student: {activity_id: @activity_student.activity_id, student_id: @activity_student.student_id}}
    end

    assert_redirected_to activity_student_url(ActivityStudent.last)
  end

  test "should show activity_student" do
    get activity_student_url(@activity_student)
    assert_response :success
  end

  test "should get edit" do
    get edit_activity_student_url(@activity_student)
    assert_response :success
  end

  test "should update activity_student" do
    patch activity_student_url(@activity_student), params: {activity_student: {activity_id: @activity_student.activity_id, student_id: @activity_student.student_id}}
    assert_redirected_to activity_student_url(@activity_student)
  end

  test "should destroy activity_student" do
    assert_difference("ActivityStudent.count", -1) do
      delete activity_student_url(@activity_student)
    end

    assert_redirected_to activity_students_url
  end
end
