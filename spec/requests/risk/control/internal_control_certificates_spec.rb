require 'rails_helper'

RSpec.describe "Risk::Control::InternalControlCertificates", type: :request do
  describe "GET /edit" do
    it "returns http success" do
      get "/risk/control/internal_control_certificates/edit"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /index" do
    it "returns http success" do
      get "/risk/control/internal_control_certificates/index"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /new" do
    it "returns http success" do
      get "/risk/control/internal_control_certificates/new"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /show" do
    it "returns http success" do
      get "/risk/control/internal_control_certificates/show"
      expect(response).to have_http_status(:success)
    end
  end

end
