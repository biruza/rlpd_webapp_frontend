require "rails_helper"

RSpec.describe "Km::FlowItems", type: :request do
  describe "GET /index" do
    it "returns http success" do
      get "/km/flow_items/index"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /show" do
    it "returns http success" do
      get "/km/flow_items/show"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /edit" do
    it "returns http success" do
      get "/km/flow_items/edit"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /new" do
    it "returns http success" do
      get "/km/flow_items/new"
      expect(response).to have_http_status(:success)
    end
  end
end
