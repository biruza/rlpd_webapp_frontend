require "csv"

desc "Import Init Data Klass Teacher from csv file"
task import_init_data_klass_teacher: [:environment] do
  csv_text1 = File.read("db/for_init/klass_teachers.csv")
  csv1 = CSV.parse(csv_text1, headers: true)
  csv1.each do |row|
    KlassTeacher.create!(row.to_hash)
  end

  ActiveRecord::Base.connection.execute("
    select setval('klass_teachers_id_seq', m)
	from (
	    select max(id) from klass_teachers
	    ) as dt(m)
	")
end
