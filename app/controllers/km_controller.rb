class KmController < ApplicationController
  layout :layout_by_resource

  def checks
  end

  def approve
  end

  def approved
  end

  def dashboards
  end

  def status_work_user01
  end

  def create_plan_user01
  end

  def create_report_user01
  end

  def sendplan_user01
  end

  def sendreport_user01
  end

  def check_plan_user02
  end

  def check_report_user02
  end

  def comment_plan_user02
  end

  def comment_report_user02
  end

  def pmqa_admin01_user_main
  end

  def pmqa_admin01_form_main
  end
end
