class UsersController < ApplicationController
  load_and_authorize_resource
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, alert: exception.message
  end
  before_action :authenticate_user!
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :set_select, only: [:index, :new, :create, :edit, :update]

  # skip_before_action :ensure_subdomain

  def index
  end

  def new
    @urlname = "users_create"
    @user = User.new
  end

  def users_create
    # user_create  ใช้โดย superadmin, administrator , supervisor
    @urlname = "users_create"
    @user = User.new(user_params)

    if !current_user.superadmin_role?
      @user.biz_id = current_user.biz_id if current_user.biz_id.present?
    end

    # เช็คว่า ถ้าไม่ใช่ superadmin ก็บังคับให้ user ใหม่ มี parent คือของคนสร้าง
    if !current_user.superadmin_role?
      # กำหนดให้ user นี้ เป็น child ของคนสร้าง นี้
      @user.parent = current_user
      # กำหนดให้ คนสร้าง มีลูก user  นี้
      # current_user.children.create :id => @user.id
    end

    if @user.save
      flash[:notice] = "Successfully created User."
      redirect_to users_path
    else
      render action: "new"
    end
  end

  def edit
    @urlname = "update"
  end

  def update
    @urlname = "update"

    params[:user].delete(:password) if params[:user][:password].blank?
    params[:user].delete(:password_confirmation) if params[:user][:password].blank? && params[:user][:password_confirmation].blank?

    # if !@user.biz_id.blank?
    #  params[:user][:biz_id] = @user.biz_id
    # end

    if @user.update(user_params)
      flash[:notice] = "Successfully updated User."

      respond_to do |format|
        if @user.update(user_params)
          if current_user.superadmin_role? || current_user.administrator_role? || current_user.supervisor_role?
            format.html { redirect_to users_path, notice: "User was successfully updated." }
          else
            format.html { redirect_to "/users/#{current_user.id}/edit", notice: "User was successfully updated." }
          end
          # format.json { render :show, status: :ok, location: @user  }
        else
          format.html { render :edit }
          # format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  def show
  end

  def destroy
    @user = User.find(params[:id])
    if @user.destroy
      flash[:notice] = "Successfully deleted User."
      redirect_to users_path
    end
  end

  private

  def set_user
    @user = if current_user.superadmin_role?
      User.find_by(id: params[:id])
    else
      User.find_by(id: params[:id], biz_id: current_user.biz_id)
    end
  end

  def set_select
    if current_user.present? && current_user.superadmin_role?
      @users = User.all.order(firstname: :asc, email: :asc)
    elsif current_user.present? && current_user.administrator_role?
      @users = User.where(biz_id: current_user.biz_id).order(firstname: :asc, email: :asc)
    elsif current_user.present? && current_user.supervisor_role?
      child_ids = current_user.child_ids.push(current_user.id)
      @users = User.where(biz_id: current_user.biz_id, id: child_ids).order(superadmin_role: :asc, administrator_role: :asc, supervisor_role: :asc, firstname: :asc, email: :asc)
    end
  end

  def user_params
    if current_user.present? && current_user.superadmin_role?
      params.require(:user).permit(:egp_role, :parent_ids, :administrator_role, :supervisor_role, :staff_role, :user_role, :biz_id, :prefix, :percode, :nation, :race, :origin, :phone, :mobile, :address, :city, :amphoe, :district, :province, :zipcode, :job, :job_location, :job_phone, :salary, :firstname, :lastname, :email, :avatar, :password, :password_confirmation, :parent_id, :institution_id)
    elsif current_user.present? && current_user.administrator_role?
      params.require(:user).permit(:parent_ids, :administrator_role, :supervisor_role, :staff_role, :user_role, :biz_id, :prefix, :percode, :nation, :race, :origin, :phone, :mobile, :address, :city, :amphoe, :district, :province, :zipcode, :job, :job_location, :job_phone, :salary, :firstname, :lastname, :email, :avatar, :password, :password_confirmation, :parent_id, :institution_id)
    elsif current_user.present? && current_user.supervisor_role?
      params.require(:user).permit(:parent_ids, :staff_role, :biz_id, :prefix, :percode, :nation, :race, :origin, :phone, :mobile, :address, :city, :amphoe, :district, :province, :zipcode, :job, :job_location, :job_phone, :salary, :firstname, :lastname, :email, :employee_id, :avatar, :password, :password_confirmation, :institution_id)
    else
      params.require(:user).permit(:parent_ids, :biz_id, :prefix, :percode, :nation, :race, :origin, :phone, :mobile, :address, :city, :amphoe, :district, :province, :zipcode, :job, :job_location, :job_phone, :salary, :firstname, :lastname, :email, :avatar, :password, :password_confirmation, :institution_id)
    end
  end
end
