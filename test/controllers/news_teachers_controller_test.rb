require "test_helper"

class NewsTeachersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @news_teacher = news_teachers(:one)
  end

  test "should get index" do
    get news_teachers_url
    assert_response :success
  end

  test "should get new" do
    get new_news_teacher_url
    assert_response :success
  end

  test "should create news_teacher" do
    assert_difference("NewsTeacher.count") do
      post news_teachers_url, params: {news_teacher: {description: @news_teacher.description, image_data: @news_teacher.image_data, teacher_id: @news_teacher.teacher_id, title_en: @news_teacher.title_en, title_th: @news_teacher.title_th}}
    end

    assert_redirected_to news_teacher_url(NewsTeacher.last)
  end

  test "should show news_teacher" do
    get news_teacher_url(@news_teacher)
    assert_response :success
  end

  test "should get edit" do
    get edit_news_teacher_url(@news_teacher)
    assert_response :success
  end

  test "should update news_teacher" do
    patch news_teacher_url(@news_teacher), params: {news_teacher: {description: @news_teacher.description, image_data: @news_teacher.image_data, teacher_id: @news_teacher.teacher_id, title_en: @news_teacher.title_en, title_th: @news_teacher.title_th}}
    assert_redirected_to news_teacher_url(@news_teacher)
  end

  test "should destroy news_teacher" do
    assert_difference("NewsTeacher.count", -1) do
      delete news_teacher_url(@news_teacher)
    end

    assert_redirected_to news_teachers_url
  end
end
