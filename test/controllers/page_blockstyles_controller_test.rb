require "test_helper"

class PageBlockstylesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @page_blockstyle = page_blockstyles(:one)
  end

  test "should get index" do
    get page_blockstyles_url
    assert_response :success
  end

  test "should get new" do
    get new_page_blockstyle_url
    assert_response :success
  end

  test "should create page_blockstyle" do
    assert_difference("PageBlockstyle.count") do
      post page_blockstyles_url, params: {page_blockstyle: {image: @page_blockstyle.image, slug: @page_blockstyle.slug, title: @page_blockstyle.title}}
    end

    assert_redirected_to page_blockstyle_url(PageBlockstyle.last)
  end

  test "should show page_blockstyle" do
    get page_blockstyle_url(@page_blockstyle)
    assert_response :success
  end

  test "should get edit" do
    get edit_page_blockstyle_url(@page_blockstyle)
    assert_response :success
  end

  test "should update page_blockstyle" do
    patch page_blockstyle_url(@page_blockstyle), params: {page_blockstyle: {image: @page_blockstyle.image, slug: @page_blockstyle.slug, title: @page_blockstyle.title}}
    assert_redirected_to page_blockstyle_url(@page_blockstyle)
  end

  test "should destroy page_blockstyle" do
    assert_difference("PageBlockstyle.count", -1) do
      delete page_blockstyle_url(@page_blockstyle)
    end

    assert_redirected_to page_blockstyles_url
  end
end
