require "rails_helper"

RSpec.describe "manufacturers/show", type: :view do
  before(:each) do
    @manufacturer = assign(:manufacturer, Manufacturer.create!(
      name: "Name",
      image_data: "MyText",
      address: "MyText",
      phone: "Phone",
      fax: "Fax",
      email: "Email",
      slug: "Slug",
      personal_firstname: "Personal Firstname",
      personal_lastname: "Personal Lastname",
      personal_phone: "Personal Phone",
      personal_email: "Personal Email",
      user: nil,
      biz: nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/Phone/)
    expect(rendered).to match(/Fax/)
    expect(rendered).to match(/Email/)
    expect(rendered).to match(/Slug/)
    expect(rendered).to match(/Personal Firstname/)
    expect(rendered).to match(/Personal Lastname/)
    expect(rendered).to match(/Personal Phone/)
    expect(rendered).to match(/Personal Email/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
  end
end
