require "rails_helper"

RSpec.describe "Km::Activities", type: :request do
  describe "GET /index" do
    it "returns http success" do
      get "/km/activities/index"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /show" do
    it "returns http success" do
      get "/km/activities/show"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /new" do
    it "returns http success" do
      get "/km/activities/new"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /edit" do
    it "returns http success" do
      get "/km/activities/edit"
      expect(response).to have_http_status(:success)
    end
  end
end
