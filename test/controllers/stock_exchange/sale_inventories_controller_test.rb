require "test_helper"

class StockExchange::SaleInventoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @stock_exchange_sale_inventory = stock_exchange_sale_inventories(:one)
  end

  test "should get index" do
    get stock_exchange_sale_inventories_url
    assert_response :success
  end

  test "should get new" do
    get new_stock_exchange_sale_inventory_url
    assert_response :success
  end

  test "should create stock_exchange_sale_inventory" do
    assert_difference("StockExchange::SaleInventory.count") do
      post stock_exchange_sale_inventories_url, params: {stock_exchange_sale_inventory: {action_date: @stock_exchange_sale_inventory.action_date, action_type: @stock_exchange_sale_inventory.action_type, biz_id: @stock_exchange_sale_inventory.biz_id, company_id: @stock_exchange_sale_inventory.company_id, deadline_date: @stock_exchange_sale_inventory.deadline_date, description_en: @stock_exchange_sale_inventory.description_en, description_th: @stock_exchange_sale_inventory.description_th, doc_number: @stock_exchange_sale_inventory.doc_number, doc_ref1: @stock_exchange_sale_inventory.doc_ref1, doc_ref2: @stock_exchange_sale_inventory.doc_ref2, doc_ref3: @stock_exchange_sale_inventory.doc_ref3, file_data: @stock_exchange_sale_inventory.file_data, user_id: @stock_exchange_sale_inventory.user_id}}
    end

    assert_redirected_to stock_exchange_sale_inventory_url(StockExchange::SaleInventory.last)
  end

  test "should show stock_exchange_sale_inventory" do
    get stock_exchange_sale_inventory_url(@stock_exchange_sale_inventory)
    assert_response :success
  end

  test "should get edit" do
    get edit_stock_exchange_sale_inventory_url(@stock_exchange_sale_inventory)
    assert_response :success
  end

  test "should update stock_exchange_sale_inventory" do
    patch stock_exchange_sale_inventory_url(@stock_exchange_sale_inventory), params: {stock_exchange_sale_inventory: {action_date: @stock_exchange_sale_inventory.action_date, action_type: @stock_exchange_sale_inventory.action_type, biz_id: @stock_exchange_sale_inventory.biz_id, company_id: @stock_exchange_sale_inventory.company_id, deadline_date: @stock_exchange_sale_inventory.deadline_date, description_en: @stock_exchange_sale_inventory.description_en, description_th: @stock_exchange_sale_inventory.description_th, doc_number: @stock_exchange_sale_inventory.doc_number, doc_ref1: @stock_exchange_sale_inventory.doc_ref1, doc_ref2: @stock_exchange_sale_inventory.doc_ref2, doc_ref3: @stock_exchange_sale_inventory.doc_ref3, file_data: @stock_exchange_sale_inventory.file_data, user_id: @stock_exchange_sale_inventory.user_id}}
    assert_redirected_to stock_exchange_sale_inventory_url(@stock_exchange_sale_inventory)
  end

  test "should destroy stock_exchange_sale_inventory" do
    assert_difference("StockExchange::SaleInventory.count", -1) do
      delete stock_exchange_sale_inventory_url(@stock_exchange_sale_inventory)
    end

    assert_redirected_to stock_exchange_sale_inventories_url
  end
end
