require "csv"

desc "mg_add_user_to_board_users"
task mg_add_user_to_board_users: [:environment] do
  Board.all do |board|
    if board.board_users.blank?
      board_user = BoardUser.new
      board_user.board_id = board.id
      board_user.user_id = board.user_id
      board_user.biz_id = board.biz_id
      board_user.primary = true
      board_user.save
    end
  end
end
