<% if namespaced? -%>
require_dependency "<%= namespaced_path %>/application_controller"

<% end -%>
<% module_namespacing do -%>
class <%= class_name.pluralize %>Controller < ApplicationController
  require 'csv'
  load_and_authorize_resource
  rescue_from CanCan::AccessDenied do |exception|
  redirect_to root_url, alert: exception.message
  end
  before_action :authenticate_user!
  before_action :set_<%= singular_table_name %>, only: %i[ show edit update destroy ]

  # GET <%= route_url %>
  # GET <%= route_url %>.json
  def index
    @<%= plural_table_name %> = <%= class_name %>.where(biz_id: current_user.biz_id)

    respond_to do |format|
      format.html
      format.json
      format.csv { send_data @<%= plural_table_name %>.to_csv }
      format.pdf do
        render pdf: "<%= plural_table_name %>-#{Time.now.strftime('%Y%m%d-%H%M')}",
        page_size: 'A4',
        layout: "pdf.pdf",
        orientation: "Portrait",
        lowquality: true,
        zoom: 1,
        dpi: 100
      end
    end

  end

  # GET <%= route_url %>/1
  # GET <%= route_url %>/1.json
  def show
  end

  # GET <%= route_url %>/new
  def new
    @<%= singular_table_name %> = <%= class_name %>.new
  end

  # GET <%= route_url %>/1/edit
  def edit
  end

  # POST <%= route_url %>
  # POST <%= route_url %>.json
  def create
    @<%= singular_table_name %> = <%= class_name %>.new(<%= singular_table_name %>_params)

    @<%= singular_table_name %>.user_id = current_user.id
    @<%= singular_table_name %>.biz_id = current_user.biz_id

    respond_to do |format|
      if @<%= singular_table_name %>.save
        format.html { redirect_to <%= index_helper %>_url, notice: <%= "'#{human_name} was successfully created.'" %> }
        format.json { render :show, status: :created, location: @<%= singular_table_name %> }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @<%= singular_table_name %>.errors, status: :unprocessable_entity }
      end
    end

  end

  # PATCH/PUT <%= route_url %>/1
  # PATCH/PUT <%= route_url %>/1.json
  def update
    respond_to do |format|
      if @<%= singular_table_name %>.update(<%= singular_table_name %>_params)
        format.html { redirect_to <%= index_helper %>_url, notice: <%= "'#{human_name} was successfully updated.'" %> }
        format.json { render :show, status: :ok, location: @<%= singular_table_name %> }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @<%= singular_table_name %>.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE <%= route_url %>/1
  # DELETE <%= route_url %>/1.json
  def destroy
    @<%= singular_table_name %>.destroy
    respond_to do |format|
      format.html { redirect_to <%= index_helper %>_url, notice: <%= "'#{human_name} was successfully destroyed.'" %> }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_<%= singular_table_name %>
      @<%= singular_table_name %> = <%= class_name %>.find_by(id: params[:id],biz_id: current_user.biz_id)
    end

    # Only allow a list of trusted parameters through.
    def <%= "#{singular_table_name}_params" %>
      <%- if attributes_names.empty? -%>
      params.fetch(:<%= singular_table_name %>, {})
      <%- else -%>
      # params.require(:<%= singular_table_name %>).permit(:<%= attributes.map(&:name).join(', :') %>)
      params.require(:<%= singular_table_name %>).permit(<%= permitted_params %>)
      <%- end -%>
    end
end
<% end -%>
