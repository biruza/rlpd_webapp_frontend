require "rails_helper"

RSpec.describe "tpl_lists/edit", type: :view do
  before(:each) do
    @tpl_list = assign(:tpl_list, TplList.create!(
      list: nil,
      biz: nil
    ))
  end

  it "renders the edit tpl_list form" do
    render

    assert_select "form[action=?][method=?]", tpl_list_path(@tpl_list), "post" do
      assert_select "input[name=?]", "tpl_list[list_id]"

      assert_select "input[name=?]", "tpl_list[biz_id]"
    end
  end
end
