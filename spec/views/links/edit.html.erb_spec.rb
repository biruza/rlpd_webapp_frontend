require "rails_helper"

RSpec.describe "links/edit", type: :view do
  before(:each) do
    @link = assign(:link, Link.create!(
      source: nil,
      target: nil,
      link_type: "MyString",
      user: nil,
      biz: nil
    ))
  end

  it "renders the edit link form" do
    render

    assert_select "form[action=?][method=?]", link_path(@link), "post" do
      assert_select "input[name=?]", "link[source_id]"

      assert_select "input[name=?]", "link[target_id]"

      assert_select "input[name=?]", "link[link_type]"

      assert_select "input[name=?]", "link[user_id]"

      assert_select "input[name=?]", "link[biz_id]"
    end
  end
end
