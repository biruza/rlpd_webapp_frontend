require "test_helper"

class WebTemplatesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @web_template = web_templates(:one)
  end

  test "should get index" do
    get web_templates_url
    assert_response :success
  end

  test "should get new" do
    get new_web_template_url
    assert_response :success
  end

  test "should create web_template" do
    assert_difference("WebTemplate.count") do
      post web_templates_url, params: {web_template: {biz_id: @web_template.biz_id, description_en: @web_template.description_en, description_th: @web_template.description_th, image_data: @web_template.image_data, order_number: @web_template.order_number, slug: @web_template.slug, title_en: @web_template.title_en, title_th: @web_template.title_th, url: @web_template.url}}
    end

    assert_redirected_to web_template_url(WebTemplate.last)
  end

  test "should show web_template" do
    get web_template_url(@web_template)
    assert_response :success
  end

  test "should get edit" do
    get edit_web_template_url(@web_template)
    assert_response :success
  end

  test "should update web_template" do
    patch web_template_url(@web_template), params: {web_template: {biz_id: @web_template.biz_id, description_en: @web_template.description_en, description_th: @web_template.description_th, image_data: @web_template.image_data, order_number: @web_template.order_number, slug: @web_template.slug, title_en: @web_template.title_en, title_th: @web_template.title_th, url: @web_template.url}}
    assert_redirected_to web_template_url(@web_template)
  end

  test "should destroy web_template" do
    assert_difference("WebTemplate.count", -1) do
      delete web_template_url(@web_template)
    end

    assert_redirected_to web_templates_url
  end
end
