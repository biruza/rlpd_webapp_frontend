require "test_helper"

class RecordSaItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @record_sa_item = record_sa_items(:one)
  end

  test "should get index" do
    get record_sa_items_url
    assert_response :success
  end

  test "should get new" do
    get new_record_sa_item_url
    assert_response :success
  end

  test "should create record_sa_item" do
    assert_difference("RecordSaItem.count") do
      post record_sa_items_url, params: {record_sa_item: {course_subject_student_id: @record_sa_item.course_subject_student_id, record_school_attendance_id: @record_sa_item.record_school_attendance_id, status: @record_sa_item.status, value: @record_sa_item.value}}
    end

    assert_redirected_to record_sa_item_url(RecordSaItem.last)
  end

  test "should show record_sa_item" do
    get record_sa_item_url(@record_sa_item)
    assert_response :success
  end

  test "should get edit" do
    get edit_record_sa_item_url(@record_sa_item)
    assert_response :success
  end

  test "should update record_sa_item" do
    patch record_sa_item_url(@record_sa_item), params: {record_sa_item: {course_subject_student_id: @record_sa_item.course_subject_student_id, record_school_attendance_id: @record_sa_item.record_school_attendance_id, status: @record_sa_item.status, value: @record_sa_item.value}}
    assert_redirected_to record_sa_item_url(@record_sa_item)
  end

  test "should destroy record_sa_item" do
    assert_difference("RecordSaItem.count", -1) do
      delete record_sa_item_url(@record_sa_item)
    end

    assert_redirected_to record_sa_items_url
  end
end
