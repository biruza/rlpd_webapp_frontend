require "test_helper"

class PostsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @post = posts(:one)
  end

  test "should get index" do
    get posts_url
    assert_response :success
  end

  test "should get new" do
    get new_post_url
    assert_response :success
  end

  test "should create post" do
    assert_difference("Post.count") do
      post posts_url, params: {post: {biz_id: @post.biz_id, description_en: @post.description_en, description_th: @post.description_th, image_data: @post.image_data, order_number: @post.order_number, post_collection_id: @post.post_collection_id, post_type_id: @post.post_type_id, publish_end_date: @post.publish_end_date, publish_start_date: @post.publish_start_date, slug: @post.slug, title_en: @post.title_en, title_th: @post.title_th, user: @post.user}}
    end

    assert_redirected_to post_url(Post.last)
  end

  test "should show post" do
    get post_url(@post)
    assert_response :success
  end

  test "should get edit" do
    get edit_post_url(@post)
    assert_response :success
  end

  test "should update post" do
    patch post_url(@post), params: {post: {biz_id: @post.biz_id, description_en: @post.description_en, description_th: @post.description_th, image_data: @post.image_data, order_number: @post.order_number, post_collection_id: @post.post_collection_id, post_type_id: @post.post_type_id, publish_end_date: @post.publish_end_date, publish_start_date: @post.publish_start_date, slug: @post.slug, title_en: @post.title_en, title_th: @post.title_th, user: @post.user}}
    assert_redirected_to post_url(@post)
  end

  test "should destroy post" do
    assert_difference("Post.count", -1) do
      delete post_url(@post)
    end

    assert_redirected_to posts_url
  end
end
