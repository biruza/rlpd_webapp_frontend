module Api
  module V1
    class LocationsController < ApplicationController
      before_action :set_user, only: [:index]
      # before_action :authenticate_request!

      def index
        @locations = Location.where(user_id: @user.id)
        render json: @locations.as_json(only: [:id, :title, :lat, :lng],
          include: {
            user: {only: [:email]}
          }), message: "Success"
      end

      def create
        @location = Location.new(location_params)

        respond_to do |format|
          if @location.save
            render json: @locations.as_json(only: [:id, :title, :image_data]), message: "Success"
          else
            render json: @locations.as_json(only: [:id, :title, :image_data]), status: :unprocessable_entity
          end
        end
      end

      private

      def set_user
        @user = User.find(auth_token[:user_id]) if auth_token
      end

      def location_params
        params.require(:location).permit(:title, :image_data, :description, :lat, :lng, :slug)
      end
    end
  end
end
