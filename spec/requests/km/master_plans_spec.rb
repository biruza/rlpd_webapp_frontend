require "rails_helper"

RSpec.describe "Km::MasterPlans", type: :request do
  describe "GET /index" do
    it "returns http success" do
      get "/km/master_plans/index"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /show" do
    it "returns http success" do
      get "/km/master_plans/show"
      expect(response).to have_http_status(:success)
    end
  end
end
