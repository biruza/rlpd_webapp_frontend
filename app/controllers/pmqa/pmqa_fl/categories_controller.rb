class Pmqa::PmqaFl::CategoriesController < ApplicationController
  before_action :set_default, only: %i[index show new edit create update destroy print]
  before_action :set_select, only: %i[ ]
  @sub_system = "/pmqa_fl"

  def index
    url = URI("#{$server[@controller_title]}/api/v1/categories")
    @datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/categories/#{params["id"]}")
    @datas = request_api(url, "get", nil)
  end

  def new
    #     url = URI("#{$server[@controller_title]}/api/v1/plans")
    #     @data_plans = request_api(url,'get',nil)
    #
    #     url = URI("#{$server[@controller_title]}/api/v1/institutions")
    #     @data_institutions = request_api(url,'get',nil)
    #
    #     url = URI("#{$server[@controller_title]}/api/v1/responsibles")
    #     @data_responsibles = request_api(url,'get',nil)
    #
    #     url = URI("#{$server[@controller_title]}/api/v1/issues")
    #     @data_issues = request_api(url,'get',nil)
    #
    #     url = URI("#{$server[@controller_title]}/api/v1/kpis")
    #     @data_kpis = request_api(url,'get',nil)
    #
    #     url = URI("#{$server[@controller_title]}/api/v1/target_kpis")
    #     @data_target_kpis = request_api(url,'get',nil)
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/categories/#{params["id"]}")
    @datas = request_api(url, "get", nil)



  end

  def create
    # params[:category][:user_id] = $user[session.id.to_s][@controller_title]['id']
    # params[:category][:biz_id] = $user[session.id.to_s][@controller_title]['biz_id']

    url = URI("#{$server[@controller_title]}/api/v1/categories")
    @datas = request_api(url, "post", category_params)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/plans/#{@datas["data"]["plan_id"]}", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_main_url}/plans/#{params["category"]["plan_id"]}", alert: @datas["data"]
    end
  end

  def update


  #  idx = 0
=begin
    if !params[:category][:lists_attributes].blank?
      params[:category][:lists_attributes].each do |item|
       
           params[:category][:lists_attributes]["#{idx}"]['order_sort'] =params[:category][:lists_attributes]["#{idx}"][:order_sort]
           params[:category][:lists_attributes]["#{idx}"]['title'] = params[:category][:lists_attributes]["#{idx}"][:title]
           params[:category][:lists_attributes]["#{idx}"]['fl_total_score'] = params[:category][:lists_attributes]["#{idx}"][:fl_total_score]
           params[:category][:lists_attributes]["#{idx}"]['slug'] = params[:category][:lists_attributes]["#{idx}"][:slug]
           params[:category][:lists_attributes]["#{idx}"]['fl_kpi_value'] = params[:category][:lists_attributes]["#{idx}"][:fl_kpi_value]
           params[:category][:lists_attributes]["#{idx}"]['fl_first_value'] = params[:category][:lists_attributes]["#{idx}"][:fl_first_value]
           params[:category][:lists_attributes]["#{idx}"]['fl_second_value'] = params[:category][:lists_attributes]["#{idx}"][:fl_second_value]
           params[:category][:lists_attributes]["#{idx}"]['fl_third_value'] = params[:category][:lists_attributes]["#{idx}"][:fl_third_value]
        idx += 1
      end
    end
=end

    category_params = {
      plan_id: params[:category][:plan_id],
      order_sort: params[:category][:order_sort],
      title: params[:category][:title],
      fl_total_score: params[:category][:fl_total_score],
      slug: params[:category][:slug],
      fl_kpi_year: Date.new(params[:category][:fl_kpi_year].to_i - 543, 1, 1),
      fl_first_year: Date.new(params[:category][:fl_first_year].to_i - 543, 1, 1),
      fl_second_year: Date.new(params[:category][:fl_second_year].to_i - 543, 1, 1),
      fl_third_year: Date.new(params[:category][:fl_third_year].to_i - 543, 1, 1)
=begin
     lists_attributes: {
      "#{idx}": {
        order_sort: params[:category][:lists_attributes]["0"][:order_sort],
        title: params[:category][:lists_attributes]["0"][:title],
        fl_total_score: params[:category][:lists_attributes]["0"][:fl_total_score],
        fl_kpi_value: params[:category][:lists_attributes]["0"][:fl_kpi_value],
        fl_first_value: params[:category][:lists_attributes]["0"][:fl_first_value],
        fl_second_value: params[:category][:lists_attributes]["0"][:fl_second_value],
        fl_third_value: params[:category][:lists_attributes]["0"][:fl_third_value],
        user_id: $user[session.id.to_s][@controller_title]["id"],
        biz_id: $user[session.id.to_s][@controller_title]["biz_id"]
      }
    
  }
=end
    }
  # params['fl_kpi_year'] =(params['fl_kpi_year'] - 543).to_date
  p "=============="
  p "==lists_attributes==#{params[:category][:lists_attributes]}"
p "=============="

p "====category_params====>#{category_params}"


    url = URI("#{$server[@controller_title]}/api/v1/categories/#{params["id"]}")
    @datas = request_api(url, "patch", category_params)

    if @datas && @datas["status"] == "Success"
      # redirect_to "/#{@controller_title}/master_plans/#{@datas['data']['plan']['master_plan_id']}", notice: @datas['message']
      #redirect_to "/#{@controller_main_url}/categories/#{params["id"]}/edit?plan_id=#{params["category"]["plan_id"]}", notice: @datas["message"]
      redirect_to "/#{@controller_main_url}/plans/#{params["category"]["plan_id"]}", notice: @datas["message"]

    else
      redirect_to "/#{@controller_main_url}/categories/#{params["id"]}/edit?plan_id=#{params["category"]["plan_id"]}", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/categories/#{params["id"]}")
    @datas_ori = request_api(url, "get", nil)
    @datas = request_api(url, "delete", nil)

    url_plan = URI("#{$server[@controller_title]}/api/v1/plans/#{@datas_ori["data"]["plan_id"]}")
    @plan = request_api(url_plan, "get", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/master_plans/#{@plan["data"]["master_plan_id"]}", notice: @datas["message"]
    else
      redirect_to "/#{@controller_main_url}/master_plans/#{@plan["data"]["master_plan_id"]}", alert: t(:can_not_delete)
    end
  end

  private

  def set_default
    @sub_system = "pmqa_fl"
    @controller_main_url = "#{@controller_title}/#{@sub_system}"
  end

  def set_select
    url = URI("#{$server[@controller_title]}/api/v1/plans/#{params["plan_id"]}")
    @data_plan = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/plans")
    @data_plans = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/institutions")
    @data_institutions = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/responsibles")
    @data_responsibles = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/issues")
    @data_issues = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/kpis")
    @data_kpis = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/target_kpis")
    @data_target_kpis = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/activities")
    @data_activities = request_api(url, "get", nil)
  end

  def category_params
    params.require(:category).permit(:fl_kpi_year, :fl_first_year, :fl_second_year, :fl_third_year, :order_sort, :title, :fl_total_score, :slug, :approve, :plan_id, :user_id, :biz_id,
   # lists_attributes: [:id, :_destroy, :category_id, :title, :status, :slug, :order_sort, :activity_id, :fl_kpi_value, :fl_first_value, :fl_second_value, :fl_third_value, :user_id, :biz_id,
    lists_attributes: [:id, :_destroy, :order_sort, :title, :status, :slug, :category_id, :category_master_list_item_id, :fl_kpi_value, :fl_first_value, :fl_second_value, :fl_third_value, :user_id, :biz_id,
        tasks_attributes: [:id, :_destroy, :category_id, :slug, :status, :order_sort, :list_id, :plan_report_id, :user_id, :biz_id]])
  end

  def authenticate_user!
    if user_signed_in?
      super
    else
      redirect_to "accounts/sign_in", notice: "Please Login to view that page!"
    end
  end
end
