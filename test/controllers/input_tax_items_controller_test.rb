require "test_helper"

class InputTaxItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @input_tax_item = input_tax_items(:one)
  end

  test "should get index" do
    get input_tax_items_url
    assert_response :success
  end

  test "should get new" do
    get new_input_tax_item_url
    assert_response :success
  end

  test "should create input_tax_item" do
    assert_difference("InputTaxItem.count") do
      post input_tax_items_url, params: {input_tax_item: {amount: @input_tax_item.amount, input_tax_id: @input_tax_item.input_tax_id, order: @input_tax_item.order, price: @input_tax_item.price, product_desc: @input_tax_item.product_desc, product_id: @input_tax_item.product_id, product_title: @input_tax_item.product_title, qty: @input_tax_item.qty}}
    end

    assert_redirected_to input_tax_item_url(InputTaxItem.last)
  end

  test "should show input_tax_item" do
    get input_tax_item_url(@input_tax_item)
    assert_response :success
  end

  test "should get edit" do
    get edit_input_tax_item_url(@input_tax_item)
    assert_response :success
  end

  test "should update input_tax_item" do
    patch input_tax_item_url(@input_tax_item), params: {input_tax_item: {amount: @input_tax_item.amount, input_tax_id: @input_tax_item.input_tax_id, order: @input_tax_item.order, price: @input_tax_item.price, product_desc: @input_tax_item.product_desc, product_id: @input_tax_item.product_id, product_title: @input_tax_item.product_title, qty: @input_tax_item.qty}}
    assert_redirected_to input_tax_item_url(@input_tax_item)
  end

  test "should destroy input_tax_item" do
    assert_difference("InputTaxItem.count", -1) do
      delete input_tax_item_url(@input_tax_item)
    end

    assert_redirected_to input_tax_items_url
  end
end
