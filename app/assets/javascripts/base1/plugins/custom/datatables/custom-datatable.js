// Demo datatables
// -----------------------------------


(function(window, document, $, undefined){

  if ( ! $.fn.dataTable ) return;

  $(function(){

    //
    // Zero configuration
    //

    $('#datatable1').dataTable({
        'paging':   true,  // Table pagination
        'ordering': true,  // Column ordering
        'info':     true,  // Bottom left status text
        'responsive': false, // https://datatables.net/extensions/responsive/examples/
        'pageLength': 50,
        // Text translation options
        // Note the required keywords between underscores (e.g _MENU_)
        "order": [[ 0, "desc" ]],
        oLanguage: {
            sSearch:      '<span class="ml">ค้นหา</span>',
            sLengthMenu:  '_MENU_ จำนวนรายการต่อหน้า ',
            info:         'หน้า _PAGE_ of _PAGES_',
            zeroRecords:  'ไม่พบรายการ',
            infoEmpty:    'ไม่มีข้อมูลบันทึก',
            infoFiltered: '(filtered from _MAX_ total records)'
        },
        // Datatable Buttons setup
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'colvis',  className: 'btn btn-sm', text: 'Hide columns'},
            {extend: 'copy',  className: 'btn btn-sm' },
            {extend: 'csv',   className: 'btn btn-sm' },
            {extend: 'excel', className: 'btn btn-sm', title: 'XLS-File'},
            //{extend: 'pdf',   className: 'btn btn-sm', title: $('title').text() },//มีปัญหากับภาษาไทย
            {extend: 'print', className: 'btn btn-sm' }
        ],
        'oSearch': {"sSearch": $('#datatable_search').val() }
    });

    $('.datatable1').dataTable({
        'paging':   true,  // Table pagination
        'ordering': true,  // Column ordering
        'info':     true,  // Bottom left status text
        'responsive': false, // https://datatables.net/extensions/responsive/examples/
        'pageLength': 50,
        // Text translation options
        // Note the required keywords between underscores (e.g _MENU_)
        "order": [[ 0, "desc" ]],
        oLanguage: {
            sSearch:      '<span class="ml">ค้นหา</span>',
            sLengthMenu:  '_MENU_ จำนวนรายการต่อหน้า ',
            info:         'หน้า _PAGE_ of _PAGES_',
            zeroRecords:  'ไม่พบรายการ',
            infoEmpty:    'ไม่มีข้อมูลบันทึก',
            infoFiltered: '(filtered from _MAX_ total records)'
        },
        // Datatable Buttons setup
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'colvis',  className: 'btn btn-sm', text: 'Hide columns'},
            {extend: 'copy',  className: 'btn btn-sm' },
            {extend: 'csv',   className: 'btn btn-sm' },
            {extend: 'excel', className: 'btn btn-sm', title: 'XLS-File'},
            //{extend: 'pdf',   className: 'btn btn-sm', title: $('title').text() },//มีปัญหากับภาษาไทย
            {extend: 'print', className: 'btn btn-sm' }
        ],
        'oSearch': {"sSearch": $('#datatable_search').val() }
    });

    $('.datatable_multiple').dataTable({
        'paging':   true,  // Table pagination
        'ordering': true,  // Column ordering
        'info':     true,  // Bottom left status text
        'responsive': false, // https://datatables.net/extensions/responsive/examples/
        'pageLength': 50,
        // Text translation options
        // Note the required keywords between underscores (e.g _MENU_)
        "order": [[ 0, "desc" ]],
        oLanguage: {
            sSearch:      '<span class="ml">ค้นหา</span>',
            sLengthMenu:  '_MENU_ จำนวนรายการต่อหน้า ',
            info:         'หน้า _PAGE_ of _PAGES_',
            zeroRecords:  'ไม่พบรายการ',
            infoEmpty:    'ไม่มีข้อมูลบันทึก',
            infoFiltered: '(filtered from _MAX_ total records)'
        },
        // Datatable Buttons setup
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'colvis',  className: 'btn btn-sm', text: 'Hide columns'},
            {extend: 'copy',  className: 'btn btn-sm' },
            {extend: 'csv',   className: 'btn btn-sm' },
            {extend: 'excel', className: 'btn btn-sm', title: 'XLS-File'},
            {extend: 'pdf',   className: 'btn btn-sm', title: $('title').text() }
            ,{extend: 'print', className: 'btn btn-sm' }
        ],
        'oSearch': {"sSearch": $('#datatable_search').val() }
    });

    $('#datatable1_o1').dataTable({
        'paging':   true,  // Table pagination
        'ordering': true,  // Column ordering
        'info':     true,  // Bottom left status text
        'responsive': false, // https://datatables.net/extensions/responsive/examples/
        'pageLength': 100,
        // Text translation options
        // Note the required keywords between underscores (e.g _MENU_)
        "order": [[ 1, "desc" ]],
        oLanguage: {
            sSearch:      '<span class="ml">ค้นหา</span>',
            sLengthMenu:  '_MENU_ จำนวนรายการต่อหน้า ',
            info:         'หน้า _PAGE_ of _PAGES_',
            zeroRecords:  'ไม่พบรายการ',
            infoEmpty:    'ไม่มีข้อมูลบันทึก',
            infoFiltered: '(filtered from _MAX_ total records)'
        },
        // Datatable Buttons setup
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy',  className: 'btn btn-sm' },
            {extend: 'csv',   className: 'btn btn-sm' },
            {extend: 'excel', className: 'btn btn-sm', title: 'XLS-File'},
            {extend: 'pdf',   className: 'btn btn-sm', title: $('title').text() }
            ,{extend: 'print', className: 'btn btn-sm' }
        ],
        'oSearch': {"sSearch": $('#datatable_search').val() }
    });

    $('#datatable3_asc').dataTable({
        'paging':   true,  // Table pagination
        'ordering': true,  // Column ordering
        'info':     true,  // Bottom left status text
        'responsive': false, // https://datatables.net/extensions/responsive/examples/
        'pageLength': 50,
        // Text translation options
        // Note the required keywords between underscores (e.g _MENU_)
        "order": [[ 2, "asc" ]],
        oLanguage: {
            sSearch:      '<span class="ml">ค้นหา</span>',
            sLengthMenu:  '_MENU_ จำนวนรายการต่อหน้า ',
            info:         'หน้า _PAGE_ of _PAGES_',
            zeroRecords:  'ไม่พบรายการ',
            infoEmpty:    'ไม่มีข้อมูลบันทึก',
            infoFiltered: '(filtered from _MAX_ total records)'
        },
        // Datatable Buttons setup
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy',  className: 'btn btn-sm' },
            {extend: 'csv',   className: 'btn btn-sm' },
            {extend: 'excel', className: 'btn btn-sm', title: 'XLS-File'},
            {extend: 'pdf',   className: 'btn btn-sm', title: $('title').text() }
            ,{extend: 'print', className: 'btn btn-sm' }
        ],
        'oSearch': {"sSearch": $('#datatable_search').val() }
    });

    $('#datatable1_asc').dataTable({
        'paging':   true,  // Table pagination
        'ordering': true,  // Column ordering
        'info':     true,  // Bottom left status text
        'responsive': false, // https://datatables.net/extensions/responsive/examples/
        'pageLength': 50,
        // Text translation options
        // Note the required keywords between underscores (e.g _MENU_)
        "order": [[ 0, "asc" ]],
        oLanguage: {
            sSearch:      '<span class="ml">ค้นหา</span>',
            sLengthMenu:  '_MENU_ จำนวนรายการต่อหน้า ',
            info:         'หน้า _PAGE_ of _PAGES_',
            zeroRecords:  'ไม่พบรายการ',
            infoEmpty:    'ไม่มีข้อมูลบันทึก',
            infoFiltered: '(filtered from _MAX_ total records)'
        },
        // Datatable Buttons setup
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy',  className: 'btn btn-sm' },
            {extend: 'csv',   className: 'btn btn-sm' },
            {extend: 'excel', className: 'btn btn-sm', title: 'XLS-File'},
            {extend: 'pdf',   className: 'btn btn-sm', title: $('title').text() }
            ,{extend: 'print', className: 'btn btn-sm' }
        ]
    });

    $('#datatable_order_score_achieves').dataTable({
        'paging':   true,  // Table pagination
        //'ordering': true,  // Column ordering
        'ordering': false,  // Column ordering
        'info':     true,  // Bottom left status text
        'responsive': false, // https://datatables.net/extensions/responsive/examples/
        'pageLength': 100,
        // Text translation options
        // Note the required keywords between underscores (e.g _MENU_)
        //"order": [[ 1, "asc" ],[ 3, "asc" ]],
        "order": [[ 0, "desc" ]],
        oLanguage: {
            sSearch:      '<span class="ml">ค้นหา</span>',
            sLengthMenu:  '_MENU_ จำนวนรายการต่อหน้า ',
            info:         'หน้า _PAGE_ of _PAGES_',
            zeroRecords:  'ไม่พบรายการ',
            infoEmpty:    'ไม่มีข้อมูลบันทึก',
            infoFiltered: '(filtered from _MAX_ total records)'
        },
        // Datatable Buttons setup
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy',  className: 'btn btn-sm' },
            {extend: 'csv',   className: 'btn btn-sm' },
            {extend: 'excel', className: 'btn btn-sm', title: 'XLS-File'},
            {extend: 'pdf',   className: 'btn btn-sm', title: $('title').text() }
            ,{extend: 'print', className: 'btn btn-sm' }
        ]
    });

    $('#datatable_no_order').dataTable({
        'paging':   true,  // Table pagination
        'ordering': false,  // Column ordering
        'info':     true,  // Bottom left status text
        'responsive': false, // https://datatables.net/extensions/responsive/examples/
        'pageLength': 50,
        // Text translation options
        // Note the required keywords between underscores (e.g _MENU_)
        oLanguage: {
            sSearch:      '<span class="ml">ค้นหา</span>',
            sLengthMenu:  '_MENU_ จำนวนรายการต่อหน้า ',
            info:         'หน้า _PAGE_ of _PAGES_',
            zeroRecords:  'ไม่พบรายการ',
            infoEmpty:    'ไม่มีข้อมูลบันทึก',
            infoFiltered: '(filtered from _MAX_ total records)'
        },
        // Datatable Buttons setup
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy',  className: 'btn btn-sm' },
            {extend: 'csv',   className: 'btn btn-sm' },
            {extend: 'excel', className: 'btn btn-sm', title: 'XLS-File'},
            {extend: 'pdf',   className: 'btn btn-sm', title: $('title').text() }
            ,{extend: 'print', className: 'btn btn-sm' }
        ]
    });

    $('#datatable_no_paging').dataTable({
        'paging':   false,  // Table pagination
        'ordering': true,  // Column ordering
        'info':     true,  // Bottom left status text
        'responsive': false, // https://datatables.net/extensions/responsive/examples/
        'pageLength': 50,
        "order": [[ 0, "desc" ]],
        // Text translation options
        // Note the required keywords between underscores (e.g _MENU_)
        oLanguage: {
            sSearch:      '<span class="ml">ค้นหา</span>',
            sLengthMenu:  '_MENU_ จำนวนรายการต่อหน้า ',
            info:         'หน้า _PAGE_ of _PAGES_',
            zeroRecords:  'ไม่พบรายการ',
            infoEmpty:    'ไม่มีข้อมูลบันทึก',
            infoFiltered: '(filtered from _MAX_ total records)'
        },
        // Datatable Buttons setup
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy',  className: 'btn btn-sm' },
            {extend: 'csv',   className: 'btn btn-sm' },
            {extend: 'excel', className: 'btn btn-sm', title: 'XLS-File'},
            {extend: 'pdf',   className: 'btn btn-sm', title: $('title').text() }
            ,{extend: 'print', className: 'btn btn-sm' }
        ]
    });

    $('#datatable1_asc_nbutton').dataTable({
        'paging':   false,  // Table pagination
        'ordering': true,  // Column ordering
        'info':     false,  // Bottom left status text
        'responsive': false, // https://datatables.net/extensions/responsive/examples/
        'pageLength': 50,
        // Text translation options
        // Note the required keywords between underscores (e.g _MENU_)
        "order": [[ 0, "asc" ]],
        oLanguage: {
            sSearch:      '<span class="ml">ค้นหา</span>',
            sLengthMenu:  '_MENU_ จำนวนรายการต่อหน้า ',
            info:         'หน้า _PAGE_ of _PAGES_',
            zeroRecords:  'ไม่พบรายการ',
            infoEmpty:    'ไม่มีข้อมูลบันทึก',
            infoFiltered: '(filtered from _MAX_ total records)'
        },
        // Datatable Buttons setup
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
        ]
    });

    $('#datatable1_asc_nall').dataTable({
        'paging':   false,  // Table pagination
        'ordering': true,  // Column ordering
        'info':     false,  // Bottom left status text
        'responsive': false, // https://datatables.net/extensions/responsive/examples/
        'pageLength': 50,
        // Text translation options
        // Note the required keywords between underscores (e.g _MENU_)
        "order": [[ 0, "asc" ]],
        oLanguage: {
            sSearch:      '<span class="ml">ค้นหา</span>',
            sLengthMenu:  '_MENU_ จำนวนรายการต่อหน้า ',
            info:         'หน้า _PAGE_ of _PAGES_',
            zeroRecords:  'ไม่พบรายการ',
            infoEmpty:    'ไม่มีข้อมูลบันทึก',
            infoFiltered: '(filtered from _MAX_ total records)'
        },
        // Datatable Buttons setup
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
        ]
    });

    var datatable1x = $('#datatable1x').DataTable({
      // ajax: ...,
      // autoWidth: false,
      // pagingType: 'full_numbers',
      // processing: true,
      // serverSide: true,

      // Optional, if you want full pagination controls.
      // Check dataTables documentation to learn more about available options.
      // http://datatables.net/reference/option/pagingType
      oLanguage: {
            sSearch:      'Search all columns:',
            sLengthMenu:  '_MENU_ records per page',
            info:         'Showing page _PAGE_ of _PAGES_',
            zeroRecords:  'Nothing found - sorry',
            infoEmpty:    'No records available',
            infoFiltered: '(filtered from _MAX_ total records)'
        },
      dom: 'Bfrtip',
        buttons: [
            'colvis',
            'excel',
            'print',
            'csv'
        ]
    });

    datatable1x.buttons().container()
    .appendTo( $('.col-sm-6:eq(0)', datatable1x.table().container() ) );



    //
    // Filtering by Columns
    //

    var dtInstance2 = $('#datatable2').dataTable({
        'paging':   true,  // Table pagination
        'ordering': true,  // Column ordering
        'info':     true,  // Bottom left status text
        'responsive': true, // https://datatables.net/extensions/responsive/examples/
        // Text translation options
        // Note the required keywords between underscores (e.g _MENU_)
        oLanguage: {
            sSearch:      'Search all columns:',
            sLengthMenu:  '_MENU_ records per page',
            info:         'Showing page _PAGE_ of _PAGES_',
            zeroRecords:  'Nothing found - sorry',
            infoEmpty:    'No records available',
            infoFiltered: '(filtered from _MAX_ total records)'
        }
    });
    var inputSearchClass = 'datatable_input_col_search';
    var columnInputs = $('tfoot .'+inputSearchClass);

    // On input keyup trigger filtering
    columnInputs
      .keyup(function () {
          dtInstance2.fnFilter(this.value, columnInputs.index(this));
      });


    //
    // Column Visibilty Extension
    //

    $('#datatable3').dataTable({
        'paging':   true,  // Table pagination
        'ordering': true,  // Column ordering
        'info':     true,  // Bottom left status text
        'responsive': true, // https://datatables.net/extensions/responsive/examples/
        // Text translation options
        // Note the required keywords between underscores (e.g _MENU_)
        oLanguage: {
            sSearch:      'Search all columns:',
            sLengthMenu:  '_MENU_ records per page',
            info:         'Showing page _PAGE_ of _PAGES_',
            zeroRecords:  'Nothing found - sorry',
            infoEmpty:    'No records available',
            infoFiltered: '(filtered from _MAX_ total records)'
        },
        // set columns options
        'aoColumns': [
            {'bVisible':false},
            {'bVisible':true},
            {'bVisible':true},
            {'bVisible':true},
            {'bVisible':true}
        ],
        sDom:      'C<"clear">lfrtip',
        colVis: {
            order: 'alfa',
            'buttonText': 'Show/Hide Columns'
        }
    });

    $('#datatable_record_score_achieves').dataTable({
        'paging':   false,  // Table pagination
        'ordering': true,  // Column ordering
        'info':     true,  // Bottom left status text
        'responsive': false, // https://datatables.net/extensions/responsive/examples/
        'pageLength': 50,
        "order": [[ 1, "asc" ]],
        // Text translation options
        // Note the required keywords between underscores (e.g _MENU_)
        oLanguage: {
            sSearch:      '<span class="ml">ค้นหา</span>',
            sLengthMenu:  '_MENU_ จำนวนรายการต่อหน้า ',
            info:         'หน้า _PAGE_ of _PAGES_',
            zeroRecords:  'ไม่พบรายการ',
            infoEmpty:    'ไม่มีข้อมูลบันทึก',
            infoFiltered: '(filtered from _MAX_ total records)'
        },
        // Datatable Buttons setup
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy',  className: 'btn btn-sm' },
            {extend: 'csv',   className: 'btn btn-sm' },
            {extend: 'excel', className: 'btn btn-sm', title: 'XLS-File'},
            {extend: 'pdf',   className: 'btn btn-sm', title: $('title').text() }
            ,{extend: 'print', className: 'btn btn-sm' }
        ]
    });

    //
    // AJAX
    //

    $('#datatable4').dataTable({
        'paging':   true,  // Table pagination
        'ordering': true,  // Column ordering
        'info':     true,  // Bottom left status text
        'responsive': true, // https://datatables.net/extensions/responsive/examples/
        sAjaxSource: '/api/datatable',
        aoColumns: [
          { mData: 'engine' },
          { mData: 'browser' },
          { mData: 'platform' },
          { mData: 'version' },
          { mData: 'grade' }
        ]
    });
  });

})(window, document, window.jQuery);
