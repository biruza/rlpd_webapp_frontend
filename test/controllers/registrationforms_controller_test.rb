require "test_helper"

class RegistrationformsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @registrationform = registrationforms(:one)
  end

  test "should get index" do
    get registrationforms_url
    assert_response :success
  end

  test "should get new" do
    get new_registrationform_url
    assert_response :success
  end

  test "should create registrationform" do
    assert_difference("Registrationform.count") do
      post registrationforms_url, params: {registrationform: {end_date: @registrationform.end_date, registrationform_type: @registrationform.registrationform_type, start_date: @registrationform.start_date, title_en: @registrationform.title_en, title_th: @registrationform.title_th}}
    end

    assert_redirected_to registrationform_url(Registrationform.last)
  end

  test "should show registrationform" do
    get registrationform_url(@registrationform)
    assert_response :success
  end

  test "should get edit" do
    get edit_registrationform_url(@registrationform)
    assert_response :success
  end

  test "should update registrationform" do
    patch registrationform_url(@registrationform), params: {registrationform: {end_date: @registrationform.end_date, registrationform_type: @registrationform.registrationform_type, start_date: @registrationform.start_date, title_en: @registrationform.title_en, title_th: @registrationform.title_th}}
    assert_redirected_to registrationform_url(@registrationform)
  end

  test "should destroy registrationform" do
    assert_difference("Registrationform.count", -1) do
      delete registrationform_url(@registrationform)
    end

    assert_redirected_to registrationforms_url
  end
end
