require "test_helper"

class CourseSubjectStudentItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @course_subject_student_item = course_subject_student_items(:one)
  end

  test "should get index" do
    get course_subject_student_items_url
    assert_response :success
  end

  test "should get new" do
    get new_course_subject_student_item_url
    assert_response :success
  end

  test "should create course_subject_student_item" do
    assert_difference("CourseSubjectStudentItem.count") do
      post course_subject_student_items_url, params: {course_subject_student_item: {course_subject_id: @course_subject_student_item.course_subject_id, course_subject_student_id: @course_subject_student_item.course_subject_student_id, status: @course_subject_student_item.status}}
    end

    assert_redirected_to course_subject_student_item_url(CourseSubjectStudentItem.last)
  end

  test "should show course_subject_student_item" do
    get course_subject_student_item_url(@course_subject_student_item)
    assert_response :success
  end

  test "should get edit" do
    get edit_course_subject_student_item_url(@course_subject_student_item)
    assert_response :success
  end

  test "should update course_subject_student_item" do
    patch course_subject_student_item_url(@course_subject_student_item), params: {course_subject_student_item: {course_subject_id: @course_subject_student_item.course_subject_id, course_subject_student_id: @course_subject_student_item.course_subject_student_id, status: @course_subject_student_item.status}}
    assert_redirected_to course_subject_student_item_url(@course_subject_student_item)
  end

  test "should destroy course_subject_student_item" do
    assert_difference("CourseSubjectStudentItem.count", -1) do
      delete course_subject_student_item_url(@course_subject_student_item)
    end

    assert_redirected_to course_subject_student_items_url
  end
end
