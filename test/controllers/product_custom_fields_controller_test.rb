require "test_helper"

class ProductCustomFieldsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @product_custom_field = product_custom_fields(:one)
  end

  test "should get index" do
    get product_custom_fields_url
    assert_response :success
  end

  test "should get new" do
    get new_product_custom_field_url
    assert_response :success
  end

  test "should create product_custom_field" do
    assert_difference("ProductCustomField.count") do
      post product_custom_fields_url, params: {product_custom_field: {biz_id: @product_custom_field.biz_id, image_data: @product_custom_field.image_data, product_id: @product_custom_field.product_id, title_th: @product_custom_field.title_th, value_number1: @product_custom_field.value_number1, value_text1: @product_custom_field.value_text1}}
    end

    assert_redirected_to product_custom_field_url(ProductCustomField.last)
  end

  test "should show product_custom_field" do
    get product_custom_field_url(@product_custom_field)
    assert_response :success
  end

  test "should get edit" do
    get edit_product_custom_field_url(@product_custom_field)
    assert_response :success
  end

  test "should update product_custom_field" do
    patch product_custom_field_url(@product_custom_field), params: {product_custom_field: {biz_id: @product_custom_field.biz_id, image_data: @product_custom_field.image_data, product_id: @product_custom_field.product_id, title_th: @product_custom_field.title_th, value_number1: @product_custom_field.value_number1, value_text1: @product_custom_field.value_text1}}
    assert_redirected_to product_custom_field_url(@product_custom_field)
  end

  test "should destroy product_custom_field" do
    assert_difference("ProductCustomField.count", -1) do
      delete product_custom_field_url(@product_custom_field)
    end

    assert_redirected_to product_custom_fields_url
  end
end
