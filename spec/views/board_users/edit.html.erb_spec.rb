require "rails_helper"

RSpec.describe "board_users/edit", type: :view do
  before(:each) do
    @board_user = assign(:board_user, BoardUser.create!(
      board: nil,
      user: nil,
      order_sort: 1,
      biz: nil
    ))
  end

  it "renders the edit board_user form" do
    render

    assert_select "form[action=?][method=?]", board_user_path(@board_user), "post" do
      assert_select "input[name=?]", "board_user[board_id]"

      assert_select "input[name=?]", "board_user[user_id]"

      assert_select "input[name=?]", "board_user[order_sort]"

      assert_select "input[name=?]", "board_user[biz_id]"
    end
  end
end
