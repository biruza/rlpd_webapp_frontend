require "rails_helper"

RSpec.describe "devices/index", type: :view do
  before(:each) do
    assign(:devices, [
      Device.create!(
        location: nil,
        device_category: nil,
        title: "Title",
        image_data: "MyText",
        description: "MyText",
        slug: "Slug",
        price: "9.99",
        cost: "9.99",
        qty_max: 2,
        qty_min: 3,
        qty_current: 4,
        sn: "Sn",
        code: "Code",
        brand: "Brand",
        model: "Model",
        manufacturer: nil,
        user: nil,
        biz: nil
      ),
      Device.create!(
        location: nil,
        device_category: nil,
        title: "Title",
        image_data: "MyText",
        description: "MyText",
        slug: "Slug",
        price: "9.99",
        cost: "9.99",
        qty_max: 2,
        qty_min: 3,
        qty_current: 4,
        sn: "Sn",
        code: "Code",
        brand: "Brand",
        model: "Model",
        manufacturer: nil,
        user: nil,
        biz: nil
      )
    ])
  end

  it "renders a list of devices" do
    render
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: "Title".to_s, count: 2
    assert_select "tr>td", text: "MyText".to_s, count: 2
    assert_select "tr>td", text: "MyText".to_s, count: 2
    assert_select "tr>td", text: "Slug".to_s, count: 2
    assert_select "tr>td", text: "9.99".to_s, count: 2
    assert_select "tr>td", text: "9.99".to_s, count: 2
    assert_select "tr>td", text: 2.to_s, count: 2
    assert_select "tr>td", text: 3.to_s, count: 2
    assert_select "tr>td", text: 4.to_s, count: 2
    assert_select "tr>td", text: "Sn".to_s, count: 2
    assert_select "tr>td", text: "Code".to_s, count: 2
    assert_select "tr>td", text: "Brand".to_s, count: 2
    assert_select "tr>td", text: "Model".to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
  end
end
