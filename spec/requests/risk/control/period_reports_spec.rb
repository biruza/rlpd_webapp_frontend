require 'rails_helper'

RSpec.describe "Risk::Control::PeriodReports", type: :request do
  describe "GET /index" do
    it "returns http success" do
      get "/risk/control/period_reports/index"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /show" do
    it "returns http success" do
      get "/risk/control/period_reports/show"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /new" do
    it "returns http success" do
      get "/risk/control/period_reports/new"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /edit" do
    it "returns http success" do
      get "/risk/control/period_reports/edit"
      expect(response).to have_http_status(:success)
    end
  end

end
