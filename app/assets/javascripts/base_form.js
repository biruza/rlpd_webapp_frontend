// Base Form //
//= require base1/js/pages/crud/forms/widgets/bootstrap-datetimepicker.js
//= require select2
//= require custom-upload.js
//= require editable/bootstrap-editable
//= require editable/inputs-ext/wysihtml5
//= require editable/inputs-ext/bootstrap-wysihtml5
//= require editable/inputs-ext/wysihtml5-editable
//= require editable/rails
////


$(document).ready(function() {

	$('.datetimepicker-input').datetimepicker({
    	locale: 'th'
	});
	$('#datetimepicker_1_1').datetimepicker({
    	locale: 'th'
	});
	$('#datetimepicker_1_2').datetimepicker({
    	locale: 'th'
	});

	$('.select2-basic-multiple').select2({
		theme: "bootstrap"
	});

	/*
	$('.editable').editable({
		display: function(value) {
		$(this).text(value);
		}
	});
	*/

});






