require "test_helper"

class WebServicesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @web_service = web_services(:one)
  end

  test "should get index" do
    get web_services_url
    assert_response :success
  end

  test "should get new" do
    get new_web_service_url
    assert_response :success
  end

  test "should create web_service" do
    assert_difference("WebService.count") do
      post web_services_url, params: {web_service: {biz_id: @web_service.biz_id, description_th: @web_service.description_th, icon: @web_service.icon, image: @web_service.image, module_web_id: @web_service.module_web_id, title_th: @web_service.title_th, url: @web_service.url}}
    end

    assert_redirected_to web_service_url(WebService.last)
  end

  test "should show web_service" do
    get web_service_url(@web_service)
    assert_response :success
  end

  test "should get edit" do
    get edit_web_service_url(@web_service)
    assert_response :success
  end

  test "should update web_service" do
    patch web_service_url(@web_service), params: {web_service: {biz_id: @web_service.biz_id, description_th: @web_service.description_th, icon: @web_service.icon, image: @web_service.image, module_web_id: @web_service.module_web_id, title_th: @web_service.title_th, url: @web_service.url}}
    assert_redirected_to web_service_url(@web_service)
  end

  test "should destroy web_service" do
    assert_difference("WebService.count", -1) do
      delete web_service_url(@web_service)
    end

    assert_redirected_to web_services_url
  end
end
