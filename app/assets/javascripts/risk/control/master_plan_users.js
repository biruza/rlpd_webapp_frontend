$('.selectize-user_role').selectize({
	sortField: 'text',
	create: function(input, callback) {
		$.post('/user_roles.json', { user_role: { title: input } })
		.done(function(response){
			console.log(response)
			callback({value: response.id, text: response.title });
		})
	}
});


$('.selectize-user').selectize({
	sortField: 'text',
	create: function(input, callback) {
		$.post('/users.json', { user: { title: input } })
		.done(function(response){
			console.log(response)
			callback({value: response.id, text: response.title });
		})
	}
});