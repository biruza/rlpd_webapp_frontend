FactoryGirl.define do
  factory :task do
    title "MyString"
    description "MyText"
    cover "MyText"
    priority 1
    start_date "2020-02-22 09:16:29"
    end_date "2020-02-22 09:16:29"
    status 1
    achieve false
    user nil
    biz nil
  end
end
