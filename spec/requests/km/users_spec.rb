require "rails_helper"

RSpec.describe "Km::Users", type: :request do
  describe "GET /index" do
    it "returns http success" do
      get "/#{@controller_title}/users/index"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /show" do
    it "returns http success" do
      get "/#{@controller_title}/users/show"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /edit" do
    it "returns http success" do
      get "/#{@controller_title}/users/edit"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /new" do
    it "returns http success" do
      get "/#{@controller_title}/users/new"
      expect(response).to have_http_status(:success)
    end
  end
end
