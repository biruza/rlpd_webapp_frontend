require "test_helper"

class StudyperiodsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @studyperiod = studyperiods(:one)
  end

  test "should get index" do
    get studyperiods_url
    assert_response :success
  end

  test "should get new" do
    get new_studyperiod_url
    assert_response :success
  end

  test "should create studyperiod" do
    assert_difference("Studyperiod.count") do
      post studyperiods_url, params: {studyperiod: {title_en: @studyperiod.title_en, title_th: @studyperiod.title_th}}
    end

    assert_redirected_to studyperiod_url(Studyperiod.last)
  end

  test "should show studyperiod" do
    get studyperiod_url(@studyperiod)
    assert_response :success
  end

  test "should get edit" do
    get edit_studyperiod_url(@studyperiod)
    assert_response :success
  end

  test "should update studyperiod" do
    patch studyperiod_url(@studyperiod), params: {studyperiod: {title_en: @studyperiod.title_en, title_th: @studyperiod.title_th}}
    assert_redirected_to studyperiod_url(@studyperiod)
  end

  test "should destroy studyperiod" do
    assert_difference("Studyperiod.count", -1) do
      delete studyperiod_url(@studyperiod)
    end

    assert_redirected_to studyperiods_url
  end
end
