require "test_helper"

class BizsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @biz = bizs(:one)
  end

  test "should get index" do
    get bizs_url
    assert_response :success
  end

  test "should get new" do
    get new_biz_url
    assert_response :success
  end

  test "should create biz" do
    assert_difference("Biz.count") do
      post bizs_url, params: {biz: {address_en: @biz.address_en, address_th: @biz.address_th, email: @biz.email, fax: @biz.fax, image_data: @biz.image_data, package_id: @biz.package_id, phone: @biz.phone, qty_package: @biz.qty_package, start_date: @biz.start_date, status: @biz.status, taxid: @biz.taxid, title_en: @biz.title_en, title_th: @biz.title_th}}
    end

    assert_redirected_to biz_url(Biz.last)
  end

  test "should show biz" do
    get biz_url(@biz)
    assert_response :success
  end

  test "should get edit" do
    get edit_biz_url(@biz)
    assert_response :success
  end

  test "should update biz" do
    patch biz_url(@biz), params: {biz: {address_en: @biz.address_en, address_th: @biz.address_th, email: @biz.email, fax: @biz.fax, image_data: @biz.image_data, package_id: @biz.package_id, phone: @biz.phone, qty_package: @biz.qty_package, start_date: @biz.start_date, status: @biz.status, taxid: @biz.taxid, title_en: @biz.title_en, title_th: @biz.title_th}}
    assert_redirected_to biz_url(@biz)
  end

  test "should destroy biz" do
    assert_difference("Biz.count", -1) do
      delete biz_url(@biz)
    end

    assert_redirected_to bizs_url
  end
end
