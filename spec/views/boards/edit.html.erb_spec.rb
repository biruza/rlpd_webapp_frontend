require "rails_helper"

RSpec.describe "boards/edit", type: :view do
  before(:each) do
    @board = assign(:board, Board.create!(
      title: "MyString",
      description: "MyText",
      user: nil,
      biz: nil
    ))
  end

  it "renders the edit board form" do
    render

    assert_select "form[action=?][method=?]", board_path(@board), "post" do
      assert_select "input[name=?]", "board[title]"

      assert_select "textarea[name=?]", "board[description]"

      assert_select "input[name=?]", "board[user_id]"

      assert_select "input[name=?]", "board[biz_id]"
    end
  end
end
