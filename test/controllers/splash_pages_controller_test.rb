require "test_helper"

class SplashPagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @splash_page = splash_pages(:one)
  end

  test "should get index" do
    get splash_pages_url
    assert_response :success
  end

  test "should get new" do
    get new_splash_page_url
    assert_response :success
  end

  test "should create splash_page" do
    assert_difference("SplashPage.count") do
      post splash_pages_url, params: {splash_page: {biz_id: @splash_page.biz_id, description_en: @splash_page.description_en, description_th: @splash_page.description_th, image_data: @splash_page.image_data, order_number: @splash_page.order_number, publish_end_date: @splash_page.publish_end_date, publish_start_date: @splash_page.publish_start_date, slug: @splash_page.slug, title_en: @splash_page.title_en, title_th: @splash_page.title_th, user: @splash_page.user}}
    end

    assert_redirected_to splash_page_url(SplashPage.last)
  end

  test "should show splash_page" do
    get splash_page_url(@splash_page)
    assert_response :success
  end

  test "should get edit" do
    get edit_splash_page_url(@splash_page)
    assert_response :success
  end

  test "should update splash_page" do
    patch splash_page_url(@splash_page), params: {splash_page: {biz_id: @splash_page.biz_id, description_en: @splash_page.description_en, description_th: @splash_page.description_th, image_data: @splash_page.image_data, order_number: @splash_page.order_number, publish_end_date: @splash_page.publish_end_date, publish_start_date: @splash_page.publish_start_date, slug: @splash_page.slug, title_en: @splash_page.title_en, title_th: @splash_page.title_th, user: @splash_page.user}}
    assert_redirected_to splash_page_url(@splash_page)
  end

  test "should destroy splash_page" do
    assert_difference("SplashPage.count", -1) do
      delete splash_page_url(@splash_page)
    end

    assert_redirected_to splash_pages_url
  end
end
