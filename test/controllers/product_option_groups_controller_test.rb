require "test_helper"

class ProductOptionGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @product_option_group = product_option_groups(:one)
  end

  test "should get index" do
    get product_option_groups_url
    assert_response :success
  end

  test "should get new" do
    get new_product_option_group_url
    assert_response :success
  end

  test "should create product_option_group" do
    assert_difference("ProductOptionGroup.count") do
      post product_option_groups_url, params: {product_option_group: {biz_id: @product_option_group.biz_id, parent: @product_option_group.parent, product_option_id: @product_option_group.product_option_id, title_th: @product_option_group.title_th}}
    end

    assert_redirected_to product_option_group_url(ProductOptionGroup.last)
  end

  test "should show product_option_group" do
    get product_option_group_url(@product_option_group)
    assert_response :success
  end

  test "should get edit" do
    get edit_product_option_group_url(@product_option_group)
    assert_response :success
  end

  test "should update product_option_group" do
    patch product_option_group_url(@product_option_group), params: {product_option_group: {biz_id: @product_option_group.biz_id, parent: @product_option_group.parent, product_option_id: @product_option_group.product_option_id, title_th: @product_option_group.title_th}}
    assert_redirected_to product_option_group_url(@product_option_group)
  end

  test "should destroy product_option_group" do
    assert_difference("ProductOptionGroup.count", -1) do
      delete product_option_group_url(@product_option_group)
    end

    assert_redirected_to product_option_groups_url
  end
end
