require "test_helper"

class RevenueTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @revenue_type = revenue_types(:one)
  end

  test "should get index" do
    get revenue_types_url
    assert_response :success
  end

  test "should get new" do
    get new_revenue_type_url
    assert_response :success
  end

  test "should create revenue_type" do
    assert_difference("RevenueType.count") do
      post revenue_types_url, params: {revenue_type: {description: @revenue_type.description, title_en: @revenue_type.title_en, title_th: @revenue_type.title_th}}
    end

    assert_redirected_to revenue_type_url(RevenueType.last)
  end

  test "should show revenue_type" do
    get revenue_type_url(@revenue_type)
    assert_response :success
  end

  test "should get edit" do
    get edit_revenue_type_url(@revenue_type)
    assert_response :success
  end

  test "should update revenue_type" do
    patch revenue_type_url(@revenue_type), params: {revenue_type: {description: @revenue_type.description, title_en: @revenue_type.title_en, title_th: @revenue_type.title_th}}
    assert_redirected_to revenue_type_url(@revenue_type)
  end

  test "should destroy revenue_type" do
    assert_difference("RevenueType.count", -1) do
      delete revenue_type_url(@revenue_type)
    end

    assert_redirected_to revenue_types_url
  end
end
