class MainsController < ApplicationController
  def set_user_active
    url = URI("#{params[:server_url]}/api/v1/#{params[:model_name]}_users/#{params["id"]}")
    @model_user_original = request_api(url, "get", nil)
        p "======> comment_params ========> #{comment_params}"
    url = URI("#{params[:server_url]}/api/v1/mains/set_user_active/#{params["id"]}?model_name=#{params[:model_name]}_user")
    @model_user = request_api(url, "patch", comment_params)

    if @model_user["status"] == "Success"
      if params[:original_url]
        redirect_to params[:original_url], notice: @model_user["message"]
      else
        redirect_to "/#{get_server_name_by_url(params[:server_url])}/#{params[:model_name]}s/#{@model_user_original["data"]["#{params[:model_name]}_id"]}", notice: @model_user["message"]
      end
    else
      redirect_to "/#{get_server_name_by_url(params[:server_url])}/#{params[:model_name]}s/#{@model_user_original["data"]["#{params[:model_name]}_id"]}", alert: @model_user["message"]
    end
  end

  def delete_image
    @name = params[:name].to_s
    @id = params[:id].to_s
  end

  private

  def comment_params
    params.require(:comment).permit(:original_url, :status, :model_name, :id, :user_id, :user_role_id, :active, :description, :image, :order_sort,
      attachments_attributes: [:id, :_destroy, :file_data, :file, :file_data_uri, :description])
  end
end
