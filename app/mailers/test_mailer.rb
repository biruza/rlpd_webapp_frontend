class TestMailer < ActionMailer::Base
  default from: "hello.nextbiz@gmail.com"
  layout "mailer"

  def schedule_notification
    @task = params[:schedule]
    # mail(to: @task.user, subject: "My subject")
    mail(to: "monthon.biza@gmail.com", subject: "My subject")
  end
end
