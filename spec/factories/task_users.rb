FactoryGirl.define do
  factory :task_user do
    task nil
    user nil
    primary false
    status 1
    biz nil
  end
end
