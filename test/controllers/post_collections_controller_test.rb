require "test_helper"

class PostCollectionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @post_collection = post_collections(:one)
  end

  test "should get index" do
    get post_collections_url
    assert_response :success
  end

  test "should get new" do
    get new_post_collection_url
    assert_response :success
  end

  test "should create post_collection" do
    assert_difference("PostCollection.count") do
      post post_collections_url, params: {post_collection: {biz_id: @post_collection.biz_id, description_en: @post_collection.description_en, description_th: @post_collection.description_th, post_collection_parent: @post_collection.post_collection_parent, post_template: @post_collection.post_template, title_en: @post_collection.title_en, title_th: @post_collection.title_th, user_id: @post_collection.user_id}}
    end

    assert_redirected_to post_collection_url(PostCollection.last)
  end

  test "should show post_collection" do
    get post_collection_url(@post_collection)
    assert_response :success
  end

  test "should get edit" do
    get edit_post_collection_url(@post_collection)
    assert_response :success
  end

  test "should update post_collection" do
    patch post_collection_url(@post_collection), params: {post_collection: {biz_id: @post_collection.biz_id, description_en: @post_collection.description_en, description_th: @post_collection.description_th, post_collection_parent: @post_collection.post_collection_parent, post_template: @post_collection.post_template, title_en: @post_collection.title_en, title_th: @post_collection.title_th, user_id: @post_collection.user_id}}
    assert_redirected_to post_collection_url(@post_collection)
  end

  test "should destroy post_collection" do
    assert_difference("PostCollection.count", -1) do
      delete post_collection_url(@post_collection)
    end

    assert_redirected_to post_collections_url
  end
end
