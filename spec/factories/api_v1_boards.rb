FactoryGirl.define do
  factory :api_v1_board, class: "Api::V1::Board" do
    title "MyString"
    description "MyText"
    user nil
    biz nil
  end
end
