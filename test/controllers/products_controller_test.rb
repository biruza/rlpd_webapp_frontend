require "test_helper"

class ProductsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @product = products(:one)
  end

  test "should get index" do
    get products_url
    assert_response :success
  end

  test "should get new" do
    get new_product_url
    assert_response :success
  end

  test "should create product" do
    assert_difference("Product.count") do
      post products_url, params: {product: {biz_id: @product.biz_id, cost: @product.cost, description_th: @product.description_th, featured_image_data: @product.featured_image_data, price: @product.price, product_type_id: @product.product_type_id, slug: @product.slug, title_th: @product.title_th}}
    end

    assert_redirected_to product_url(Product.last)
  end

  test "should show product" do
    get product_url(@product)
    assert_response :success
  end

  test "should get edit" do
    get edit_product_url(@product)
    assert_response :success
  end

  test "should update product" do
    patch product_url(@product), params: {product: {biz_id: @product.biz_id, cost: @product.cost, description_th: @product.description_th, featured_image_data: @product.featured_image_data, price: @product.price, product_type_id: @product.product_type_id, slug: @product.slug, title_th: @product.title_th}}
    assert_redirected_to product_url(@product)
  end

  test "should destroy product" do
    assert_difference("Product.count", -1) do
      delete product_url(@product)
    end

    assert_redirected_to products_url
  end
end
