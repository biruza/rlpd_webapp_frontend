class Risk::RiskAnalysisReportsController < ApplicationController
  before_action :set_default, only: %i[index show new edit create update destroy print]
  before_action :set_select, only: %i[new create edit update]

  def index
    url = URI("#{$server[@controller_title]}/api/v1/risk_analysis_reports?order=updated_at&sort=desc&limit=10")
    @risk_analysis_reports = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/risk_analysis_reports/#{params["id"]}")
    @risk_analysis_report = request_api(url, "get", nil)
  end

  def new
    if !params[:plan_report_id].blank?
      url = URI("#{$server[@controller_title]}/api/v1/plan_reports/#{params[:plan_report_id]}")
      @plan_report = request_api(url, "get", nil)

      risk_analysis_report_params = {
        plan_report_id: @plan_report["data"]["id"],
        user_id: @plan_report["data"]["user_id"],
        biz_id: @plan_report["data"]["biz_id"]
      }

      url = URI("#{$server[@controller_title]}/api/v1/risk_analysis_reports")
      @datas = request_api(url, "post", risk_analysis_report_params)
#p"risk_analysis_reports_ID====================#{@datas["data"]["id"]}"
      if @datas["status"] == "Success"
        url = URI("#{$server[@controller_title]}/api/v1/risk_analysis_reports/#{@datas["data"]["id"]}")
        @risk_analysis_report = request_api(url, "get", nil)

        @risk_analysis_report["data"]["plan_report"]["tasks"].each_with_index do |task,idx|
          task['list']['list_risk_factors'].each do |list_risk_factor|
            p"list_risk_factors_id========#{list_risk_factor['id']}"
            risk_analysis_params = {
              task_id: task["id"],
              list_risk_factor_id: list_risk_factor['id'],
              risk_analysis_report_id: @risk_analysis_report["data"]["id"],
              category_id: @risk_analysis_report["data"]["plan_report"]["category"]["id"],
              list_id: list_risk_factor["list_id"],
              order_sort: list_risk_factor["activity_no"]&.to_i,
              #order_sort: @risk_analysis_report["data"]["plan_report"]["tasks"][idx]["list"]["order_sort"]&.to_i,
              user_id: @plan_report["data"]["user_id"],
              biz_id: @plan_report["data"]["biz_id"]
            }
=begin          
          risk_analysis_params = {
            task_id: task["id"],
            risk_analysis_report_id: @risk_analysis_report["data"]["id"],
            category_id: @risk_analysis_report["data"]["plan_report"]["category"]["id"],
            list_id: task["list_id"],
            order_sort: @risk_analysis_report["data"]["plan_report"]["tasks"][idx]["list"]["order_sort"]&.to_i,
            user_id: @plan_report["data"]["user_id"],
            biz_id: @plan_report["data"]["biz_id"]
          }
=end
          url_risk_analysis = URI("#{$server[@controller_title]}/api/v1/risk_analysises")
          url_risk_analysis_new = request_api(url_risk_analysis, "post", risk_analysis_params)
        end
      end

        redirect_to "/#{@controller_main_url}/risk_analysis_reports/#{@datas["data"]["id"]}/edit", notice: @datas["messages"]
      else
        render :new, status: :unprocessable_entity
      end

    else
      redirect_to "/#{@controller_main_url}/master_plans", alert: (begin
        t(:can_not_found)
      rescue
        nil
      end).to_s
    end
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/risk_analysis_reports/#{params["id"]}")
    @risk_analysis_report = request_api(url, "get", nil)
  end

  def update
    params[:risk_analysis].each do |item|
      item_params = {
        risk_analysise: {
          id: params[:risk_analysis][(item[0]).to_s][:id],
          category_id: params[:risk_analysis][(item[0]).to_s][:category_id],
          #list_risk_factor_id: params[:risk_analysis][(item[0]).to_s][:list_risk_factor_id],
          list_id: params[:risk_analysis][(item[0]).to_s][:list_id],
          task_id: params[:risk_analysis][(item[0]).to_s][:task_id],
          risk_analysis_chances_attributes: {
            "0": {
              risk_analysis_id: params[:risk_analysis][(item[0]).to_s][:id],
              title: params[:risk_analysis][(item[0]).to_s][:risk_analysis_chances_attributes]["0"][:title],
              user_id: $user[session.id.to_s][@controller_title]["id"],
              biz_id: $user[session.id.to_s][@controller_title]["biz_id"],
              id: params[:risk_analysis][(item[0]).to_s][:risk_analysis_chances_attributes]["0"][:id]
            }
          },
          risk_analysis_effects_attributes: {
            "0": {
              risk_analysis_id: params[:risk_analysis][(item[0]).to_s][:id],
              title: params[:risk_analysis][(item[0]).to_s][:risk_analysis_effects_attributes]["0"][:title],
              user_id: $user[session.id.to_s][@controller_title]["id"],
              biz_id: $user[session.id.to_s][@controller_title]["biz_id"],
              id: params[:risk_analysis][(item[0]).to_s][:risk_analysis_effects_attributes]["0"][:id]
            }
          }
        }
      }

      url = URI("#{$server[@controller_title]}/api/v1/risk_analysises/#{params[:risk_analysis][(item[0]).to_s][:id]}")
      @datas = request_api(url, "patch", item_params)
    end

    url_risk_analysis_report = URI("#{$server[@controller_title]}/api/v1/risk_analysis_reports/#{params["id"]}")
    @risk_analysis_report = request_api(url_risk_analysis_report, "get", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/risk_analysis_reports/#{@risk_analysis_report["data"]["id"]}/edit", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_main_url}/risk_analysis_reports/#{@risk_analysis_report["data"]["id"]}/edit", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/risk_analysis_reports/#{params["id"]}")
    @risk_analysis_report = request_api(url, "get", nil)

    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/plans/#{@risk_analysis_report["data"]["plan_report"]["category"]["plan_id"]}", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_main_url}/plans/#{@risk_analysis_report["data"]["plan_report"]["category"]["plan_id"]}", alert: @datas["data"]
    end
  end

  def print
    url = URI("#{$server[@controller_title]}/api/v1/risk_analysis_reports/#{params["id"]}")
    @risk_analysis_report = request_api(url, "get", nil)
  end

  private

  def set_default
    # @sub_system = "pmqa_fl"
    # @controller_main_url = "#{@controller_title}/#{@sub_system}"
    @controller_main_url = @controller_title
  end

  def set_user
    @user = User.find(auth_token[:user_id])
  end

  def set_select
    # url = URI("#{$server[@controller_title]}/api/v1/plans")
    # @data_plans = request_api(url,'get',nil)

    # url = URI("#{$server[@controller_title]}/api/v1/categories")
    # @data_categories = request_api(url,'get',nil)

    # url = URI("#{$server[@controller_title]}/api/v1/master_plans")
    # @data_master_plans = request_api(url,'get',nil)
  end

  def risk_analysis_report_params
    params.require(:risk_analysis_report).permit(:title1, :title2, :startdate, :enddate, :slug, :order_sort, :plan_report_id, :master_plan_id, :category_id, :plan_id, :list_id, :user_id, :biz_id,
      risk_analyses_attributes: [:id, :_destroy, :title, :risk_analysis_report_id, :category_id, :task_id, :list_id, :order_sort, :list_risk_factor_id,
        {
          risk_analysis_chances_attributes: [:risk_analysis_id, :id, :_destroy, :title]
        },
        {
          risk_analysis_effects_attributes: [:risk_analysis_id, :id, :_destroy, :title]
        }])
  end
end
