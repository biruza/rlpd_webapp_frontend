require "test_helper"

class RegistrationItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @registration_item = registration_items(:one)
  end

  test "should get index" do
    get registration_items_url
    assert_response :success
  end

  test "should get new" do
    get new_registration_item_url
    assert_response :success
  end

  test "should create registration_item" do
    assert_difference("RegistrationItem.count") do
      post registration_items_url, params: {registration_item: {biz_id: @registration_item.biz_id, member_id: @registration_item.member_id, registration_id: @registration_item.registration_id, status: @registration_item.status, user_id: @registration_item.user_id}}
    end

    assert_redirected_to registration_item_url(RegistrationItem.last)
  end

  test "should show registration_item" do
    get registration_item_url(@registration_item)
    assert_response :success
  end

  test "should get edit" do
    get edit_registration_item_url(@registration_item)
    assert_response :success
  end

  test "should update registration_item" do
    patch registration_item_url(@registration_item), params: {registration_item: {biz_id: @registration_item.biz_id, member_id: @registration_item.member_id, registration_id: @registration_item.registration_id, status: @registration_item.status, user_id: @registration_item.user_id}}
    assert_redirected_to registration_item_url(@registration_item)
  end

  test "should destroy registration_item" do
    assert_difference("RegistrationItem.count", -1) do
      delete registration_item_url(@registration_item)
    end

    assert_redirected_to registration_items_url
  end
end
