require "rails_helper"

RSpec.describe "egp_configs/show", type: :view do
  before(:each) do
    @egp_config = assign(:egp_config, EgpConfig.create!(
      tags: "Tags",
      categories: "Categories",
      biz: nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Tags/)
    expect(rendered).to match(/Categories/)
    expect(rendered).to match(//)
  end
end
