class ApplicationController < ActionController::Base
  add_flash_types :info, :error, :warning

  require "securerandom"
  include Pagy::Backend
  after_action :track_action

  # for lang
  before_action :set_locale
  ####

  layout :layout_by_resource

  before_action :controller_title
  before_action :set_current_user

  helper_method :set_locale, :get_color, :get_result_level_pmqa_fr, :get_existing_manage, :check_role, :get_color_kpi, :get_color_risk, :convert_thai_date, :convert_thai_month, :get_status_action, :get_status_tasks_sign, :get_status_ico_check, :get_status_enable, :get_status_tasks_percent, :get_status_tasks, :get_server_name_by_url, :get_status_flow, :render_btn_action_master_plan, :get_page_title, :request_api, :get_bg, :get_progressbar_master_plans, :cal_date_count, :gen_form, :strip_trailing_zero

  def check_role(user_id, model_name, id)
    url = URI("#{$server[@controller_title]}/api/v1/mains/check_role?model_name=#{model_name}&id=#{id}")
    return request_api(url, "get", nil)['status'] rescue false
  end

  def strip_trailing_zero(n)
    n.to_s.sub(/\.0+$/, '')
   
  end

  def convert_thai_date(datetime, type)
    time = nil
    case type
    when 'default'
      time = "#{l datetime.to_date.in_time_zone, format: "%d %B"} #{datetime.to_date.in_time_zone.strftime("%Y").to_i + 543}" if datetime
    when 'long'
      time = "#{l datetime.to_date.in_time_zone, format: "%d %B"} #{datetime.to_date.in_time_zone.strftime("%Y").to_i + 543} #{datetime.in_time_zone.strftime("%H:%M:%S")}" if datetime
    else
      time = datetime if datetime
    end
    return time
  end

  def convert_thai_month(datetime, type)
    time = nil
    case type
    when 'default'
      time = "#{l datetime.to_date.in_time_zone, format: "%B"} #{datetime.to_date.in_time_zone.strftime("%Y").to_i + 543}" if datetime
    when 'long'
      time = "#{l datetime.to_date.in_time_zone, format: "%B"} #{datetime.to_date.in_time_zone.strftime("%Y").to_i + 543} #{datetime.in_time_zone.strftime("%H:%M:%S")}" if datetime
    else
      time = datetime if datetime
    end
    return time
  end

  def get_result_level_pmqa_fr(score)
    case score
    when 1..299
      title = 'ต่ำกว่าเกณฑ์'
    when 300..399
      title = 'รางวัล PMQA ระดับรายหมวด'
    when 400..649
      title = 'รางวัล PMQA ระดับดีเด่น'
    when 650..1000
      title = 'รางวัล PMQA ระดับยอดเยีนม'
    else
      title = '...'
    end
    return title
  end

  def get_color_kpi(score)
    case score
    when 1..1
      "<span class='badge fs-4' style='background-color: #f20222;color: #ffffff;'>#{score}</span>".html_safe
    when 2..2
      "<span class='badge fs-4' style='background-color: #db25d5;color: #ffffff;'>#{score}</span>".html_safe
    when 3..3
      "<span class='badge fs-4' style='background-color: #f1ee30;color: #333333;'>#{score}</span>".html_safe
    when 4..4
      "<span class='badge fs-4' style='background-color: #2dd87b;color: #ffffff;'>#{score}</span>".html_safe
    when 5..10000
      "<span class='badge fs-4' style='background-color: #1d9151;color: #ffffff;'>#{score}</span>".html_safe
    when nil, 0
      "<span class='badge' style='background-color: #eeeeee;color: #333333;'>#{t('in-process')}</span>".html_safe
    end
  end

  def get_color_risk(score)
    case score
    when 0..3
      "<span class='badge' style='background-color: #1ec842;color: #ffffff;'>#{score}</span>".html_safe
    when 4..8
      "<span class='badge' style='background-color: #ffdc06;color: #333333;'>#{score}</span>".html_safe
    when 9..14
      "<span class='badge' style='background-color: #ffa006;color: #ffffff;'>#{score}</span>".html_safe
    when 15..1000
      "<span class='badge' style='background-color: #ff003e;color: #ffffff;'>#{score}</span>".html_safe
    end
  end

  def get_bg
    if @controller_title || "pages"
      "header-bg.png"
    elsif @controller_title
      "header-bg-#{@controller_title}.png"
    else
      "header-bg.png"
    end
  end

  def get_existing_manage(slug)
    case slug
    when "enough"
      "เพียงพอ".html_safe
    when "fair"
      "พอใช้".html_safe
    when "adjust"
      "ต้องปรับปรุง".html_safe
    end
  end

  def get_status_enable(slug)
    case slug
    when true
      "<span class='badge badge-light-success'>#{t(:enable)}</span>".html_safe
    else
    end
  end

  def get_status_ico_check(slug)
    case slug
    when true
      "✓"
    else
    end
  end


  


  def get_status_action(slug)
    case slug
    when "show"
      "<span class='badge badge-light text-wrap'>#{t(:view)}</span>".html_safe
    when "new"
      "<span class='badge badge-light-primary text-wrap'>#{t(:create)}</span>".html_safe
    when "create"
      "<span class='badge badge-light-primary text-wrap'>#{t(:create)}</span>".html_safe
    when "edit"
      "<span class='badge badge-light text-wrap'>#{t(:edit)}</span>".html_safe
    when "update"
      "<span class='badge badge-light-success text-wrap'>#{t(:edit)}</span>".html_safe
    when "status"
      "<span class='badge badge-light-warning text-wrap'>#{t(:edit_status)}</span>".html_safe
    when "destroy"
      "<span class='badge badge-light-danger text-wrap'>#{t(:delete)}</span>".html_safe
    else
      "<span class='badge badge-light text-wrap'>#{t("#{slug}") rescue nil}</span>".html_safe
    end
  end


  def get_status_tasks(slug)
    case slug
    when "completed-on-schedule"
      "<span class='badge badge-success text-wrap min-w-90px'>ดำเนินการแล้ว เสร็จตามกำหนด</span>".html_safe
    when "completed-later-than-scheduled"
      "<span class='badge badge-light-success text-wrap min-w-90px'>ดำเนินการแล้ว เสร็จล่าช้ากว่ากำหนด</span>".html_safe
    when "not-started"
      "<span class='badge badge-light text-wrap min-w-90px'>ยังไม่ดำเนินการ</span>".html_safe
    when "in-process"
      "<span class='badge badge-warning text-wrap min-w-90px'>อยู่ระหว่างดำเนินการ</span>".html_safe
    end
  end

  def get_status_tasks_sign(slug)
    case slug
    when "completed-on-schedule"
      "<span class='badge badge-success fs-2'>*</span>".html_safe
    when "completed-later-than-scheduled"
      "<span class='badge badge-light-success fs-2'>√</span>".html_safe
    when "not-started"
      "<span class='badge badge-light fs-2'>X</span>".html_safe
    when "in-process"
      "<span class='badge badge-warning fs-2'>Ɵ</span>".html_safe
    end
  end

  def get_status_tasks_percent(slug)
    case slug
    when "completed-on-schedule"
      percent = 100
    when "completed-later-than-scheduled"
      percent = 100
    when "not-started"
      percent = 0
    when "in-process"
      percent = 50
    end
    return percent
  end

  def get_color(b)
    case b
    when 0,6,12
      color = "#05a84e"
    when 1,7,13
      color = "#0563a8"
    when 2,8,14
      color = "#dc9c2a"
    when 3,9,15
      color = "#dc2a80"
    when 4,10,16
      color = "#9c2dc1"
    when 5,11,17
      color = "#2d9dc1"
    else
      color = "#b889de"
    end

      return color
  end

  def render_btn_action_master_plan(model_name, master_plan_id)
    url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{master_plan_id}/current_action")
    @current_action = request_api(url, "get", nil)

    model_name = model_name.camelize

    if $user[session.id.to_s][@controller_title]["superadmin_role"] == true
      "manager"
    elsif !@current_action["data"].blank? && !@current_action["data"]["user_role"].blank?
      @current_action["data"]["user_role"]["slug"]
    else
      "viewer"
    end
  end

  def get_status_flow(status)
    case status
    when "approved"
      "success"
    when "not-approve"
      "danger"
    when "completed"
      "info"
    else
      "default"
    end
  end

  def gen_form
    case action_name
    when "new"
      method_name = "post"
      params_name = ""
    when "create"
      method_name = "post"
      params_name = ""
    when "edit"
      method_name = "patch"
      params_name = "/#{params[:id]}"
    when "update"
      method_name = "patch"
      params_name = "/#{params[:id]}"
    end

    # p "----------- #{@sub_system}"

    url_name = if controller_path.split("/")[1] == "user"
      "#{@controller_title}#{"/#{@sub_system}" if @sub_system}/user"
    else
      "#{@controller_title}#{"/#{@sub_system}" if @sub_system}"
    end

    [url_name, params_name, method_name]
  end

  def cal_date_count(startdate, enddate, type)
    type = "day" if type.blank?
    count = (startdate..enddate).count

    case type
    when "day"
      result = "#{count} วัน"
    end

    result
  end

  def gen_master_plan_users
    # หลังเมื่อ save master_plan เสร็จ
    [["editor", "ผู้ทำ"], ["approver", "หัวหน้า"], ["checker", "กพร."], ["approver", "หัวหน้า กพร."], ["approver", "เลขารองอธิบดี"], ["approver", "รองอธิบดี"], ["approver", "เลขาอธิบดี"], ["approver", "อธิบดี"]].each_with_index do |item, idx|
      master_plan_user_new = MasterPlanUser.new
      master_plan_user_new.order_sort = idx + 1
      master_plan_user_new.master_plan_id = @master_plan.id

      # หา user_role ต้องมี table user_role ก่อน
      user_role = UserRole.find_by(slug: item[0], title: item[1])
      master_plan_user_new.user_role_id = user_role.id

      master_plan_user_new.master_plan_id = @master_plan.id

      master_plan_user_new.save
    end
  end

  def get_progressbar_master_plans
    master_plan = MasterPlan.find_by(id: params[:id])
    status_current_size = master_plan.master_plan_users.not.where(status: nil).size
    status_all_size = master_plan.master_plan_users.size
    status_current_size / status_all_size * 100
  end

  def set_locale
    I18n.locale = params[:locale] || session[:locale] || I18n.default_locale
    session[:locale] = I18n.locale
  end

  def get_page_title(b)
    if request.host == 'asd.rlpd.go.th'
    if b.present?
      "RLPD APPLICATION : #{b}"
    else
      "RLPD APPLICATION"
    end
  else
    if b.present?
      "PMQA APPLICATION : #{b}"
    else
      "PMQA APPLICATION"
    end
  end

  end

  def get_server_name_by_url(url)
    url = URI(url)
    $server.each_with_index do |server, idx|
      server[1] = URI(server[1])
      full_url = "#{url.host}:#{url.port}"
      full_server = "#{server[1].host}:#{server[1].port}"

      if full_url == full_server
        @server_name = server[0]
      end
    end
    @server_name
  end

  def request_api(url, method_name, data)
    url = URI(url)
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = Rails.env.production? || Rails.env.staging?
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    token = $auth_token[session.id.to_s][get_server_name_by_url(url)]

    case method_name.to_s
    when "get", nil
      request = Net::HTTP::Get.new(url)

    when "post"
      request = Net::HTTP::Post.new(url)

    when "patch"
      request = Net::HTTP::Patch.new(url)

    when "delete"
      request = Net::HTTP::Delete.new(url)
    end

    request["authorization"] = "Bearer #{token}"
    request["content-type"] = "application/json"
    request.body = data.to_json if data

    response = http.request(request)

    # puts "response : #{response.body}"

    # if response && response.body
    #	data = JSON.parse(response.body)
    # else
    #	data = response
    # end

    if response.body.blank?
      response.body
    else
      JSON.parse(response.read_body.force_encoding("UTF-8"))
      # data = response.read_body.force_encoding("UTF-8")
      # data = response.read_body.to_json
    end

    # puts "-------------------------------------------------response : #{response.body}"

    # end
  end

  protected

  def controller_title
    @controller_title = controller_path.split("/").first
    if @controller_title.blank?
      @controller_title = controller_name
    end
  end

  def set_current_user
    #if $auth_token.blank? || $auth_token[session.id.to_s][@controller_title].blank?
    if $auth_token.blank? || $auth_token[session.id.to_s].blank? || ($auth_token[session.id.to_s] && $auth_token[session.id.to_s][@controller_title].blank?)
    #if $auth_token.blank? 
      # p "$auth_token[session.id.to_s][@controller_title] = #{$auth_token[session.id.to_s][@controller_title]}"
      if @controller_title != "accounts" && @controller_title != "mains" && @controller_title != "master_plan_users" && @controller_title != "plan_users" && @controller_title != "pages"
        redirect_to "/accounts/sign_in", alert: response["errors"]
      end
    elsif @set_current_user.blank?
      @set_current_user = $user[session.id.to_s][@controller_title]

      if $user && $user[session.id.to_s] && $user[session.id.to_s][@controller_title]['superadmin_role'] == true
         @role_title = 'superadmin_role'
      elsif $user && $user[session.id.to_s] && $user[session.id.to_s][@controller_title]['administrator_role'] == true
         @role_title = 'administrator_role'
      elsif $user && $user[session.id.to_s] && $user[session.id.to_s][@controller_title]['user_role'] == true
         @role_title = 'user_role'
      end

      #@set_current_user = $user[@controller_title]
      @set_current_user
    end
  end

  def track_action
    #if user_signed_in?

    #end
    ahoy.track("Requested #{request.path}", {
      system_name: "#{params[:controller]}",
      action: "#{params[:action]}",
      path: request.path,
      path_parameters: request.path_parameters,
      query_string: Rack::Utils.parse_nested_query(request.query_string),
      user_id: ($user[session.id.to_s][@controller_title]['id'] rescue nil),
      user_name: "#{$user[session.id.to_s][@controller_title]['firstname'] rescue nil} #{$user[session.id.to_s][@controller_title]['lastname'] rescue nil}",
    })
  end

  private

  def layout_by_resource
    if devise_controller?
      "blank"
    else
      "application"
    end
  end
end
