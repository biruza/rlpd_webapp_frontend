class Risk::Control::FollowUpControlHistoriesController < ApplicationController
  before_action :set_default, only: %i[index show new edit create update destroy print]
  before_action :set_select, only: %i[new create edit update]

  def index
    url = URI("#{$server[@controller_title]}/api/v1/follow_up_control_histories")
    @task_plan_reports = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/follow_up_control_histories/#{params["id"]}")
    @follow_up_control_history = request_api(url, "get", nil)
  end

  def new
  end

  def create
  end


 def edit
  url = URI("#{$server[@controller_title]}/api/v1/follow_up_control_histories/#{params['id']}")
  @follow_up_control_history = request_api(url,'get',nil)

  url = URI("#{$server[@controller_title]}/api/v1/follow_up_summaries/#{params['id']}")
  @follow_up_summary = request_api(url,'get',nil)
 end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/follow_up_control_histories/#{params['id']}")
    @follow_up_control_history = request_api(url,'get',nil)
   # url_category = URI("#{$server[@controller_title]}/api/v1/categories/#{@follow_up_control_history['data']['category_id']}")
   # @category = request_api(url_category,'get',nil)

    @datas = request_api(url,'patch',follow_up_control_history_params)

  if  @datas && @datas['status'] == 'Success'
    redirect_to "/#{@controller_main_url}/follow_up_control_reports/#{@follow_up_control_history['data']['follow_up_control_report_id']}/edit", notice: @datas['messages']

  else
    redirect_to "/#{@controller_main_url}/follow_up_control_reports/#{@follow_up_control_history['data']['follow_up_control_report_id']}/edit", alert: @datas['data']
  end

  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/follow_up_control_histories/#{params["id"]}")
    @datas_ori = request_api(url, "get", nil)
    @datas = request_api(url, "delete", nil)

    url = URI("#{$server[@controller_title]}/api/v1/categories/#{@datas_ori["data"]["category_id"]}")
    @category = request_api(url, "get", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/plans/#{@category["data"]["plan_id"]}", notice: @datas["message"]
    else
      redirect_to "/#{@controller_main_url}/plans/#{@category["data"]["plan_id"]}", alert: @datas["data"]
    end
  end

  private

  def set_select

    url = URI("#{$server[@controller_title]}/api/v1/operation_statuses")
    @operation_statuses = request_api(url,'get',nil)

  end

  def set_default
    @sub_system = "control"
    @controller_main_url = "#{@controller_title}/#{@sub_system}"
    # @controller_main_url = @controller_title
  end

  def follow_up_control_history_params
    # params.require(:follow_up_control_history).permit(:title, :period, :description, :slug, :user, :biz)

    params.require(:follow_up_control_history).permit(:title, :follow_up_control_report_id, :task_list_id, :operation_status_id, :status, :plan_report_id, :category_id, :evaluate_control_id, :user_id, :biz_id,
      follow_up_adjusts_attributes: [:id, :_destroy, :title, :slug, :follow_up_control_history_id, :user_id, :biz_id],
      follow_up_summaries_attributes: [:id, :_destroy, :title, :slug, :follow_up_control_history_id, :user_id, :biz_id],
      follow_up_control_attachments_attributes: [:id, :_destroy, :file, :file_data_uri, :description, :slug, :follow_up_control_history_id, :user_id, :biz_id])
  end
end
