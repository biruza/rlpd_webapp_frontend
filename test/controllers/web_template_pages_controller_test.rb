require "test_helper"

class WebTemplatePagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @web_template_page = web_template_pages(:one)
  end

  test "should get index" do
    get web_template_pages_url
    assert_response :success
  end

  test "should get new" do
    get new_web_template_page_url
    assert_response :success
  end

  test "should create web_template_page" do
    assert_difference("WebTemplatePage.count") do
      post web_template_pages_url, params: {web_template_page: {biz_id: @web_template_page.biz_id, description_en: @web_template_page.description_en, description_th: @web_template_page.description_th, image_data: @web_template_page.image_data, order_number: @web_template_page.order_number, slug: @web_template_page.slug, title_en: @web_template_page.title_en, title_th: @web_template_page.title_th, url: @web_template_page.url, web_template_id: @web_template_page.web_template_id}}
    end

    assert_redirected_to web_template_page_url(WebTemplatePage.last)
  end

  test "should show web_template_page" do
    get web_template_page_url(@web_template_page)
    assert_response :success
  end

  test "should get edit" do
    get edit_web_template_page_url(@web_template_page)
    assert_response :success
  end

  test "should update web_template_page" do
    patch web_template_page_url(@web_template_page), params: {web_template_page: {biz_id: @web_template_page.biz_id, description_en: @web_template_page.description_en, description_th: @web_template_page.description_th, image_data: @web_template_page.image_data, order_number: @web_template_page.order_number, slug: @web_template_page.slug, title_en: @web_template_page.title_en, title_th: @web_template_page.title_th, url: @web_template_page.url, web_template_id: @web_template_page.web_template_id}}
    assert_redirected_to web_template_page_url(@web_template_page)
  end

  test "should destroy web_template_page" do
    assert_difference("WebTemplatePage.count", -1) do
      delete web_template_page_url(@web_template_page)
    end

    assert_redirected_to web_template_pages_url
  end
end
