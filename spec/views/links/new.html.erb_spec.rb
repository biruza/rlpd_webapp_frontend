require "rails_helper"

RSpec.describe "links/new", type: :view do
  before(:each) do
    assign(:link, Link.new(
      source: nil,
      target: nil,
      link_type: "MyString",
      user: nil,
      biz: nil
    ))
  end

  it "renders new link form" do
    render

    assert_select "form[action=?][method=?]", links_path, "post" do
      assert_select "input[name=?]", "link[source_id]"

      assert_select "input[name=?]", "link[target_id]"

      assert_select "input[name=?]", "link[link_type]"

      assert_select "input[name=?]", "link[user_id]"

      assert_select "input[name=?]", "link[biz_id]"
    end
  end
end
