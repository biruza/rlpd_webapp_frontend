class Risk::TaskListsController < ApplicationController
  before_action :set_select, only: %i[new create edit update]
  before_action :set_default, only: %i[index show new edit create update destroy print]

  def index
    url = URI("#{$server[@controller_title]}/api/v1/categories")
    @catagory_datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/categories/#{params["id"]}")
    @datas = request_api(url, "get", nil)
  end

  def new
  end

  def create
    url = URI("#{$server[@controller_title]}/api/v1/task_lists")
    @datas = request_api(url, "post", task_list_params)

    if @datas["status"] == "Success"
      url = URI("#{$server[@controller_title]}/api/v1/categories/#{params[:list][:category_id]}")
      @category = request_api(url, "get", nil)

      redirect_to "/#{@controller_main_url}/task_lists/#{@datas["data"]["id"]}/edit?original_url=/#{@controller_title}/plans/#{@category["data"]["plan_id"]}", notice: @datas["messages"]
      # redirect_to "/#{@controller_main_url}/plans/#{@category['data']['plan_id']}", notice: @datas['messages']
    else
      redirect_to params[:original_url].to_s, alert: @datas["data"]
    end
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/task_lists/#{params["id"]}")
    @datas = request_api(url, "get", nil)
    url = URI("#{$server[@controller_title]}/api/v1/categories/#{params["id"]}")
    @catagory_datas = request_api(url, "get", nil)
  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/task_lists/#{params["id"]}")
    @task_list = request_api(url, "get", nil)
    url_category = URI("#{$server[@controller_title]}/api/v1/categories/#{@list["data"]["category_id"]}")
    @category = request_api(url_category, "get", nil)

    @datas = request_api(url, "patch", task_list_params)

    if @datas && @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/plans/#{@category["data"]["plan_id"]}", notice: @datas["messages"]

    else
      redirect_to "/#{@controller_main_url}/lists/#{params[:id]}/edit", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/task_lists/#{params["id"]}")
    @datas_ori = request_api(url, "get", nil)
    @datas = request_api(url, "delete", nil)
    # @datas = request_api(url,'get',nil)
    url = URI("#{$server[@controller_title]}/api/v1/categories/#{@datas_ori["data"]["category_id"]}")
    @category = request_api(url, "get", nil)

    # @category['data'].each do |item|
    p @datas_ori["data"]
    # end
    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/plans/#{@category["data"]["plan_id"]}", notice: @datas["message"]
    else
      redirect_to "/#{@controller_main_url}/plans/#{@category["data"]["plan_id"]}", alert: @datas["data"]
    end
  end

  private

  def set_select
    # url = URI("#{$server[@controller_title]}/api/v1/activities")
    # @data_activities = request_api(url,'get',nil)
  end

  def set_default
    # @sub_system = "control"
    # @controller_main_url = "#{@controller_title}/#{@sub_system}"
    @controller_main_url = @controller_title
  end

  def task_list_params
    # params.require(:task_list).permit(:title, :period, :description, :slug, :user, :biz)
    params.require(:task_list).permit(:existing_manage, :title, :risk_accept, :risk_avert, :risk_transfer, :risk_control, :risk_enough, :risk_fair, :risk_adjust, :order_sort, :task_plan_report_id, :plan_report_id, :category_id, :list_id, :user_id, :biz_id, :risk_analyses_id,
      task_list_controls_attributes: [:id, :_destroy, :title, :slug, :task_list_id, :user_id, :biz_id])
  end
end
