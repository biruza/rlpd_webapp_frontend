require "test_helper"

class ImprovementHistoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @improvement_history = improvement_histories(:one)
  end

  test "should get index" do
    get improvement_histories_url
    assert_response :success
  end

  test "should get new" do
    get new_improvement_history_url
    assert_response :success
  end

  test "should create improvement_history" do
    assert_difference("ImprovementHistory.count") do
      post improvement_histories_url, params: {improvement_history: {action: @improvement_history.action, qty: @improvement_history.qty, spare_part_id: @improvement_history.spare_part_id}}
    end

    assert_redirected_to improvement_history_url(ImprovementHistory.last)
  end

  test "should show improvement_history" do
    get improvement_history_url(@improvement_history)
    assert_response :success
  end

  test "should get edit" do
    get edit_improvement_history_url(@improvement_history)
    assert_response :success
  end

  test "should update improvement_history" do
    patch improvement_history_url(@improvement_history), params: {improvement_history: {action: @improvement_history.action, qty: @improvement_history.qty, spare_part_id: @improvement_history.spare_part_id}}
    assert_redirected_to improvement_history_url(@improvement_history)
  end

  test "should destroy improvement_history" do
    assert_difference("ImprovementHistory.count", -1) do
      delete improvement_history_url(@improvement_history)
    end

    assert_redirected_to improvement_histories_url
  end
end
