require "test_helper"

class QuotationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @quotation = quotations(:one)
  end

  test "should get index" do
    get quotations_url
    assert_response :success
  end

  test "should get new" do
    get new_quotation_url
    assert_response :success
  end

  test "should create quotation" do
    assert_difference("Quotation.count") do
      post quotations_url, params: {quotation: {amount: @quotation.amount, biz_id: @quotation.biz_id, company_id: @quotation.company_id, description: @quotation.description, discount_amount: @quotation.discount_amount, doc_number: @quotation.doc_number, doc_ref: @quotation.doc_ref, doc_ref_date: @quotation.doc_ref_date, doc_sign: @quotation.doc_sign, image_data: @quotation.image_data, payee_type: @quotation.payee_type, payment_bank: @quotation.payment_bank, payment_number: @quotation.payment_number, payment_type: @quotation.payment_type, project_id: @quotation.project_id, revenue_type_id: @quotation.revenue_type_id, status: @quotation.status, vat: @quotation.vat, vat_inout: @quotation.vat_inout, vat_status: @quotation.vat_status, withholding_tax_amount: @quotation.withholding_tax_amount, withholding_tax_status: @quotation.withholding_tax_status}}
    end

    assert_redirected_to quotation_url(Quotation.last)
  end

  test "should show quotation" do
    get quotation_url(@quotation)
    assert_response :success
  end

  test "should get edit" do
    get edit_quotation_url(@quotation)
    assert_response :success
  end

  test "should update quotation" do
    patch quotation_url(@quotation), params: {quotation: {amount: @quotation.amount, biz_id: @quotation.biz_id, company_id: @quotation.company_id, description: @quotation.description, discount_amount: @quotation.discount_amount, doc_number: @quotation.doc_number, doc_ref: @quotation.doc_ref, doc_ref_date: @quotation.doc_ref_date, doc_sign: @quotation.doc_sign, image_data: @quotation.image_data, payee_type: @quotation.payee_type, payment_bank: @quotation.payment_bank, payment_number: @quotation.payment_number, payment_type: @quotation.payment_type, project_id: @quotation.project_id, revenue_type_id: @quotation.revenue_type_id, status: @quotation.status, vat: @quotation.vat, vat_inout: @quotation.vat_inout, vat_status: @quotation.vat_status, withholding_tax_amount: @quotation.withholding_tax_amount, withholding_tax_status: @quotation.withholding_tax_status}}
    assert_redirected_to quotation_url(@quotation)
  end

  test "should destroy quotation" do
    assert_difference("Quotation.count", -1) do
      delete quotation_url(@quotation)
    end

    assert_redirected_to quotations_url
  end
end
