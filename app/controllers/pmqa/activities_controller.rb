class Pmqa::ActivitiesController < ApplicationController
  def index
    url = URI("#{$server[@controller_title]}/api/v1/activities")

    @datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/activities/#{params["id"]}")
    @datas = request_api(url, "get", nil)
  end

  def new
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/activities/#{params["id"]}")

    @datas = request_api(url, "get", nil)

    # puts "plan edit = #{@plan['title']}"
  end

  def create
    params[:activity][:user_id] = $user_id
    params[:activity][:biz_id] = $biz_id

    url = URI("#{$server[@controller_title]}/api/v1/activities")
    @datas = request_api(url, "post", activity_params)

    if @datas["status"] == "Success"
      redirect_to "/km/activities", notice: @datas["messages"]
    else
      redirect_to "/km/activities/new", alert: @datas["data"]
    end
  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/activities/#{params["id"]}")
    @datas = request_api(url, "patch", activity_params)

    # puts "update status : #{res}"

    if @datas["status"] == "Success"
      redirect_to "/km/activities", notice: @datas["message"]
    else
      redirect_to "/km/activities", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/activities/#{params["id"]}")
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
      redirect_to "/km/activities", notice: @datas["message"]
    else
      redirect_to "/km/activities", alert: @datas["data"]
    end
  end

  private

  def activity_params
    params.require(:activity).permit(:title, :user_id, :biz_id)
  end

  def authenticate_user!
    if user_signed_in?
      super
    else
      redirect_to "accounts/sign_in", notice: "Please Login to view that page!"
    end
  end
end
