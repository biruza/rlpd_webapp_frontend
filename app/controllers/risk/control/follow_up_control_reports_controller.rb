class Risk::Control::FollowUpControlReportsController < ApplicationController
  before_action :set_default, only: %i[index show new edit create update destroy print]
  before_action :set_select, only: %i[new create edit update]

  def index
    url = URI("#{$server[@controller_title]}/api/v1/follow_up_control_reports?order=updated_at&sort=desc&limit=10")
    @follow_up_control_reports = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/follow_up_control_reports/#{params["id"]}")
    @follow_up_control_report = request_api(url, "get", nil)
  end

  def new
    if !params[:evaluate_control_report_id].blank?
      url = URI("#{$server[@controller_title]}/api/v1/evaluate_control_reports/#{params[:evaluate_control_report_id]}")
      @evaluate_control_report = request_api(url, "get", nil)

    else
      redirect_to "/#{@controller_main_url}/master_plans", alert: (begin
        t(:can_not_found)
      rescue
        nil
      end).to_s
    end
  end

  def create
    url = URI("#{$server[@controller_title]}/api/v1/evaluate_control_reports/#{params[:follow_up_control_report][:evaluate_control_report_id]}")
    @evaluate_control_report = request_api(url, "get", nil)

    follow_up_control_report_params = {
      evaluate_control_report_id: params[:follow_up_control_report][:evaluate_control_report_id],
      user_id: @evaluate_control_report["data"]["user_id"],
      biz_id: @evaluate_control_report["data"]["biz_id"],
      period_report_id: params[:follow_up_control_report][:period_report_id],
      startdate: params[:follow_up_control_report][:startdate],
      enddate: params[:follow_up_control_report][:enddate]
    }

    url = URI("#{$server[@controller_title]}/api/v1/follow_up_control_reports")
    @datas = request_api(url, "post", follow_up_control_report_params)

    if @datas["status"] == "Success"

      url = URI("#{$server[@controller_title]}/api/v1/follow_up_control_reports/#{@datas["data"]["id"]}")
      @follow_up_control_report = request_api(url, "get", nil)
      @follow_up_control_report["data"]["evaluate_control_report"]["evaluate_controls"].each do |evaluate_control|
        follow_up_control_histories_params = {
          evaluate_control_id: evaluate_control["id"],
          follow_up_control_report_id: @follow_up_control_report["data"]["id"],
          task_list_id: evaluate_control["task_list_id"],
          order_sort: evaluate_control["order_sort"]&.to_i,
          list_id: evaluate_control["list_id"],
          plan_report_id: evaluate_control["plan_report_id"],
          category_id: evaluate_control["category_id"],
          operation_status_id: @data_operation_statuses["data"][0]["id"],
          user_id: evaluate_control["user_id"],
          biz_id: evaluate_control["biz_id"]
        }
        url_follow_up_control_histories = URI("#{$server[@controller_title]}/api/v1/follow_up_control_histories")
        url_follow_up_control_histories_new = request_api(url_follow_up_control_histories, "post", follow_up_control_histories_params)
      end

      redirect_to "/#{@controller_main_url}/follow_up_control_reports/#{@datas["data"]["id"]}/edit", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_main_url}/follow_up_control_reports/new?evaluate_control_report_id=#{params[:follow_up_control_report][:evaluate_control_report_id]}", alert: @datas["data"]
    end
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/follow_up_control_reports/#{params["id"]}")
    @follow_up_control_report = request_api(url, "get", nil)
  end

  def update
    params[:task_list].each do |item|
      params[:task_list][(item[0]).to_s][:risk_accept] = true if params[:task_list][(item[0]).to_s][:risk_accept] == "true"

      item_params = {
        task_list: {
          risk_accept: params[:task_list][(item[0]).to_s][:risk_accept],
          risk_avert: params[:task_list][(item[0]).to_s][:risk_avert],
          risk_transfer: params[:task_list][(item[0]).to_s][:risk_transfer],
          risk_control: params[:task_list][(item[0]).to_s][:risk_control],
          existing_manage: params[:task_list][(item[0]).to_s][:existing_manage],

          task_list_controls_attributes: {
            "0": {
              task_list_id: params[:task_list][(item[0]).to_s][:id],
              title: params[:task_list][(item[0]).to_s][:task_list_controls_attributes]["0"][:title],
              user_id: $user[session.id.to_s][@controller_title]["id"],
              biz_id: $user[session.id.to_s][@controller_title]["biz_id"],
              id: params[:task_list][(item[0]).to_s][:task_list_controls_attributes]["0"][:id]
            }
          }
        }
      }

      url = URI("#{$server[@controller_title]}/api/v1/task_lists/#{params[:task_list][(item[0]).to_s][:id]}")
      @datas = request_api(url, "patch", item_params)
    end

    url_task_plan_report = URI("#{$server[@controller_title]}/api/v1/task_plan_reports/#{params["id"]}")
    @url_task_plan_report = request_api(url_task_plan_report, "get", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/task_plan_reports/#{@url_task_plan_report["data"]["id"]}/edit", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_main_url}/task_plan_reports/#{@url_task_plan_report["data"]["id"]}/edit", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/follow_up_control_reports/#{params["id"]}")
    @follow_up_control_report_ori = request_api(url, "get", nil)

    @follow_up_control_report = request_api(url, "delete", nil)

    if @follow_up_control_report["status"] == "Success"
      redirect_to "/#{@controller_main_url}/plans/#{@follow_up_control_report_ori["data"]["evaluate_control_report"]["plan_report"]["plan_id"]}", notice: @follow_up_control_report["messages"]
    else
      redirect_to "/#{@controller_main_url}/plans/#{@follow_up_control_report_ori["data"]["evaluate_control_report"]["plan_report"]["plan_id"]}", alert: @follow_up_control_report["data"]
    end
  end

  def print
    url = URI("#{$server[@controller_title]}/api/v1/follow_up_control_reports/#{params["id"]}")
    @follow_up_control_report = request_api(url, "get", nil)
  end

  private

  def set_default
    @sub_system = "control"
    @controller_main_url = "#{@controller_title}/#{@sub_system}"
    # @controller_main_url = @controller_title
  end

  def set_user
    @user = User.find(auth_token[:user_id])
  end

  def set_select
    url = URI("#{$server[@controller_title]}/api/v1/operation_statuses")
    @data_operation_statuses = request_api(url, "get", nil)

    # url = URI("#{$server[@controller_title]}/api/v1/categories")
    # @data_categories = request_api(url,'get',nil)

    # url = URI("#{$server[@controller_title]}/api/v1/master_plans")
    # @data_master_plans = request_api(url,'get',nil)
  end

  def follow_up_control_report_params
    # params.require(:follow_up_control_report).permit(:title1, :title2, :startdate, :enddate, :slug, :order_sort, :master_follow_up_control_report, :category, :user, :biz)
    params.require(:follow_up_control_report).permit(:title1, :title2, :startdate, :enddate, :slug, :order_sort, :list_id, :user_id, :biz_id, :evaluate_control_report_id, :period_report_id,
      follow_up_control_histories_attributes: [:id, :_destroy, :title, :follow_up_control_report_id, :task_list_id, :operation_status_id, :status, :follow_up_control_report_id, :category_id, :evaluate_control_id, :user_id, :biz_id,
        {
          follow_up_adjusts_attributes: [:id, :_destroy, :title, :slug, :follow_up_control_history_id, :user_id, :biz_id],
          follow_up_summaries_attributes: [:id, :_destroy, :title, :slug, :follow_up_control_history_id, :user_id, :biz_id],
          follow_up_control_attachments_attributes: [:id, :_destroy, :file, :file_data_uri, :description, :slug, :follow_up_control_history_id, :user_id, :biz_id]
        }])
  end
end
