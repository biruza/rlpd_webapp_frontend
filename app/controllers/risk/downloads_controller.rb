class Risk::DownloadsController < ApplicationController
  def index
    url = URI("#{$server[@controller_title]}/api/v1/downloads")

    @datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/downloads/#{params["id"]}")

    @datas = request_api(url, "get", nil)
  end

  def new
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/downloads/#{params["id"]}")

    @datas = request_api(url, "get", nil)

    # puts "plan edit = #{@plan['title']}"
  end

  def create
    # params[:download][:user_id] = $user_id
    # params[:download][:biz_id] = $biz_id

    url = URI("#{$server[@controller_title]}/api/v1/downloads")
    @datas = request_api(url, "post", download_params)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/downloads", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_title}/downloads/new", alert: @datas["data"]
    end
  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/downloads/#{params["id"]}")
    @datas = request_api(url, "patch", download_params)

    # puts "update status : #{res}"

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/downloads", notice: @datas["message"]
    else
      redirect_to "/#{@controller_title}/downloads", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/downloads/#{params["id"]}")
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/downloads", notice: @datas["message"]
    else
      redirect_to "/#{@controller_title}/downloads", alert: @datas["data"]
    end
  end

  private

  def download_params
    params.require(:download).permit( :order_sort, :title, :file_data, :file, :file_data_uri, :view_count, :user_id, :biz_id)
  end
end
