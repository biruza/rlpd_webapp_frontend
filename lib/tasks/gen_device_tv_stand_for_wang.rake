require "csv"

desc "gen_device_tv_stand_for_wang"
task gen_device_tv_stand_for_wang: [:environment] do
  count_location = 0
  count_device_category = 0
  count_manufacturer = 0
  count_device = 0

  biz = Biz.find_by(title_th: "Biza Enterprise Co.,ltd.")

  puts biz.title_th

  location_new = Location.find_by(title: "wang")

  device_category_new = DeviceCategory.new
  device_category_new.title = "tv-stand"
  device_category_new.slug = device_category_new.title
  device_category_new.biz_id = biz.id
  if device_category_new.save
    count_device_category += 1
  end

  manufacturer_new = Manufacturer.new
  manufacturer_new.name = "tv-stand"
  manufacturer_new.slug = manufacturer_new.name
  manufacturer_new.biz_id = biz.id
  if manufacturer_new.save
    count_manufacturer += 1
  end

  (1..48).each do |device_name|
    device_new = Device.new
    device_new.location_id = location_new.id
    device_new.manufacturer_id = manufacturer_new.id
    device_new.device_category_id = device_category_new.id
    device_new.title = "device-#{manufacturer_new.slug}-#{device_name}"
    device_new.biz_id = biz.id
    if device_new.save
      count_device += 1
    end
  end

  puts "count_location = #{count_location}, count_device_category = #{count_device_category}, count_manufacturer = #{count_manufacturer}, count_device = #{count_device}"
end
