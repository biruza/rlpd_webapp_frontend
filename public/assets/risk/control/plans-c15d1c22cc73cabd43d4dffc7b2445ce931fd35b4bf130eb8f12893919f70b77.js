$('.selectize-master_plan').selectize({
	sortField: 'text',
	create: function(input, callback) {
		$.post('/master_plans.json', { plan: { title: input } })
		.done(function(response){
			console.log(response)
			callback({value: response.id, text: response.title });
		})
	}
}); 
