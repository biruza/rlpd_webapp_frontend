class Kpi::MasterPlansController < ApplicationController
  def last_action
    @master_plan = MasterPlan.find_by(id: params[:id])

    @master_plan_user = @master_plan.master_plan_users.where.not(status: nil).order(updated_at: :desc)

    render json: {status: "Success", message: "Loaded Last Action Master Plan Users", data: @master_plan_user.as_json({only: [:id, :status, :order_sort, :master_plan_id, :user_id, :user_role_id, :updated_at, :created_at, :user_id, :biz_id],
                      include: {
                        user: {only: [:id, :firstname, :lastname]},
                        user_role: {only: [:id, :title, :slug, :user, :biz]}

                      }})}, status: :ok
  end

  def current_action
    @master_plan = MasterPlan.find_by(id: params[:id])
    @master_plan_user_last_action = @master_plan.master_plan_users.where.not(status: nil).order(updated_at: :desc).first
    @master_plan_user_current_action = @master_plan.master_plan_users.where("order_sort > ?", @master_plan_user_last_action.order_sort).order(order_sort: :desc).first

    render json: {status: "Success", message: "Loaded Current Action Master Plan Users", data: @master_plan_user_current_action.as_json({only: [:id, :status, :order_sort, :master_plan_id, :user_id, :user_role_id, :updated_at, :created_at, :user_id, :biz_id],
                      include: {
                        user: {only: [:id, :firstname, :lastname]},
                        user_role: {only: [:id, :title, :slug, :user, :biz]}

                      }})}, status: :ok
  end

  def index
    url = URI("#{$server[@controller_title]}/api/v1/master_plans?order=period_year&sort=desc")
    @datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params["id"]}")
    @datas = request_api(url, "get", nil)

    url_master_plan_user = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params["id"]}/master_plan_users?order=order_sort&sort=asc")
    @master_plan_users = request_api(url_master_plan_user, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params["id"]}/master_plan_user_comments")
    @user_commments = request_api(url, "get", nil)

    p "---------------------- #{@datas}"

    # if @datas['status'] != 'Success'
    # redirect_to "/#{@controller_title}/master_plans", alert: 'Alert'
    # end
  end

  def new
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params["id"]}")

    @datas = request_api(url, "get", nil)

    # puts "plan edit = #{@plan['title']}"
  end

  def create
    # params[:master_plan][:user_id] = $user[session.id.to_s][@controller_title]['id']
    # params[:master_plan][:biz_id] = $user[session.id.to_s][@controller_title]['biz_id']
    master_plan_params = {
    master_plan: {
      period_year: Date.new(params[:master_plan][:period_year].to_i - 543, 1, 1),
      startdate: Date.new(params[:master_plan][:period_year].to_i - 543 - 1, 10, 1),
      enddate: Date.new(params[:master_plan][:period_year].to_i - 543, 9, 30)
      }
    }

    url = URI("#{$server[@controller_title]}/api/v1/master_plans")
    @datas = request_api(url, "post", master_plan_params)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/master_plans", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_title}/master_plans/new", alert: @datas["data"]
    end
  end

  def update
    master_plan_params = {
    master_plan: {
      period_year: Date.new(params[:master_plan][:period_year].to_i - 543, 1, 1),
      startdate: Date.new(params[:master_plan][:period_year].to_i - 543 - 1, 10, 1),
      enddate: Date.new(params[:master_plan][:period_year].to_i - 543, 9, 30)
      }
    }
    
    url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params["id"]}")
    @datas = request_api(url, "patch", master_plan_params)

    # puts "update status : #{res}"

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/master_plans/#{params[:id]}", notice: @datas["message"]
    else
      redirect_to "/#{@controller_title}/master_plans/#{params[:id]}/edit", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params["id"]}")
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/master_plans", notice: @datas["message"]
    else
      redirect_to "/#{@controller_title}/master_plans", alert: t(:can_not_delete)
    end
  end

  def print
    url = URI("#{$server[@controller_title]}/api/v1/master_plans/#{params["id"]}")
    @datas = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/devision_directors")
    @devision_directors = request_api(url, "get", nil)
  end

  private

  def master_plan_params
    params.require(:master_plan).permit(:title, :description, :user_id, :biz_id, :type_name, :startdate, :enddate, :period_year)
  end
end
