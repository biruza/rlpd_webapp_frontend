require "rails_helper"

RSpec.describe "tpl_task_checklists/index", type: :view do
  before(:each) do
    assign(:tpl_task_checklists, [
      TplTaskChecklist.create!(
        tpl_task: nil,
        order_sort: 2,
        title: "Title",
        checklist_type: "MyText",
        status: "Status",
        score: "9.99",
        custom: "",
        description: "MyText",
        weight: "9.99",
        user: nil,
        biz: nil
      ),
      TplTaskChecklist.create!(
        tpl_task: nil,
        order_sort: 2,
        title: "Title",
        checklist_type: "MyText",
        status: "Status",
        score: "9.99",
        custom: "",
        description: "MyText",
        weight: "9.99",
        user: nil,
        biz: nil
      )
    ])
  end

  it "renders a list of tpl_task_checklists" do
    render
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: 2.to_s, count: 2
    assert_select "tr>td", text: "Title".to_s, count: 2
    assert_select "tr>td", text: "MyText".to_s, count: 2
    assert_select "tr>td", text: "Status".to_s, count: 2
    assert_select "tr>td", text: "9.99".to_s, count: 2
    assert_select "tr>td", text: "".to_s, count: 2
    assert_select "tr>td", text: "MyText".to_s, count: 2
    assert_select "tr>td", text: "9.99".to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
  end
end
