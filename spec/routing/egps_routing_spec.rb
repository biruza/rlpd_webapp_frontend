require "rails_helper"

RSpec.describe EgpsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/egps").to route_to("egps#index")
    end

    it "routes to #new" do
      expect(get: "/egps/new").to route_to("egps#new")
    end

    it "routes to #show" do
      expect(get: "/egps/1").to route_to("egps#show", id: "1")
    end

    it "routes to #edit" do
      expect(get: "/egps/1/edit").to route_to("egps#edit", id: "1")
    end

    it "routes to #create" do
      expect(post: "/egps").to route_to("egps#create")
    end

    it "routes to #update via PUT" do
      expect(put: "/egps/1").to route_to("egps#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/egps/1").to route_to("egps#update", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/egps/1").to route_to("egps#destroy", id: "1")
    end
  end
end
