require "rails_helper"

RSpec.describe "task_users/edit", type: :view do
  before(:each) do
    @task_user = assign(:task_user, TaskUser.create!(
      task: nil,
      user: nil,
      primary: false,
      status: 1,
      biz: nil
    ))
  end

  it "renders the edit task_user form" do
    render

    assert_select "form[action=?][method=?]", task_user_path(@task_user), "post" do
      assert_select "input[name=?]", "task_user[task_id]"

      assert_select "input[name=?]", "task_user[user_id]"

      assert_select "input[name=?]", "task_user[primary]"

      assert_select "input[name=?]", "task_user[status]"

      assert_select "input[name=?]", "task_user[biz_id]"
    end
  end
end
