class Pmqa::CategoryMasterListItemsController < ApplicationController
  def index
    url = URI("#{$server[@controller_title]}/api/v1/category_master_list_items")

    @datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/category_master_list_items/#{params["id"]}")

    @datas = request_api(url, "get", nil)
  end

  def new
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/category_master_list_items/#{params["id"]}")

    @datas = request_api(url, "get", nil)

    # puts "plan edit = #{@plan['title']}"
  end

  def create
    params[:category_master_list_item][:user_id] = $user_id
    params[:category_master_list_item][:biz_id] = $biz_id

    url = URI("#{$server[@controller_title]}/api/v1/category_master_list_items")
    @datas = request_api(url, "post", category_master_list_item_params)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/category_masters", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_title}/category_master_list_items/new", alert: @datas["data"]
    end
  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/category_master_list_items/#{params["id"]}")
    @datas = request_api(url, "patch", category_master_list_item_params)

    # puts "update status : #{res}"

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/category_masters", notice: @datas["message"]
    else
      redirect_to "/#{@controller_title}/category_master_list_items", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/category_master_list_items/#{params["id"]}")
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/category_masters", notice: @datas["message"]
    else
      redirect_to "/#{@controller_title}/category_master_list_items", alert: @datas["data"]
    end
  end

  private

  def category_master_list_item_params
    params.require(:category_master_list_item).permit(:order_sort, :title, :user_id, :biz_id, :category_master_list_id)
  end
end
