require "test_helper"

class WebBlogCategoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @web_blog_category = web_blog_categories(:one)
  end

  test "should get index" do
    get web_blog_categories_url
    assert_response :success
  end

  test "should get new" do
    get new_web_blog_category_url
    assert_response :success
  end

  test "should create web_blog_category" do
    assert_difference("WebBlogCategory.count") do
      post web_blog_categories_url, params: {web_blog_category: {biz_id: @web_blog_category.biz_id, description_th: @web_blog_category.description_th, image: @web_blog_category.image, order: @web_blog_category.order, title_th: @web_blog_category.title_th}}
    end

    assert_redirected_to web_blog_category_url(WebBlogCategory.last)
  end

  test "should show web_blog_category" do
    get web_blog_category_url(@web_blog_category)
    assert_response :success
  end

  test "should get edit" do
    get edit_web_blog_category_url(@web_blog_category)
    assert_response :success
  end

  test "should update web_blog_category" do
    patch web_blog_category_url(@web_blog_category), params: {web_blog_category: {biz_id: @web_blog_category.biz_id, description_th: @web_blog_category.description_th, image: @web_blog_category.image, order: @web_blog_category.order, title_th: @web_blog_category.title_th}}
    assert_redirected_to web_blog_category_url(@web_blog_category)
  end

  test "should destroy web_blog_category" do
    assert_difference("WebBlogCategory.count", -1) do
      delete web_blog_category_url(@web_blog_category)
    end

    assert_redirected_to web_blog_categories_url
  end
end
