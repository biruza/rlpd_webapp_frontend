module Api
  module V1
    class PostsController < ApplicationController
      before_action :set_user, only: [:index]
      # before_action :authenticate_request!

      def index
        @posts = Post.where(user_id: @user.id)
        render json: @posts.as_json(only: [:id, :title, :image_data, :biz_id],
          include: {
            user: {only: [:email]}
          }), message: "Success"
      end

      def create
        @post = Post.new(post_params)

        if @post.save
          render json: @post.as_json(only: [:id, :title, :image_data]), message: "Success"
        else
          render json: @post.errors, status: :unprocessable_entity
        end
      end

      private

      def set_user
        @user = User.find(auth_token[:user_id]) if auth_token
      end

      def post_params
        params.require(:post).permit(:title, :image_data, :biz_id)
      end
    end
  end
end
