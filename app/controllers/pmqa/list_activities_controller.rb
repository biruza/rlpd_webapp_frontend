class Pmqa::ListActivitiesController < ApplicationController
  before_action :set_select, only: %i[new create edit update]

  def new
  end

  def create
    url = URI("#{$server[@controller_title]}/api/v1/list_activities")
    @datas = request_api(url, "post", list_activities_params)

    if @datas["status"] == "Success"
      url = URI("#{$server[@controller_title]}/api/v1/lists/#{params[:list_activity][:list_id]}")
      @list = request_api(url, "get", nil)

      url = URI("#{$server[@controller_title]}/api/v1/categories/#{@list["data"]["category_id"]}")
      @category = request_api(url, "get", nil)

      redirect_to "/#{@controller_title}/plans/#{@category["data"]["plan_id"]}", notice: @datas["messages"]
    else
      redirect_to params[:original_url].to_s, alert: @datas["data"]
    end
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/list_activities/#{params["id"]}")
    @datas = request_api(url, "get", nil)
  end

  def update
    idx = 0
    if !params[:list_activity][:list_results_attributes].blank?
      params[:list_activity][:list_results_attributes].each do |item|
        if params[:list_activity][:list_results_attributes][idx.to_s]["title"].blank?
          #params[:list_activity][:list_results_attributes][idx.to_s]     #Update 11/7/2023 แก้ส่วนของการ ไม่ Update ค่าว่าง
          params[:list_activity][:list_results_attributes][idx.to_s]["title"] = nil   #Insert 11/7/2023
          
        else
          params[:list_activity][:list_results_attributes][idx.to_s]["user_id"] = $user[session.id.to_s][@controller_title]["id"]
          params[:list_activity][:list_results_attributes][idx.to_s]["biz_id"] = $user[session.id.to_s][@controller_title]["biz_id"]
        end
        idx += 1
      end
    end

    idx = 0
    if !params[:list_activity][:list_responsibles_attributes].blank?
      params[:list_activity][:list_responsibles_attributes].each do |item|
        if params[:list_activity][:list_responsibles_attributes][idx.to_s]["title"].blank?
          #params[:list_activity][:list_responsibles_attributes][idx.to_s] = nil  #Update 11/7/2023 แก้ส่วนของการ ไม่ Update ค่าว่าง
          params[:list_activity][:list_responsibles_attributes][idx.to_s]["title"] = nil   #Insert 11/7/2023
          #p "2 blank - #{params[:list_activity][:list_responsibles_attributes][idx.to_s]}"

        else
          params[:list_activity][:list_responsibles_attributes][idx.to_s]["user_id"] = $user[session.id.to_s][@controller_title]["id"]
          params[:list_activity][:list_responsibles_attributes][idx.to_s]["biz_id"] = $user[session.id.to_s][@controller_title]["biz_id"]
        end
        idx += 1
      end
    end

    idx = 0
    if !params[:list_activity][:list_accomplices_attributes].blank?
      params[:list_activity][:list_accomplices_attributes].each do |item|
        if params[:list_activity][:list_accomplices_attributes][idx.to_s]["title"].blank?
          #params[:list_activity][:list_accomplices_attributes][idx.to_s] = nil
          params[:list_activity][:list_accomplices_attributes][idx.to_s]["title"] = nil
          #p "3 blank"
        else
          params[:list_activity][:list_accomplices_attributes][idx.to_s]["user_id"] = $user[session.id.to_s][@controller_title]["id"]
          params[:list_activity][:list_accomplices_attributes][idx.to_s]["biz_id"] = $user[session.id.to_s][@controller_title]["biz_id"]
        end
        idx += 1
      end
    end

    idx = 0
    if !params[:list_activity][:list_pmqa_levels_attributes].blank?
      params[:list_activity][:list_pmqa_levels_attributes].each do |item|
        if params[:list_activity][:list_pmqa_levels_attributes][idx.to_s]["title"].blank?
          #params[:list_activity][:list_pmqa_levels_attributes][idx.to_s] = nil
          params[:list_activity][:list_pmqa_levels_attributes][idx.to_s]["title"] = nil
         
        else
          params[:list_activity][:list_pmqa_levels_attributes][idx.to_s]["user_id"] = $user[session.id.to_s][@controller_title]["id"]
          params[:list_activity][:list_pmqa_levels_attributes][idx.to_s]["biz_id"] = $user[session.id.to_s][@controller_title]["biz_id"]
        end
        idx += 1
      end
    end

    idx = 0
    if !params[:list_activity][:list_budgets_attributes].blank?
      params[:list_activity][:list_budgets_attributes].each do |item|
        if params[:list_activity][:list_budgets_attributes][idx.to_s]["title"].blank?
          #params[:list_activity][:list_budgets_attributes][idx.to_s] = nil
          params[:list_activity][:list_budgets_attributes][idx.to_s]["title"] = nil
         # p "4 blank"
        else
          params[:list_activity][:list_budgets_attributes][idx.to_s]["user_id"] = $user[session.id.to_s][@controller_title]["id"]
          params[:list_activity][:list_budgets_attributes][idx.to_s]["biz_id"] = $user[session.id.to_s][@controller_title]["biz_id"]
        end
        idx += 1
      end
    end

    url = URI("#{$server[@controller_title]}/api/v1/list_activities/#{params["id"]}")
    @datas = request_api(url, "patch", list_activities_params)

    # puts "update status : #{res}"

    if @datas && @datas["status"] == "Success"
      # case params[:current_url]
      # when 'plan_reports'
      redirect_to "#{params[:original_url]}?plan_report_id=#{begin
        params[:list_activity][:plan_report_id]
      rescue
        nil
      end}", notice: @datas["message"]
      # else
      # redirect_to "/#{@controller_title}/categories/#{@datas['data']['category_id']}/edit", notice: @datas['message']
      # end

    else
      redirect_to "/#{@controller_title}/list_activities/#{params[:id]}/edit", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/list_activities/#{params["id"]}")
    @datas_ori = request_api(url, "get", nil)
    @datas = request_api(url, "delete", nil)

    url = URI("#{$server[@controller_title]}/api/v1/lists/#{@datas_ori["data"]["list_id"]}")
    @list = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/categories/#{@list["data"]["category_id"]}")
    @category = request_api(url, "get", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/plans/#{@category["data"]["plan_id"]}", notice: @datas["message"]
    else
      redirect_to "/#{@controller_title}/plans/#{@category["data"]["plan_id"]}", alert: t(:can_not_delete)
    end
  end

  def set_select
    url = URI("#{$server[@controller_title]}/api/v1/accomplices")
    @accomplices = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/responsibles")
    @responsibles = request_api(url, "get", nil)
  end

  def list_activities_params
    params.require(:list_activity).permit(:id, :plan_report_id, :title, :status, :slug, :order_sort, :list_id, :user_id, :biz_id,
      list_periods_attributes: [:id, :_destroy, :amount, :unit, :list_activity_id, :list_id, :user_id, :biz_id],
      list_monthly_periods_attributes: [:id, :_destroy, :startdate, :enddate, :list_activity_id, :list_id, :user_id, :biz_id],
      list_results_attributes: [:id, :_destroy, :title, :list_id, :list_activity_id, :user_id, :biz_id],
      list_responsibles_attributes: [:id, :_destroy, :title, :responsible_id, :list_id, :list_activity_id, :user_id, :biz_id],
      list_accomplices_attributes: [:id, :_destroy, :title, :accomplice_id, :list_activity_id, :list_id, :user_id, :biz_id],
      list_pmqa_levels_attributes: [:id, :_destroy, :title, :list_id, :list_activity_id, :user_id, :biz_id],
      list_budgets_attributes: [:id, :_destroy, :title, :list_id, :list_activity_id, :user_id, :biz_id])
  end
end
