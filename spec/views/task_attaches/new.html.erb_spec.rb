require "rails_helper"

RSpec.describe "task_attaches/new", type: :view do
  before(:each) do
    assign(:task_attach, TaskAttach.new(
      task: nil,
      title: "MyString",
      file: "MyText",
      slug: "MyString",
      source: "MyString",
      user: nil,
      biz: nil
    ))
  end

  it "renders new task_attach form" do
    render

    assert_select "form[action=?][method=?]", task_attaches_path, "post" do
      assert_select "input[name=?]", "task_attach[task_id]"

      assert_select "input[name=?]", "task_attach[title]"

      assert_select "textarea[name=?]", "task_attach[file]"

      assert_select "input[name=?]", "task_attach[slug]"

      assert_select "input[name=?]", "task_attach[source]"

      assert_select "input[name=?]", "task_attach[user_id]"

      assert_select "input[name=?]", "task_attach[biz_id]"
    end
  end
end
