require "test_helper"

class SubjectsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @subject = subjects(:one)
  end

  test "should get index" do
    get subjects_url
    assert_response :success
  end

  test "should get new" do
    get new_subject_url
    assert_response :success
  end

  test "should create subject" do
    assert_difference("Subject.count") do
      post subjects_url, params: {subject: {code: @subject.code, lessons_per_week: @subject.lessons_per_week, perform: @subject.perform, status: @subject.status, subject_group_id: @subject.subject_group_id, subject_type_id: @subject.subject_type_id, theory: @subject.theory, title_en: @subject.title_en, title_th: @subject.title_th, unit: @subject.unit}}
    end

    assert_redirected_to subject_url(Subject.last)
  end

  test "should show subject" do
    get subject_url(@subject)
    assert_response :success
  end

  test "should get edit" do
    get edit_subject_url(@subject)
    assert_response :success
  end

  test "should update subject" do
    patch subject_url(@subject), params: {subject: {code: @subject.code, lessons_per_week: @subject.lessons_per_week, perform: @subject.perform, status: @subject.status, subject_group_id: @subject.subject_group_id, subject_type_id: @subject.subject_type_id, theory: @subject.theory, title_en: @subject.title_en, title_th: @subject.title_th, unit: @subject.unit}}
    assert_redirected_to subject_url(@subject)
  end

  test "should destroy subject" do
    assert_difference("Subject.count", -1) do
      delete subject_url(@subject)
    end

    assert_redirected_to subjects_url
  end
end
