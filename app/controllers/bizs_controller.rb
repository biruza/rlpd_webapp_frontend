class BizsController < ApplicationController
  layout :layout_by_resource
  load_and_authorize_resource
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, alert: exception.message
  end
  before_action :authenticate_user!

  before_action :set_biz, only: [:show, :edit, :update, :destroy]

  # GET /bizs
  # GET /bizs.json
  def index
    if current_user.superadmin_role?
      @bizs = Biz.all
    else
      redirect_to root_url
    end
  end

  # GET /bizs/1
  # GET /bizs/1.json
  def show
  end

  # GET /bizs/new
  def new
    @biz = Biz.new
    @biz.email = current_user.email
  end

  # GET /bizs/1/edit
  def edit
  end

  # POST /bizs
  # POST /bizs.json
  def create
    @biz = Biz.new(biz_params)
    @biz.start_date = Time.now

    respond_to do |format|
      if @biz.save
        @user = User.find_by(id: current_user.id)
        @user.update(biz_id: @biz.id, administrator_role: true, user_role: false)

        format.html { redirect_to root_url, notice: "Biz was successfully created." }
        format.json { render :show, status: :created, location: @board }
      else
        format.html { render :new }
        format.json { render json: @biz.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bizs/1
  # PATCH/PUT /bizs/1.json
  def update
    respond_to do |format|
      if @biz.update(biz_params)
        format.html { redirect_to root_url, notice: "Biz was successfully updated." }
        format.json { render :show, status: :ok, location: @biz }
      else
        format.html { render :edit }
        format.json { render json: @biz.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bizs/1
  # DELETE /bizs/1.json
  def destroy
    @biz.destroy
    respond_to do |format|
      format.html { redirect_to bizs_url, notice: "Biz was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_biz
    @biz = if current_user.superadmin_role?
      Biz.find(params[:id])
    else
      Biz.find(current_user.biz_id)
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def biz_params
    params.require(:biz).permit(:subdomain, :domain, :task_type, :doc_type_re_sign_image, :doc_type_iv_sign_image, :doc_type_qt_sign_image, :doc_type_ex_sign_image, :branch, :doc_type_ex, :doc_type_ex_start, :doc_type_ex_note, :doc_type_ex_sign, :web_description, :web_keywords, :stamp, :doc_type_re, :doc_type_iv, :doc_type_qt, :doc_type_re_start, :doc_type_iv_start, :doc_type_qt_start, :doc_type_re_note, :doc_type_iv_note, :doc_type_qt_note, :doc_type_re_sign, :doc_type_iv_sign, :doc_type_qt_sign, :package_id, :start_date, :qty_package, :status, :title_th, :title_en, :address_th, :address_en, :phone, :fax, :email, :taxid, :image, :li, :fb, :tw, :ig, :yt, :li_url, :fb_url, :tw_url, :ig_url, :yt_url)
  end

  def layout_by_resource
    if action_name == "new"
      "blank"
    else
      "application"
    end
  end
end
