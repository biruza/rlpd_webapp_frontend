require "rails_helper"

RSpec.describe "task_checklists/edit", type: :view do
  before(:each) do
    @task_checklist = assign(:task_checklist, TaskChecklist.create!(
      task: nil,
      order_sort: 1,
      title: "MyString",
      type: "",
      checklist_type: "MyString",
      respons_data: "MyText",
      status: "MyString",
      score: "9.99",
      custom: "",
      description: "MyText",
      user: nil,
      biz: nil
    ))
  end

  it "renders the edit task_checklist form" do
    render

    assert_select "form[action=?][method=?]", task_checklist_path(@task_checklist), "post" do
      assert_select "input[name=?]", "task_checklist[task_id]"

      assert_select "input[name=?]", "task_checklist[order_sort]"

      assert_select "input[name=?]", "task_checklist[title]"

      assert_select "input[name=?]", "task_checklist[type]"

      assert_select "input[name=?]", "task_checklist[checklist_type]"

      assert_select "textarea[name=?]", "task_checklist[respons_data]"

      assert_select "input[name=?]", "task_checklist[status]"

      assert_select "input[name=?]", "task_checklist[score]"

      assert_select "input[name=?]", "task_checklist[custom]"

      assert_select "textarea[name=?]", "task_checklist[description]"

      assert_select "input[name=?]", "task_checklist[user_id]"

      assert_select "input[name=?]", "task_checklist[biz_id]"
    end
  end
end
