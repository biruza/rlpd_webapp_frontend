require "test_helper"

class InvoicesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @invoice = invoices(:one)
  end

  test "should get index" do
    get invoices_url
    assert_response :success
  end

  test "should get new" do
    get new_invoice_url
    assert_response :success
  end

  test "should create invoice" do
    assert_difference("Invoice.count") do
      post invoices_url, params: {invoice: {amount: @invoice.amount, biz_id: @invoice.biz_id, company_id: @invoice.company_id, description: @invoice.description, discount_amount: @invoice.discount_amount, doc_number: @invoice.doc_number, doc_ref: @invoice.doc_ref, doc_ref_date: @invoice.doc_ref_date, doc_sign: @invoice.doc_sign, image_data: @invoice.image_data, payee_type: @invoice.payee_type, payment_bank: @invoice.payment_bank, payment_number: @invoice.payment_number, payment_type: @invoice.payment_type, project_id: @invoice.project_id, revenue_type_id: @invoice.revenue_type_id, status: @invoice.status, vat: @invoice.vat, vat_inout: @invoice.vat_inout, vat_status: @invoice.vat_status, withholding_tax_amount: @invoice.withholding_tax_amount, withholding_tax_status: @invoice.withholding_tax_status}}
    end

    assert_redirected_to invoice_url(Invoice.last)
  end

  test "should show invoice" do
    get invoice_url(@invoice)
    assert_response :success
  end

  test "should get edit" do
    get edit_invoice_url(@invoice)
    assert_response :success
  end

  test "should update invoice" do
    patch invoice_url(@invoice), params: {invoice: {amount: @invoice.amount, biz_id: @invoice.biz_id, company_id: @invoice.company_id, description: @invoice.description, discount_amount: @invoice.discount_amount, doc_number: @invoice.doc_number, doc_ref: @invoice.doc_ref, doc_ref_date: @invoice.doc_ref_date, doc_sign: @invoice.doc_sign, image_data: @invoice.image_data, payee_type: @invoice.payee_type, payment_bank: @invoice.payment_bank, payment_number: @invoice.payment_number, payment_type: @invoice.payment_type, project_id: @invoice.project_id, revenue_type_id: @invoice.revenue_type_id, status: @invoice.status, vat: @invoice.vat, vat_inout: @invoice.vat_inout, vat_status: @invoice.vat_status, withholding_tax_amount: @invoice.withholding_tax_amount, withholding_tax_status: @invoice.withholding_tax_status}}
    assert_redirected_to invoice_url(@invoice)
  end

  test "should destroy invoice" do
    assert_difference("Invoice.count", -1) do
      delete invoice_url(@invoice)
    end

    assert_redirected_to invoices_url
  end
end
