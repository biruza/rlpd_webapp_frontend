require "rails_helper"

RSpec.describe "task_attaches/show", type: :view do
  before(:each) do
    @task_attach = assign(:task_attach, TaskAttach.create!(
      task: nil,
      title: "Title",
      file: "MyText",
      slug: "Slug",
      source: "Source",
      user: nil,
      biz: nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/Slug/)
    expect(rendered).to match(/Source/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
  end
end
