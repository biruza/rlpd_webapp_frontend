class Pmqa::IssuesController < ApplicationController
  def index
    url = URI("#{$server[@controller_title]}/api/v1/issues")
    @datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/issues/#{params["id"]}")
    @datas = request_api(url, "get", nil)
  end

  def new
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/issues/#{params["id"]}")

    @datas = request_api(url, "get", nil)

    # puts "plan edit = #{@plan['title']}"
  end

  def create
    params[:issues][:user_id] = $user_id
    params[:issues][:biz_id] = $biz_id

    url = URI("#{$server[@controller_title]}/api/v1/issues")
    @datas = request_api(url, "post", issue_params)

    if @datas["status"] == "Success"
      redirect_to "/pmqa/issues", notice: @datas["messages"]
    else
      redirect_to "/pmqa/issues/new", alert: @datas["data"]
    end
  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/issues/#{params["id"]}")
    @datas = request_api(url, "patch", issue_params)

    # puts "update status : #{res}"

    if @datas["status"] == "Success"
      redirect_to "/pmqa/issues", notice: @datas["message"]
    else
      redirect_to "/pmqa/issues", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/issues/#{params["id"]}")
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
      redirect_to "/pmqa/issues", notice: @datas["message"]
    else
      redirect_to "/pmqa/issues", alert: @datas["data"]
    end
  end

  private

  def issue_params
    params.require(:issue).permit(:title, :user_id, :biz_id)
  end
end
