require "rails_helper"

RSpec.describe "tpl_tasks/edit", type: :view do
  before(:each) do
    @tpl_task = assign(:tpl_task, TplTask.create!(
      list: nil,
      title: "MyString",
      description: "MyText",
      cover: "MyText",
      priority: 1,
      status: 1,
      achieve: false,
      user: nil,
      biz: nil
    ))
  end

  it "renders the edit tpl_task form" do
    render

    assert_select "form[action=?][method=?]", tpl_task_path(@tpl_task), "post" do
      assert_select "input[name=?]", "tpl_task[list_id]"

      assert_select "input[name=?]", "tpl_task[title]"

      assert_select "textarea[name=?]", "tpl_task[description]"

      assert_select "textarea[name=?]", "tpl_task[cover]"

      assert_select "input[name=?]", "tpl_task[priority]"

      assert_select "input[name=?]", "tpl_task[status]"

      assert_select "input[name=?]", "tpl_task[achieve]"

      assert_select "input[name=?]", "tpl_task[user_id]"

      assert_select "input[name=?]", "tpl_task[biz_id]"
    end
  end
end
