require "test_helper"

class SparePartGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @spare_part_group = spare_part_groups(:one)
  end

  test "should get index" do
    get spare_part_groups_url
    assert_response :success
  end

  test "should get new" do
    get new_spare_part_group_url
    assert_response :success
  end

  test "should create spare_part_group" do
    assert_difference("SparePartGroup.count") do
      post spare_part_groups_url, params: {spare_part_group: {name: @spare_part_group.name}}
    end

    assert_redirected_to spare_part_group_url(SparePartGroup.last)
  end

  test "should show spare_part_group" do
    get spare_part_group_url(@spare_part_group)
    assert_response :success
  end

  test "should get edit" do
    get edit_spare_part_group_url(@spare_part_group)
    assert_response :success
  end

  test "should update spare_part_group" do
    patch spare_part_group_url(@spare_part_group), params: {spare_part_group: {name: @spare_part_group.name}}
    assert_redirected_to spare_part_group_url(@spare_part_group)
  end

  test "should destroy spare_part_group" do
    assert_difference("SparePartGroup.count", -1) do
      delete spare_part_group_url(@spare_part_group)
    end

    assert_redirected_to spare_part_groups_url
  end
end
