require "rails_helper"

RSpec.describe "tpl_lists/index", type: :view do
  before(:each) do
    assign(:tpl_lists, [
      TplList.create!(
        list: nil,
        biz: nil
      ),
      TplList.create!(
        list: nil,
        biz: nil
      )
    ])
  end

  it "renders a list of tpl_lists" do
    render
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
  end
end
