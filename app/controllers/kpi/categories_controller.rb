class Kpi::CategoriesController < ApplicationController
  before_action :set_select, only: %i[new edit]

  def index
    url = URI("#{$server[@controller_title]}/api/v1/categories")
    @datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/categories/#{params["id"]}")
    @datas = request_api(url, "get", nil)
  end

  def new
    url = URI("#{$server[@controller_title]}/api/v1/users/#{$user[session.id.to_s][@controller_title]["id"]}")
    @user = request_api(url, "get", nil)
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/categories/#{params["id"]}")
    @datas = request_api(url, "get", nil)
  end

  def create
    url = URI("#{$server[@controller_title]}/api/v1/categories")
    @datas = request_api(url, "post", category_params)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/plans/#{@datas["data"]["plan_id"]}", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_title}/plans/#{params["category"]["plan_id"]}", alert: @datas["data"]
    end
  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/categories/#{params["id"]}")
    @datas = request_api(url, "patch", category_params)

    if @datas && @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/plans/#{params["category"]["plan_id"]}", notice: @datas["message"]
    else
      redirect_to "/#{@controller_title}/categories/#{params["id"]}/edit?plan_id=#{params["category"]["plan_id"]}", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/categories/#{params["id"]}")
    @datas_ori = request_api(url, "get", nil)
    @datas = request_api(url, "delete", nil)

    url_plan = URI("#{$server[@controller_title]}/api/v1/plans/#{@datas_ori["data"]["plan_id"]}")
    @plan = request_api(url_plan, "get", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/master_plans/#{@plan["data"]["master_plan_id"]}", notice: @datas["message"]
    else
      redirect_to "/#{@controller_title}/master_plans/#{@plan["data"]["master_plan_id"]}", alert: t(:can_not_delete)
    end
  end

  private

  def set_select
    url = URI("#{$server[@controller_title]}/api/v1/plans/#{params["plan_id"]}")
    @data_plan = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/plans")
    @data_plans = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/institutions")
    @data_institutions = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/responsibles")
    @data_responsibles = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/issues")
    @data_issues = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/kpis")
    @data_kpis = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/target_kpis")
    @data_target_kpis = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/activities")
    @data_activities = request_api(url, "get", nil)
  end

  def category_params
    params.require(:category).permit(:title, :cognitive, :slug, :approve, :plan_id, :institution_id, :issue_id, :kpi_id, :target_kpi_id, :responsible_id, :user_id, :biz_id,
      lists_attributes: [:id, :_destroy, :category_id, :status, :slug, :order_sort, :activity_id, :user_id, :biz_id,
        tasks_attributes: [:id, :_destroy, :category_id, :slug, :status, :order_sort, :list_id, :plan_report_id, :user_id, :biz_id]])
  end

  def authenticate_user!
    if user_signed_in?
      super
    else
      redirect_to "accounts/sign_in", notice: "Please Login to view that page!"
    end
  end
end
