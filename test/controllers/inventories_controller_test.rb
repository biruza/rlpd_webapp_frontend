require "test_helper"

class InventoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @inventory = inventories(:one)
  end

  test "should get index" do
    get inventories_url
    assert_response :success
  end

  test "should get new" do
    get new_inventory_url
    assert_response :success
  end

  test "should create inventory" do
    assert_difference("Inventory.count") do
      post inventories_url, params: {inventory: {biz_id: @inventory.biz_id, code: @inventory.code, description_en: @inventory.description_en, description_th: @inventory.description_th, doc_number: @inventory.doc_number, location_id: @inventory.location_id, product_id: @inventory.product_id, qty_max: @inventory.qty_max, qty_min: @inventory.qty_min, unit_id: @inventory.unit_id, user_id: @inventory.user_id}}
    end

    assert_redirected_to inventory_url(Inventory.last)
  end

  test "should show inventory" do
    get inventory_url(@inventory)
    assert_response :success
  end

  test "should get edit" do
    get edit_inventory_url(@inventory)
    assert_response :success
  end

  test "should update inventory" do
    patch inventory_url(@inventory), params: {inventory: {biz_id: @inventory.biz_id, code: @inventory.code, description_en: @inventory.description_en, description_th: @inventory.description_th, doc_number: @inventory.doc_number, location_id: @inventory.location_id, product_id: @inventory.product_id, qty_max: @inventory.qty_max, qty_min: @inventory.qty_min, unit_id: @inventory.unit_id, user_id: @inventory.user_id}}
    assert_redirected_to inventory_url(@inventory)
  end

  test "should destroy inventory" do
    assert_difference("Inventory.count", -1) do
      delete inventory_url(@inventory)
    end

    assert_redirected_to inventories_url
  end
end
