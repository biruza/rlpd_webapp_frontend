# frozen_string_literal: true

class Users::RegistrationsController < Devise::RegistrationsController
  # before_action :configure_sign_up_params, only: [:create]
  # before_action :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  def new
    super
  end

  # POST /resource
  def create
    # user create ใช้เพื่อสมัครอย่างเดียว
    @user = User.new(user_params)

    # if !current_user.present?
    #  @user.user_role = true
    if current_user.present? && !current_user.superadmin_role? && current_user.biz_id.present?
      @user.biz_id = current_user.biz_id
    end

    # if (verify_recaptcha(model: @user) && @user.save) || (current_user.superadmin_role? && @user.save) || (current_user.administrator_role? && @user.save) || (current_user.supervisor_role? && @user.save)
    # if verify_recaptcha(model: @user) && @user.save
    if @user.save
      flash[:notice] = "Successfully created User."

      sign_in(@user) if !current_user

      if current_user.present? && current_user.biz_id.present?
        redirect_to users_path
      else
        redirect_to new_biz_path
      end
    else
      render action: "new"
    end
  end

  # GET /resource/edit
  def edit
    super
  end

  # PUT /resource
  def update
    super
  end

  # DELETE /resource
  def destroy
    super
  end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_up_params
  #   devise_parameter_sanitizer.permit(:sign_up, keys: [:attribute])
  # end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
  # end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end

  private

  def set_user
    @user = if current_user.superadmin_role?
      User.find(params[:id])
    else
      User.find(current_user.id)
    end
  end

  def user_params
    if current_user.present? && current_user.superadmin_role?
      params.require(:user).permit(:parent_ids, :administrator_role, :supervisor_role, :staff_role, :user_role, :biz_id, :prefix, :percode, :nation, :race, :origin, :phone, :mobile, :address, :city, :amphoe, :district, :province, :zipcode, :job, :job_location, :job_phone, :salary, :firstname, :lastname, :email, :avatar, :password, :password_confirmation, :parent_id)
    elsif current_user.present? && current_user.administrator_role?
      params.require(:user).permit(:parent_ids, :administrator_role, :supervisor_role, :staff_role, :user_role, :biz_id, :prefix, :percode, :nation, :race, :origin, :phone, :mobile, :address, :city, :amphoe, :district, :province, :zipcode, :job, :job_location, :job_phone, :salary, :firstname, :lastname, :email, :avatar, :password, :password_confirmation, :parent_id)
    elsif current_user.present? && current_user.supervisor_role?
      params.require(:user).permit(:parent_ids, :staff_role, :biz_id, :prefix, :percode, :nation, :race, :origin, :phone, :mobile, :address, :city, :amphoe, :district, :province, :zipcode, :job, :job_location, :job_phone, :salary, :firstname, :lastname, :email, :employee_id, :avatar, :password, :password_confirmation)
    else
      params.require(:user).permit(:parent_ids, :biz_id, :prefix, :percode, :nation, :race, :origin, :phone, :mobile, :address, :city, :amphoe, :district, :province, :zipcode, :job, :job_location, :job_phone, :salary, :firstname, :lastname, :email, :avatar, :password, :password_confirmation)
    end
  end
end
