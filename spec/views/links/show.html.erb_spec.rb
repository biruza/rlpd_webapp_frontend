require "rails_helper"

RSpec.describe "links/show", type: :view do
  before(:each) do
    @link = assign(:link, Link.create!(
      source: nil,
      target: nil,
      link_type: "Link Type",
      user: nil,
      biz: nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(/Link Type/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
  end
end
