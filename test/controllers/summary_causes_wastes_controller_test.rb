require "test_helper"

class SummaryCausesWastesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @summary_causes_waste = summary_causes_wastes(:one)
  end

  test "should get index" do
    get summary_causes_wastes_url
    assert_response :success
  end

  test "should get new" do
    get new_summary_causes_waste_url
    assert_response :success
  end

  test "should create summary_causes_waste" do
    assert_difference("SummaryCausesWaste.count") do
      post summary_causes_wastes_url, params: {summary_causes_waste: {description: @summary_causes_waste.description}}
    end

    assert_redirected_to summary_causes_waste_url(SummaryCausesWaste.last)
  end

  test "should show summary_causes_waste" do
    get summary_causes_waste_url(@summary_causes_waste)
    assert_response :success
  end

  test "should get edit" do
    get edit_summary_causes_waste_url(@summary_causes_waste)
    assert_response :success
  end

  test "should update summary_causes_waste" do
    patch summary_causes_waste_url(@summary_causes_waste), params: {summary_causes_waste: {description: @summary_causes_waste.description}}
    assert_redirected_to summary_causes_waste_url(@summary_causes_waste)
  end

  test "should destroy summary_causes_waste" do
    assert_difference("SummaryCausesWaste.count", -1) do
      delete summary_causes_waste_url(@summary_causes_waste)
    end

    assert_redirected_to summary_causes_wastes_url
  end
end
