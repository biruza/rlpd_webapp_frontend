require "shrine"

if Rails.env.production?
  require "shrine/storage/s3"

  s3_options = {
    access_key_id: Rails.application.secrets.s3_access_key_id,
    secret_access_key: Rails.application.secrets.s3_secret_access_key,
    region: Rails.application.secrets.s3_region,
    bucket: Rails.application.secrets.s3_bucket
  }

  Shrine.storages = {
    # original
    cache: Shrine::Storage::S3.new(prefix: "cache", **s3_options),
    store: Shrine::Storage::S3.new(**s3_options)

    # for test expire s3
    # cache: Shrine::Storage::S3.new(prefix: "cache", upload_options: { acl: "public-read" }, **s3_options),
    # store: Shrine::Storage::S3.new(prefix: "store", upload_options: { acl: "public-read" },**s3_options),
    # store: Shrine::Storage::S3.new(upload_options: { acl: "public-read" },**s3_options),
    ####
  }

  # for test expire s3 - 2
  # Shrine.plugin :module_include
  # Shrine.file_module do
  #  def url(**options)
  #    return super unless storage.is_a?(Shrine::Storage::S3) && storage_key == 'store'
  #    Rails.cache.fetch("url:#{storage_key}:#{id}", expires_in: 6 * 24 * 60 * 60) do
  #      super(expires_in: 7 * 24 * 60 * 60, **options)
  #    end
  #  end
  # end
  ####

  # for test expire s3 - 3
  # def url
  #  Rails.cache.fetch("url", self, expires_in: 6.days) do
  #    super(public: false, expires_in: 1.week)
  #  end
  # end
  ####

else
  require "shrine/storage/file_system"

  Shrine.storages = {
    cache: Shrine::Storage::FileSystem.new("public", prefix: "uploads/cache"),
    store: Shrine::Storage::FileSystem.new("public", prefix: "uploads")
  }
end

Shrine.plugin :activerecord
Shrine.plugin :backgrounding
#Shrine.plugin :logging
Shrine.plugin :determine_mime_type
Shrine.plugin :cached_attachment_data
Shrine.plugin :restore_cached_data

# for test expire s3
# Shrine.plugin :default_url_options, cache: { public: true }, store: { public: true }
####

if Rails.env.production?
  Shrine.plugin :presign_endpoint, presign_options: {method: :put}

  # for test expire s3
  # Shrine.plugin :presign_endpoint, presign_options: -> (request) {
  # Uppy will send the "filename" and "type" query parameters
  # filename = request.params["filename"]
  # type     = request.params["type"]

  # {
  #  content_disposition:    ContentDisposition.inline(filename), # set download filename
  #  content_type:           type,                                # set content type (required if using DigitalOcean Spaces)
  #  content_length_range:   0..(10*1024*1024),                   # limit upload size to 10 MB
  # }
  # }
  ####

else
  Shrine.plugin :upload_endpoint
end

Shrine::Attacher.promote { |data| PromoteJob.perform_async(data) }
Shrine::Attacher.delete { |data| DeleteJob.perform_async(data) }
