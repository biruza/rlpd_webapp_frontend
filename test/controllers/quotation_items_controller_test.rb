require "test_helper"

class QuotationItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @quotation_item = quotation_items(:one)
  end

  test "should get index" do
    get quotation_items_url
    assert_response :success
  end

  test "should get new" do
    get new_quotation_item_url
    assert_response :success
  end

  test "should create quotation_item" do
    assert_difference("QuotationItem.count") do
      post quotation_items_url, params: {quotation_item: {amount: @quotation_item.amount, price: @quotation_item.price, product_desc: @quotation_item.product_desc, product_id: @quotation_item.product_id, product_title: @quotation_item.product_title, qty: @quotation_item.qty, quotation_id: @quotation_item.quotation_id}}
    end

    assert_redirected_to quotation_item_url(QuotationItem.last)
  end

  test "should show quotation_item" do
    get quotation_item_url(@quotation_item)
    assert_response :success
  end

  test "should get edit" do
    get edit_quotation_item_url(@quotation_item)
    assert_response :success
  end

  test "should update quotation_item" do
    patch quotation_item_url(@quotation_item), params: {quotation_item: {amount: @quotation_item.amount, price: @quotation_item.price, product_desc: @quotation_item.product_desc, product_id: @quotation_item.product_id, product_title: @quotation_item.product_title, qty: @quotation_item.qty, quotation_id: @quotation_item.quotation_id}}
    assert_redirected_to quotation_item_url(@quotation_item)
  end

  test "should destroy quotation_item" do
    assert_difference("QuotationItem.count", -1) do
      delete quotation_item_url(@quotation_item)
    end

    assert_redirected_to quotation_items_url
  end
end
