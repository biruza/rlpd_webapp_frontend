require "test_helper"

class SparePartStoresControllerTest < ActionDispatch::IntegrationTest
  setup do
    @spare_part_store = spare_part_stores(:one)
  end

  test "should get index" do
    get spare_part_stores_url
    assert_response :success
  end

  test "should get new" do
    get new_spare_part_store_url
    assert_response :success
  end

  test "should create spare_part_store" do
    assert_difference("SparePartStore.count") do
      post spare_part_stores_url, params: {spare_part_store: {improvement_history_id: @spare_part_store.improvement_history_id, location_id: @spare_part_store.location_id, manufacturer_id: @spare_part_store.manufacturer_id, max: @spare_part_store.max, min: @spare_part_store.min, price_per_unit: @spare_part_store.price_per_unit, qty: @spare_part_store.qty, spare_part_id: @spare_part_store.spare_part_id}}
    end

    assert_redirected_to spare_part_store_url(SparePartStore.last)
  end

  test "should show spare_part_store" do
    get spare_part_store_url(@spare_part_store)
    assert_response :success
  end

  test "should get edit" do
    get edit_spare_part_store_url(@spare_part_store)
    assert_response :success
  end

  test "should update spare_part_store" do
    patch spare_part_store_url(@spare_part_store), params: {spare_part_store: {improvement_history_id: @spare_part_store.improvement_history_id, location_id: @spare_part_store.location_id, manufacturer_id: @spare_part_store.manufacturer_id, max: @spare_part_store.max, min: @spare_part_store.min, price_per_unit: @spare_part_store.price_per_unit, qty: @spare_part_store.qty, spare_part_id: @spare_part_store.spare_part_id}}
    assert_redirected_to spare_part_store_url(@spare_part_store)
  end

  test "should destroy spare_part_store" do
    assert_difference("SparePartStore.count", -1) do
      delete spare_part_store_url(@spare_part_store)
    end

    assert_redirected_to spare_part_stores_url
  end
end
