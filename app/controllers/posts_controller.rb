class PostsController < ApplicationController
  require "csv"
  load_and_authorize_resource
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, alert: exception.message
  end
  before_action :authenticate_user!
  before_action :set_post, only: %i[show edit update destroy]

  # GET /posts
  # GET /posts.json
  def index
    @posts = Post.where(biz_id: current_user.biz_id)

    respond_to do |format|
      format.html
      format.json
      format.csv { send_data @posts.to_csv }
      format.pdf do
        render pdf: "posts-#{Time.now.strftime("%Y%m%d-%H%M")}",
          page_size: "A4",
          layout: "pdf.pdf",
          orientation: "Portrait",
          lowquality: true,
          zoom: 1,
          dpi: 100
      end
    end
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new(post_params)

    @post.user_id = current_user.id
    @post.biz_id = current_user.biz_id

    respond_to do |format|
      if @post.save
        format.html { redirect_to posts_url, notice: "User was successfully created." }
        format.json { render :show, status: :created, location: @post }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to posts_url, notice: "Post was successfully updated." }
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to posts_url, notice: "Post was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_post
    @post = Post.find_by(id: params[:id], biz_id: current_user.biz_id)
  end

  # Only allow a list of trusted parameters through.
  def post_params
    # params.require(:post).permit(:title, :image_data, :biz)
    params.require(:post).permit(:title, :image_data, :biz_id)
  end
end
