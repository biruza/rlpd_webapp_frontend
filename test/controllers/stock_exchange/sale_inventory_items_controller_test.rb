require "test_helper"

class StockExchange::SaleInventoryItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @stock_exchange_sale_inventory_item = stock_exchange_sale_inventory_items(:one)
  end

  test "should get index" do
    get stock_exchange_sale_inventory_items_url
    assert_response :success
  end

  test "should get new" do
    get new_stock_exchange_sale_inventory_item_url
    assert_response :success
  end

  test "should create stock_exchange_sale_inventory_item" do
    assert_difference("StockExchange::SaleInventoryItem.count") do
      post stock_exchange_sale_inventory_items_url, params: {stock_exchange_sale_inventory_item: {biz_id: @stock_exchange_sale_inventory_item.biz_id, cost: @stock_exchange_sale_inventory_item.cost, inventory_id: @stock_exchange_sale_inventory_item.inventory_id, price: @stock_exchange_sale_inventory_item.price, sale_inventory_id: @stock_exchange_sale_inventory_item.sale_inventory_id, status: @stock_exchange_sale_inventory_item.status, user_id: @stock_exchange_sale_inventory_item.user_id}}
    end

    assert_redirected_to stock_exchange_sale_inventory_item_url(StockExchange::SaleInventoryItem.last)
  end

  test "should show stock_exchange_sale_inventory_item" do
    get stock_exchange_sale_inventory_item_url(@stock_exchange_sale_inventory_item)
    assert_response :success
  end

  test "should get edit" do
    get edit_stock_exchange_sale_inventory_item_url(@stock_exchange_sale_inventory_item)
    assert_response :success
  end

  test "should update stock_exchange_sale_inventory_item" do
    patch stock_exchange_sale_inventory_item_url(@stock_exchange_sale_inventory_item), params: {stock_exchange_sale_inventory_item: {biz_id: @stock_exchange_sale_inventory_item.biz_id, cost: @stock_exchange_sale_inventory_item.cost, inventory_id: @stock_exchange_sale_inventory_item.inventory_id, price: @stock_exchange_sale_inventory_item.price, sale_inventory_id: @stock_exchange_sale_inventory_item.sale_inventory_id, status: @stock_exchange_sale_inventory_item.status, user_id: @stock_exchange_sale_inventory_item.user_id}}
    assert_redirected_to stock_exchange_sale_inventory_item_url(@stock_exchange_sale_inventory_item)
  end

  test "should destroy stock_exchange_sale_inventory_item" do
    assert_difference("StockExchange::SaleInventoryItem.count", -1) do
      delete stock_exchange_sale_inventory_item_url(@stock_exchange_sale_inventory_item)
    end

    assert_redirected_to stock_exchange_sale_inventory_items_url
  end
end
