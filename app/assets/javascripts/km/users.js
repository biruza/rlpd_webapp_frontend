$('.selectize-institution').selectize({
	sortField: 'text',
	create: function(input, callback) {
		$.post('/institutions.json', { institution: { title: input } })
		.done(function(response){
			console.log(response)
			callback({value: response.id, text: response.title });
		})
	}
});