require "test_helper"

class PlanDeviceSparePartsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @plan_device_spare_part = plan_device_spare_parts(:one)
  end

  test "should get index" do
    get plan_device_spare_parts_url
    assert_response :success
  end

  test "should get new" do
    get new_plan_device_spare_part_url
    assert_response :success
  end

  test "should create plan_device_spare_part" do
    assert_difference("PlanDeviceSparePart.count") do
      post plan_device_spare_parts_url, params: {plan_device_spare_part: {plan_device_id: @plan_device_spare_part.plan_device_id, spare_part_id: @plan_device_spare_part.spare_part_id}}
    end

    assert_redirected_to plan_device_spare_part_url(PlanDeviceSparePart.last)
  end

  test "should show plan_device_spare_part" do
    get plan_device_spare_part_url(@plan_device_spare_part)
    assert_response :success
  end

  test "should get edit" do
    get edit_plan_device_spare_part_url(@plan_device_spare_part)
    assert_response :success
  end

  test "should update plan_device_spare_part" do
    patch plan_device_spare_part_url(@plan_device_spare_part), params: {plan_device_spare_part: {plan_device_id: @plan_device_spare_part.plan_device_id, spare_part_id: @plan_device_spare_part.spare_part_id}}
    assert_redirected_to plan_device_spare_part_url(@plan_device_spare_part)
  end

  test "should destroy plan_device_spare_part" do
    assert_difference("PlanDeviceSparePart.count", -1) do
      delete plan_device_spare_part_url(@plan_device_spare_part)
    end

    assert_redirected_to plan_device_spare_parts_url
  end
end
