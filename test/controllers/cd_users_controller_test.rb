require "test_helper"

class CdUsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @cd_user = cd_users(:one)
  end

  test "should get index" do
    get cd_users_url
    assert_response :success
  end

  test "should get new" do
    get new_cd_user_url
    assert_response :success
  end

  test "should create cd_user" do
    assert_difference("CdUser.count") do
      post cd_users_url, params: {cd_user: {biz_id: @cd_user.biz_id, status: @cd_user.status, username: @cd_user.username}}
    end

    assert_redirected_to cd_user_url(CdUser.last)
  end

  test "should show cd_user" do
    get cd_user_url(@cd_user)
    assert_response :success
  end

  test "should get edit" do
    get edit_cd_user_url(@cd_user)
    assert_response :success
  end

  test "should update cd_user" do
    patch cd_user_url(@cd_user), params: {cd_user: {biz_id: @cd_user.biz_id, status: @cd_user.status, username: @cd_user.username}}
    assert_redirected_to cd_user_url(@cd_user)
  end

  test "should destroy cd_user" do
    assert_difference("CdUser.count", -1) do
      delete cd_user_url(@cd_user)
    end

    assert_redirected_to cd_users_url
  end
end
