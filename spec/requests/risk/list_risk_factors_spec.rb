require 'rails_helper'

RSpec.describe "Risk::ListRiskFactors", type: :request do
  describe "GET /index" do
    it "returns http success" do
      get "/risk/list_risk_factors/index"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /show" do
    it "returns http success" do
      get "/risk/list_risk_factors/show"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /edit" do
    it "returns http success" do
      get "/risk/list_risk_factors/edit"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /new" do
    it "returns http success" do
      get "/risk/list_risk_factors/new"
      expect(response).to have_http_status(:success)
    end
  end

end
