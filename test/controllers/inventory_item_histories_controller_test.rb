require "test_helper"

class InventoryItemHistoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @inventory_item_history = inventory_item_histories(:one)
  end

  test "should get index" do
    get inventory_item_histories_url
    assert_response :success
  end

  test "should get new" do
    get new_inventory_item_history_url
    assert_response :success
  end

  test "should create inventory_item_history" do
    assert_difference("InventoryItemHistory.count") do
      post inventory_item_histories_url, params: {inventory_item_history: {biz_id: @inventory_item_history.biz_id, inventory_item_id: @inventory_item_history.inventory_item_id, qty: @inventory_item_history.qty, status: @inventory_item_history.status, user_id: @inventory_item_history.user_id}}
    end

    assert_redirected_to inventory_item_history_url(InventoryItemHistory.last)
  end

  test "should show inventory_item_history" do
    get inventory_item_history_url(@inventory_item_history)
    assert_response :success
  end

  test "should get edit" do
    get edit_inventory_item_history_url(@inventory_item_history)
    assert_response :success
  end

  test "should update inventory_item_history" do
    patch inventory_item_history_url(@inventory_item_history), params: {inventory_item_history: {biz_id: @inventory_item_history.biz_id, inventory_item_id: @inventory_item_history.inventory_item_id, qty: @inventory_item_history.qty, status: @inventory_item_history.status, user_id: @inventory_item_history.user_id}}
    assert_redirected_to inventory_item_history_url(@inventory_item_history)
  end

  test "should destroy inventory_item_history" do
    assert_difference("InventoryItemHistory.count", -1) do
      delete inventory_item_history_url(@inventory_item_history)
    end

    assert_redirected_to inventory_item_histories_url
  end
end
