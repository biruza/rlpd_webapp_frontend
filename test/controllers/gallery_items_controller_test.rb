require "test_helper"

class GalleryItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @gallery_item = gallery_items(:one)
  end

  test "should get index" do
    get gallery_items_url
    assert_response :success
  end

  test "should get new" do
    get new_gallery_item_url
    assert_response :success
  end

  test "should create gallery_item" do
    assert_difference("GalleryItem.count") do
      post gallery_items_url, params: {gallery_item: {biz_id: @gallery_item.biz_id, description_en: @gallery_item.description_en, description_th: @gallery_item.description_th, gallery_id: @gallery_item.gallery_id, image_data: @gallery_item.image_data, order_number: @gallery_item.order_number, title_en: @gallery_item.title_en, title_th: @gallery_item.title_th, url: @gallery_item.url}}
    end

    assert_redirected_to gallery_item_url(GalleryItem.last)
  end

  test "should show gallery_item" do
    get gallery_item_url(@gallery_item)
    assert_response :success
  end

  test "should get edit" do
    get edit_gallery_item_url(@gallery_item)
    assert_response :success
  end

  test "should update gallery_item" do
    patch gallery_item_url(@gallery_item), params: {gallery_item: {biz_id: @gallery_item.biz_id, description_en: @gallery_item.description_en, description_th: @gallery_item.description_th, gallery_id: @gallery_item.gallery_id, image_data: @gallery_item.image_data, order_number: @gallery_item.order_number, title_en: @gallery_item.title_en, title_th: @gallery_item.title_th, url: @gallery_item.url}}
    assert_redirected_to gallery_item_url(@gallery_item)
  end

  test "should destroy gallery_item" do
    assert_difference("GalleryItem.count", -1) do
      delete gallery_item_url(@gallery_item)
    end

    assert_redirected_to gallery_items_url
  end
end
