require "rails_helper"

RSpec.describe "egps/index", type: :view do
  before(:each) do
    assign(:egps, [
      Egp.create!(
        dep_id: "Dep",
        category: "Category",
        title: "Title",
        link: "Link",
        description: "MyText",
        status: "Status"
      ),
      Egp.create!(
        dep_id: "Dep",
        category: "Category",
        title: "Title",
        link: "Link",
        description: "MyText",
        status: "Status"
      )
    ])
  end

  it "renders a list of egps" do
    render
    assert_select "tr>td", text: "Dep".to_s, count: 2
    assert_select "tr>td", text: "Category".to_s, count: 2
    assert_select "tr>td", text: "Title".to_s, count: 2
    assert_select "tr>td", text: "Link".to_s, count: 2
    assert_select "tr>td", text: "MyText".to_s, count: 2
    assert_select "tr>td", text: "Status".to_s, count: 2
  end
end
