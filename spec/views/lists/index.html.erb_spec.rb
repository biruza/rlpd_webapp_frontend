require "rails_helper"

RSpec.describe "lists/index", type: :view do
  before(:each) do
    assign(:lists, [
      List.create!(
        title: "Title",
        order_sort: 2,
        slug: "Slug",
        board: nil,
        archived: false,
        user: nil,
        biz: nil
      ),
      List.create!(
        title: "Title",
        order_sort: 2,
        slug: "Slug",
        board: nil,
        archived: false,
        user: nil,
        biz: nil
      )
    ])
  end

  it "renders a list of lists" do
    render
    assert_select "tr>td", text: "Title".to_s, count: 2
    assert_select "tr>td", text: 2.to_s, count: 2
    assert_select "tr>td", text: "Slug".to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: false.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
  end
end
