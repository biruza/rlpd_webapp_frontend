require "test_helper"

class InputTaxesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @input_tax = input_taxes(:one)
  end

  test "should get index" do
    get input_taxes_url
    assert_response :success
  end

  test "should get new" do
    get new_input_tax_url
    assert_response :success
  end

  test "should create input_tax" do
    assert_difference("InputTax.count") do
      post input_taxes_url, params: {input_tax: {amount: @input_tax.amount, biz_id: @input_tax.biz_id, company_id: @input_tax.company_id, doc_ref: @input_tax.doc_ref, doc_ref_date: @input_tax.doc_ref_date, revenue_type_id: @input_tax.revenue_type_id, vat: @input_tax.vat}}
    end

    assert_redirected_to input_tax_url(InputTax.last)
  end

  test "should show input_tax" do
    get input_tax_url(@input_tax)
    assert_response :success
  end

  test "should get edit" do
    get edit_input_tax_url(@input_tax)
    assert_response :success
  end

  test "should update input_tax" do
    patch input_tax_url(@input_tax), params: {input_tax: {amount: @input_tax.amount, biz_id: @input_tax.biz_id, company_id: @input_tax.company_id, doc_ref: @input_tax.doc_ref, doc_ref_date: @input_tax.doc_ref_date, revenue_type_id: @input_tax.revenue_type_id, vat: @input_tax.vat}}
    assert_redirected_to input_tax_url(@input_tax)
  end

  test "should destroy input_tax" do
    assert_difference("InputTax.count", -1) do
      delete input_tax_url(@input_tax)
    end

    assert_redirected_to input_taxes_url
  end
end
