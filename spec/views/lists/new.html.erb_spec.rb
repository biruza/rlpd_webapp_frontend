require "rails_helper"

RSpec.describe "lists/new", type: :view do
  before(:each) do
    assign(:list, List.new(
      title: "MyString",
      order_sort: 1,
      slug: "MyString",
      board: nil,
      archived: false,
      user: nil,
      biz: nil
    ))
  end

  it "renders new list form" do
    render

    assert_select "form[action=?][method=?]", lists_path, "post" do
      assert_select "input[name=?]", "list[title]"

      assert_select "input[name=?]", "list[order_sort]"

      assert_select "input[name=?]", "list[slug]"

      assert_select "input[name=?]", "list[board_id]"

      assert_select "input[name=?]", "list[archived]"

      assert_select "input[name=?]", "list[user_id]"

      assert_select "input[name=?]", "list[biz_id]"
    end
  end
end
