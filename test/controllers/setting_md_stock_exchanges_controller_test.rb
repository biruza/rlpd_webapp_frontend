require "test_helper"

class SettingMdStockExchangesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @setting_md_stock_exchange = setting_md_stock_exchanges(:one)
  end

  test "should get index" do
    get setting_md_stock_exchanges_url
    assert_response :success
  end

  test "should get new" do
    get new_setting_md_stock_exchange_url
    assert_response :success
  end

  test "should create setting_md_stock_exchange" do
    assert_difference("SettingMdStockExchange.count") do
      post setting_md_stock_exchanges_url, params: {setting_md_stock_exchange: {biz_id: @setting_md_stock_exchange.biz_id, buy_doc_ref1: @setting_md_stock_exchange.buy_doc_ref1, buy_inventory_from_name_id: @setting_md_stock_exchange.buy_inventory_from_name_id, buy_inventory_to_name_id: @setting_md_stock_exchange.buy_inventory_to_name_id, sale_doc_ref1: @setting_md_stock_exchange.sale_doc_ref1, sale_inventory_from_name_id: @setting_md_stock_exchange.sale_inventory_from_name_id, sale_inventory_to_name_id: @setting_md_stock_exchange.sale_inventory_to_name_id}}
    end

    assert_redirected_to setting_md_stock_exchange_url(SettingMdStockExchange.last)
  end

  test "should show setting_md_stock_exchange" do
    get setting_md_stock_exchange_url(@setting_md_stock_exchange)
    assert_response :success
  end

  test "should get edit" do
    get edit_setting_md_stock_exchange_url(@setting_md_stock_exchange)
    assert_response :success
  end

  test "should update setting_md_stock_exchange" do
    patch setting_md_stock_exchange_url(@setting_md_stock_exchange), params: {setting_md_stock_exchange: {biz_id: @setting_md_stock_exchange.biz_id, buy_doc_ref1: @setting_md_stock_exchange.buy_doc_ref1, buy_inventory_from_name_id: @setting_md_stock_exchange.buy_inventory_from_name_id, buy_inventory_to_name_id: @setting_md_stock_exchange.buy_inventory_to_name_id, sale_doc_ref1: @setting_md_stock_exchange.sale_doc_ref1, sale_inventory_from_name_id: @setting_md_stock_exchange.sale_inventory_from_name_id, sale_inventory_to_name_id: @setting_md_stock_exchange.sale_inventory_to_name_id}}
    assert_redirected_to setting_md_stock_exchange_url(@setting_md_stock_exchange)
  end

  test "should destroy setting_md_stock_exchange" do
    assert_difference("SettingMdStockExchange.count", -1) do
      delete setting_md_stock_exchange_url(@setting_md_stock_exchange)
    end

    assert_redirected_to setting_md_stock_exchanges_url
  end
end
