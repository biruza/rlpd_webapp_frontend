require "test_helper"

class PostPhotosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @post_photo = post_photos(:one)
  end

  test "should get index" do
    get post_photos_url
    assert_response :success
  end

  test "should get new" do
    get new_post_photo_url
    assert_response :success
  end

  test "should create post_photo" do
    assert_difference("PostPhoto.count") do
      post post_photos_url, params: {post_photo: {biz_id: @post_photo.biz_id, description_en: @post_photo.description_en, description_th: @post_photo.description_th, post_id: @post_photo.post_id, slug: @post_photo.slug, slug: @post_photo.slug, title_en: @post_photo.title_en, title_th: @post_photo.title_th, user: @post_photo.user}}
    end

    assert_redirected_to post_photo_url(PostPhoto.last)
  end

  test "should show post_photo" do
    get post_photo_url(@post_photo)
    assert_response :success
  end

  test "should get edit" do
    get edit_post_photo_url(@post_photo)
    assert_response :success
  end

  test "should update post_photo" do
    patch post_photo_url(@post_photo), params: {post_photo: {biz_id: @post_photo.biz_id, description_en: @post_photo.description_en, description_th: @post_photo.description_th, post_id: @post_photo.post_id, slug: @post_photo.slug, slug: @post_photo.slug, title_en: @post_photo.title_en, title_th: @post_photo.title_th, user: @post_photo.user}}
    assert_redirected_to post_photo_url(@post_photo)
  end

  test "should destroy post_photo" do
    assert_difference("PostPhoto.count", -1) do
      delete post_photo_url(@post_photo)
    end

    assert_redirected_to post_photos_url
  end
end
