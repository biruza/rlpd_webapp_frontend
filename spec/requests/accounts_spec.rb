require "rails_helper"

RSpec.describe "Accounts", type: :request do
  describe "GET /sign_in" do
    it "returns http success" do
      get "/accounts/sign_in"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /create_session" do
    it "returns http success" do
      get "/accounts/create_session"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /sign_out" do
    it "returns http success" do
      get "/accounts/sign_out"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /delete_session" do
    it "returns http success" do
      get "/accounts/delete_session"
      expect(response).to have_http_status(:success)
    end
  end
end
