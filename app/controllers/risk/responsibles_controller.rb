class Risk::ResponsiblesController < ApplicationController
  def index
    url = URI("#{$server[@controller_title]}/api/v1/responsibles")

    @datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/responsibles/#{params["id"]}")

    @datas = request_api(url, "get", nil)
  end

  def new
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/responsibles/#{params["id"]}")

    @datas = request_api(url, "get", nil)

    # puts "plan edit = #{@plan['title']}"
  end

  def create
    params[:responsible][:user_id] = $user_id
    params[:responsible][:biz_id] = $biz_id

    url = URI("#{$server[@controller_title]}/api/v1/responsibles")
    @datas = request_api(url, "post", responsible_params)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/responsibles", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_main_url}/responsibles/new", alert: @datas["data"]
    end
  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/responsibles/#{params["id"]}")
    @datas = request_api(url, "patch", responsible_params)

    # puts "update status : #{res}"

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/responsibles", notice: @datas["message"]
    else
      redirect_to "/#{@controller_main_url}/responsibles", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/responsibles/#{params["id"]}")
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/responsibles", notice: @datas["message"]
    else
      redirect_to "/#{@controller_main_url}/responsibles", alert: @datas["data"]
    end
  end

  private

  def responsible_params
    params.require(:responsible).permit(:title, :user_id, :biz_id)
  end
end
