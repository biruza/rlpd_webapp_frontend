require "rails_helper"

RSpec.describe "Km::Kpis", type: :request do
  describe "GET /index" do
    it "returns http success" do
      get "/km/kpis/index"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /show" do
    it "returns http success" do
      get "/km/kpis/show"
      expect(response).to have_http_status(:success)
    end
  end
end
