class Kpi::ListsController < ApplicationController
  before_action :set_select, only: %i[new create edit update]

  def index
    url = URI("#{$server[@controller_title]}/api/v1/categories")
    @catagory_datas = request_api(url, "get", nil)
  end

  def show
    # url = URI("#{$server[@controller_title]}/api/v1/categories/#{params['id']}")
    # @datas = request_api(url,'get',nil)

    url = URI("#{$server[@controller_title]}/api/v1/lists/#{params["id"]}")
    @list = request_api(url, "get", nil)
  end

  def new
  end

  def create
    url = URI("#{$server[@controller_title]}/api/v1/lists")
    @datas = request_api(url, "post", list_params)

    if @datas["status"] == "Success"
      url = URI("#{$server[@controller_title]}/api/v1/categories/#{params[:list][:category_id]}")
      @category = request_api(url, "get", nil)

      redirect_to "/#{@controller_title}/lists/#{@datas["data"]["id"]}/edit?original_url=/#{@controller_title}/plans/#{@category["data"]["plan_id"]}", notice: @datas["messages"]
      # redirect_to "/#{@controller_title}/plans/#{@category['data']['plan_id']}", notice: @datas['messages']
    else
      redirect_to params[:original_url].to_s, alert: @datas["data"]
    end
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/lists/#{params["id"]}")
    @datas = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/categories/#{params["id"]}")
    @catagory_datas = request_api(url, "get", nil)
  end

  def update
=begin
    idx = 0
    params[:list][:list_kpi_owners_attributes].each do |list_kpi_owner|
      params[:list][:list_kpi_owners_attributes][idx.to_s] = nil if params[:list][:list_kpi_owners_attributes][idx.to_s][:devision_director_id].blank?
      idx += 1
    end

    idx = 0
    params[:list][:list_kpi_supporters_attributes].each do |list_kpi_supporter|
      params[:list][:list_kpi_supporters_attributes][idx.to_s] = nil if params[:list][:list_kpi_supporters_attributes][idx.to_s][:devision_director_id].blank?
      idx += 1
    end
=end
    url = URI("#{$server[@controller_title]}/api/v1/lists/#{params["id"]}")
    @datas = request_api(url, "patch", list_params)

    # puts "update status : #{res}"

    if @datas && @datas["status"] == "Success"
      # case params[:current_url]
      # when 'plan_reports'
      redirect_to "#{params[:original_url]}?plan_report_id=#{begin
        params[:list][:plan_report_id]
      rescue
        nil
      end}", notice: @datas["message"]
      # else
      # redirect_to "/#{@controller_title}/categories/#{@datas['data']['category_id']}/edit", notice: @datas['message']
      # end

    else
      redirect_to "/#{@controller_title}/lists/#{params[:id]}/edit", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/lists/#{params["id"]}")
    @datas_ori = request_api(url, "get", nil)
    @datas = request_api(url, "delete", nil)

    url = URI("#{$server[@controller_title]}/api/v1/categories/#{@datas_ori["data"]["category_id"]}")
    @category = request_api(url, "get", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/plans/#{@category["data"]["plan_id"]}", notice: @datas["message"]
    else
      redirect_to "/#{@controller_title}/plans/#{@category["data"]["plan_id"]}", alert: t(:can_not_delete)
    end
  end

  def set_select
    url = URI("#{$server[@controller_title]}/api/v1/activities")
    @data_activities = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/list_types")
    @data_list_types = request_api(url, "get", nil)

    url = URI("#{$server[@controller_title]}/api/v1/devision_directors")
    @data_devision_directors = request_api(url, "get", nil)

  end

  def list_params
    # params.require(:list).permit( :status, :slug, :order_sort, :category, :activity, :user, :biz)
    params.require(:list).permit(:status, :title, :description, :slug, :order_sort, :category_id, :activity_id, :list_target_id, :user_id, :biz_id, :list_type_id,
      list_type: [:id],
      list_activities_attributes: [:id, :_destroy, :title, :slug, :order_sort, :list_id, :user_id, :biz_id],
      list_kpi_full_years_attributes: [:id, :_destroy, :title, :kpi_value, :kpi_value_unit, :slug, :order_sort, :list_id, :user_id, :biz_id],
      list_kpi_half_years_attributes: [:id, :_destroy, :title, :kpi_value, :kpi_value_unit, :slug, :order_sort, :list_id, :user_id, :biz_id],
      list_scores_attributes: [:id, :_destroy, :title, :title_first, :score_first, :unit_first, :title_second, :score_second, :unit_second, :title_third, :score_third, :unit_third, :title_fourth, :score_fourth, :unit_fourth, :title_fifth, :score_fifth, :unit_fifth, :slug, :order_sort, :list_id, :user_id, :biz_id],
      list_attachments: [:id, :_destroy, :image, :image_data_uri, :description, :slug, :list_id, :user_id, :biz_id],
      list_data_sources_attributes: [:id, :_destroy, :title, :slug, :order_sort, :list_id, :user_id, :biz_id],
      list_kpi_owners_attributes: [:id, :_destroy, :title, :description, :slug, :order_sort, :list_id, :devision_director_id, :user_id, :biz_id],
      list_kpi_supporters_attributes: [:id, :_destroy, :title, :description, :slug, :order_sort, :list_id, :devision_director_id, :user_id, :biz_id],
      list_kpi_persons_attributes: [:id, :_destroy, :title, :phone, :slug, :order_sort, :list_id, :user_id, :biz_id],
      list_kpi_onlines_attributes: [:id, :_destroy, :title, :phone, :slug, :order_sort, :list_id, :user_id, :biz_id],
      list_remarks_attributes: [:id, :_destroy, :title, :slug, :order_sort, :list_id, :user_id, :biz_id],
      list_formulas_attributes: [:id, :_destroy, :title, :image, :image_data_uri, :image_data, :slug, :order_sort, :list_id, :user_id, :biz_id],
      )
  end
end
