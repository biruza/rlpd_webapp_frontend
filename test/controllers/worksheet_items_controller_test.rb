require "test_helper"

class WorksheetItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @worksheet_item = worksheet_items(:one)
  end

  test "should get index" do
    get worksheet_items_url
    assert_response :success
  end

  test "should get new" do
    get new_worksheet_item_url
    assert_response :success
  end

  test "should create worksheet_item" do
    assert_difference("WorksheetItem.count") do
      post worksheet_items_url, params: {worksheet_item: {order: @worksheet_item.order, title: @worksheet_item.title, value: @worksheet_item.value, worksheet_id: @worksheet_item.worksheet_id}}
    end

    assert_redirected_to worksheet_item_url(WorksheetItem.last)
  end

  test "should show worksheet_item" do
    get worksheet_item_url(@worksheet_item)
    assert_response :success
  end

  test "should get edit" do
    get edit_worksheet_item_url(@worksheet_item)
    assert_response :success
  end

  test "should update worksheet_item" do
    patch worksheet_item_url(@worksheet_item), params: {worksheet_item: {order: @worksheet_item.order, title: @worksheet_item.title, value: @worksheet_item.value, worksheet_id: @worksheet_item.worksheet_id}}
    assert_redirected_to worksheet_item_url(@worksheet_item)
  end

  test "should destroy worksheet_item" do
    assert_difference("WorksheetItem.count", -1) do
      delete worksheet_item_url(@worksheet_item)
    end

    assert_redirected_to worksheet_items_url
  end
end
