class Risk::Control::ListsController < ApplicationController
  before_action :set_select, only: %i[new create edit update]
  before_action :set_default, only: %i[index show new edit create update destroy print]

  def index
    url = URI("#{$server[@controller_title]}/api/v1/categories")
    @catagory_datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/categories/#{params["id"]}")
    @datas = request_api(url, "get", nil)
  end

  def new
  end

  def create
    url = URI("#{$server[@controller_title]}/api/v1/lists")
    @datas = request_api(url, "post", list_params)

    if @datas["status"] == "Success"
      url = URI("#{$server[@controller_title]}/api/v1/categories/#{params[:list][:category_id]}")
      @category = request_api(url, "get", nil)

      redirect_to "/#{@controller_main_url}/lists/#{@datas["data"]["id"]}/edit?original_url=/#{@controller_main_url}/plans/#{@category["data"]["plan_id"]}", notice: @datas["messages"]
      
      #redirect_to "/#{@controller_main_url}/plans/#{@category["data"]["plan_id"]}", notice: @datas["messages"]


      # redirect_to "/#{@controller_main_url}/plans/#{@category['data']['plan_id']}", notice: @datas['messages']
    else
      redirect_to params[:original_url].to_s, alert: @datas["data"]
    end
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/lists/#{params["id"]}")
    @datas = request_api(url, "get", nil)
    url = URI("#{$server[@controller_title]}/api/v1/categories/#{params["id"]}")
    @catagory_datas = request_api(url, "get", nil)
    url = URI("#{$server[@controller_title]}/api/v1/risk_types")
    @risk_type_datas = request_api(url, "get", nil)
  end

  def update
    #       idx = 0
    #       if !params[:list][:list_objectives_attributes].blank?
    #         params[:list][:list_objectives_attributes].each do |item|
    #           if params[:list][:list_objectives_attributes]["#{idx}"]['title'].blank?
    #             params[:list][:list_objectives_attributes]["#{idx}"] = nil
    #
    #           else
    #             params[:list][:list_objectives_attributes]["#{idx}"]['user_id'] = $user[session.id.to_s][@controller_title]['id']
    #             params[:list][:list_objectives_attributes]["#{idx}"]['biz_id'] = $user[session.id.to_s][@controller_title]['biz_id']
    #           end
    #           idx = idx + 1
    #         end
    #       end
    #
    #
    #
    #       idx = 0
    #       if !params[:list][:list_risks_attributes].blank?
    #         params[:list][:list_risks_attributes].each do |item|
    #           if params[:list][:list_risks_attributes]["#{idx}"]['title'].blank?
    #             params[:list][:list_risks_attributes]["#{idx}"] = nil
    #
    #           else
    #             params[:list][:list_risks_attributes]["#{idx}"]['user_id'] = $user[session.id.to_s][@controller_title]['id']
    #             params[:list][:list_risks_attributes]["#{idx}"]['biz_id'] = $user[session.id.to_s][@controller_title]['biz_id']
    #           end
    #           idx = idx + 1
    #         end
    #       end
    #
    #
    #
=begin
           idx = 0
           if !params[:list][:list_risk_factors_attributes].blank?
             params[:list][:list_risk_factors_attributes].each do |item|
               if params[:list][:list_risk_factors_attributes]["#{idx}"]['title'].blank?
                 params[:list][:list_risk_factors_attributes]["#{idx}"] = nil
    
               else
                params[:list][:list_risk_factors_attributes]["#{idx}"]['list_risk_id'] = params[:list][:list_risks_attributes]["0"]['id']
                 #params[:list][:list_risk_factors_attributes]["#{idx}"]['user_id'] = $user[session.id.to_s][@controller_title]['id']
                 #params[:list][:list_risk_factors_attributes]["#{idx}"]['biz_id'] = $user[session.id.to_s][@controller_title]['biz_id']
                 p '=============='
                 p  params[:list][:list_risk_factors_attributes]["#{idx}"]['list_risk_id'] 
                 p '=============='
               end
               idx = idx + 1
             end
           end
=end
    #
    #
    #       idx = 0
    #       if !params[:list][:list_activity_controls_attributes].blank?
    #         params[:list][:list_activity_controls_attributes].each do |item|
    #           if params[:list][:list_activity_controls_attributes]["#{idx}"]['title'].blank?
    #             params[:list][:list_activity_controls_attributes]["#{idx}"] = nil
    #
    #           else
    #             params[:list][:list_activity_controls_attributes]["#{idx}"]['user_id'] = $user[session.id.to_s][@controller_title]['id']
    #             params[:list][:list_activity_controls_attributes]["#{idx}"]['biz_id'] = $user[session.id.to_s][@controller_title]['biz_id']
    #           end
    #           idx = idx + 1
    #         end
    #       end
    # =    url = URI("#{$server[@controller_title]}/api/v1/lists/#{params["id"]}")
    url = URI("#{$server[@controller_title]}/api/v1/lists/#{params["id"]}")
    @list = request_api(url, "get", nil)
    
    url_category = URI("#{$server[@controller_title]}/api/v1/categories/#{@list["data"]["category_id"]}")
    @category = request_api(url_category, "get", nil)

    @datas = request_api(url, "patch", list_params)

    if @datas && @datas["status"] == "Success"
      if !params[:original_url].blank?
        redirect_to params[:original_url], notice: @datas["messages"]
      else
        redirect_to "/#{@controller_main_url}/plans/#{@category["data"]["plan_id"]}", notice: @datas["message"]
      end
    else
      #redirect_to "/#{@controller_main_url}/plans/#{@category["data"]["plan_id"]}", notice: @datas["message"]
      redirect_to "/#{@controller_main_url}/lists/#{params[:id]}/edit", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/lists/#{params["id"]}")
    @datas_ori = request_api(url, "get", nil)
    @datas = request_api(url, "delete", nil)

    url = URI("#{$server[@controller_title]}/api/v1/categories/#{@datas_ori["data"]["category_id"]}")
    @category = request_api(url, "get", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/plans/#{@category["data"]["plan_id"]}", notice: @datas["message"]
    else
      redirect_to "/#{@controller_main_url}/plans/#{@category["data"]["plan_id"]}", alert: @datas["data"]
    end
  end

  private

  def set_select
    # url = URI("#{$server[@controller_title]}/api/v1/activities")
    # @data_activities = request_api(url,'get',nil)
  end

  def set_default
    @sub_system = "control"
    @controller_main_url = "#{@controller_title}/#{@sub_system}"
    # @controller_main_url = @controller_title
  end

  def list_params
    # params.require(:list).permit( :status, :slug, :order_sort, :category, :activity, :user, :biz)
    params.require(:list).permit(:activity_no, :title, :description, :status, :slug, :order_sort, :category_id, :activity_id, :user_id, :biz_id,
      list_activities_attributes: [:id, :_destroy, :status, :title, :description, :slug, :order_sort, :list_id, :user_id, :biz_id],
      list_objectives_attributes: [:id, :_destroy, :status, :title, :description, :slug, :order_sort, :list_id, :user_id, :biz_id],
      list_risks_attributes: [:id, :_destroy, :status, :title, :description, :slug, :order_sort, :list_id, :user_id, :biz_id],
      list_risk_factors_attributes: [:id, :_destroy, :status, :title, :activity_no, :risk_chance, :risk_effect, :risk_score, :description, :slug, :order_sort, :list_id, :user_id, :biz_id],
      list_activity_controls_attributes: [:id, :_destroy, :status, :title, :description, :slug, :order_sort, :list_id, :user_id, :biz_id],
      list_risk_types_attributes: [:id, :_destroy, :risk_type_id, :slug, :order_sort, :list_id, :user_id, :biz_id],
      tasks_attributes: [:id, :_destroy, :title, :risk_chance, :risk_effect, :risk_score, :order_sort, :plan_report_id, :category_id, :list_id, :list_activity_id, :user_id, :biz_id])
  end
end
