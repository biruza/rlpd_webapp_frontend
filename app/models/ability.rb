class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)

    if user.superadmin_role?
      can :manage, :all

    elsif user.administrator_role?
      # basic
      can :manage, User, biz_id: user.biz_id
      can :create, Biz
      can :update, Biz, id: user.biz_id
      can :read, Biz, id: user.biz_id
      ####

      # model
      can :manage, Location, user_id: user.id
      ####

      # controller only
      can [:report1], :report
    ####

    elsif user.user_role?
      # basic
      can :read, User, id: user.id
      can :update, User, id: user.id
      can :create, Biz
      ####

      # model
      can :manage, Location, user_id: user.id
      ####

      # controller only
      can [:report1], :report
    ####

    else
      can [:index], :dashboard

      can :manage, Location

    end

    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
