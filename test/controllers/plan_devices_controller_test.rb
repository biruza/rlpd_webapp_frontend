require "test_helper"

class PlanDevicesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @plan_device = plan_devices(:one)
  end

  test "should get index" do
    get plan_devices_url
    assert_response :success
  end

  test "should get new" do
    get new_plan_device_url
    assert_response :success
  end

  test "should create plan_device" do
    assert_difference("PlanDevice.count") do
      post plan_devices_url, params: {plan_device: {device_id: @plan_device.device_id, expense_1: @plan_device.expense_1, expense_2: @plan_device.expense_2, expense_3: @plan_device.expense_3, expense_4: @plan_device.expense_4, expense_total: @plan_device.expense_total, plan_id: @plan_device.plan_id, working_time_min: @plan_device.working_time_min}}
    end

    assert_redirected_to plan_device_url(PlanDevice.last)
  end

  test "should show plan_device" do
    get plan_device_url(@plan_device)
    assert_response :success
  end

  test "should get edit" do
    get edit_plan_device_url(@plan_device)
    assert_response :success
  end

  test "should update plan_device" do
    patch plan_device_url(@plan_device), params: {plan_device: {device_id: @plan_device.device_id, expense_1: @plan_device.expense_1, expense_2: @plan_device.expense_2, expense_3: @plan_device.expense_3, expense_4: @plan_device.expense_4, expense_total: @plan_device.expense_total, plan_id: @plan_device.plan_id, working_time_min: @plan_device.working_time_min}}
    assert_redirected_to plan_device_url(@plan_device)
  end

  test "should destroy plan_device" do
    assert_difference("PlanDevice.count", -1) do
      delete plan_device_url(@plan_device)
    end

    assert_redirected_to plan_devices_url
  end
end
