require "test_helper"

class ProductTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @product_type = product_types(:one)
  end

  test "should get index" do
    get product_types_url
    assert_response :success
  end

  test "should get new" do
    get new_product_type_url
    assert_response :success
  end

  test "should create product_type" do
    assert_difference("ProductType.count") do
      post product_types_url, params: {product_type: {biz_id: @product_type.biz_id, description_th: @product_type.description_th, image_data: @product_type.image_data, product_category_id: @product_type.product_category_id, slug: @product_type.slug, title_th: @product_type.title_th}}
    end

    assert_redirected_to product_type_url(ProductType.last)
  end

  test "should show product_type" do
    get product_type_url(@product_type)
    assert_response :success
  end

  test "should get edit" do
    get edit_product_type_url(@product_type)
    assert_response :success
  end

  test "should update product_type" do
    patch product_type_url(@product_type), params: {product_type: {biz_id: @product_type.biz_id, description_th: @product_type.description_th, image_data: @product_type.image_data, product_category_id: @product_type.product_category_id, slug: @product_type.slug, title_th: @product_type.title_th}}
    assert_redirected_to product_type_url(@product_type)
  end

  test "should destroy product_type" do
    assert_difference("ProductType.count", -1) do
      delete product_type_url(@product_type)
    end

    assert_redirected_to product_types_url
  end
end
