require "test_helper"

class StudentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @student = students(:one)
  end

  test "should get index" do
    get students_url
    assert_response :success
  end

  test "should get new" do
    get new_student_url
    assert_response :success
  end

  test "should create student" do
    assert_difference("Student.count") do
      post students_url, params: {student: {address1: @student.address1, address2: @student.address2, address_father: @student.address_father, address_mother: @student.address_mother, address_parent: @student.address_parent, birth: @student.birth, birth_lacation: @student.birth_lacation, blood: @student.blood, cdisease: @student.cdisease, chumclass: @student.chumclass, city1: @student.city1, city2: @student.city2, city_father: @student.city_father, city_mother: @student.city_mother, city_parent: @student.city_parent, datestart: @student.datestart, email: @student.email, email_father: @student.email_father, email_mother: @student.email_mother, email_parent: @student.email_parent, finisholdschool: @student.finisholdschool, firstname: @student.firstname, firstname_father: @student.firstname_father, firstname_mother: @student.firstname_mother, firstname_parent: @student.firstname_parent, grownoldschool: @student.grownoldschool, height: @student.height, image_data: @student.image_data, job_father: @student.job_father, job_location_father: @student.job_location_father, job_location_mother: @student.job_location_mother, job_location_parent: @student.job_location_parent, job_mother: @student.job_mother, job_parent: @student.job_parent, job_phone_father: @student.job_phone_father, job_phone_mother: @student.job_phone_mother, job_phone_parent: @student.job_phone_parent, lastname: @student.lastname, lastname_father: @student.lastname_father, lastname_mother: @student.lastname_mother, lastname_parent: @student.lastname_parent, mobile: @student.mobile, mobile_father: @student.mobile_father, mobile_mother: @student.mobile_mother, mobile_parent: @student.mobile_parent, nation: @student.nation, nation_father: @student.nation_father, nation_mother: @student.nation_mother, nation_parent: @student.nation_parent, nickname: @student.nickname, number_elder: @student.number_elder, number_you: @student.number_you, number_younger: @student.number_younger, oldschool: @student.oldschool, orgin_father: @student.orgin_father, orgin_mother: @student.orgin_mother, orgin_parent: @student.orgin_parent, origin: @student.origin, percode: @student.percode, percode_father: @student.percode_father, percode_mother: @student.percode_mother, percode_parent: @student.percode_parent, phone: @student.phone, phone_father: @student.phone_father, phone_mother: @student.phone_mother, phone_parent: @student.phone_parent, prefix: @student.prefix, prefix_father: @student.prefix_father, prefix_mother: @student.prefix_mother, prefix_parent: @student.prefix_parent, province1: @student.province1, province2: @student.province2, province_father: @student.province_father, province_mother: @student.province_mother, province_parent: @student.province_parent, race: @student.race, race_father: @student.race_father, race_mother: @student.race_mother, race_parent: @student.race_parent, relation: @student.relation, s_code: @student.s_code, s_number: @student.s_number, salary_father: @student.salary_father, salary_mother: @student.salary_mother, salary_parent: @student.salary_parent, startclass: @student.startclass, status: @student.status, talent: @student.talent, teacher_id: @student.teacher_id, termstart: @student.termstart, weight: @student.weight, zipcode1: @student.zipcode1, zipcode2: @student.zipcode2, zipcode_father: @student.zipcode_father, zipcode_mother: @student.zipcode_mother, zipcode_parent: @student.zipcode_parent}}
    end

    assert_redirected_to student_url(Student.last)
  end

  test "should show student" do
    get student_url(@student)
    assert_response :success
  end

  test "should get edit" do
    get edit_student_url(@student)
    assert_response :success
  end

  test "should update student" do
    patch student_url(@student), params: {student: {address1: @student.address1, address2: @student.address2, address_father: @student.address_father, address_mother: @student.address_mother, address_parent: @student.address_parent, birth: @student.birth, birth_lacation: @student.birth_lacation, blood: @student.blood, cdisease: @student.cdisease, chumclass: @student.chumclass, city1: @student.city1, city2: @student.city2, city_father: @student.city_father, city_mother: @student.city_mother, city_parent: @student.city_parent, datestart: @student.datestart, email: @student.email, email_father: @student.email_father, email_mother: @student.email_mother, email_parent: @student.email_parent, finisholdschool: @student.finisholdschool, firstname: @student.firstname, firstname_father: @student.firstname_father, firstname_mother: @student.firstname_mother, firstname_parent: @student.firstname_parent, grownoldschool: @student.grownoldschool, height: @student.height, image_data: @student.image_data, job_father: @student.job_father, job_location_father: @student.job_location_father, job_location_mother: @student.job_location_mother, job_location_parent: @student.job_location_parent, job_mother: @student.job_mother, job_parent: @student.job_parent, job_phone_father: @student.job_phone_father, job_phone_mother: @student.job_phone_mother, job_phone_parent: @student.job_phone_parent, lastname: @student.lastname, lastname_father: @student.lastname_father, lastname_mother: @student.lastname_mother, lastname_parent: @student.lastname_parent, mobile: @student.mobile, mobile_father: @student.mobile_father, mobile_mother: @student.mobile_mother, mobile_parent: @student.mobile_parent, nation: @student.nation, nation_father: @student.nation_father, nation_mother: @student.nation_mother, nation_parent: @student.nation_parent, nickname: @student.nickname, number_elder: @student.number_elder, number_you: @student.number_you, number_younger: @student.number_younger, oldschool: @student.oldschool, orgin_father: @student.orgin_father, orgin_mother: @student.orgin_mother, orgin_parent: @student.orgin_parent, origin: @student.origin, percode: @student.percode, percode_father: @student.percode_father, percode_mother: @student.percode_mother, percode_parent: @student.percode_parent, phone: @student.phone, phone_father: @student.phone_father, phone_mother: @student.phone_mother, phone_parent: @student.phone_parent, prefix: @student.prefix, prefix_father: @student.prefix_father, prefix_mother: @student.prefix_mother, prefix_parent: @student.prefix_parent, province1: @student.province1, province2: @student.province2, province_father: @student.province_father, province_mother: @student.province_mother, province_parent: @student.province_parent, race: @student.race, race_father: @student.race_father, race_mother: @student.race_mother, race_parent: @student.race_parent, relation: @student.relation, s_code: @student.s_code, s_number: @student.s_number, salary_father: @student.salary_father, salary_mother: @student.salary_mother, salary_parent: @student.salary_parent, startclass: @student.startclass, status: @student.status, talent: @student.talent, teacher_id: @student.teacher_id, termstart: @student.termstart, weight: @student.weight, zipcode1: @student.zipcode1, zipcode2: @student.zipcode2, zipcode_father: @student.zipcode_father, zipcode_mother: @student.zipcode_mother, zipcode_parent: @student.zipcode_parent}}
    assert_redirected_to student_url(@student)
  end

  test "should destroy student" do
    assert_difference("Student.count", -1) do
      delete student_url(@student)
    end

    assert_redirected_to students_url
  end
end
