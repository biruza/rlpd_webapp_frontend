class Pmqa::CategoryMasterListsController < ApplicationController
  def index
    url = URI("#{$server[@controller_title]}/api/v1/category_master_lists")

    @datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/category_master_lists/#{params["id"]}")

    @datas = request_api(url, "get", nil)
  end

  def new
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/category_master_lists/#{params["id"]}")

    @datas = request_api(url, "get", nil)

    # puts "plan edit = #{@plan['title']}"
  end

  def create
    params[:category_master_list][:user_id] = $user_id
    params[:category_master_list][:biz_id] = $biz_id

    url = URI("#{$server[@controller_title]}/api/v1/category_master_lists")
    @datas = request_api(url, "post", category_master_list_params)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/category_masters", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_title}/category_master_lists/new", alert: @datas["data"]
    end
  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/category_master_lists/#{params["id"]}")
    @datas = request_api(url, "patch", category_master_list_params)

    # puts "update status : #{res}"

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/category_masters", notice: @datas["message"]
    else
      redirect_to "/#{@controller_title}/category_master_lists", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/category_master_lists/#{params["id"]}")
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/category_masters", notice: @datas["message"]
    else
      redirect_to "/#{@controller_title}/category_master_lists", alert: @datas["data"]
    end
  end

  private

  def category_master_list_params
    params.require(:category_master_list).permit(:order_sort, :title, :user_id, :biz_id, :category_master_id)
  end
end
