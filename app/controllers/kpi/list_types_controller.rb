class Kpi::ListTypesController < ApplicationController
  def index
    url = URI("#{$server[@controller_title]}/api/v1/list_types")

    @datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/list_types/#{params["id"]}")

    @datas = request_api(url, "get", nil)
  end

  def new
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/list_types/#{params["id"]}")

    @datas = request_api(url, "get", nil)

    # puts "plan edit = #{@plan['title']}"
  end

  def create
    params[:list_type][:user_id] = $user_id
    params[:list_type][:biz_id] = $biz_id

    url = URI("#{$server[@controller_title]}/api/v1/list_types")
    @datas = request_api(url, "post", list_type_params)

    if @datas["status"] == "Success"
      redirect_to "/kpi/list_types", notice: @datas["messages"]
    else
      redirect_to "/kpi/list_types/new", alert: @datas["data"]
    end
  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/list_types/#{params["id"]}")
    @datas = request_api(url, "patch", list_type_params)

    # puts "update status : #{res}"

    if @datas["status"] == "Success"
      redirect_to "/kpi/list_types", notice: @datas["message"]
    else
      redirect_to "/kpi/list_types", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/list_types/#{params["id"]}")
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
      redirect_to "/kpi/list_types", notice: @datas["message"]
    else
      redirect_to "/kpi/list_types", alert: @datas["data"]
    end
  end

  private

  def list_type_params
    params.require(:list_type).permit(:title, :order_sort, :user_id, :biz_id)
  end
end
