require "test_helper"

class CheckinStudentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @checkin_student = checkin_students(:one)
  end

  test "should get index" do
    get checkin_students_url
    assert_response :success
  end

  test "should get new" do
    get new_checkin_student_url
    assert_response :success
  end

  test "should create checkin_student" do
    assert_difference("CheckinStudent.count") do
      post checkin_students_url, params: {checkin_student: {date_action: @checkin_student.date_action, student_code: @checkin_student.student_code, user_id: @checkin_student.user_id}}
    end

    assert_redirected_to checkin_student_url(CheckinStudent.last)
  end

  test "should show checkin_student" do
    get checkin_student_url(@checkin_student)
    assert_response :success
  end

  test "should get edit" do
    get edit_checkin_student_url(@checkin_student)
    assert_response :success
  end

  test "should update checkin_student" do
    patch checkin_student_url(@checkin_student), params: {checkin_student: {date_action: @checkin_student.date_action, student_code: @checkin_student.student_code, user_id: @checkin_student.user_id}}
    assert_redirected_to checkin_student_url(@checkin_student)
  end

  test "should destroy checkin_student" do
    assert_difference("CheckinStudent.count", -1) do
      delete checkin_student_url(@checkin_student)
    end

    assert_redirected_to checkin_students_url
  end
end
