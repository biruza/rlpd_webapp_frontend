require "rails_helper"

RSpec.describe "egp_configs/index", type: :view do
  before(:each) do
    assign(:egp_configs, [
      EgpConfig.create!(
        tags: "Tags",
        categories: "Categories",
        biz: nil
      ),
      EgpConfig.create!(
        tags: "Tags",
        categories: "Categories",
        biz: nil
      )
    ])
  end

  it "renders a list of egp_configs" do
    render
    assert_select "tr>td", text: "Tags".to_s, count: 2
    assert_select "tr>td", text: "Categories".to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
  end
end
