class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
    :recoverable, :rememberable, :trackable, :validatable

  belongs_to :biz, optional: true

  has_many :board_users

  has_many :notifications, as: :recipient

  include ImageUploader::Attachment.new(:avatar)

  has_ancestry
  # has_ancestry(primary_key_format: /\A[\w\-]+(\/[\w\-]+)*\z/)

  def self.search(search)
    if search
      where("firstname LIKE ?", "%#{search}%")
    else
      all
    end
  end

  def title
    if firstname.blank? || lastname.blank?
      email.to_s
    else
      "#{firstname} #{lastname}"
    end
  end

  protected

  # I disable this method because I don't use the confirmable Devise module
  def confirmation_required?
    false
  end
end
