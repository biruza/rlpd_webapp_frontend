document.addEventListener("DOMContentLoaded", function() {
    var elements = document.getElementsByTagName("INPUT");
    for (var i = 0; i < elements.length; i++) {
        elements[i].oninvalid = function(e) {
            e.target.setCustomValidity("");
            if (!e.target.validity.valid) {
                e.target.setCustomValidity("ต้องไม่เว้นว่างเอาไว้");
            }
            if (e.target.validity.rangeUnderflow) {
                e.target.setCustomValidity("ต้องมีค่ามากกว่าหรือเท่ากับ " + e.target.min);
            }
            if (e.target.validity.rangeOverflow) {
                e.target.setCustomValidity("ต้องมีค่าน้อยกว่าหรือเท่ากับ " + e.target.max);
            }
        };
        elements[i].oninput = function(e) {
            e.target.setCustomValidity("");
        };
    }



    var elements = document.querySelectorAll("input[type=email]");
    for (var i = 0; i < elements.length; i++) {
        elements[i].oninvalid = function(e) {
            e.target.setCustomValidity("");
            if (e.target.validity.typeMismatch) {
                e.target.setCustomValidity("คุณป้อนที่อยู่อีเมลที่ไม่ถูกต้อง");
            }
        };
        elements[i].oninput = function(e) {
            e.target.setCustomValidity("");
        };
    }  





    var elements = document.getElementsByTagName("TEXTAREA");
    for (var i = 0; i < elements.length; i++) {
        elements[i].oninvalid = function(e) {
            e.target.setCustomValidity("");
            if (!e.target.validity.valid) {
                e.target.setCustomValidity("ต้องไม่เว้นว่างเอาไว้");
            }
        };
        elements[i].oninput = function(e) {
            e.target.setCustomValidity("");
        };
    }

    var elements = document.getElementsByTagName("SELECT");
    for (var i = 0; i < elements.length; i++) {
        elements[i].oninvalid = function(e) {
            e.target.setCustomValidity("");
            if (!e.target.validity.valid) {
                e.target.setCustomValidity("ต้องไม่เว้นว่างเอาไว้");
            }
        };
        elements[i].oninput = function(e) {
            e.target.setCustomValidity("");
        };
    }
});
