class Ahoy::Store < Ahoy::DatabaseStore
end

# set to true for JavaScript tracking
Ahoy.api = false

# set to true for geocoding (and add the geocoder gem to your Gemfile)
# we recommend configuring local geocoding as well
# see https://github.com/ankane/ahoy#geocoding
Ahoy.geocode = false
Ahoy::Visit.where("started_at < ?", 2.years.ago).find_in_batches do |visits|
    visit_ids = visits.map(&:id)
    Ahoy::Event.where(visit_id: visit_ids).delete_all
    Ahoy::Visit.where(id: visit_ids).delete_all
  end