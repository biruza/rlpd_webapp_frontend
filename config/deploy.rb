set :repo_url, "git@bitbucket.org:biruza/rlpd_webapp_frontend.git"
set :deploy_to, "/home/rlpd/apps/rlpd_webapp_frontend"

append :linked_files, "config/database.yml"

append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system", "public/uploads"

set :sudo, "sudo -u rlpd -i"

# Capistrano::rbenv
set :rbenv_path, "/home/rlpd/.rbenv"
set :rbenv_ruby, File.read(".ruby-version").strip
set :rbenv_type, :user

set :keep_releases, 5
# Capistrano::Rails
set :rails_env, "production"

namespace :nginx do
  task :status do
    on roles(:app) do
      execute :sudo, :systemctl, :status, "nginx"
    end
  end
  task :start do
    on roles(:app) do
      execute :sudo, :systemctl, :start, "nginx"
    end
  end
  task :stop do
    on roles(:app) do
      execute :sudo, :systemctl, :stop, "nginx"
    end
  end
  task :restart do
    on roles(:app) do
      execute :sudo, :systemctl, :restart, "nginx"
    end
  end
end

namespace :deploy do
  desc "Config bundler"
  task :config_bundler do
    on roles(/.*/) do
      #execute :bundle, "config", "--global silence_root_warning true"
      execute :bundle, "config", "--local deployment", true
      execute :bundle, "config", "--local", "without", "development:test"
      execute :bundle, "config", "--local", "path", shared_path.join("bundle")
    end
  end
end

before "bundler:install", "deploy:config_bundler"

namespace :deploy do
  after :publishing, "nginx:restart"
end
