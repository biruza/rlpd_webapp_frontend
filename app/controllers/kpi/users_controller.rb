class Kpi::UsersController < ApplicationController
  before_action :set_user, only: %i[show edit update destroy]
  before_action :set_select, only: %i[new create edit update]

  def index
    url = URI("#{$server[@controller_title]}/api/v1/users")
    @datas = request_api(url, "get", nil)
  end

  def show
  end

  def new
  end

  def create
    params[:user][:biz_id] = $user[session.id.to_s]["biz_id"]

    url = URI("#{$server[@controller_title]}/api/v1/users_create")
    @datas = request_api(url, "post", user_params)

    if @datas && @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/users", notice: @datas["message"]
    else
      redirect_to "/#{@controller_title}/users", alert: @datas["data"]
    end
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/users/#{params["id"]}")
    @datas = request_api(url, "get", nil)
  end

  def update
    params[:user][:superadmin_role] = true if params[:user][:superadmin_role] == "true"
    params[:user][:superadmin_role] = false if params[:user][:superadmin_role] == "false"

    params[:user][:administrator_role] = true if params[:user][:administrator_role] == "true"
    params[:user][:administrator_role] = false if params[:user][:administrator_role] == "false"

    params[:user][:user_role] = true if params[:user][:user_role] == "true"
    params[:user][:user_role] = false if params[:user][:user_role] == "false"

    params[:user][:is_active] = true if params[:user][:is_active] == "true"
    params[:user][:is_active] = false if params[:user][:is_active] == "false"

    url = URI("#{$server[@controller_title]}/api/v1/users/#{params["id"]}")
    @datas = request_api(url, "patch", user_params)

    if @datas && @datas["status"] == "Success"
      redirect_to "/#{@controller_title}/users", notice: @datas["message"]
    else
      redirect_to "/#{@controller_title}/users", alert: @datas["data"]
    end
  end

  private

  def set_user
    url = URI("#{$server[@controller_title]}/api/v1/users/#{params["id"]}")
    @datas = request_api(url, "get", nil)
  end

  def set_select
    url_dep = URI("#{$server[@controller_title]}/api/v1/departments")
    @department_datas = request_api(url_dep, "get", nil)

    url_ins = URI("#{$server[@controller_title]}/api/v1/institutions")
    @institution_datas = request_api(url_ins, "get", nil)
  end

  def user_params
    params.require(:user).permit(:file_data, :file, :file_data_uri, :registration_role, :is_active, :description, :institution_id, :superadmin_role, :administrator_role, :user_role, :biz_id, :prefix, :percode, :nation, :race, :origin, :phone, :mobile, :address, :city, :amphoe, :district, :province, :zipcode, :job, :job_location, :job_phone, :salary, :firstname, :lastname, :email, :avatar, :password, :password_confirmation, :parent_id, :department_id, :position)
    
  end
end
