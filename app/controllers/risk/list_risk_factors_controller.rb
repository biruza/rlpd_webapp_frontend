class Risk::ListRiskFactorsController < ApplicationController
  def index
    url = URI("#{$server[@controller_title]}/api/v1/list_risk_factors")
    @datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/list_risk_factors/#{params["id"]}")
    @datas = request_api(url, "get", nil)
  end

  def new
  end

  def edit
  end

  def create
    params[:list_risk_factor][:user_id] = $user_id
    params[:list_risk_factor][:biz_id] = $biz_id

    url = URI("#{$server[@controller_title]}/api/v1/list_risk_factors")
    @datas = request_api(url, "post", list_risk_factor_params)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/list_risk_factors", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_main_url}/list_risk_factors/new", alert: @datas["data"]
    end

  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/list_risk_factors/#{params["id"]}")
    @datas = request_api(url, "patch", list_risk_factor_params)

    # puts "update status : #{res}"

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/list_risk_factors", notice: @datas["message"]
    else
      redirect_to "/#{@controller_main_url}/list_risk_factors", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/list_risk_factors/#{params["id"]}")
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/list_risk_factors", notice: @datas["message"]
    else
      redirect_to "/#{@controller_main_url}/list_risk_factors", alert: @datas["data"]
    end
  end

  private

  def list_risk_factor_params
    params.require(:list_risk_factor).permit(:title, :slug, :order_sort, :list_id, :user_id, :biz_id)
  end


end
