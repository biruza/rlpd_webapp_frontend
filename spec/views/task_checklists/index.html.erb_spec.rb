require "rails_helper"

RSpec.describe "task_checklists/index", type: :view do
  before(:each) do
    assign(:task_checklists, [
      TaskChecklist.create!(
        task: nil,
        order_sort: 2,
        title: "Title",
        type: "Type",
        checklist_type: "Checklist Type",
        respons_data: "MyText",
        status: "Status",
        score: "9.99",
        custom: "",
        description: "MyText",
        user: nil,
        biz: nil
      ),
      TaskChecklist.create!(
        task: nil,
        order_sort: 2,
        title: "Title",
        type: "Type",
        checklist_type: "Checklist Type",
        respons_data: "MyText",
        status: "Status",
        score: "9.99",
        custom: "",
        description: "MyText",
        user: nil,
        biz: nil
      )
    ])
  end

  it "renders a list of task_checklists" do
    render
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: 2.to_s, count: 2
    assert_select "tr>td", text: "Title".to_s, count: 2
    assert_select "tr>td", text: "Type".to_s, count: 2
    assert_select "tr>td", text: "Checklist Type".to_s, count: 2
    assert_select "tr>td", text: "MyText".to_s, count: 2
    assert_select "tr>td", text: "Status".to_s, count: 2
    assert_select "tr>td", text: "9.99".to_s, count: 2
    assert_select "tr>td", text: "".to_s, count: 2
    assert_select "tr>td", text: "MyText".to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
  end
end
