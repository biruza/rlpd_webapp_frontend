require "test_helper"

class ClassLevelsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @class_level = class_levels(:one)
  end

  test "should get index" do
    get class_levels_url
    assert_response :success
  end

  test "should get new" do
    get new_class_level_url
    assert_response :success
  end

  test "should create class_level" do
    assert_difference("ClassLevel.count") do
      post class_levels_url, params: {class_level: {title_en: @class_level.title_en, title_th: @class_level.title_th}}
    end

    assert_redirected_to class_level_url(ClassLevel.last)
  end

  test "should show class_level" do
    get class_level_url(@class_level)
    assert_response :success
  end

  test "should get edit" do
    get edit_class_level_url(@class_level)
    assert_response :success
  end

  test "should update class_level" do
    patch class_level_url(@class_level), params: {class_level: {title_en: @class_level.title_en, title_th: @class_level.title_th}}
    assert_redirected_to class_level_url(@class_level)
  end

  test "should destroy class_level" do
    assert_difference("ClassLevel.count", -1) do
      delete class_level_url(@class_level)
    end

    assert_redirected_to class_levels_url
  end
end
