require "test_helper"

class ScoreTemplateItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @score_template_item = score_template_items(:one)
  end

  test "should get index" do
    get score_template_items_url
    assert_response :success
  end

  test "should get new" do
    get new_score_template_item_url
    assert_response :success
  end

  test "should create score_template_item" do
    assert_difference("ScoreTemplateItem.count") do
      post score_template_items_url, params: {score_template_item: {score_template_id: @score_template_item.score_template_id, total_score: @score_template_item.total_score, weight_score: @score_template_item.weight_score}}
    end

    assert_redirected_to score_template_item_url(ScoreTemplateItem.last)
  end

  test "should show score_template_item" do
    get score_template_item_url(@score_template_item)
    assert_response :success
  end

  test "should get edit" do
    get edit_score_template_item_url(@score_template_item)
    assert_response :success
  end

  test "should update score_template_item" do
    patch score_template_item_url(@score_template_item), params: {score_template_item: {score_template_id: @score_template_item.score_template_id, total_score: @score_template_item.total_score, weight_score: @score_template_item.weight_score}}
    assert_redirected_to score_template_item_url(@score_template_item)
  end

  test "should destroy score_template_item" do
    assert_difference("ScoreTemplateItem.count", -1) do
      delete score_template_item_url(@score_template_item)
    end

    assert_redirected_to score_template_items_url
  end
end
