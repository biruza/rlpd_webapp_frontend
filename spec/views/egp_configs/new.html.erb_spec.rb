require "rails_helper"

RSpec.describe "egp_configs/new", type: :view do
  before(:each) do
    assign(:egp_config, EgpConfig.new(
      tags: "MyString",
      categories: "MyString",
      biz: nil
    ))
  end

  it "renders new egp_config form" do
    render

    assert_select "form[action=?][method=?]", egp_configs_path, "post" do
      assert_select "input[name=?]", "egp_config[tags]"

      assert_select "input[name=?]", "egp_config[categories]"

      assert_select "input[name=?]", "egp_config[biz_id]"
    end
  end
end
