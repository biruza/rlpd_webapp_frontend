require "rails_helper"

RSpec.describe "Km::Departments", type: :request do
  describe "GET /index" do
    it "returns http success" do
      get "/km/departments/index"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /show" do
    it "returns http success" do
      get "/km/departments/show"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /new" do
    it "returns http success" do
      get "/km/departments/new"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /edit" do
    it "returns http success" do
      get "/km/departments/edit"
      expect(response).to have_http_status(:success)
    end
  end
end
