class Risk::KpisController < ApplicationController
  def index
    url = URI("#{$server[@controller_title]}/api/v1/kpis")
    @datas = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/kpis/#{params["id"]}")
    @datas = request_api(url, "get", nil)
  end

  def new
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/kpis/#{params["id"]}")

    @datas = request_api(url, "get", nil)

    # puts "plan edit = #{@plan['title']}"
  end

  def create
    params[:kpi][:user_id] = $user_id
    params[:kpi][:biz_id] = $biz_id

    url = URI("#{$server[@controller_title]}/api/v1/kpis")
    @datas = request_api(url, "post", kpi_params)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/kpis", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_main_url}/kpis/new", alert: @datas["data"]
    end
  end

  def update
    url = URI("#{$server[@controller_title]}/api/v1/kpis/#{params["id"]}")
    @datas = request_api(url, "patch", kpi_params)

    # puts "update status : #{res}"

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/kpis", notice: @datas["message"]
    else
      redirect_to "/#{@controller_main_url}/kpis", alert: @datas["data"]
    end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/kpis/#{params["id"]}")
    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/kpis", notice: @datas["message"]
    else
      redirect_to "/#{@controller_main_url}/kpis", alert: @datas["data"]
    end
  end

  private

  def kpi_params
    params.require(:kpi).permit(:title, :user_id, :biz_id)
  end
end
