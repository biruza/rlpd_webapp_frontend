require "test_helper"

class WebBlockFilesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @web_block_file = web_block_files(:one)
  end

  test "should get index" do
    get web_block_files_url
    assert_response :success
  end

  test "should get new" do
    get new_web_block_file_url
    assert_response :success
  end

  test "should create web_block_file" do
    assert_difference("WebBlockFile.count") do
      post web_block_files_url, params: {web_block_file: {biz_id: @web_block_file.biz_id, description_en: @web_block_file.description_en, description_th: @web_block_file.description_th, file_data: @web_block_file.file_data, slug: @web_block_file.slug, slug: @web_block_file.slug, title_en: @web_block_file.title_en, title_th: @web_block_file.title_th, user: @web_block_file.user, web_block_id: @web_block_file.web_block_id}}
    end

    assert_redirected_to web_block_file_url(WebBlockFile.last)
  end

  test "should show web_block_file" do
    get web_block_file_url(@web_block_file)
    assert_response :success
  end

  test "should get edit" do
    get edit_web_block_file_url(@web_block_file)
    assert_response :success
  end

  test "should update web_block_file" do
    patch web_block_file_url(@web_block_file), params: {web_block_file: {biz_id: @web_block_file.biz_id, description_en: @web_block_file.description_en, description_th: @web_block_file.description_th, file_data: @web_block_file.file_data, slug: @web_block_file.slug, slug: @web_block_file.slug, title_en: @web_block_file.title_en, title_th: @web_block_file.title_th, user: @web_block_file.user, web_block_id: @web_block_file.web_block_id}}
    assert_redirected_to web_block_file_url(@web_block_file)
  end

  test "should destroy web_block_file" do
    assert_difference("WebBlockFile.count", -1) do
      delete web_block_file_url(@web_block_file)
    end

    assert_redirected_to web_block_files_url
  end
end
