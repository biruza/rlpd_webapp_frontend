require "test_helper"

class StudyperiodItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @studyperiod_item = studyperiod_items(:one)
  end

  test "should get index" do
    get studyperiod_items_url
    assert_response :success
  end

  test "should get new" do
    get new_studyperiod_item_url
    assert_response :success
  end

  test "should create studyperiod_item" do
    assert_difference("StudyperiodItem.count") do
      post studyperiod_items_url, params: {studyperiod_item: {day_id: @studyperiod_item.day_id, order: @studyperiod_item.order, period: @studyperiod_item.period, study_period_id: @studyperiod_item.study_period_id}}
    end

    assert_redirected_to studyperiod_item_url(StudyperiodItem.last)
  end

  test "should show studyperiod_item" do
    get studyperiod_item_url(@studyperiod_item)
    assert_response :success
  end

  test "should get edit" do
    get edit_studyperiod_item_url(@studyperiod_item)
    assert_response :success
  end

  test "should update studyperiod_item" do
    patch studyperiod_item_url(@studyperiod_item), params: {studyperiod_item: {day_id: @studyperiod_item.day_id, order: @studyperiod_item.order, period: @studyperiod_item.period, study_period_id: @studyperiod_item.study_period_id}}
    assert_redirected_to studyperiod_item_url(@studyperiod_item)
  end

  test "should destroy studyperiod_item" do
    assert_difference("StudyperiodItem.count", -1) do
      delete studyperiod_item_url(@studyperiod_item)
    end

    assert_redirected_to studyperiod_items_url
  end
end
