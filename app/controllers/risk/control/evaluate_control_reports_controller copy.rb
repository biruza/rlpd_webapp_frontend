class Risk::Control::EvaluateControlReportsController < ApplicationController
  before_action :set_default, only: %i[index show new edit create update destroy print]
  before_action :set_select, only: %i[new create edit update]

  def index
    url = URI("#{$server[@controller_title]}/api/v1/evaluate_control_reports?order=updated_at&sort=desc&limit=10")
    @evaluate_control_reports = request_api(url, "get", nil)
  end

  def show
    url = URI("#{$server[@controller_title]}/api/v1/evaluate_control_reports/#{params["id"]}")
    @evaluate_control_report = request_api(url, "get", nil)
  end

  def new
    if !params[:task_plan_report_id].blank?
      url = URI("#{$server[@controller_title]}/api/v1/task_plan_reports/#{params[:task_plan_report_id]}")
      @task_plan_report = request_api(url, "get", nil)

      url = URI("#{$server[@controller_title]}/api/v1/risk_analysis_reports/#{@task_plan_report["data"]["risk_analysis_report_id"]}")
      @risk_analysis_report = request_api(url, "get", nil)

      evaluate_control_report_params = {
        task_plan_report_id: @task_plan_report["data"]["id"],
        plan_report_id: @risk_analysis_report["data"]["plan_report_id"],
        user_id: @task_plan_report["data"]["user_id"],
        biz_id: @task_plan_report["data"]["biz_id"]
      }

      url = URI("#{$server[@controller_title]}/api/v1/evaluate_control_reports")
      @datas = request_api(url, "post", evaluate_control_report_params)

      if @datas["status"] == "Success"
         url = URI("#{$server[@controller_title]}/api/v1/evaluate_control_reports/#{@datas['data']['id']}")
         @evaluate_control_report = request_api(url,'get',nil)

=begin    
         @evaluate_control_report['data']['task_plan_report']['task_lists'].each do |task_list| #add 28/06/2023

          if task_list["existing_manage"]] == "adjust" #add 28/06/2023
          url = URI("#{$server[@controller_title]}/api/v1/tasks/#{task_list["list_id"]}")
          @task = request_api(url, "get", nil)

          evaluate_controls_params = {
            # task_list_id: task['id'],
            plan_report_id: @task["data"]["plan_report_id"],
            order_sort:  @task["data"]["order_sort"]&.to_i,
            category_id:  @task["data"]["category_id"],
            list_id:  @task["data"]["list_id"],
            evaluate_control_report_id: @evaluate_control_report["data"]["id"],
            user_id: @task["data"]["user_id"],
            biz_id: @task["data"]["biz_id"]
          }
      

          url_evaluate_controls = URI("#{$server[@controller_title]}/api/v1/evaluate_controls")
          url_evaluate_controls_new = request_api(url_evaluate_controls, "post", evaluate_controls_params)
          end
         end #task_list
=end        
        url = URI("#{$server[@controller_title]}/api/v1/evaluate_control_reports/#{@datas["data"]["id"]}")
        @evaluate_control_report = request_api(url, "get", nil)

        @evaluate_control_report["data"]["plan_report"]["tasks"].each do |task|
          #task_list[0][existing_manage]  
       # if task_list["existing_manage"]] == "adjust" #add 28/06/2023
         
          evaluate_controls_params = {
            # task_list_id: task['id'],
            plan_report_id: task["plan_report_id"],
            order_sort: task["order_sort"]&.to_i,
            category_id: task["category_id"],
            list_id: task["list_id"],
            evaluate_control_report_id: @evaluate_control_report["data"]["id"],
            user_id: task["user_id"],
            biz_id: task["biz_id"]
          }
      

          url_evaluate_controls = URI("#{$server[@controller_title]}/api/v1/evaluate_controls")
          url_evaluate_controls_new = request_api(url_evaluate_controls, "post", evaluate_controls_params)

       # end # end if
        end  # end task

     

        redirect_to "/#{@controller_main_url}/evaluate_control_reports/#{@datas["data"]["id"]}/edit", notice: @datas["messages"]
      else
        render :new, status: :unprocessable_entity
        
      end

    else
      redirect_to "/#{@controller_main_url}/master_plans", alert: (begin
        t(:can_not_found)
      rescue
        nil
      end).to_s
    end
  end

  def edit
    url = URI("#{$server[@controller_title]}/api/v1/evaluate_control_reports/#{params["id"]}")
    @evaluate_control_report = request_api(url, "get", nil)
  end

  def update
    params[:evaluate_control].each do |item|
      if !params[:evaluate_control][(item[0]).to_s][:id].blank?
        item_params = {
          evaluate_control: {
            id: params[:evaluate_control][(item[0]).to_s][:id],
            title: params[:evaluate_control][(item[0]).to_s][:title],
            successdate: params[:evaluate_control][(item[0]).to_s][:successdate],
            task_list_id: params[:evaluate_control][(item[0]).to_s][:task_list_id],
            user_id: $user[session.id.to_s][@controller_title]["id"],
            biz_id: $user[session.id.to_s][@controller_title]["biz_id"]
          }
        }
      end

      p "evaluate_controls ID============#{params[:evaluate_control][(item[0]).to_s][:id]}"

      url = URI("#{$server[@controller_title]}/api/v1/evaluate_controls/#{params[:evaluate_control][(item[0]).to_s][:id]}")
      @datas = request_api(url, "patch", item_params)
    end
    url_evaluate_control_report = URI("#{$server[@controller_title]}/api/v1/evaluate_control_reports/#{params["id"]}")
    @evaluate_control_report = request_api(url_evaluate_control_report, "get", nil)

    if @datas["status"] == "Success"
      redirect_to "/#{@controller_main_url}/evaluate_control_reports/#{@evaluate_control_report["data"]["id"]}/edit", notice: @datas["messages"]
    else
      redirect_to "/#{@controller_main_url}/evaluate_control_reports/#{@evaluate_control_report["data"]["id"]}/edit", alert: @datas["data"]
    end

    #     params[:task_list].each do |item|
    #
    #       params[:task_list]["#{item[0]}"][:risk_accept] = true if params[:task_list]["#{item[0]}"][:risk_accept] == "true"
    #
    #
    #       item_params = {
    #         task_list: {
    #           risk_accept: params[:task_list]["#{item[0]}"][:risk_accept],
    #           risk_avert: params[:task_list]["#{item[0]}"][:risk_avert],
    #           risk_transfer: params[:task_list]["#{item[0]}"][:risk_transfer],
    #           risk_control: params[:task_list]["#{item[0]}"][:risk_control],
    #           existing_manage: params[:task_list]["#{item[0]}"][:existing_manage],
    #
    #           task_list_controls_attributes: {
    #             "0": {
    #               task_list_id: params[:task_list]["#{item[0]}"][:id],
    #               title: params[:task_list]["#{item[0]}"][:task_list_controls_attributes]["0"][:title],
    #               user_id: $user[session.id.to_s][@controller_title]['id'],
    #               biz_id: $user[session.id.to_s][@controller_title]['biz_id'],
    #               id: params[:task_list]["#{item[0]}"][:task_list_controls_attributes]["0"][:id]
    #             }
    #           },
    #         }
    #       }
    #
    #
    #       url = URI("#{$server[@controller_title]}/api/v1/task_lists/#{params[:task_list]["#{item[0]}"][:id]}")
    #       @datas = request_api(url,'patch',item_params)
    #
    #     end
    #
    #     url_task_plan_report = URI("#{$server[@controller_title]}/api/v1/task_plan_reports/#{params['id']}")
    #     @url_task_plan_report = request_api(url_task_plan_report,'get',nil)
    #
    #
    #     if  @datas['status'] == 'Success'
    #       redirect_to "/#{@controller_main_url}/task_plan_reports/#{@url_task_plan_report['data']['id']}/edit", notice: @datas['messages']
    #     else
    #       redirect_to "/#{@controller_main_url}/task_plan_reports/#{@url_task_plan_report['data']['id']}/edit", alert: @datas['data']
    #     end
  end

  def destroy
    url = URI("#{$server[@controller_title]}/api/v1/evaluate_control_reports/#{params["id"]}")
    @evaluate_control_report = request_api(url, "get", nil)

    @datas = request_api(url, "delete", nil)

    if @datas["status"] == "Success"
      if params[:original_url]
        redirect_to params[:original_url], notice: @datas["messages"]
      else
        redirect_to "/#{@controller_main_url}/plans/#{@evaluate_control_report["data"]["task_plan_report"]["risk_analysis_report"]["plan_report"]["category"]["plan_id"] rescue nil}", notice: @datas["messages"]
      end
    else
      if params[:original_url]
        redirect_to "#{request.base_url}/#{params[:original_url]}", alert: t(:can_not_delete)
      else
        redirect_to "/#{@controller_main_url}/master_plans", alert: t(:can_not_delete)
      end
    end
  end

  def print
    url = URI("#{$server[@controller_title]}/api/v1/evaluate_control_reports/#{params["id"]}")
    @evaluate_control_report = request_api(url, "get", nil)
  end

  private

  def set_default
    @sub_system = "control"
    @controller_main_url = "#{@controller_title}/#{@sub_system}"
    # @controller_main_url = @controller_title
  end

  def set_user
    @user = User.find(auth_token[:user_id])
  end

  def set_select
    # url = URI("#{$server[@controller_title]}/api/v1/plans")
    # @data_plans = request_api(url,'get',nil)

    # url = URI("#{$server[@controller_title]}/api/v1/categories")
    # @data_categories = request_api(url,'get',nil)

    # url = URI("#{$server[@controller_title]}/api/v1/master_plans")
    # @data_master_plans = request_api(url,'get',nil)
  end

  def evaluate_control_report_params
    params.require(:evaluate_control_report).permit(:title1, :title2, :startdate, :enddate, :slug, :order_sort, :task_plan_report_id, :master_plan_id, :category_id, :plan_id, :user_id, :biz_id,
      evaluate_controls_attributes: [:id, :_destroy, :title, :successdate, :task_list_id, :evaluate_control_report_id, :list_id, :category_id, :list_id, :user_id, :biz_id])
  end
end
