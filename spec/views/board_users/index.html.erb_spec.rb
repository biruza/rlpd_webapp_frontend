require "rails_helper"

RSpec.describe "board_users/index", type: :view do
  before(:each) do
    assign(:board_users, [
      BoardUser.create!(
        board: nil,
        user: nil,
        order_sort: 2,
        biz: nil
      ),
      BoardUser.create!(
        board: nil,
        user: nil,
        order_sort: 2,
        biz: nil
      )
    ])
  end

  it "renders a list of board_users" do
    render
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: 2.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
  end
end
