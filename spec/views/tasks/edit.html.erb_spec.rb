require "rails_helper"

RSpec.describe "tasks/edit", type: :view do
  before(:each) do
    @task = assign(:task, Task.create!(
      title: "MyString",
      description: "MyText",
      cover: "MyText",
      priority: 1,
      status: 1,
      achieve: false,
      user: nil,
      biz: nil
    ))
  end

  it "renders the edit task form" do
    render

    assert_select "form[action=?][method=?]", task_path(@task), "post" do
      assert_select "input[name=?]", "task[title]"

      assert_select "textarea[name=?]", "task[description]"

      assert_select "textarea[name=?]", "task[cover]"

      assert_select "input[name=?]", "task[priority]"

      assert_select "input[name=?]", "task[status]"

      assert_select "input[name=?]", "task[achieve]"

      assert_select "input[name=?]", "task[user_id]"

      assert_select "input[name=?]", "task[biz_id]"
    end
  end
end
